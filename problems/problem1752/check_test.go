package problem1752

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestCheck(t *testing.T) {
	scenarios := [...]struct {
		nums   []int
		output bool
	}{
		0: {
			nums:   []int{3, 4, 5, 1, 2},
			output: true,
		},
		1: {
			nums:   []int{2, 1, 3, 4},
			output: false,
		},
		2: {
			nums:   []int{1, 2, 3},
			output: true,
		},
	}
	for i, scenario := range scenarios {
		output := check(scenario.nums)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
