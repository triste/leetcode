package problem1992

func findFarmland(land [][]int) (farmlands [][]int) {
	for i := 0; i < len(land); i++ {
		for j := 0; j < len(land[i]); j++ {
			if land[i][j] == 0 {
				continue
			}
			var x, y int
			for x = i; x < len(land) && land[x][j] == 1; x++ {
				for y = j; y < len(land[x]) && land[x][y] == 1; y++ {
					land[x][y] = 0
				}
			}
			farmlands = append(farmlands, []int{i, j, x - 1, y - 1})
			j = y - 1
		}
	}
	return
}
