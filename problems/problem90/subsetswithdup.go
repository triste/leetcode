package problem90

import (
	"sort"
)

func subsetsWithDup(nums []int) (subsets [][]int) {
	sort.Ints(nums)
	var path []int
	var solve func(nums []int)
	solve = func(nums []int) {
		if len(nums) == 0 {
			subset := make([]int, len(path))
			copy(subset, path)
			subsets = append(subsets, subset)
			return
		}
		var i int
		for i = 1; i < len(nums); i++ {
			if nums[i] != nums[0] {
				break
			}
		}
		solve(nums[i:])
		path = append(path, nums[0])
		solve(nums[1:])
		path = path[:len(path)-1]
	}
	solve(nums)
	return
}

/*
	1,2,2

	000: nil
	001: 1
	010: 2
	011: 1,2
	100: 2
	101: 1,2
	110: 2,2,
	111: 1,2,2
*/
