package problem1489

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestFindcriticalandpseudocriticaledges(t *testing.T) {
	scenarios := [...]struct {
		n      int
		edges  [][]int
		output [][]int
	}{
		0: {
			n: 5,
			edges: [][]int{
				{0, 1, 1}, {1, 2, 1}, {2, 3, 2}, {0, 3, 2}, {0, 4, 3}, {3, 4, 3}, {1, 4, 6},
			},
			output: [][]int{
				{0, 1}, {2, 3, 4, 5},
			},
		},
		1: {
			n: 4,
			edges: [][]int{
				{0, 1, 1}, {1, 2, 1}, {2, 3, 1}, {0, 3, 1},
			},
			output: [][]int{
				nil, {0, 1, 2, 3},
			},
		},
	}
	for i, scenario := range scenarios {
		output := findCriticalAndPseudoCriticalEdges(scenario.n, scenario.edges)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
