package problem852

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestPeakindexinmountainarray(t *testing.T) {
	scenarios := [...]struct {
		arr    []int
		output int
	}{
		0: {
			arr:    []int{0, 1, 0},
			output: 1,
		},
		1: {
			arr:    []int{0, 2, 1, 0},
			output: 1,
		},
		2: {
			arr:    []int{0, 10, 5, 2},
			output: 1,
		},
		3: {
			arr:    []int{0, 1, 2, 3, 4, 3, 2, 1, 0},
			output: 4,
		},
	}
	for i, scenario := range scenarios {
		output := peakIndexInMountainArray(scenario.arr)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
