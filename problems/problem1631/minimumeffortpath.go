package problem1631

import (
	"container/heap"
	"math"
)

type Point struct {
	x int
	y int
}

type Node struct {
	dist  int
	point Point
}

type MinHeap []Node

func (pq MinHeap) Len() int {
	return len(pq)
}

func (pq MinHeap) Less(i, j int) bool {
	return pq[i].dist < pq[j].dist
}

func (pq MinHeap) Swap(i, j int) {
	pq[i], pq[j] = pq[j], pq[i]
}

func (pq *MinHeap) Push(x any) {
	*pq = append(*pq, x.(Node))
}

func (pq *MinHeap) Pop() any {
	x := (*pq)[len(*pq)-1]
	(*pq) = (*pq)[:len(*pq)-1]
	return x
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func minimumEffortPath(heights [][]int) int {
	rowCount, colCount := len(heights), len(heights[0])
	tentativeDists := make([][]int, len(heights))
	for i := range tentativeDists {
		tentativeDists[i] = make([]int, len(heights[i]))
		for j := range tentativeDists[i] {
			tentativeDists[i][j] = math.MaxInt
		}
	}
	tentativeDists[0][0] = 0
	dirs := [...]Point{{-1, 0}, {1, 0}, {0, -1}, {0, 1}}
	pq := &MinHeap{{}}
	heap.Init(pq)
	for pq.Len() > 0 {
		node := heap.Pop(pq).(Node)
		x, y := node.point.x, node.point.y
		if x == rowCount-1 && y == colCount-1 {
			return node.dist
		}
		for _, dir := range dirs {
			r := x + dir.x
			c := y + dir.y
			if r >= 0 && r < len(heights) && c >= 0 && c < len(heights[r]) {
				diff := heights[r][c] - heights[x][y]
				if diff < 0 {
					diff = -diff
				}
				tentativeDist := max(tentativeDists[node.point.x][node.point.y], diff)
				if tentativeDist < tentativeDists[r][c] {
					tentativeDists[r][c] = tentativeDist
					heap.Push(pq, Node{tentativeDist, Point{r, c}})
				}
			}
		}
	}
	return tentativeDists[rowCount-1][colCount-1]
}
