package problem2130

import (
	"testing"

	"github.com/google/go-cmp/cmp"
	. "gitlab.com/triste/leetcode/pkg/list"
)

func TestPairsum(t *testing.T) {
	scenarios := [...]struct {
		head   *ListNode
		output int
	}{
		0: {
			head:   NewListFromSlice(5, 4, 2, 1),
			output: 6,
		},
		1: {
			head:   NewListFromSlice(4, 2, 2, 3),
			output: 7,
		},
		2: {
			head:   NewListFromSlice(1, 100000),
			output: 100001,
		},
	}
	for i, scenario := range scenarios {
		output := pairSum(scenario.head)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
