package problem2616

import (
	"sort"
)

func minimizeMax(nums []int, p int) int {
	sort.Ints(nums)
	l, r := 0, nums[len(nums)-1]-nums[0]
	for l < r {
		m := l + (r-l)/2
		pairCount := 0
		for i := 1; i < len(nums); i++ {
			if nums[i]-nums[i-1] <= m {
				pairCount++
				i++
			}
		}
		if pairCount >= p {
			r = m
		} else {
			l = m + 1
		}
	}
	return l
}

/*
	1,2,2,4
	1,0,2
*/
