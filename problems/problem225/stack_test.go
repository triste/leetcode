package problem225

import (
	"testing"
)

func TestStack(t *testing.T) {
	stack := Constructor()
	stack.Push(1)
	stack.Push(2)
	if got := stack.Top(); got != 2 {
		t.Errorf("Top: got %v, want 2", got)
	}
	if got := stack.Pop(); got != 2 {
		t.Errorf("Pop: got %v, want 2", got)
	}
	if got := stack.Empty(); got != false {
		t.Errorf("Empty: got %v, want false", got)
	}
}
