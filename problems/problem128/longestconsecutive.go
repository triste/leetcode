package problem128

func longestConsecutive(nums []int) int {
	starts := make(map[int]int, len(nums))
	ends := make(map[int]int, len(nums))
	used := make(map[int]bool, len(nums))
	longestLenght := 0
	for _, num := range nums {
		if used[num] {
			continue
		}
		used[num] = true
		left := num
		if start, ok := starts[num-1]; ok {
			left = start
		}
		right := num
		if end, ok := ends[num+1]; ok {
			right = end
		}
		starts[right] = left
		ends[left] = right
		if lenght := right - left + 1; lenght > longestLenght {
			longestLenght = lenght
		}
	}
	return longestLenght
}

/*
	100,4,200,1,3,2

	100, 4, 200, 1
		{100,100}, {4,4}, {200, 200}, {1,1}
	3:
		{100,100}, {3,4}, {200, 200}, {1,1}
*/

/*
	nums:   []int{-6, 8, -5, 7, -9, -1, -7, -6, -9, -7, 5, 7, -1, -8, -8, -2, 0},

	-6, 8:
	starts := -6:-6, 8:8
	ends := -6:-6, 8:8
	-5:
	starts := -5:-6, -6:-6, 8:8
	ends := -6:-5, 8:8
	7:
	starts :=

-5
*/
