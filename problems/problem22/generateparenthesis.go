package problem22

func generateParenthesis(n int) (output []string) {
	m := make(map[string]struct{})
	dp := [][]string{
		0: {"()"},
	}
	for i := 2; i <= n; i++ {
		strs := []string{}
		for j := 1; j < i; j++ {
			for _, left := range dp[j-1] {
				for _, right := range dp[i-j-1] {
					newStr := left + right
					if _, ok := m[newStr]; !ok {
						strs = append(strs, newStr)
						m[newStr] = struct{}{}
					}
				}
			}
		}
		for _, str := range dp[i-2] {
			strs = append(strs, "(" + str + ")")
		}
		dp = append(dp, strs)
	}
	return dp[n-1]
}

/*
	case 1:
		()
	case 2:
		case1.case1, case1.case1, (case1) = ()(), ()(), (())
		()case1, (case1)
	case 3:
		case1.case2, case2.case1, (case2) = ()()(), ()(()), ()()(), (())(), (()()), ((()))
		1+2, 2+1
		()case2, (case2)
		(case1)(), ()case2, (case2) = (())(), ()()(), ()(()), (()()), ((()))
	case 4:
		3+1, 2+2, 1+3
		(case2)(), ()case3, (case3)
*/
