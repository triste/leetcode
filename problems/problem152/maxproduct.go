package problem152

import (
	"math"
)

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func maxProduct(nums []int) int {
	maxProduct := math.MinInt
	currentProdMax := 1
	currentProdMin := 1
	for _, num := range nums {
		prod1 := currentProdMax * num
		prod2 := currentProdMin * num
		currentProdMax = max(num, max(prod1, prod2))
		currentProdMin = min(num, min(prod1, prod2))
		maxProduct = max(maxProduct, currentProdMax)
	}
	return maxProduct
}
