package problem2472

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestMaxpalindromes(t *testing.T) {
	scenarios := [...]struct {
		s      string
		k      int
		output int
	}{
		0: {
			s:      "abaccdbbd",
			k:      3,
			output: 2,
		},
		1: {
			s:      "adbcda",
			k:      2,
			output: 0,
		},
		2: {
			s:      "pwfptewrvpeepvrwetpfuegohrdleteldrhogeuyricjoepiwncucnwipeojebpajdpwsdpnnpdswpdjapwqxifmkcmmoztsxiixstzommckcyqkdmxcjfjcxmdkqychcrfclblcdmmdclblcfrcxxpmyxciicxympxxvtwrrenfssfnerrwtvkdaddevwedewveddadkrsirisltnikcckintlsirixgbjvoetsasteovjbgxwtgkdlsuusldkgtwmnzofrxmhsshmxrfozswtlmylpqvvqplymltwrvxhkmgrwwrgmkhxvrwhtqvabzwyllywzbavqtzkibseqhgsmzifizmsghqezdifukpksbbskpkufigsakogkerjjrekgokasquzdujtoonwnootjudzupqpafqofiusjsuifoqfafbtvflzfauuafzlfvtbfkpheguosijlzljisougehsqtglzqrzfhmseptatpesmhfzrppdvlfmnvvnmflvdppggnxtyrdsgsdrytxnggcmyqcbdfparbggbrapfdbcqxenpjehvvhejpnexnirypjvmwpwmvjpyrintqsxxc",
			k:      16,
			output: 30,
		},
	}
	for i, scenario := range scenarios {
		output := maxPalindromes(scenario.s, scenario.k)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
