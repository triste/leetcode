package problem705

type MyHashSet struct {
	flags [(1_000_064 >> 6)]uint64
}

func Constructor() MyHashSet {
	return MyHashSet{}
}

func (this *MyHashSet) Add(key int) {
	quotient := key >> 6
	remainder := key & 63
	this.flags[quotient] |= 1 << remainder
}

func (this *MyHashSet) Remove(key int) {
	quotient := key >> 6
	remainder := key & 63
	this.flags[quotient] &= ^(1 << remainder)
}

func (this *MyHashSet) Contains(key int) bool {
	quotient := key >> 6
	remainder := key & 63
	return (this.flags[quotient]>>remainder)&1 == 1
}
