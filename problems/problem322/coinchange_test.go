package problem322

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestCoinchange(t *testing.T) {
	scenarios := [...]struct {
		coins  []int
		amount int
		output int
	}{
		0: {
			coins:  []int{1, 2, 5},
			amount: 11,
			output: 3,
		},
		1: {
			coins:  []int{2},
			amount: 3,
			output: -1,
		},
		2: {
			coins:  []int{1},
			amount: 0,
			output: 0,
		},
		3: {
			coins:  []int{1, 2, 5},
			amount: 100,
			output: 20,
		},
	}
	for i, scenario := range scenarios {
		output := coinChange(scenario.coins, scenario.amount)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
