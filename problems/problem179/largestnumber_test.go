package problem179

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestLargestnumber(t *testing.T) {
	scenarios := [...]struct {
		nums   []int
		output string
	}{
		0: {
			nums:   []int{10, 2},
			output: "210",
		},
		1: {
			nums:   []int{3, 30, 34, 5, 9},
			output: "9534330",
		},
	}
	for i, scenario := range scenarios {
		output := largestNumber(scenario.nums)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
