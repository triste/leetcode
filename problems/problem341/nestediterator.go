package problem341

/**
 * // This is the interface that allows for creating nested lists.
 * // You should not implement it, or speculate about its implementation
 * type NestedInteger struct {
 * }
 *
 * // Return true if this NestedInteger holds a single integer, rather than a nested list.
 * func (this NestedInteger) IsInteger() bool {}
 *
 * // Return the single integer that this NestedInteger holds, if it holds a single integer
 * // The result is undefined if this NestedInteger holds a nested list
 * // So before calling this method, you should have a check
 * func (this NestedInteger) GetInteger() int {}
 *
 * // Set this NestedInteger to hold a single integer.
 * func (n *NestedInteger) SetInteger(value int) {}
 *
 * // Set this NestedInteger to hold a nested list and adds a nested integer to it.
 * func (this *NestedInteger) Add(elem NestedInteger) {}
 *
 * // Return the nested list that this NestedInteger holds, if it holds a nested list
 * // The list length is zero if this NestedInteger holds a single integer
 * // You can access NestedInteger's List element directly if you want to modify it
 * func (this NestedInteger) GetList() []*NestedInteger {}
 */

type NestedInteger struct {
	val   int
	list  []*NestedInteger
	isInt bool
}

func (n NestedInteger) IsInteger() bool {
	return n.isInt
}

func (n NestedInteger) GetInteger() int {
	return n.val
}

func (n *NestedInteger) SetInteger(value int) {
	n.val = value
	n.isInt = true
}

func (n *NestedInteger) Add(elem NestedInteger) {
	n.list = append(n.list, &elem)
	n.isInt = false
}

func (n NestedInteger) GetList() []*NestedInteger {
	return n.list
}

type NestedIterator struct {
	nums []int
	i    int
}

func Constructor(list []*NestedInteger) *NestedIterator {
	var nums []int
	var dfs func(list []*NestedInteger)
	dfs = func(list []*NestedInteger) {
		for _, elem := range list {
			if elem.IsInteger() {
				nums = append(nums, elem.GetInteger())
			} else {
				dfs(elem.GetList())
			}
		}
	}
	dfs(list)
	return &NestedIterator{
		nums: nums,
		i:    0,
	}
}

func (ni *NestedIterator) Next() int {
	num := ni.nums[ni.i]
	ni.i++
	return num
}

func (ni *NestedIterator) HasNext() bool {
	return ni.i < len(ni.nums)
}
