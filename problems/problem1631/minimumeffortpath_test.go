package problem1631

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestMinimumeffortpath(t *testing.T) {
	scenarios := [...]struct {
		heights [][]int
		output  int
	}{
		0: {
			heights: [][]int{
				{1, 2, 2},
				{3, 8, 2},
				{5, 3, 5},
			},
			output: 2,
		},
		1: {
			heights: [][]int{
				{1, 2, 3},
				{3, 8, 4},
				{5, 3, 5},
			},
			output: 1,
		},
		2: {
			heights: [][]int{
				{1, 2, 1, 1, 1},
				{1, 2, 1, 2, 1},
				{1, 2, 1, 2, 1},
				{1, 2, 1, 2, 1},
				{1, 1, 1, 2, 1},
			},
			output: 0,
		},
	}
	for i, scenario := range scenarios {
		output := minimumEffortPath(scenario.heights)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
