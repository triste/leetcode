package list

type ListNode struct {
	Val  int
	Next *ListNode
}

func NewListFromSlice(nums ...int) *ListNode {
	root := ListNode{}
	ptr := &root
	for _, v := range nums {
		ptr.Next = &ListNode{v, nil}
		ptr = ptr.Next
	}
	return root.Next
}

func NewList(nums ...interface{}) *ListNode {
	var root ListNode
	current := &root
	for _, num := range nums {
		if num, ok := num.(int); ok {
			current.Next = &ListNode{num, nil}
			current = current.Next
		} else {
			break
		}
	}
	return root.Next
}
