package problem1647

import (
	"sort"
)

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func minDeletions(s string) (delCount int) {
	var freqs [26]int
	for _, ch := range s {
		freqs[ch-'a']++
	}
	sort.Sort(sort.Reverse(sort.IntSlice(freqs[:])))
	for i := 1; i < len(freqs); i++ {
		if freqs[i] < freqs[i-1] {
			continue
		}
		newFreq := max(0, freqs[i-1]-1)
		delCount += freqs[i] - newFreq
		freqs[i] = newFreq
	}
	return
}
