package problem2742

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestPaintwalls(t *testing.T) {
	scenarios := [...]struct {
		cost   []int
		time   []int
		output int
	}{
		0: {
			cost:   []int{1, 2, 3, 2},
			time:   []int{1, 2, 3, 2},
			output: 3,
		},
		1: {
			cost:   []int{2, 3, 4, 2},
			time:   []int{1, 1, 1, 1},
			output: 4,
		},
	}
	for i, scenario := range scenarios {
		output := paintWalls(scenario.cost, scenario.time)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
