package problem86

import (
	. "gitlab.com/triste/leetcode/pkg/list"
)

func partition(head *ListNode, x int) *ListNode {
	var left, right ListNode
	l, r := &left, &right
	for p := head; p != nil; p = p.Next {
		if p.Val < x {
			l.Next = p
			l = l.Next
		} else {
			r.Next = p
			r = r.Next
		}
	}
	r.Next = nil
	l.Next = right.Next
	return left.Next
}
