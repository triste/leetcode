package problem2366

func minimumReplacement(nums []int) (minReplCount int64) {
	for prev, r := nums[len(nums)-1], len(nums)-2; r >= 0; r-- {
		if nums[r] <= prev {
			prev = nums[r]
			continue
		}
		numCount := (nums[r] + prev - 1) / prev
		minReplCount += int64(numCount - 1)
		prev = nums[r] / numCount
	}
	return
}

/*
	821,881,275

	1: 821,660,221,274
	2: 821,440,220,221,274
	3: 821,220,220,220,221,274
	4: 615,206,220,220,220,221,274
	5: 410,205,206,220,220,220,221,274
	6: 205,205,205,206,220,220,220,221,274
*/
