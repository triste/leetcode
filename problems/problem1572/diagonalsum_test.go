package problem1572

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestDiagonalsum(t *testing.T) {
	scenarios := [...]struct {
		mat    [][]int
		output int
	}{
		0: {
			mat: [][]int{
				{7, 3, 1, 9},
				{3, 4, 6, 9},
				{6, 9, 6, 6},
				{9, 5, 8, 5},
			},
			output: 55,
		},
	}
	for i, scenario := range scenarios {
		output := diagonalSum(scenario.mat)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
