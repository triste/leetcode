package problem309

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

type State = int

const (
	Empty State = iota
	Full
)

func maxProfitRecursive(prices []int) int {
	var helper func(state State, i int) int
	helper = func(state State, i int) int {
		if i >= len(prices) {
			return 0
		}
		var max int
		if state == Empty {
			max = helper(Full, i+1) - prices[i]
			if v := helper(Empty, i+1); v > max {
				max = v
			}
		} else if state == Full {
			max = helper(Empty, i+2) + prices[i]
			if v := helper(Full, i+1); v > max {
				max = v
			}
		}
		return max
	}
	return helper(Empty, 0)
}

func maxProfitDP(prices []int) int {
	dp := make([][2]int, len(prices)+2)
	for i := len(prices) - 1; i >= 0; i-- {
		dp[i] = [2]int{
			max(dp[i+1][1]-prices[i], dp[i+1][0]),
			max(dp[i+2][0]+prices[i], dp[i+1][1]),
		}
	}
	return dp[0][0]
}

func maxProfitDP2(prices []int) int {
	full := 0
	empty := [2]int{0, 0}
	for i := len(prices) - 1; i >= 0; i-- {
		full, empty = max(empty[1]+prices[i], full), [2]int{max(full-prices[i], empty[0]), empty[0]}
	}
	return empty[0]
}

func maxProfit(prices []int) int {
	return maxProfitDP2(prices)
}

/*
       |  1  |  2  |  3  |  0  |  2  |     |     |
empty  |     |     |     |     |  0  |  0  |  0  |
full   |     |     |     |     |     |  0  |  0  |
*/
