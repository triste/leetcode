package problem802

func eventualSafeNodes(graph [][]int) []int {
	visited := make([]bool, len(graph))
	processed := make([]bool, len(graph))
	safed := make([]bool, len(graph))
	var dfs func(i int)
	dfs = func(i int) {
		if processed[i] {
			return
		}
		if visited[i] {
			safed[i] = false
		} else {
			visited[i] = true
			safed[i] = true
			for _, e := range graph[i] {
				dfs(e)
				if !safed[e] {
					safed[i] = false
					break
				}
			}
		}
		visited[i] = false
		processed[i] = true
	}
	var safeNodes []int
	for i := range graph {
		dfs(i)
		if safed[i] {
			safeNodes = append(safeNodes, i)
		}
	}
	return safeNodes
}
