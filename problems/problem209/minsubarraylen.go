package problem209

import "math"

func minSubArrayLen(target int, nums []int) int {
	left := 0
	sum := 0
	output := math.MaxInt
	for i, num := range nums {
		if sum += num; sum < target {
			continue
		}
		for {
			sum -= nums[left]
			left++
			if sum < target {
				break
			}
		}
		if l := i - left; l < output {
			output = l
		}
	}
	if output == math.MaxInt {
		return 0
	}
	return output + 2
}
