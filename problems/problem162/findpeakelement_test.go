package problem162

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestFindpeakelement(t *testing.T) {
	scenarios := [...]struct {
		nums   []int
		output int
	}{
		0: {[]int{1, 2, 3, 1}, 2},
		1: {[]int{1, 2, 1, 3, 5, 6, 4}, 5},
		2: {[]int{1, 2, 1, 3, 5, 6, 7}, 6},
	}
	for i, scenario := range scenarios {
		output := findPeakElement(scenario.nums)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
