package problem39

func combinationSum(candidates []int, target int) (combinations [][]int) {
	candidateUseCount := make([]int, len(candidates))
	var solve func(i, sum int)
	solve = func(i, sum int) {
		if sum == target {
			var combination []int
			for i, uses := range candidateUseCount {
				for j := 0; j < uses; j++ {
					combination = append(combination, candidates[i])
				}
			}
			combinations = append(combinations, combination)

		} else if sum < target {
			for j := i; j < len(candidates); j++ {
				candidateUseCount[j]++
				solve(j, sum+candidates[j])
				candidateUseCount[j]--
			}
		}
	}
	solve(0, 0)
	return combinations
}
