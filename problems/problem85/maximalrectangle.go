package problem85

type Bar struct {
	height int
	width  int
}

func maximalRectangle(matrix [][]byte) (maxRect int) {
	dp := make([]int, len(matrix[0]))
	bars := make([]Bar, 1, len(dp)+1)
	for i := 0; i < len(matrix); i++ {
		bars = bars[:1]
		for j := 0; j < len(dp); j++ {
			if matrix[i][j] == '0' {
				dp[j] = 0
			} else {
				dp[j]++
			}
			height := dp[j]
			lastBarIdx := len(bars) - 1
			if bars[lastBarIdx].height == height {
				bars[lastBarIdx].width++
			} else if bars[lastBarIdx].height < height {
				bars = append(bars, Bar{height, 1})
			} else {
				newWidth := 1
				widthAcc := 0
				for i := lastBarIdx; i >= 0; i-- {
					widthAcc += bars[i].width
					area := bars[i].height * widthAcc
					if area > maxRect {
						maxRect = area
					}
					if bars[i].height > height {
						newWidth += bars[i].width
						bars = bars[:i]
					}
				}
				bars = append(bars, Bar{height, newWidth})
			}
		}
		widthAcc := 0
		for i := len(bars) - 1; i >= 0; i-- {
			widthAcc += bars[i].width
			area := bars[i].height * widthAcc
			if area > maxRect {
				maxRect = area
			}
		}

	}
	return
}
