package problem373

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestKsmallestpairs(t *testing.T) {
	scenarios := [...]struct {
		nums1  []int
		nums2  []int
		k      int
		output [][]int
	}{
		0: {
			nums1: []int{1, 7, 11},
			nums2: []int{2, 4, 6},
			k:     3,
			output: [][]int{
				{1, 2}, {1, 4}, {1, 6},
			},
		},
		1: {
			nums1: []int{1, 1, 2},
			nums2: []int{1, 2, 3},
			k:     2,
			output: [][]int{
				{1, 1}, {1, 1},
			},
		},
		2: {
			nums1: []int{1, 2},
			nums2: []int{3},
			k:     3,
			output: [][]int{
				{1, 3}, {2, 3},
			},
		},
		3: {
			nums1: []int{1, 5, 7},
			nums2: []int{2, 3, 11},
			k:     4,
			output: [][]int{
				{1, 2}, {1, 3}, {5, 2}, {5, 3},
			},
		},
	}
	for i, scenario := range scenarios {
		output := kSmallestPairs(scenario.nums1, scenario.nums2, scenario.k)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
