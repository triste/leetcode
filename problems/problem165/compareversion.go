package problem165

func parseRevision(version string) (rev int, tail string) {
	for i := 0; i < len(version); i++ {
		if version[i] == '.' {
			tail = version[i+1:]
			return
		}
		rev = rev*10 + int(version[i]) - '0'
	}
	return
}

func compareVersion(version1 string, version2 string) int {
	for len(version1) > 0 || len(version2) > 0 {
		var rev1, rev2 int
		rev1, version1 = parseRevision(version1)
		rev2, version2 = parseRevision(version2)
		if rev1 < rev2 {
			return -1
		} else if rev1 > rev2 {
			return 1
		}
	}
	return 0
}
