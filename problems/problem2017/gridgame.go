package problem2017

func max(a, b int64) int64 {
	if a > b {
		return a
	}
	return b
}

func gridGame(grid [][]int) int64 {
	n := len(grid[0])
	var sums [2]int64
	for i := 1; i < n; i++ {
		sums[0] += int64(grid[0][i])
	}
	secondRobotMaxPointCount := sums[0]
	for i := 1; i < n; i++ {
		sums[0] -= int64(grid[0][i])
		sums[1] += int64(grid[1][i-1])
		points := max(sums[0], sums[1])
		if points < secondRobotMaxPointCount {
			secondRobotMaxPointCount = points
		}
	}
	return secondRobotMaxPointCount
}
