package problem199

import (
	"testing"

	"github.com/google/go-cmp/cmp"
	. "gitlab.com/triste/leetcode/pkg/tree"
)

func TestRightsideview(t *testing.T) {
	scenarios := [...]struct {
		root   *TreeNode
		output []int
	}{
		0: {
			NewTree(1,2,3,nil,5,nil,4),
			[]int{1,3,4},
		},
		1: {
			NewTree(1,nil,3),
			[]int{1,3},
		},
		2: {
			NewTree(1,2,3,nil,5,6,nil,4),
			[]int{1,3,6,4},
		},
	}
	for i, scenario := range scenarios {
		output := rightSideView(scenario.root)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
