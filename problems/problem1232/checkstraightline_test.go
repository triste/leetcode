package problem1232

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestCheckstraightline(t *testing.T) {
	scenarios := [...]struct {
		coordinates [][]int
		output      bool
	}{
		0: {
			coordinates: [][]int{
				{1, 2}, {2, 3}, {3, 4}, {4, 5}, {5, 6}, {6, 7},
			},
			output: true,
		},
		1: {
			coordinates: [][]int{
				{1, 1}, {2, 2}, {3, 4}, {4, 5}, {5, 6}, {7, 7},
			},
			output: false,
		},
		2: {
			coordinates: [][]int{
				{4, 8}, {-2, 8}, {1, 8}, {8, 8}, {-5, 8}, {0, 8}, {7, 8}, {5, 8},
			},
			output: true,
		},
		3: {
			coordinates: [][]int{
				{0, 0}, {0, 1}, {0, -1},
			},
			output: true,
		},
	}
	for i, scenario := range scenarios {
		output := checkStraightLine(scenario.coordinates)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
