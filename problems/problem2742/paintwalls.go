package problem2742

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func paintWalls(cost []int, time []int) int {
	dp := make([]int, len(cost)+1)
	for i := 1; i < len(dp); i++ {
		dp[i] = 500_000_000
	}
	for i := range cost {
		for j := len(dp) - 1; j > 0; j-- {
			dp[j] = min(dp[j], dp[max(0, j-time[i]-1)]+cost[i])
		}
	}
	return dp[len(dp)-1]
}

/*
  | 0 | 1 | 2 | 3 | 4 |
1 |   |   |   |   | y |
2 |   | x | x |   |   |
3 |   |   |   |   |   |
2 |   |   |   |   |   |
$ | 0 |inf|inf|inf|inf|
*/
