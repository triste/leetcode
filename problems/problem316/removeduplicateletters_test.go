package problem316

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestRemoveduplicateletters(t *testing.T) {
	scenarios := [...]struct {
		s      string
		output string
	}{
		0: {
			s:      "bcabc",
			output: "abc",
		},
		1: {
			s:      "cbacdcbc",
			output: "acdb",
		},
	}
	for i, scenario := range scenarios {
		output := removeDuplicateLetters(scenario.s)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
