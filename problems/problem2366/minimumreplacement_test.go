package problem2366

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestMinimumreplacement(t *testing.T) {
	scenarios := [...]struct {
		nums   []int
		output int64
	}{
		0: {
			nums:   []int{3, 9, 3},
			output: 2,
		},
		1: {
			nums:   []int{1, 2, 3, 4, 5},
			output: 0,
		},
		2: {
			nums:   []int{821, 881, 275},
			output: 6,
		},
	}
	for i, scenario := range scenarios {
		output := minimumReplacement(scenario.nums)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
