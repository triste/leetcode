package problem96

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestNumtrees(t *testing.T) {
	scenarios := [...]struct {
		n      int
		output int
	}{
		0: {
			n:      3,
			output: 5,
		},
		1: {
			n:      1,
			output: 1,
		},
	}
	for i, scenario := range scenarios {
		output := numTrees(scenario.n)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
