package problem1048

import "sort"

func longestStrChain(words []string) int {
	sort.Slice(words, func(i, j int) bool {
		return len(words[i]) < len(words[j])
	})
	dp := make(map[string]int, len(words))
	maxChainLenght := 0
	for _, word := range words {
		currentMaxChainLenght := 0
		for j := range word {
			chainLenght := dp[word[:j]+word[j+1:]] + 1
			if chainLenght > currentMaxChainLenght {
				currentMaxChainLenght = chainLenght
				if chainLenght > maxChainLenght {
					maxChainLenght = chainLenght
				}
			}
		}
		dp[word] = currentMaxChainLenght
	}
	return maxChainLenght
}
