package problem1396

import "testing"

func TestUndergroundSystem(t *testing.T) {
	us := Constructor()
	us.CheckIn(45, "Leyton", 3)
	us.CheckIn(32, "Paradise", 8)
	us.CheckIn(27, "Leyton", 10)
	us.CheckOut(45, "Waterloo", 15)
	us.CheckOut(27, "Waterloo", 20)
	us.CheckOut(32, "Cambridge", 22)
	if got := us.GetAverageTime("Paradise", "Cambridge"); got != 14.0 {
		t.Errorf("Paradise -> Cambridge average time 14.0; got %v", got)
	}
	if got := us.GetAverageTime("Leyton", "Waterloo"); got != 11.0 {
		t.Errorf("Paradise -> Cambridge average time 11.0; got %v", got)
	}
	us.CheckIn(10, "Leyton", 24)
	if got := us.GetAverageTime("Leyton", "Waterloo"); got != 11.0 {
		t.Errorf("Paradise -> Cambridge average time 11.0; got %v", got)
	}
	us.CheckOut(10, "Waterloo", 38)
	if got := us.GetAverageTime("Leyton", "Waterloo"); got != 12.0 {
		t.Errorf("Paradise -> Cambridge average time 12.0; got %v", got)
	}
}
