package problem93

import (
	"strings"
)

func restoreIpAddresses(s string) (ips []string) {
	ipParts := make([]string, 4)
	var solve func(s string, part int)
	solve = func(s string, part int) {
		if part >= 4 {
			if len(s) == 0 {
				ips = append(ips, strings.Join(ipParts, "."))
			}
			return
		}
		acc := 0
		for i, ch := range s {
			acc = acc*10 + int(ch-'0')
			if acc > 255 {
				break
			}
			ipParts[part] = s[:i+1]
			solve(s[i+1:], part+1)
			if acc == 0 {
				break
			}
		}

	}
	solve(s, 0)
	return
}
