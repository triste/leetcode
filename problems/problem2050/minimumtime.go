package problem2050

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func minimumTime(n int, relations [][]int, time []int) (minTime int) {
	graph := make([][]int, n)
	for _, rel := range relations {
		prev := rel[0] - 1
		next := rel[1] - 1
		graph[next] = append(graph[next], prev)
	}
	memo := make([]int, n)
	var dfs func(i int) int
	dfs = func(i int) int {
		if memo[i] > 0 {
			return memo[i]
		}
		minTime := 0
		for _, j := range graph[i] {
			minTime = max(minTime, dfs(j))
		}
		memo[i] = time[i] + minTime
		return memo[i]
	}
	for i := range graph {
		minTime = max(minTime, dfs(i))
	}
	return minTime
}
