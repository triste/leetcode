package problem1027

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestLongestarithseqlength(t *testing.T) {
	scenarios := [...]struct {
		nums   []int
		output int
	}{
		0: {
			nums:   []int{3, 6, 9, 12},
			output: 4,
		},
		1: {
			nums:   []int{9, 4, 7, 2, 10},
			output: 3,
		},
		2: {
			nums:   []int{20, 1, 15, 3, 10, 5, 8},
			output: 4,
		},
		3: {
			nums:   []int{24, 13, 1, 100, 0, 94, 3, 0, 3},
			output: 2,
		},
		4: {
			nums:   []int{12, 28, 13, 6, 34, 36, 53, 24, 29, 2, 23, 0, 22, 25, 53, 34, 23, 50, 35, 43, 53, 11, 48, 56, 44, 53, 31, 6, 31, 57, 46, 6, 17, 42, 48, 28, 5, 24, 0, 14, 43, 12, 21, 6, 30, 37, 16, 56, 19, 45, 51, 10, 22, 38, 39, 23, 8, 29, 60, 18},
			output: 4,
		},
	}
	for i, scenario := range scenarios {
		output := longestArithSeqLength(scenario.nums)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
