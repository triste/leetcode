package problem1252

func oddCells(m int, n int, indices [][]int) (count int) {
	rowAccums := make([]int, m)
	colAccums := make([]int, n)
	for _, idx := range indices {
		rowAccums[idx[0]]++
		colAccums[idx[1]]++
	}
	for _, r := range rowAccums {
		for _, c := range colAccums {
			if (r+c)&1 != 0 {
				count++
			}
		}
	}
	return
}
