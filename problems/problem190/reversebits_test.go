package problem190

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestReversebits(t *testing.T) {
	scenarios := [...]struct {
		num    uint32
		output uint32
	}{
		0: {
			num:    43261596,
			output: 964176192,
		},
		1: {
			num:    4294967293,
			output: 3221225471,
		},
	}
	for i, scenario := range scenarios {
		output := reverseBits(scenario.num)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
