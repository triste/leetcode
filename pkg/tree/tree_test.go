package tree

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestTree(t *testing.T) {
	tree := NewTree(nil)
	if diff := cmp.Diff((*TreeNode)(nil), tree); diff != "" {
		t.Errorf("(-want +got):\n%s", diff)
	}

	tree = NewTree(1)
	if diff := cmp.Diff(1, tree.Val); diff != "" {
		t.Errorf("(-want +got):\n%s", diff)
	}
	if diff := cmp.Diff((*TreeNode)(nil), tree.Left); diff != "" {
		t.Errorf("(-want +got):\n%s", diff)
	}
	if diff := cmp.Diff((*TreeNode)(nil), tree.Right); diff != "" {
		t.Errorf("(-want +got):\n%s", diff)
	}

	tree = NewTree(1, 2)
	if diff := cmp.Diff(1, tree.Val); diff != "" {
		t.Errorf("(-want +got):\n%s", diff)
	}
	if diff := cmp.Diff(2, tree.Left.Val); diff != "" {
		t.Errorf("(-want +got):\n%s", diff)
	}
	if diff := cmp.Diff((*TreeNode)(nil), tree.Right); diff != "" {
		t.Errorf("(-want +got):\n%s", diff)
	}

	tree = NewTree(1, nil, 2)
	if diff := cmp.Diff(1, tree.Val); diff != "" {
		t.Errorf("(-want +got):\n%s", diff)
	}
	if diff := cmp.Diff((*TreeNode)(nil), tree.Left); diff != "" {
		t.Errorf("(-want +got):\n%s", diff)
	}
	if diff := cmp.Diff(2, tree.Right.Val); diff != "" {
		t.Errorf("(-want +got):\n%s", diff)
	}
}
