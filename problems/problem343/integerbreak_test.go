package problem343

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestIntegerbreak(t *testing.T) {
	scenarios := [...]struct {
		n      int
		output int
	}{
		0: {
			n:      2,
			output: 1,
		},
		1: {
			n:      10,
			output: 36,
		},
		2: {
			n:      3,
			output: 2,
		},
		3: {
			n:      4,
			output: 4,
		},
	}
	for i, scenario := range scenarios {
		output := integerBreak(scenario.n)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
