package problem312

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestMaxcoins(t *testing.T) {
	scenarios := [...]struct {
		nums   []int
		output int
	}{
		0: {
			nums:   []int{3, 1, 5, 8},
			output: 167,
		},
		1: {
			nums:   []int{1, 5},
			output: 10,
		},
		2: {
			nums:   []int{8, 2, 6, 8, 9, 8, 1, 4, 1, 5, 3, 0, 7, 7, 0, 4, 2, 2, 5},
			output: 3630,
		},
	}
	for i, scenario := range scenarios {
		output := maxCoins(scenario.nums)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
