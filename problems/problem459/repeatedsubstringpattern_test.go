package problem459

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestRepeatedsubstringpattern(t *testing.T) {
	scenarios := [...]struct {
		s      string
		output bool
	}{
		0: {
			s:      "abab",
			output: true,
		},
		1: {
			s:      "aba",
			output: false,
		},
		2: {
			s:      "abcabcabcabc",
			output: true,
		},
	}
	for i, scenario := range scenarios {
		output := repeatedSubstringPattern(scenario.s)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
