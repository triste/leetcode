package problem785

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestIsbipartite(t *testing.T) {
	scenarios := [...]struct {
		graph  [][]int
		output bool
	}{
		0: {
			graph: [][]int{
				{1, 2, 3},
				{0, 2},
				{0, 1, 3},
				{0, 2},
			},
			output: false,
		},
		1: {
			graph: [][]int{
				{1, 3},
				{0, 2},
				{1, 3},
				{0, 2},
			},
			output: true,
		},
		2: {
			graph: [][]int{
				{},
				{2, 4, 6},
				{1, 4, 8, 9},
				{7, 8},
				{1, 2, 8, 9},
				{6, 9},
				{1, 5, 7, 8, 9},
				{3, 6, 9},
				{2, 3, 4, 6, 9},
				{2, 4, 5, 6, 7, 8},
			},
			output: false,
		},
	}
	for i, scenario := range scenarios {
		output := isBipartite(scenario.graph)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
