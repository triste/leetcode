package problem342

import (
	"math/bits"
)

func isPowerOfFour(n int) bool {
	x := uint32(n)
	return n > 0 && bits.OnesCount32(x) == 1 && bits.LeadingZeros32(x)%2 == 1
}
