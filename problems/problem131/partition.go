package problem131

func partition(s string) (output [][]string) {
	var dp [16][16]bool
	for diff := 0; diff < len(s); diff++ {
		for l := 0; l < len(s)-diff; l++ {
			r := l + diff
			dp[l][r] = (diff < 2 || dp[l+1][r-1]) && s[l] == s[r]
		}
	}
	var solve func(l int, path []string)
	solve = func(l int, path []string) {
		if l == len(s) {
			parts := make([]string, len(path))
			copy(parts, path)
			output = append(output, parts)
			return
		}
		for r := l; r < len(s); r++ {
			if dp[l][r] {
				solve(r+1, append(path, s[l:r+1]))
			}
		}
	}
	solve(0, nil)
	return
}
