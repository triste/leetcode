package problem1249

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestMinremovetomakevalid(t *testing.T) {
	scenarios := [...]struct {
		s      string
		output string
	}{
		0: {"lee(t(c)o)de)", "lee(t(c)o)de"},
		1: {"a)b(c)d", "ab(c)d"},
		2: {"))((", ""},
		3: {"())()(((", "()()"},
	}
	for i, scenario := range scenarios {
		output := minRemoveToMakeValid(scenario.s)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
