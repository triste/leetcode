package problem1359

func countOrders(n int) int {
	acc := 1
	for i := 2; i <= n; i++ {
		acc = acc * i * (i*2 - 1) % 1_000_000_007
	}
	return acc
}

/*
case 1:
	P1, D1
case 2:
	P1, D1, P2, D2
	P1, P2, D1, D2
	P1, P2, D2, D1


	1, x1, 2, x2, 3, x3, 4, x4, 5
*/
