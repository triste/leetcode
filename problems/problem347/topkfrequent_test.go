package problem347

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestTopkfrequent(t *testing.T) {
	scenarios := [...]struct {
		nums   []int
		k      int
		output []int
	}{
		0: {[]int{1, 1, 1, 2, 2, 3}, 2, []int{2, 1}},
		1: {[]int{1}, 1, []int{1}},
	}
	for i, scenario := range scenarios {
		output := topKFrequent(scenario.nums, scenario.k)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
