package problem646

import (
	"sort"
)

func findLongestChain(pairs [][]int) (output int) {
	sort.Slice(pairs, func(i, j int) bool {
		return pairs[i][1] < pairs[j][1]
	})
	chainLenght := 1
	end := pairs[0][1]
	for _, pair := range pairs[1:] {
		if pair[0] > end {
			chainLenght++
			end = pair[1]
		}
	}
	return chainLenght
}

/*
	{1,2},{2,3}

	{1,2}
*/
