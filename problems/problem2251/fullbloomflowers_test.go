package problem2251

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestFullbloomflowers(t *testing.T) {
	scenarios := [...]struct {
		flowers [][]int
		people  []int
		output  []int
	}{
		0: {
			flowers: [][]int{
				{1, 6}, {3, 7}, {9, 12}, {4, 13},
			},
			people: []int{
				2, 3, 7, 11,
			},
			output: []int{1, 2, 2, 2},
		},
		1: {
			flowers: [][]int{
				{1, 10}, {3, 3},
			},
			people: []int{3, 3, 2},
			output: []int{2, 2, 1},
		},
	}
	for i, scenario := range scenarios {
		output := fullBloomFlowers(scenario.flowers, scenario.people)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
