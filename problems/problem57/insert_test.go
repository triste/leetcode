package problem57

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestInsert(t *testing.T) {
	scenarios := [...]struct {
		intervals   [][]int
		newInterval []int
		output      [][]int
	}{
		0: {
			intervals: [][]int{
				{1, 3}, {6, 9},
			},
			newInterval: []int{2, 5},
			output: [][]int{
				{1, 5}, {6, 9},
			},
		},
		1: {
			intervals: [][]int{
				{1, 2}, {3, 5}, {6, 7}, {8, 10}, {12, 16},
			},
			newInterval: []int{4, 8},
			output: [][]int{
				{1, 2}, {3, 10}, {12, 16},
			},
		},
		2: {
			intervals: [][]int{
				{3, 5}, {6, 7}, {8, 10}, {12, 16},
			},
			newInterval: []int{1, 2},
			output: [][]int{
				{1, 2}, {3, 5}, {6, 7}, {8, 10}, {12, 16},
			},
		},
		3: {
			intervals: [][]int{
				{1, 2}, {3, 5}, {6, 7}, {8, 10},
			},
			newInterval: []int{12, 16},
			output: [][]int{
				{1, 2}, {3, 5}, {6, 7}, {8, 10}, {12, 16},
			},
		},
		4: {
			intervals: [][]int{
				{1, 2}, {3, 5}, {6, 7}, {8, 10},
			},
			newInterval: []int{1, 11},
			output: [][]int{
				{1, 11},
			},
		},
		5: {
			intervals: [][]int{
				{1, 2}, {3, 5}, {6, 7}, {8, 10},
			},
			newInterval: []int{2, 3},
			output: [][]int{
				{1, 5}, {6, 7}, {8, 10},
			},
		},
		6: {
			intervals: [][]int{
				{1, 2}, {3, 5}, {6, 7}, {8, 10},
			},
			newInterval: []int{1, 5},
			output: [][]int{
				{1, 5}, {6, 7}, {8, 10},
			},
		},
		7: {
			intervals: [][]int{
				{3, 5}, {12, 15},
			},
			newInterval: []int{6, 6},
			output: [][]int{
				{3, 5}, {6, 6}, {12, 15},
			},
		},
	}
	for i, scenario := range scenarios {
		output := insert(scenario.intervals, scenario.newInterval)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
