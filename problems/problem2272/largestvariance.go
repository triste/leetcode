package problem2272

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func largestVariance(s string) (largestVariance int) {
	var charCounts [26]int
	uniqueChars := make([]rune, 0, 26)
	for _, ch := range s {
		if charCounts[ch-'a'] == 0 {
			uniqueChars = append(uniqueChars, ch)
		}
		charCounts[ch-'a']++
	}
	for _, a := range uniqueChars {
		for _, b := range uniqueChars {
			if a == b {
				continue
			}
			bCount := 0
			bCountGlobal := 0
			localSum := 0
			for _, ch := range s {
				if ch == a {
					localSum++
				} else if ch == b {
					bCountGlobal++
					bCount++
					localSum--
					if localSum < 0 && bCountGlobal < charCounts[b-'a'] {
						bCount = 0
						localSum = 0
					}
				}
				if localSum > largestVariance && bCount > 0 {
					largestVariance = localSum
				}
			}
		}
	}
	return largestVariance
}
