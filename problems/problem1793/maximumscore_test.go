package problem1793

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestMaximumscore(t *testing.T) {
	scenarios := [...]struct {
		nums   []int
		k      int
		output int
	}{
		0: {
			nums:   []int{1, 4, 3, 7, 4, 5},
			k:      3,
			output: 15,
		},
		1: {
			nums:   []int{5, 5, 4, 5, 4, 1, 1, 1},
			k:      0,
			output: 20,
		},
	}
	for i, scenario := range scenarios {
		output := maximumScore(scenario.nums, scenario.k)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
