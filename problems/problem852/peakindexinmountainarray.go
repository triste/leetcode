package problem852

func peakIndexInMountainArray(arr []int) int {
	l, r := 0, len(arr)-2
	for l < r {
		m := (l + r) / 2
		if arr[m] < arr[m+1] {
			l = m + 1
		} else {
			r = m
		}
	}
	return r
}

/*
	0, 1, 0
	-1, 1

	0, 2, 1, 0
	-2, 1, 1

	0, 10, 5, 2
	-10, 5,
*/
