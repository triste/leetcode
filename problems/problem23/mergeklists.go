package problem23

import (
	"sort"

	. "gitlab.com/triste/leetcode/pkg/list"
)

func mergeKLists(lists []*ListNode) *ListNode {
	var nonEmptyLists []*ListNode
	for _, l := range lists {
		if l != nil {
			nonEmptyLists = append(nonEmptyLists, l)
		}
	}
	sort.Slice(nonEmptyLists, func(i, j int) bool {
		return nonEmptyLists[i].Val > nonEmptyLists[j].Val
	})
	output := &ListNode{}
	lastNode := output
	for len(nonEmptyLists) != 0 {
		endIndex := len(nonEmptyLists) - 1
		current := nonEmptyLists[endIndex]
		next := current.Next
		current.Next = nil
		lastNode.Next = current
		lastNode = current
		if next == nil {
			nonEmptyLists = nonEmptyLists[:endIndex]
			continue
		}
		i := sort.Search(endIndex, func(i int) bool {
			return nonEmptyLists[i].Val < next.Val
		})
		copy(nonEmptyLists[i+1:], nonEmptyLists[i:endIndex])
		nonEmptyLists[i] = next
	}
	return output.Next
}
