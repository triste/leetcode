package problem1

func twoSum(nums []int, target int) []int {
	m := make(map[int]int, len(nums) * 2)
	for i, v1 := range nums {
		if j, ok := m[target-v1]; ok {
			return []int{j, i}
		}
		m[v1] = i
	}
	return nil
}
