package problem1396

type UndergroundSystem struct {
	times  map[[2]string][2]int
	checks map[int]Check
}

type Check struct {
	time    int
	station string
}

func Constructor() UndergroundSystem {
	return UndergroundSystem{
		times:  make(map[[2]string][2]int),
		checks: make(map[int]Check),
	}
}

func (u *UndergroundSystem) CheckIn(id int, station string, t int) {
	u.checks[id] = Check{t, station}
}

func (u *UndergroundSystem) CheckOut(id int, station string, t int) {
	in := u.checks[id]
	delete(u.checks, id)
	travel := [2]string{in.station, station}
	times := u.times[travel]
	times[0] += t - in.time
	times[1]++
	u.times[travel] = times
}

func (u *UndergroundSystem) GetAverageTime(startStation string, endStation string) float64 {
	travel := [2]string{startStation, endStation}
	times := u.times[travel]
	return float64(times[0]) / float64(times[1])
}
