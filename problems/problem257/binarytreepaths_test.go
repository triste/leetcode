package problem257

import (
	"testing"

	"github.com/google/go-cmp/cmp"
	. "gitlab.com/triste/leetcode/pkg/tree"
)

func TestBinarytreepaths(t *testing.T) {
	scenarios := [...]struct {
		root   *TreeNode
		output []string
	}{
		0: {
			root: NewTree(1, 2, 3, nil, 5),
			output: []string{
				"1->2->5", "1->3",
			},
		},
		1: {
			root:   NewTree(1),
			output: []string{"1"},
		},
	}
	for i, scenario := range scenarios {
		output := binaryTreePaths(scenario.root)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
