package problem99

import (
	"math"

	. "gitlab.com/triste/leetcode/pkg/tree"
)

func recoverTree(root *TreeNode) {
	prevNode := &TreeNode{
		Val:   math.MinInt,
		Left:  nil,
		Right: nil,
	}
	var swappedNodes [2]*TreeNode
	var bfs func(root *TreeNode)
	bfs = func(root *TreeNode) {
		if root == nil {
			return
		}
		bfs(root.Left)
		if root.Val < prevNode.Val {
			if swappedNodes[0] == nil {
				swappedNodes[0] = prevNode
			}
			swappedNodes[1] = root
		}
		prevNode = root
		bfs(root.Right)
	}
	bfs(root)
	swappedNodes[0].Val, swappedNodes[1].Val = swappedNodes[1].Val, swappedNodes[0].Val
}

/*
          3
		   \
		    2
			 \
			  1
*/

/*
	 2
	/ \
   3   1

   2 nil,nil

   3 nil,2
*/

/*
           4
		 /   \
		2     6
	   / \   / \
	  1   3 5   7

case 1:
           3
		 /   \
		2     6
	   / \   / \
	  1   4 5   7

case 2:
           4
		 /   \
		2     6
	   / \   / \
	  1   7 5   3
*/
