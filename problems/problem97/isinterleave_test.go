package problem97

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestIsinterleave(t *testing.T) {
	scenarios := [...]struct {
		s1     string
		s2     string
		s3     string
		output bool
	}{
		0: {
			s1:     "aabcc",
			s2:     "dbbca",
			s3:     "aadbbcbcac",
			output: true,
		},
		1: {
			s1:     "aabcc",
			s2:     "dbbca",
			s3:     "aadbbbaccc",
			output: false,
		},
		2: {
			s1:     "",
			s2:     "",
			s3:     "",
			output: true,
		},
		3: {
			s1:     "deeff",
			s2:     "abbcc",
			s3:     "abbccdeeff",
			output: true,
		},
		4: {
			s1:     "aaaaaaaaaaaaaaaaaaaaaaaaaaa",
			s2:     "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
			s3:     "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
			output: false,
		},
	}
	for i, scenario := range scenarios {
		output := isInterleave(scenario.s1, scenario.s2, scenario.s3)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
