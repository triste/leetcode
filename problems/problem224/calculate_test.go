package problem224

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestCalculate(t *testing.T) {
	scenarios := [...]struct {
		s      string
		output int
	}{
		0: {
			s:      "1 + 1",
			output: 2,
		},
		1: {
			s:      "2-1 + 2",
			output: 3,
		},
		2: {
			s:      "(1+(4+5+2)-3)+(6+8)",
			output: 23,
		},
	}
	for i, scenario := range scenarios {
		output := calculate(scenario.s)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
