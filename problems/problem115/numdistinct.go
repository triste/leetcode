package problem115

func numDistinct(s string, t string) int {
	dp := make([]int, len(s)+1)
	dpPrev := make([]int, len(s)+1)
	for i := range dpPrev {
		dpPrev[i] = 1
	}
	for _, ch2 := range t {
		dp[0] = 0
		for i, ch1 := range s {
			if ch1 == ch2 {
				dp[i+1] = dpPrev[i] + dp[i]
			} else {
				dp[i+1] = dp[i]
			}
		}
		dp, dpPrev = dpPrev, dp
	}
	return dpPrev[len(s)]
}

/*
   | b | a | b | g | b | a | g |
 b | 1 | 1 | 2 | 2 | 3 | 3 | 3 |
 a | 0 | 1 | 1 | 1 | 1 | 4 | 4 |
 g | 0 | 0 | 0 | 1 | 1 | 1 | 5 |
*/
