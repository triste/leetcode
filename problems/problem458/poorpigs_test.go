package problem458

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestPoorpigs(t *testing.T) {
	scenarios := [...]struct {
		buckets       int
		minutesToDie  int
		minutesToTest int
		output        int
	}{
		0: {
			buckets:       4,
			minutesToDie:  15,
			minutesToTest: 15,
			output:        2,
		},
		1: {
			buckets:       4,
			minutesToDie:  15,
			minutesToTest: 30,
			output:        2,
		},
	}
	for i, scenario := range scenarios {
		output := poorPigs(scenario.buckets, scenario.minutesToDie, scenario.minutesToTest)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
