package problem38

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestCountandsay(t *testing.T) {
	scenarios := [...]struct {
		n      int
		output string
	}{
		0: {
			n:      1,
			output: "1",
		},
		1: {
			n:      4,
			output: "1211",
		},
		2: {
			n:      10,
			output: "13211311123113112211",
		},
		3: {
			n:      13,
			output: "1321132132111213122112311311222113111221131221",
		},
	}
	for i, scenario := range scenarios {
		output := countAndSay(scenario.n)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
