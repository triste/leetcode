package problem1361

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestValidatebinarytreenodes(t *testing.T) {
	scenarios := [...]struct {
		n          int
		leftChild  []int
		rightChild []int
		output     bool
	}{
		0: {
			n:          4,
			leftChild:  []int{1, -1, 3, -1},
			rightChild: []int{2, -1, -1, -1},
			output:     true,
		},
		1: {
			n:          4,
			leftChild:  []int{1, -1, 3, -1},
			rightChild: []int{2, 3, -1, -1},
			output:     false,
		},
		2: {
			n:          2,
			leftChild:  []int{1, 0},
			rightChild: []int{-1, -1},
			output:     false,
		},
		3: {
			n:          4,
			leftChild:  []int{1, 0, 3, -1},
			rightChild: []int{-1, -1, -1, -1},
			output:     false,
		},
	}
	for i, scenario := range scenarios {
		output := validateBinaryTreeNodes(scenario.n, scenario.leftChild, scenario.rightChild)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
