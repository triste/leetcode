package problem90

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestSubsetswithdup(t *testing.T) {
	scenarios := [...]struct {
		nums   []int
		output [][]int
	}{
		0: {
			nums: []int{1, 2, 2},
			output: [][]int{
				{}, {1}, {1, 2}, {1, 2, 2}, {2}, {2, 2},
			},
		},
		1: {
			nums:   []int{0},
			output: [][]int{{}, {0}},
		},
	}
	for i, scenario := range scenarios {
		output := subsetsWithDup(scenario.nums)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
