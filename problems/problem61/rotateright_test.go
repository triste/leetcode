package problem61

import (
	"testing"

	"github.com/google/go-cmp/cmp"
	. "gitlab.com/triste/leetcode/pkg/list"
)

func TestRotateright(t *testing.T) {
	scenarios := [...]struct {
		head   *ListNode
		k      int
		output *ListNode
	}{
		0: {
			head:   NewList(1, 2, 3, 4, 5),
			k:      2,
			output: NewList(4, 5, 1, 2, 3),
		},
		1: {
			head:   NewList(0, 1, 2),
			k:      4,
			output: NewList(2, 0, 1),
		},
		2: {
			head:   nil,
			k:      0,
			output: nil,
		},
		3: {
			head:   NewList(1, 2, 3, 4, 5),
			k:      0,
			output: NewList(1, 2, 3, 4, 5),
		},
		4: {
			head:   NewList(1),
			k:      1,
			output: NewList(1),
		},
		5: {
			head:   NewList(1, 2),
			k:      2,
			output: NewList(1, 2),
		},
	}
	for i, scenario := range scenarios {
		output := rotateRight(scenario.head, scenario.k)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
