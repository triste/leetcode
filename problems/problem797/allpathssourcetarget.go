package problem797

func allPathsSourceTarget(graph [][]int) (output [][]int) {
	queue := [][]int{{0}}
	lastnode := len(graph) - 1
	var tmpqueue [][]int
	for len(queue) > 0 {
		for _, path := range queue {
			lenght := len(path)
			tail := path[lenght-1]
			lastlink := len(graph[tail]) - 1
			for i, node := range graph[tail] {
				var newpath []int
				if i == lastlink {
					newpath = append(path, node)
				} else {
					newpath = make([]int, lenght+1)
					copy(newpath, path)
					newpath[lenght] = node
				}
				if node == lastnode {
					output = append(output, newpath)
				} else {
					tmpqueue = append(tmpqueue, newpath)
				}
			}
		}
		queue, tmpqueue = tmpqueue, queue[:0]
	}
	return
}
