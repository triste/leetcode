package problem18

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestFoursum(t *testing.T) {
	scenarios := [...]struct {
		nums   []int
		target int
		output [][]int
	}{
		0: {
			[]int{1, 0, -1, 0, -2, 2},
			0,
			[][]int{
				{-2, -1, 1, 2},
				{-2, 0, 0, 2},
				{-1, 0, 0, 1},
			},
		},
		1: {
			[]int{2, 2, 2, 2, 2},
			8,
			[][]int{{2, 2, 2, 2}},
		},
	}
	for i, scenario := range scenarios {
		output := fourSum(scenario.nums, scenario.target)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
