package problem60

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestGetpermutation(t *testing.T) {
	scenarios := [...]struct {
		n      int
		k      int
		output string
	}{
		0: {
			n:      3,
			k:      3,
			output: "213",
		},
		1: {
			n:      4,
			k:      9,
			output: "2314",
		},
		2: {
			n:      3,
			k:      1,
			output: "123",
		},
	}
	for i, scenario := range scenarios[1:2] {
		output := getPermutation(scenario.n, scenario.k)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
