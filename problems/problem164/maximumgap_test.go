package problem164

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestMaximumgap(t *testing.T) {
	scenarios := [...]struct {
		nums   []int
		output int
	}{
		0: {
			nums:   []int{3, 6, 9, 1},
			output: 3,
		},
		1: {
			nums:   []int{10},
			output: 0,
		},
		2: {
			nums:   []int{170, 45, 75, 90, 2, 802, 2, 66},
			output: 34,
		},
	}
	for i, scenario := range scenarios {
		output := maximumGap(scenario.nums)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
