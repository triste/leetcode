package problem14

func longestCommonPrefix(strs []string) (output string) {
	longest := len(strs[0])
	for _, str := range strs[1:] {
		if len(str) < longest {
			longest = len(str)
		}
		for i := longest - 1; i >= 0; i-- {
			if str[i] != strs[0][i] {
				longest = i
			}
		}
	}
	return strs[0][:longest]
}
