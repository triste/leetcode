package problem146

type Node struct {
	key   int
	value int
	next  *Node
	prev  *Node
}

type List struct {
	head *Node
	tail *Node
	size int
}

func (l *List) PushBack(node *Node) {
	if l.tail == nil {
		l.head = node
		l.tail = node
	} else {
		l.tail.next = node
		node.prev = l.tail
		l.tail = node
	}
	l.size++
}

func (l *List) PopBack() *Node {
	if l.size == 0 {
		return nil
	}
	node := l.tail
	if l.size == 1 {
		l.head = nil
		l.tail = nil
	} else {
		l.tail = l.tail.prev
		l.tail.next = nil
	}
	node.prev = nil
	l.size--
	return node
}

func (l *List) PushFront(node *Node) {
	if l.size == 0 {
		l.head = node
		l.tail = node
	} else {
		l.head.prev = node
		node.next = l.head
		l.head = node
	}
	l.size++
}

func (l *List) PopFront() *Node {
	if l.size == 0 {
		return nil
	}
	node := l.head
	if l.size == 1 {
		l.head = nil
		l.tail = nil
	} else {
		l.head = l.head.next
		l.head.prev = nil
	}
	node.next = nil
	l.size--
	return node
}

func (l *List) Pop(node *Node) {
	if node == l.head {
		l.PopFront()
		return
	}
	if node == l.tail {
		l.PopBack()
		return
	}
	node.prev.next = node.next
	node.next.prev = node.prev
	node.prev = nil
	node.next = nil
	l.size--
}

func (l *List) Size() int {
	return l.size
}

type LRUCache struct {
	nodes    map[int]*Node
	list     List
	capacity int
}

func Constructor(capacity int) LRUCache {
	return LRUCache{
		nodes:    make(map[int]*Node, capacity),
		capacity: capacity,
	}
}

func (l List) Slice() []int {
	slice := make([]int, 0, l.size)
	for p := l.head; p != nil; p = p.next {
		slice = append(slice, p.value)
	}
	return slice
}

func (c *LRUCache) Get(key int) int {
	node, found := c.nodes[key]
	if !found {
		return -1
	}
	c.list.Pop(node)
	c.list.PushFront(node)
	return node.value
}

func (c *LRUCache) Put(key int, value int) {
	if node, ok := c.nodes[key]; ok {
		node.value = value
		c.list.Pop(node)
		c.list.PushFront(node)
		return
	}
	if c.capacity == c.list.Size() {
		lru := c.list.PopBack()
		delete(c.nodes, lru.key)
	}
	node := &Node{key, value, nil, nil}
	c.list.PushFront(node)
	c.nodes[key] = node
}
