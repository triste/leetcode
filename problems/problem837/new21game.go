package problem837

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func new21Game(n int, k int, maxPts int) (output float64) {
	if n == 0 || k == 0 {
		return 1.0
	}
	dp := make([]float64, n+1)
	dp[0] = 1
	maxPtsSum := 1.0
	probability := 0.0
	for i := 1; i < k; i++ {
		dp[i] = maxPtsSum / float64(maxPts)
		maxPtsSum += dp[i]
		if i-maxPts < k && i-maxPts >= 0 {
			maxPtsSum -= dp[i-maxPts]
		}
	}
	for i := k; i <= n; i++ {
		dp[i] = maxPtsSum / float64(maxPts)
		if i-maxPts < k && i-maxPts >= 0 {
			maxPtsSum -= dp[i-maxPts]
		}
		probability += dp[i]
	}
	return probability
}

/*
	1, 2, 3, 4, 5, 6, 7, 8, 9, 10
	k                          n == maxPts

	1, 2, 3, 4, 5, 6, 7, 8, 9, 10
	k              n           maxPts

	1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26
	                           maxPts                      k               n                   k+maxPts-1
*/

/*
	0: => 1.0
	1: 1 => 0.1
	2: 1-1, 2 => 0.1*0.1 + 1.0*0.1
*/
/*
	n = 10
	k = 1
	maxPts = 10

	start = 0
		start + [1, 10]
		probabilities: 1, 2, ..., 10


	n = 6
	k = 1
	maxPts = 10
	start = 0
		start + [1, 10]
		probabilities: 1, 2, ..., 10

	n = 21, k = 17, maxPts = 10
	start = 0
		start + [1, 10]
		probabilities: 1, 2, ..., 10
	start = 1
		start + [1, 10]
	start = 2 ...


	n = 21, k = 17, maxPtrs = 10
	ends = [17, 26]
	17, 18, 19, 20, 21



	1: 1/10
	2: 1+1, 2 => 1/10*1/10, 1/10 => 1/100 + 1/10 = 11/100
	3: 1+1+1, 2+1, 1+2, 3 => 0.1*0.1*0.1+0.1*0.1+0.1*0.1+0.1
	...
	10: 1+1+1+...+1
*/
