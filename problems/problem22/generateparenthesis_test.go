package problem22

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestGenerateparenthesis(t *testing.T) {
	scenarios := [...]struct {
		n      int
		output []string
	}{
		/*
			0: {3, []string{"((()))","(()())","(())()","()(())","()()()"}},
			1: {1, []string{"()"}},
			2: {4, []string{
				"(((())))","((()()))","((())())","((()))()","(()(()))",
				"(()()())","(()())()","(())(())","(())()()","()((()))",
				"()(()())","()(())()","()()(())","()()()()"},
			},
		*/
		/* needs:
			(())(()),(())()()
		/*
		["()()()()","()()(())"]
		*/
	}
	for i, scenario := range scenarios {
		output := generateParenthesis(scenario.n)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}

/*
got: "()()()()" "(()()())" "()(()())" "((()()))" "(()())()" "()()(())" "(()(()))" "()(())()" "()((()))" "(((())))" "((()))()" "((())())" "(())()()"
want: "(((())))" "((()()))" "((())())" "((()))()" "(()(()))" "(()()())" "(()())()" "(())(())" "(())()()" "()((()))" "()(()())" "()(())()" "()()(())" "()()()()"
*/
