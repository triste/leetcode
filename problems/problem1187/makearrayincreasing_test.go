package problem1187

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestMakearrayincreasing(t *testing.T) {
	scenarios := [...]struct {
		arr1   []int
		arr2   []int
		output int
	}{
		0: {
			arr1:   []int{1, 5, 3, 6, 7},
			arr2:   []int{1, 3, 2, 4},
			output: 1,
		},
		1: {
			arr1:   []int{1, 5, 3, 6, 7},
			arr2:   []int{4, 3, 1},
			output: 2,
		},
		2: {
			arr1:   []int{1, 5, 3, 6, 7},
			arr2:   []int{1, 6, 3, 3},
			output: -1,
		},
		3: {
			arr1:   []int{0, 11, 6, 1, 4, 3},
			arr2:   []int{5, 4, 11, 10, 1, 0},
			output: 5,
		},
		4: {
			arr1:   []int{5, 16, 19, 2, 1, 12, 7, 14, 5, 16},
			arr2:   []int{6, 17, 4, 3, 6, 13, 4, 3, 18, 17, 16, 7, 14, 1, 16},
			output: 8,
		},
	}
	for i, scenario := range scenarios {
		output := makeArrayIncreasing(scenario.arr1, scenario.arr2)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
