package problem1337

import (
	"container/heap"
)

type Node struct {
	rowIdx       int
	soldierCount int
}

type MinHeap []Node

func (pq MinHeap) Len() int {
	return len(pq)
}

func (pq MinHeap) Less(i, j int) bool {
	if pq[i].soldierCount == pq[j].soldierCount {
		return pq[i].rowIdx > pq[j].rowIdx
	}
	return pq[i].soldierCount > pq[j].soldierCount
}

func (pq MinHeap) Swap(i, j int) {
	pq[i], pq[j] = pq[j], pq[i]
}

func (pq *MinHeap) Push(x any) {
	*pq = append(*pq, x.(Node))
}

func (pq *MinHeap) Pop() any {
	x := (*pq)[len(*pq)-1]
	*pq = (*pq)[:len(*pq)-1]
	return x
}

func kWeakestRows(mat [][]int, k int) []int {
	pq := make(MinHeap, 0, k)
	heap.Init(&pq)
	for i, row := range mat {
		soldierCount := 0
		for j := 0; j < len(row) && row[j] == 1; j++ {
			soldierCount++
		}
		node := Node{i, soldierCount}
		if i < k {
			heap.Push(&pq, node)
		} else if pq[0].soldierCount > soldierCount {
			pq[0] = node
			heap.Fix(&pq, 0)
		}
	}
	weakestRows := make([]int, k)
	for i := k - 1; i >= 0; i-- {
		weakestRows[i] = heap.Pop(&pq).(Node).rowIdx
	}
	return weakestRows
}
