package problem199

import (
	. "gitlab.com/triste/leetcode/pkg/tree"
)

func rightSideView(root *TreeNode) (output []int) {
	if root == nil {
		return nil
	}
	stack := []*TreeNode{root}
	for len(stack) > 0 {
		output = append(output, stack[len(stack)-1].Val)
		newStack := []*TreeNode{}
		for _, node := range stack {
			if node.Left != nil {
				newStack = append(newStack, node.Left)
			}
			if node.Right != nil {
				newStack = append(newStack, node.Right)
			}
		}
		stack = newStack
	}
	return
}
