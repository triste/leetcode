package problem151

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestReversewords(t *testing.T) {
	scenarios := [...]struct {
		s      string
		output string
	}{
		0: {
			s:      "the sky is blue",
			output: "blue is sky the",
		},
		1: {
			s:      "  hello world  ",
			output: "world hello",
		},
		2: {
			s:      "a good   example",
			output: "example good a",
		},
	}
	for i, scenario := range scenarios {
		output := reverseWords(scenario.s)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
