package problem218

import "container/heap"

type End struct {
	x          int
	height     int
	maxHeapIdx int
}

type MinHeap []*End

func (h MinHeap) Len() int {
	return len(h)
}

func (h MinHeap) Less(i, j int) bool {
	return h[i].x < h[j].x
}

func (h MinHeap) Swap(i, j int) {
	h[i], h[j] = h[j], h[i]
}

func (h *MinHeap) Push(x any) {
	*h = append(*h, x.(*End))
}

func (h *MinHeap) Pop() any {
	x := (*h)[len(*h)-1]
	*h = (*h)[:len(*h)-1]
	return x
}

type MaxHeap []*End

func (h MaxHeap) Len() int {
	return len(h)
}

func (h MaxHeap) Less(i, j int) bool {
	if h[i].height < h[j].height {
		return true
	} else {
		h[i].maxHeapIdx = i
		h[j].maxHeapIdx = j
		return false
	}
}

func (h MaxHeap) Swap(i, j int) {
	h[i], h[j] = h[j], h[i]
	h[i].maxHeapIdx = i
	h[j].maxHeapIdx = j
}

func (h *MaxHeap) Push(x any) {
	*h = append(*h, x.(*End))
}

func (h *MaxHeap) Pop() any {
	x := (*h)[len(*h)-1]
	*h = (*h)[:len(*h)-1]
	return x
}

func getSkyline(buildings [][]int) (skyline [][]int) {
	ends := &MinHeap{}
	heap.Init(ends)
	floors := &MaxHeap{&End{0, 0, 0}}
	heap.Init(floors)
	prevX := -1
	for i := 0; i < len(buildings) || ends.Len() > 0; {
		if i < len(buildings) && ends.Len() > 0 && (*ends)[0].x < buildings[i][0] || i == len(buildings) {
			prevHeight := -(*floors)[0].height
			end := heap.Pop(ends).(*End)
			heap.Remove(floors, end.maxHeapIdx)
			height := -(*floors)[0].height
			if height < prevHeight {
				if end.x == prevX {
					skyline[len(skyline)-1][1] = height
				} else {
					prevX = end.x
					skyline = append(skyline, []int{end.x, height})
				}
			}
		} else {
			prevHeight := -(*floors)[0].height
			if buildings[i][2] > prevHeight {
				if buildings[i][0] == prevX {
					skyline[len(skyline)-1][1] = buildings[i][2]
				} else {
					prevX = buildings[i][0]
					skyline = append(skyline, []int{buildings[i][0], buildings[i][2]})
				}
			}
			end := &End{buildings[i][1], -buildings[i][2], 0}
			heap.Push(ends, end)
			heap.Push(floors, end)
			i++
		}
	}
	return
}
