package problem33

func search(nums []int, target int) (output int) {
	k := findK(nums)
	if target >= nums[k] && target <= nums[len(nums)-1] {
		if i := binsearch(nums[k:], target); i < 0 {
			return -1
		} else {
			return k + i
		}
	} else {
		return binsearch(nums[:k], target)
	}
}

func findK(nums []int) int {
	left := 0
	right := len(nums) - 1
	for left < right && nums[left] > nums[right] {
		mid := (left + right) / 2
		if nums[left] > nums[mid] {
			right = mid
		} else {
			left = mid + 1
		}
	}
	return left
}

func binsearch(nums []int, target int) int {
	left := 0
	right := len(nums) - 1
	for left <= right {
		mid := (left + right) / 2
		if nums[mid] == target {
			return mid
		}
		if nums[mid] < target {
			left = mid + 1
		} else {
			right = mid - 1
		}
	}
	return -1
}

/*
	0 1 2 3 4 5 6 7
	k = 3: 3 4 5 6 7 0 1 2
*/
