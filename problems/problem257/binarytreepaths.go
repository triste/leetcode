package problem257

import (
	"strconv"
	"strings"

	. "gitlab.com/triste/leetcode/pkg/tree"
)

func binaryTreePaths(root *TreeNode) (paths []string) {
	var dfs func(root *TreeNode, path []string)
	dfs = func(root *TreeNode, path []string) {
		if root == nil {
			return
		}
		path = append(path, strconv.Itoa(root.Val))
		if root.Left == nil && root.Right == nil {
			paths = append(paths, strings.Join(path, "->"))
			return
		}
		dfs(root.Left, path)
		dfs(root.Right, path)
	}
	dfs(root, nil)
	return
}
