package problem785

type Set bool

const (
	SetA Set = false
	SetB Set = true
)

type Node struct {
	visited bool
	set     Set
}

func isBipartite(graph [][]int) (output bool) {
	n := len(graph)
	nodes := make([]Node, n)
	var dfs func(node int, set Set) bool
	dfs = func(i int, set Set) bool {
		if node := nodes[i]; node.visited {
			return node.set == set
		}
		nodes[i] = Node{true, set}
		for _, i := range graph[i] {
			if !dfs(i, !set) {
				return false
			}
		}
		return true
	}
	for i := range graph {
		if !dfs(i, nodes[i].set) {
			return false
		}
	}
	return true
}
