package problem1282

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestGroupthepeople(t *testing.T) {
	scenarios := [...]struct {
		groupSizes []int
		output     [][]int
	}{
		0: {
			groupSizes: []int{3, 3, 3, 3, 3, 1, 3},
			output: [][]int{
				{0, 1, 2},
				{5},
				{3, 4, 6},
			},
		},
		1: {
			groupSizes: []int{2, 1, 3, 3, 3, 2},
			output: [][]int{
				{1},
				{2, 3, 4},
				{0, 5},
			},
		},
	}
	for i, scenario := range scenarios {
		output := groupThePeople(scenario.groupSizes)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
