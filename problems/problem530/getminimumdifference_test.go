package problem530

import (
	"testing"

	"github.com/google/go-cmp/cmp"
	. "gitlab.com/triste/leetcode/pkg/tree"
)

func TestGetminimumdifference(t *testing.T) {
	scenarios := [...]struct {
		root   *TreeNode
		output int
	}{
		0: {
			root:   NewTree(5, 2, 6, 1, 3),
			output: 1,
		},
		1: {
			root:   NewTree(3, 0, 47, nil, nil, 12, 49),
			output: 2,
		},
		2: {
			root:   NewTree(1, nil, 2),
			output: 1,
		},
	}
	for i, scenario := range scenarios {
		output := getMinimumDifference(scenario.root)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
