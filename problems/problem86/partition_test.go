package problem86

import (
	"testing"

	"github.com/google/go-cmp/cmp"
	. "gitlab.com/triste/leetcode/pkg/list"
)

func TestPartition(t *testing.T) {
	scenarios := [...]struct {
		head   *ListNode
		x      int
		output *ListNode
	}{
		0: {
			head:   NewList(1, 4, 3, 2, 5, 2),
			x:      3,
			output: NewList(1, 2, 2, 4, 3, 5),
		},
		1: {
			head:   NewList(2, 1),
			x:      2,
			output: NewList(1, 2),
		},
	}
	for i, scenario := range scenarios {
		output := partition(scenario.head, scenario.x)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
