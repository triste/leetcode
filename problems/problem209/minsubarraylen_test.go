package problem209

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestMinsubarraylen(t *testing.T) {
	scenarios := [...]struct {
		target int
		nums   []int
		output int
	}{
		0: {7, []int{2, 3, 1, 2, 4, 3}, 2},
		1: {4, []int{1, 4, 4}, 1},
		2: {11, []int{1, 1, 1, 1, 1, 1, 1, 1}, 0},
		3: {11, []int{1, 2, 3, 4, 5}, 3},
	}
	for i, scenario := range scenarios {
		output := minSubArrayLen(scenario.target, scenario.nums)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
