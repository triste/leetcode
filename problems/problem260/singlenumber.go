package problem260

import "log"

func singleNumber(nums []int) []int {
	and := 0
	xor := 0
	for _, num := range nums {
		and &= xor
		xor ^= num
	}
	log.Println(and, xor)
	return nil
}

/*
2: 010
5: 101

x: 111

1: 001 - 110
2: 010 - 101
3: 011 - 100
5: 101 - 010

*/
