package problem171

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestTitletonumber(t *testing.T) {
	scenarios := [...]struct {
		columnTitle string
		output      int
	}{
		0: {
			columnTitle: "A",
			output:      1,
		},
		1: {
			columnTitle: "AB",
			output:      28,
		},
		2: {
			columnTitle: "ZY",
			output:      701,
		},
	}
	for i, scenario := range scenarios {
		output := titleToNumber(scenario.columnTitle)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
