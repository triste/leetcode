package problem78

func subsets(nums []int) [][]int {
	subsets := make([][]int, 1<<len(nums))
	for mask := range subsets {
		var subset []int
		for i := range nums {
			if mask&(1<<i) != 0 {
				subset = append(subset, nums[i])
			}
		}
		subsets[mask] = subset
	}
	return subsets
}
