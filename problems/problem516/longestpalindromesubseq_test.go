package problem516

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestLongestpalindromesubseq(t *testing.T) {
	scenarios := [...]struct {
		s      string
		output int
	}{
		0: {
			s:      "bbbab",
			output: 4,
		},
		1: {
			s:      "cbbd",
			output: 2,
		},
		2: {
			s:      "a",
			output: 1,
		},
		3: {
			s:      "aabaaba",
			output: 6,
		},
	}
	for i, scenario := range scenarios {
		output := longestPalindromeSubseq(scenario.s)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
