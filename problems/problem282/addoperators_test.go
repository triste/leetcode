package problem282

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestAddoperators(t *testing.T) {
	scenarios := [...]struct {
		num    string
		target int
		output []string
	}{
		0: {
			num:    "123",
			target: 6,
			output: []string{
				"1*2*3", "1+2+3",
			},
		},
		1: {
			num:    "232",
			target: 8,
			output: []string{
				"2*3+2", "2+3*2",
			},
		},
		2: {
			num:    "3456237490",
			target: 9191,
			output: nil,
		},
		3: {
			num:    "105",
			target: 5,
			output: []string{
				"1*0+5",
				"10-5",
			},
		},
		4: {
			num:    "00",
			target: 0,
			output: []string{
				"0*0",
				"0+0",
				"0-0",
			},
		},
	}
	for i, scenario := range scenarios {
		output := addOperators(scenario.num, scenario.target)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
