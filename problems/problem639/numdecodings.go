package problem639

func numDecodings(s string) int {
	return numDecodingsIterative(s)
}

func numDecodingsRecursive(s string) int {
	var solve func(i int) int
	solve = func(i int) int {
		if i >= len(s) {
			return 1
		}
		if s[i] == '0' {
			return 0
		}
		decCount := 0
		if s[i] == '*' {
			decCount = 9 * solve(i+1)
			if i+1 < len(s) {
				if s[i+1] == '*' {
					decCount += 15 * solve(i+2)
				} else if s[i+1] <= '6' {
					decCount += 2 * solve(i+2)
				} else {
					decCount += solve(i + 2)
				}
			}
		} else {
			decCount = solve(i + 1)
			if i+1 < len(s) {
				if s[i+1] == '*' {
					if s[i] == '1' {
						decCount += 9 * solve(i+2)
					} else if s[i] == '2' {
						decCount += 6 * solve(i+2)
					}
				} else if (s[i]-'0')*10+s[i+1]-'0' <= 26 {
					decCount += solve(i + 2)
				}
			}
		}
		return decCount % 1_000_000_007
	}
	return solve(0)
}

func numDecodingsIterative(s string) int {
	first, second := 0, 1
	if s[len(s)-1] == '*' {
		first = 9
	} else if s[len(s)-1] != '0' {
		first = 1
	}
	for i := len(s) - 2; i >= 0; i-- {
		tmp := first
		if s[i] == '0' {
			first = 0
		} else if s[i] == '*' {
			first *= 9
			if s[i+1] == '*' {
				first += 15 * second
			} else if s[i+1] <= '6' {
				first += 2 * second
			} else {
				first += second
			}
		} else if s[i+1] == '*' {
			if s[i] == '1' {
				first += 9 * second
			} else if s[i] == '2' {
				first += 6 * second
			}
		} else if (s[i]-'0')*10+s[i+1]-'0' <= 26 {
			first += second
		}
		first %= 1_000_000_007
		second = tmp
	}
	return first
}
