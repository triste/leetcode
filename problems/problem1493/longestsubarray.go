package problem1493

func longestSubarray(nums []int) int {
	l, r := 0, 0
	for r < len(nums) && nums[r] != 0 {
		r++
	}
	if r == len(nums) {
		return r - 1
	}
	longest := 0
	for r < len(nums) {
		tmp := r + 1
		for r++; r < len(nums) && nums[r] != 0; r++ {
		}
		if r-l-1 > longest {
			longest = r - l - 1
		}
		l = tmp
	}
	return longest
}
