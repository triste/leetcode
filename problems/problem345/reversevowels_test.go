package problem345

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestReversevowels(t *testing.T) {
	scenarios := [...]struct {
		s      string
		output string
	}{
		0: {
			s:      "hello",
			output: "holle",
		},
		1: {
			s:      "leetcode",
			output: "leotcede",
		},
		2: {
			s:      " ",
			output: " ",
		},
		3: {
			s:      "aA",
			output: "Aa",
		},
	}
	for i, scenario := range scenarios {
		output := reverseVowels(scenario.s)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
