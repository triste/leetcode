package problem354

import (
	"sort"
)

func maxEnvelopes(envelopes [][]int) (output int) {
	sort.Slice(envelopes, func(i, j int) bool {
		if envelopes[i][0] == envelopes[j][0] {
			return envelopes[i][1] > envelopes[j][1]
		}
		return envelopes[i][0] < envelopes[j][0]
	})
	var lis []int
	for _, e := range envelopes {
		i := sort.SearchInts(lis, e[1])
		if i == len(lis) {
			lis = append(lis, e[1])
		} else {
			lis[i] = e[1]
		}
	}
	return len(lis)
}
