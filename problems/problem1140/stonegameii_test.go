package problem1140

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestStonegameii(t *testing.T) {
	scenarios := [...]struct {
		piles  []int
		output int
	}{
		0: {
			piles:  []int{2, 7, 9, 4, 4},
			output: 10,
		},
		1: {
			piles:  []int{1, 2, 3, 4, 5, 100},
			output: 104,
		},
		2: {
			piles:  []int{1},
			output: 1,
		},
	}
	for i, scenario := range scenarios {
		output := stoneGameII(scenario.piles)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
