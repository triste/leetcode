package problem905

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestSortarraybyparity(t *testing.T) {
	scenarios := [...]struct {
		nums   []int
		output []int
	}{
		0: {
			nums:   []int{3, 1, 2, 4},
			output: []int{2, 4, 3, 1},
		},
		1: {
			nums:   []int{0},
			output: []int{0},
		},
	}
	for i, scenario := range scenarios {
		output := sortArrayByParity(scenario.nums)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
