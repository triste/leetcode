package problem847

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestShortestpathlength(t *testing.T) {
	scenarios := [...]struct {
		graph  [][]int
		output int
	}{
		0: {
			graph: [][]int{
				{1, 2, 3},
				{0},
				{0},
				{0},
			},
			output: 4,
		},
		1: {
			graph: [][]int{
				{1},
				{0, 2, 4},
				{1, 3, 4},
				{2},
				{1, 2},
			},
			output: 4,
		},
		2: {
			graph:  [][]int{{}},
			output: 0,
		},
		3: {
			graph: [][]int{
				{1, 2, 3, 4, 5, 6, 7, 8, 9},
				{0, 2, 3, 4, 5, 6, 7, 8, 9},
				{0, 1, 3, 4, 5, 6, 7, 8, 9},
				{0, 1, 2, 4, 5, 6, 7, 8, 9},
				{0, 1, 2, 3, 5, 6, 7, 8, 9},
				{0, 1, 2, 3, 4, 6, 7, 8, 9},
				{0, 1, 2, 3, 4, 5, 7, 8, 9},
				{0, 1, 2, 3, 4, 5, 6, 8, 9},
				{0, 1, 2, 3, 4, 5, 6, 7, 9, 10},
				{0, 1, 2, 3, 4, 5, 6, 7, 8, 11},
				{8},
				{9},
			},
			output: 11,
		},
	}
	for i, scenario := range scenarios {
		output := shortestPathLength(scenario.graph)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
