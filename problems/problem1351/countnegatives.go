package problem1351

func countNegatives(grid [][]int) (negCount int) {
	m := len(grid)
	n := len(grid[0])
	for r, c := 0, n-1; ; {
		if grid[r][c] < 0 {
			c--
			if c < 0 {
				negCount += n * (m - r)
				break
			}
		} else {
			negCount += n - c - 1
			r++
			if r >= m {
				break
			}
		}
	}
	return
}
