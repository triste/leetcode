package problem133

type Node struct {
	Val       int
	Neighbors []*Node
}

func cloneGraph(node *Node) *Node {
	if node == nil {
		return nil
	}
	var clonedNodes [101]*Node
	var dfs func(node *Node) *Node
	dfs = func(node *Node) *Node {
		if clonedNodes[node.Val] != nil {
			return clonedNodes[node.Val]
		}
		clonedNode := &Node{
			Val:       node.Val,
			Neighbors: make([]*Node, len(node.Neighbors)),
		}
		clonedNodes[node.Val] = clonedNode
		for i, neighbor := range node.Neighbors {
			clonedNode.Neighbors[i] = dfs(neighbor)
		}
		return clonedNode
	}
	return dfs(node)
}
