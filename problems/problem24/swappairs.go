package problem24

import . "gitlab.com/triste/leetcode/pkg/list"

func swapPairs(head *ListNode) *ListNode {
	if head == nil {
		return nil
	}
	root := &ListNode{}
	first := head
	prev := root
	for second := first.Next; second != nil; second = first.Next {
		next := second.Next
		second.Next = first
		prev.Next = second
		prev = first
		if first = next; first == nil {
			break
		}
	}
	prev.Next = first
	return root.Next
}
