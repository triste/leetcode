package problem686

import (
	"strings"
)

func repeatedStringMatch(a string, b string) int {
	repeatCount := len(b) / len(a)
	if len(b)%len(a) != 0 {
		repeatCount++
	}
	a = strings.Repeat(a, repeatCount)
	if strings.Contains(a, b) {
		return repeatCount
	}
	if strings.Contains(a+a, b) {
		return repeatCount + 1
	}
	return -1
}
