package problem2542

import (
	"sort"
)

type PriorityQueue struct {
	heap []int
	sum  int64
}

func NewPriorityQueue(heap []int) PriorityQueue {
	var sum int64
	for _, num := range heap {
		sum += int64(num)
	}
	return PriorityQueue{heap, sum}
}

func (pq *PriorityQueue) Replace(num int) {
	pq.sum = pq.sum - int64(pq.heap[0]) + int64(num)
	pq.heap[0] = num
	for i := 0; ; {
		l, r := i*2+1, i*2+2
		j := i
		if l < len(pq.heap) && pq.heap[l] < pq.heap[j] {
			j = l
		}
		if r < len(pq.heap) && pq.heap[r] < pq.heap[j] {
			j = r
		}
		if j == i {
			break
		}
		pq.heap[i], pq.heap[j] = pq.heap[j], pq.heap[i]
		i = j
	}
}

type DoubleSlice struct {
	nums1 []int
	nums2 []int
}

func (ds DoubleSlice) Len() int {
	return len(ds.nums1)
}

func (ds DoubleSlice) Less(i, j int) bool {
	return ds.nums2[i] < ds.nums2[j]
}

func (ds DoubleSlice) Swap(i, j int) {
	ds.nums2[i], ds.nums2[j] = ds.nums2[j], ds.nums2[i]
	ds.nums1[i], ds.nums1[j] = ds.nums1[j], ds.nums1[i]
}

func maxScore(nums1 []int, nums2 []int, k int) int64 {
	sort.Sort(sort.Reverse(DoubleSlice{nums1, nums2}))
	heap := make([]int, k)
	copy(heap, nums1[:k-1])
	sort.Ints(heap)
	pq := NewPriorityQueue(heap)
	var maxProd int64
	for i := k - 1; i < len(nums2); i++ {
		pq.Replace(nums1[i])
		if prod := pq.sum * int64(nums2[i]); prod > maxProd {
			maxProd = prod
		}
	}
	return maxProd
}
