package problem1376

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestNumofminutes(t *testing.T) {
	scenarios := [...]struct {
		n          int
		headID     int
		manager    []int
		informTime []int
		output     int
	}{
		0: {
			n:          1,
			headID:     0,
			manager:    []int{-1},
			informTime: []int{0},
			output:     0,
		},
		1: {
			n:          6,
			headID:     2,
			manager:    []int{2, 2, -1, 2, 2, 2},
			informTime: []int{0, 0, 1, 0, 0, 0},
			output:     1,
		},
	}
	for i, scenario := range scenarios {
		output := numOfMinutes(scenario.n, scenario.headID, scenario.manager, scenario.informTime)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
