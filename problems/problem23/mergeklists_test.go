package problem23

import (
	"testing"

	"github.com/google/go-cmp/cmp"
	. "gitlab.com/triste/leetcode/pkg/list"
)

func TestMergeklists(t *testing.T) {
	scenarios := [...]struct {
		lists  []*ListNode
		output *ListNode
	}{
		0: {
			lists: []*ListNode{
				NewListFromSlice(1, 4, 5),
				NewListFromSlice(1, 3, 4),
				NewListFromSlice(2, 6),
			},
			output: NewListFromSlice(1, 1, 2, 3, 4, 4, 5, 6),
		},
		1: {
			lists:  []*ListNode{nil},
			output: nil,
		},
		2: {
			lists:  nil,
			output: nil,
		},
	}
	for i, scenario := range scenarios {
		output := mergeKLists(scenario.lists)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
