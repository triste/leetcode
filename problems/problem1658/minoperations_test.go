package problem1658

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestMinoperations(t *testing.T) {
	scenarios := [...]struct {
		nums   []int
		x      int
		output int
	}{
		0: {
			nums:   []int{1, 1, 4, 2, 3},
			x:      5,
			output: 2,
		},
		1: {
			nums:   []int{5, 6, 7, 8, 9},
			x:      4,
			output: -1,
		},
		2: {
			nums:   []int{3, 2, 20, 1, 1, 3},
			x:      10,
			output: 5,
		},
		3: {
			nums:   []int{8828, 9581, 49, 9818, 9974, 9869, 9991, 10000, 10000, 10000, 9999, 9993, 9904, 8819, 1231, 6309},
			x:      134365,
			output: 16,
		},
		4: {
			nums:   []int{6016, 5483, 541, 4325, 8149, 3515, 7865, 2209, 9623, 9763, 4052, 6540, 2123, 2074, 765, 7520, 4941, 5290, 5868, 6150, 6006, 6077, 2856, 7826, 9119},
			x:      31841,
			output: 6,
		},
		5: {
			nums:   []int{5, 2, 3, 1, 1},
			x:      5,
			output: 1,
		},
	}
	for i, scenario := range scenarios {
		output := minOperations(scenario.nums, scenario.x)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
