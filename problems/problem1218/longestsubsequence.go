package problem1218

func longestSubsequence(nums []int, diff int) int {
	counts := make(map[int]int)
	max := 0
	for _, num := range nums {
		count := counts[num] + 1
		counts[num+diff] = count
		if count > max {
			max = count
		}
	}
	return max
}
