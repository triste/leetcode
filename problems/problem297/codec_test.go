package problem297

import (
	"testing"

	"github.com/google/go-cmp/cmp"
	. "gitlab.com/triste/leetcode/pkg/tree"
)

func TestCodec(t *testing.T) {
	tree := NewTree(1, 2, 3, nil, nil, 4, 5)
	ser := Constructor()
	deser := Constructor()
	data := ser.serialize(tree)
	ans := deser.deserialize(data)
	if diff := cmp.Diff(tree, ans); diff != "" {
		t.Errorf("(-want +got):\n%s", diff)
	}

}
