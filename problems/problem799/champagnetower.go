package problem799

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func champagneTower(poured int, query_row int, query_glass int) float64 {
	dp := make([][2]float64, query_glass+1)
	if poured > 1 {
		dp[0] = [2]float64{1, float64(poured-1) / 2}
	} else {
		dp[0][0] = float64(poured)
	}
	for r := 1; r <= query_row; r++ {
		for prev, c := 0.0, 0; c <= min(r, query_glass); c++ {
			tmp := dp[c][1]
			if dp[c][0] = dp[c][1] + prev; dp[c][0] > 1 {
				dp[c][1] = (dp[c][0] - 1) / 2
				dp[c][0] = 1
			} else {
				dp[c][1] = 0
			}
			prev = tmp
		}
	}
	return dp[query_glass][0]
}
