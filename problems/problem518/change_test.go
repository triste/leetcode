package problem518

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestChange(t *testing.T) {
	scenarios := [...]struct {
		amount int
		coins  []int
		output int
	}{
		0: {
			amount: 5,
			coins:  []int{1, 2, 5},
			output: 4,
		},
		1: {
			amount: 3,
			coins:  []int{2},
			output: 0,
		},
		2: {
			amount: 10,
			coins:  []int{10},
			output: 1,
		},
		3: {
			amount: 500,
			coins:  []int{3, 5, 7, 8, 9, 10, 11},
			output: 35502874,
		},
		4: {
			amount: 100,
			coins:  []int{99, 1},
			output: 2,
		},
	}
	for i, scenario := range scenarios {
		output := change(scenario.amount, scenario.coins)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
