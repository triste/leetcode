package problem128

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestLongestconsecutive(t *testing.T) {
	scenarios := [...]struct {
		nums   []int
		output int
	}{
		0: {
			nums:   []int{100, 4, 200, 1, 3, 2},
			output: 4,
		},
		1: {
			nums:   []int{0, 3, 7, 2, 5, 8, 4, 6, 0, 1},
			output: 9,
		},
		2: {
			nums:   []int{-6, 8, -5, 7, -9, -1, -7, -6, -9, -7, 5, 7, -1, -8, -8, -2, 0},
			output: 5,
		},
	}
	for i, scenario := range scenarios {
		output := longestConsecutive(scenario.nums)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
