package problem474

import (
	"log"
)

func findMaxForm(strs []string, m int, n int) int {
	counts := make([][2]int, len(strs))
	for i, str := range strs {
		for _, ch := range str {
			counts[i][ch-'0']++
		}
	}
	log.Println(counts)
	dp := make([][]int, m+1)
	for i := range dp {
		dp[i] = make([]int, n+1)
	}
	dp[0][0] = 0
	for i := 0; i < len(dp); i++ {
		for j := 0; j < len(dp[i]); j++ {
			dp[i][j] = 0
			for _, count := range counts {
				r := i - count[0]
				c := j - count[1]
				if r >= 0 && c >= 0 {
					dp[i][j] = max(dp[i][j], 1+dp[r][c])
				}
			}
		}
	}
	for i := range dp {
		log.Println(dp[i])
	}
	return dp[m][n]

}

/*
  | 0 | 1 | 2 | 3 |
0 | . | . | . | . |
1 | . | . | . |   |
2 | . | . |   |   |
3 | . |   |   |   |
4 |   |   |   |   |
5 |   |   |   |   |

*/
