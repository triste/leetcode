package problem71

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestSimplifypath(t *testing.T) {
	scenarios := [...]struct {
		path   string
		output string
	}{
		0: {
			path:   "/home/",
			output: "/home",
		},
		1: {
			path:   "/../",
			output: "/",
		},
		2: {
			path:   "/home//foo/",
			output: "/home/foo",
		},
	}
	for i, scenario := range scenarios {
		output := simplifyPath(scenario.path)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
