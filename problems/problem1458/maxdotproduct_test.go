package problem1458

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestMaxdotproduct(t *testing.T) {
	scenarios := [...]struct {
		nums1  []int
		nums2  []int
		output int
	}{
		0: {
			nums1:  []int{2, 1, -2, 5},
			nums2:  []int{3, 0, -6},
			output: 18,
		},
		1: {
			nums1:  []int{3, -2},
			nums2:  []int{2, -6, 7},
			output: 21,
		},
		2: {
			nums1:  []int{-1, -1},
			nums2:  []int{1, 1},
			output: -1,
		},
		3: {
			nums1:  []int{-5, -1, -2},
			nums2:  []int{3, 3, 5, 5},
			output: -3,
		},
	}
	for i, scenario := range scenarios {
		output := maxDotProduct(scenario.nums1, scenario.nums2)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
