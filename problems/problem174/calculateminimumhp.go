package problem174

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func calculateMinimumHP(dungeon [][]int) int {
	m := len(dungeon)
	n := len(dungeon[0])
	dungeon[m-1][n-1] = max(1, 1-dungeon[m-1][n-1])
	for r := m - 2; r >= 0; r-- {
		dungeon[r][n-1] = max(1, dungeon[r+1][n-1]-dungeon[r][n-1])
	}
	for c := n - 2; c >= 0; c-- {
		dungeon[m-1][c] = max(1, dungeon[m-1][c+1]-dungeon[m-1][c])
	}
	for r := m - 2; r >= 0; r-- {
		for c := n - 2; c >= 0; c-- {
			minReqHealth := min(dungeon[r+1][c], dungeon[r][c+1])
			dungeon[r][c] = max(1, minReqHealth-dungeon[r][c])
		}
	}
	return dungeon[0][0]
}
