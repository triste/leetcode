package problem456

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestFind132pattern(t *testing.T) {
	scenarios := [...]struct {
		nums   []int
		output bool
	}{
		0: {
			nums:   []int{1, 2, 3, 4},
			output: false,
		},
		1: {
			nums:   []int{3, 1, 4, 2},
			output: true,
		},
		2: {
			nums:   []int{-1, 3, 2, 0},
			output: true,
		},
		3: {
			nums:   []int{3, 5, 0, 3, 4},
			output: true,
		},
		4: {
			nums:   []int{1, 4, 0, -1, -2, -3, -1, -2},
			output: true,
		},
	}
	for i, scenario := range scenarios {
		output := find132pattern(scenario.nums)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
