package problem125

func toLower(letter byte) (byte, bool) {
	if letter >= 'a' && letter <= 'z' {
		return letter, true
	}
	if letter >= 'A' && letter <= 'Z' {
		return letter + ('a' - 'A'), true
	}
	return letter, false
}

func isDigit(letter byte) bool {
	return letter >= '0' && letter <= '9'
}

func mapLetter(letter byte) byte {
	if letter >= 'a' && letter <= 'z' {
		return letter
	}
	if letter >= 'A' && letter <= 'Z' {
		return letter + ('a' - 'A')
	}
	if letter >= '0' && letter <= '9' {
		return letter
	}
	return 0
}

func isPalindrome(s string) bool {
	for l, r := 0, len(s)-1; l < r; l, r = l+1, r-1 {
		var leftLetter byte
		for ; l < r; l++ {
			if letter, ok := toLower(s[l]); ok || isDigit(s[l]) {
				leftLetter = letter
				break
			}
		}
		var rightLetter byte
		for ; l < r; r-- {
			if letter, ok := toLower(s[r]); ok || isDigit(s[r]) {
				rightLetter = letter
				break
			}
		}
		if l != r && leftLetter != rightLetter {
			return false
		}
	}
	return true
}
