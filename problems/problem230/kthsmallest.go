package problem230

import (
	. "gitlab.com/triste/leetcode/pkg/tree"
)

func kthSmallest(root *TreeNode, k int) (output int) {
	count := 0
	var kthSmallestHelper func(*TreeNode)
	kthSmallestHelper = func(root *TreeNode) {
		if root == nil {
			return
		}
		kthSmallestHelper(root.Left)
		count++
		if count == k {
			output = root.Val
			return
		} else if count < k {
			kthSmallestHelper(root.Right)
		}
	}
	kthSmallestHelper(root)
	return
}
