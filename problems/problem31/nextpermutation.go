package problem31

func nextPermutation(nums []int) {
	var i int
	for i = len(nums) - 2; i >= 0; i-- {
		if nums[i] < nums[i+1] {
			j := i + 1
			for j < len(nums) && nums[j] > nums[i] {
				j++
			}
			nums[i], nums[j-1] = nums[j-1], nums[i]
			break
		}
	}
	for l, r := i+1, len(nums)-1; l < r; l, r = l+1, r-1 {
		nums[l], nums[r] = nums[r], nums[l]
	}
}

/*
	1, 2, 3, 4, 5
	1, 2, 3, 5, 4
	1, 2, 4, 3, 5
	1, 2, 4, 5, 3
	1, 2, 5, 3, 4
	1, 2, 5, 4, 3

	1, 3, 2, 4, 5
*/
