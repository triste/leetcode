package problem17

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestLettercombinations(t *testing.T) {
	scenarios := [...]struct {
		digits string
		output []string
	}{
		0: {"23", []string{"ad","ae","af","bd","be","bf","cd","ce","cf"}},
		1: {"", nil},
		2: {"2", []string{"a","b","c"}},
	}
	for i, scenario := range scenarios {
		output := letterCombinations(scenario.digits)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
