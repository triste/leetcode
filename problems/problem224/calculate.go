package problem224

const (
	TokenOpenParenthesis int = iota
	TokenCloseParenthesis
	TokenMinus
	TokenPlus
	TokenNumber
)

func lex(s string, tokens chan<- int) {
	for i := 0; i < len(s); i++ {
		switch s[i] {
		case ' ':
			continue
		case '(':
			tokens <- TokenOpenParenthesis
		case ')':
			tokens <- TokenCloseParenthesis
		case '-':
			tokens <- TokenMinus
		default:
			num := int(s[i])
			for ; i+1 < len(s) && s[i+1] >= '0' && s[i+1] <= '9'; i++ {
				num = num*10 + int(s[i+1])
			}
			tokens <- TokenNumber
			tokens <- num
		}
	}
	close(tokens)
}

type Eval struct {
	val   int
	isNeg bool
}

func calculate(s string) int {
	evals := make([]Eval, 1)
	isNeg := false
	for i := 0; i < len(s); i++ {
		switch s[i] {
		case '(':
			evals = append(evals, Eval{0, isNeg})
			isNeg = false
		case ')':
			eval := evals[len(evals)-1]
			evals = evals[:len(evals)-1]
			if eval.isNeg {
				evals[len(evals)-1].val -= eval.val
			} else {
				evals[len(evals)-1].val += eval.val
			}
		case '-':
			isNeg = true
		case ' ', '+':
			continue
		default:
			num := int(s[i] - '0')
			for ; i+1 < len(s) && s[i+1] >= '0' && s[i+1] <= '9'; i++ {
				num = num*10 + int(s[i+1]-'0')
			}
			if isNeg {
				evals[len(evals)-1].val -= num
			} else {
				evals[len(evals)-1].val += num
			}
			isNeg = false
		}
	}
	return evals[0].val
}

/*
	S -> (S)
	S -> N + S
	S -> N - S
	S -> N
	S -> -S

	S -> S + S | S - S | (S) | E
	E -> num | -num
*/
