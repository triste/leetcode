package problem222

import (
	"testing"

	"github.com/google/go-cmp/cmp"
	. "gitlab.com/triste/leetcode/pkg/tree"
)

func TestCountnodes(t *testing.T) {
	scenarios := [...]struct {
		root   *TreeNode
		output int
	}{
		0: {
			root:   NewTree(1, 2, 3, 4, 5, 6),
			output: 6,
		},
		1: {
			root:   nil,
			output: 0,
		},
		2: {
			root:   NewTree(1),
			output: 1,
		},
	}
	for i, scenario := range scenarios {
		output := countNodes(scenario.root)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
