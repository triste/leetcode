package problem1269

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestNumways(t *testing.T) {
	scenarios := [...]struct {
		steps  int
		arrLen int
		output int
	}{
		0: {
			steps:  3,
			arrLen: 2,
			output: 4,
		},
		1: {
			steps:  2,
			arrLen: 4,
			output: 2,
		},
		2: {
			steps:  4,
			arrLen: 2,
			output: 8,
		},
		3: {
			steps:  27,
			arrLen: 7,
			output: 127784505,
		},
	}
	for i, scenario := range scenarios {
		output := numWays(scenario.steps, scenario.arrLen)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
