package problem341

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestNestedList(t *testing.T) {
	list := []*NestedInteger{
		{
			val: 0,
			list: []*NestedInteger{
				{
					val:   1,
					list:  nil,
					isInt: true,
				},
				{
					val:   1,
					list:  nil,
					isInt: true,
				},
			},
			isInt: false,
		},
		{
			val:   2,
			list:  nil,
			isInt: true,
		},
		{
			val: 0,
			list: []*NestedInteger{
				{
					val:   1,
					list:  nil,
					isInt: true,
				},
				{
					val:   1,
					list:  nil,
					isInt: true,
				},
			},
			isInt: false,
		},
	}
	iter := Constructor(list)
	want := []int{1, 1, 2, 1, 1}
	got := []int{}
	for iter.HasNext() {
		got = append(got, iter.Next())
	}
	if diff := cmp.Diff(want, got); diff != "" {
		t.Errorf("invalid index (-want +got):\n%s", diff)
	}
}
