package problem1584

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestMincostconnectpoints(t *testing.T) {
	scenarios := [...]struct {
		points [][]int
		output int
	}{
		0: {
			points: [][]int{
				{0, 0}, {2, 2}, {3, 10}, {5, 2}, {7, 0},
			},
			output: 20,
		},
		1: {
			points: [][]int{
				{3, 12}, {-2, 5}, {-4, 1},
			},
			output: 18,
		},
		2: {
			points: [][]int{
				{0, 0}, {1, 1}, {1, 0}, {-1, 1},
			},
			output: 4,
		},
	}
	for i, scenario := range scenarios {
		output := minCostConnectPoints(scenario.points)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
