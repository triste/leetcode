package problem332

import (
	"sort"
)

type Edge struct {
	vertexId int
	next     *Edge
}

type Vertex struct {
	name string
	edge Edge
}

func findItinerary(tickets [][]string) []string {
	sort.Slice(tickets, func(i, j int) bool {
		from1, to1 := tickets[i][0], tickets[i][1]
		from2, to2 := tickets[j][0], tickets[j][1]
		if from1 == from2 {
			return to1 > to2
		}
		return from1 > from2
	})
	airportIds := make(map[string]int, len(tickets)+1)
	graph := make([]Vertex, 0, len(tickets)+1)
	for _, ticket := range tickets {
		for _, airport := range ticket {
			if _, ok := airportIds[airport]; !ok {
				airportIds[airport] = len(graph)
				graph = append(graph, Vertex{
					name: airport,
					edge: Edge{
						vertexId: -1,
						next:     nil,
					},
				})
			}
		}
		from, to := airportIds[ticket[0]], airportIds[ticket[1]]
		graph[from].edge.next = &Edge{to, graph[from].edge.next}
	}
	var dfs func(int, []string) []string
	dfs = func(from int, path []string) []string {
		path = append(path, graph[from].name)
		if len(path) == len(tickets)+1 {
			return path
		}
		for prevEdge := &graph[from].edge; prevEdge.next != nil; prevEdge = prevEdge.next {
			curEdge := prevEdge.next
			if curEdge.vertexId == prevEdge.vertexId {
				continue
			}
			prevEdge.next = curEdge.next
			itinerary := dfs(curEdge.vertexId, path)
			if itinerary != nil {
				return itinerary
			}
			prevEdge.next = curEdge
		}
		return nil
	}
	path := make([]string, 0, len(tickets)+1)
	return dfs(airportIds["JFK"], path)
}
