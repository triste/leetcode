package problem2141

import (
	"sort"
)

func maxRunTime(n int, batteries []int) (runtime int64) {
	sort.Ints(batteries)
	largestNBats := batteries[len(batteries)-n:]
	var restPowerSum int
	for _, power := range batteries[:len(batteries)-n] {
		restPowerSum += power
	}
	runtime = int64(largestNBats[0])
	for i := 1; i < len(largestNBats); i++ {
		diff := largestNBats[i] - largestNBats[i-1]
		if diff*i > restPowerSum {
			return int64(largestNBats[i-1] + restPowerSum/i)
		}
		restPowerSum -= diff * i
		runtime = int64(largestNBats[i])
	}
	return runtime + int64(restPowerSum/n)
}

/*
	case n = 1, bats = {1}
		return bats[0]
	case n = 1, bats = {1,2}
		return sum(bats...)
	case n = 2, bats = {1,2}
		return 1
	case n = 2, bats = {1,2,3,4,5}
		{5,4,3,2,1}
		return 4 + 1 + 2 = 7
	case n = 4, bats = {11, 10, 8, 5, 4, 3, 1}

	5, 8, 10, 11

*/
