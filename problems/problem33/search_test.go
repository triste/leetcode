package problem33

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestSearch(t *testing.T) {
	scenarios := [...]struct {
		nums   []int
		target int
		output int
	}{
		0: {[]int{4, 5, 6, 7, 0, 1, 2}, 0, 4},
		1: {[]int{4, 5, 6, 7, 0, 1, 2}, 3, -1},
		2: {[]int{1}, 0, -1},
		3: {[]int{1}, 1, 0},
		4: {[]int{1, 3}, 3, 1},
		5: {[]int{1, 3, 5}, 5, 2},
		6: {[]int{3, 1}, 1, 1},
		7: {[]int{5, 1, 3}, 5, 0},
		8: {[]int{5, 1, 3}, 2, -1},
	}
	for i, scenario := range scenarios[8:] {
		output := search(scenario.nums, scenario.target)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
