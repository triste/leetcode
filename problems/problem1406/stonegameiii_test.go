package problem1406

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestStonegameiii(t *testing.T) {
	scenarios := [...]struct {
		stoneValue []int
		output     string
	}{
		0: {
			stoneValue: []int{1, 2, 3, 7},
			output:     "Bob",
		},
		1: {
			stoneValue: []int{1, 2, 3, -9},
			output:     "Alice",
		},
		2: {
			stoneValue: []int{1, 2, 3, 6},
			output:     "Tie",
		},
	}
	for i, scenario := range scenarios {
		output := stoneGameIII(scenario.stoneValue)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
