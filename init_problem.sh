#!/bin/sh

number=$1
name=$2
input=$3
output=$4

if [[ $# -lt 3 ]]
then
	echo "Illegal number of parameters"
	exit 2
fi

problem_dir="problems/problem${number}"
if [[ -d ${problem_dir} ]]
then
	echo "Problem ${number} already initialized"
	exit 0
fi
mkdir ${problem_dir}

name_lower=$(echo ${name} | tr '[:upper:]' '[:lower:]')
name_upper=$(echo ${name} | awk '{$1=toupper(substr($1,0,1))substr($1,2)}1')
first_field_name=$(echo ${input} | awk '{print $1}')
first_field_type=$(echo ${input} | awk '{print $2}' | tr -d ',')
scenario_fields="${input}, output"
if [[ $# -eq 4 ]]
then
	scenario_fields="${scenario_fields} ${output}"
	function_output="output := "
	compare_with="output"
	function_return="(output ${output}) "
else
	scenario_fields="${scenario_fields} ${first_field_type}"
	compare_with="scenario.${first_field_name}"
fi

cat > ${problem_dir}/${name_lower}.go << EOF
package problem${number}

func ${name}($input) ${function_return}{
	return
}
EOF

fields=$(echo "${scenario_fields}" | sed 's/, */\n/g')
function_args=$(echo ${input} | sed 's/, */\n' | awk '{print "scenario."$1 }' | paste -d, - - | sed 's/,/, /g')

cat > ${problem_dir}/${name_lower}_test.go << EOF
package problem${number}

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func Test${name_upper}(t *testing.T) {
	scenarios := [...]struct {
$(echo ${fields} | column --table -o ' ' | sed 's/^/\t\t/g')
	}{
		0: {},
		1: {},
	}
	for i, scenario := range scenarios {
		${function_output}${name}(${function_args})
		if diff := cmp.Diff(scenario.output, ${compare_with}); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
EOF
