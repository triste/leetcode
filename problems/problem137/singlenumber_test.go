package problem137

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestSinglenumber(t *testing.T) {
	scenarios := [...]struct {
		nums   []int
		output int
	}{
		0: {
			nums:   []int{2, 2, 3, 2},
			output: 3,
		},
		1: {
			nums:   []int{0, 1, 0, 1, 0, 1, 99},
			output: 99,
		},
		2: {
			nums:   []int{2, 1, 3, 1, 2, 2, 1},
			output: 3,
		},
		3: {
			nums:   []int{30000, 500, 100, 30000, 100, 30000, 100},
			output: 500,
		},
	}
	for i, scenario := range scenarios {
		output := singleNumber(scenario.nums)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
