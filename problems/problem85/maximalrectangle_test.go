package problem85

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestMaximalrectangle(t *testing.T) {
	scenarios := [...]struct {
		matrix [][]byte
		output int
	}{
		0: {
			matrix: [][]byte{
				{'1', '0', '1', '0', '0'},
				{'1', '0', '1', '1', '1'},
				{'1', '1', '1', '1', '1'},
				{'1', '0', '0', '1', '0'},
			},
			output: 6,
		},
		1: {
			matrix: [][]byte{
				{'0'},
			},
			output: 0,
		},
		2: {
			matrix: [][]byte{
				{'1'},
			},
			output: 1,
		},
		3: {
			matrix: [][]byte{
				{'1', '1'},
			},
			output: 2,
		},
	}
	for i, scenario := range scenarios {
		output := maximalRectangle(scenario.matrix)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
