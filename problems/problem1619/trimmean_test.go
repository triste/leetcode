package problem1619

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestTrimmean(t *testing.T) {
	scenarios := [...]struct {
		arr    []int
		output float64
	}{
		0: {
			arr:    []int{1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3},
			output: 2.0,
		},
		1: {
			arr:    []int{6, 2, 7, 5, 1, 2, 0, 3, 10, 2, 5, 0, 5, 5, 0, 8, 7, 6, 8, 0},
			output: 4.0,
		},
		2: {
			arr:    []int{6, 0, 7, 0, 7, 5, 7, 8, 3, 4, 0, 7, 8, 1, 6, 8, 1, 1, 2, 4, 8, 1, 9, 5, 4, 3, 8, 5, 10, 8, 6, 6, 1, 0, 6, 10, 8, 2, 3, 4},
			output: 4.77778,
		},
	}
	for i, scenario := range scenarios {
		output := trimMean(scenario.arr)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
