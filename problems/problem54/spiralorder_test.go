package problem54

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestSpiralorder(t *testing.T) {
	scenarios := [...]struct {
		matrix [][]int
		output []int
	}{
		0: {
			matrix: [][]int{
				{1, 2, 3},
				{4, 5, 6},
				{7, 8, 9},
			},
			output: []int{1, 2, 3, 6, 9, 8, 7, 4, 5},
		},
		1: {
			matrix: [][]int{
				{1, 2, 3, 4},
				{5, 6, 7, 8},
				{9, 10, 11, 12},
			},
			output: []int{1, 2, 3, 4, 8, 12, 11, 10, 9, 5, 6, 7},
		},
		2: {
			matrix: [][]int{
				{1},
			},
			output: []int{1},
		},
		3: {
			matrix: [][]int{
				{1, 2, 3, 4, 5},
				{6, 7, 8, 9, 10},
				{11, 12, 13, 14, 15},
				{16, 17, 18, 19, 20},
				{21, 22, 23, 24, 25},
			},
			output: []int{1, 2, 3, 4, 5, 10, 15, 20, 25, 24, 23, 22, 21, 16, 11, 6, 7, 8, 9, 14, 19, 18, 17, 12, 13},
		},
	}
	for i, scenario := range scenarios {
		output := spiralOrder(scenario.matrix)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
