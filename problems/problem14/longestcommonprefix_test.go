package problem14

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestLongestcommonprefix(t *testing.T) {
	scenarios := [...]struct {
		strs   []string
		output string
	}{
		0: {[]string{"flower", "flow", "flight"}, "fl"},
		1: {[]string{"dog","racecar","car"}, ""},
		2: {[]string{"dog"}, "dog"},
		3: {[]string{""}, ""},
	}
	for i, scenario := range scenarios[1:2] {
		output := longestCommonPrefix(scenario.strs)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
