package problem96

func numTrees(n int) int {
	dp := make([]int, n+1)
	dp[1] = 1
	for i := 2; i < len(dp); i++ {
		dp[i] = dp[i-1] * 2
		for j := 1; j < i-1; j++ {
			dp[i] += dp[j] * dp[i-1-j]
		}
	}
	return dp[n]
}

/*
	case 1:
		1
	case 2:
		1       1
	  /           \
	case(1)        case(1)
	case 3:

*/
