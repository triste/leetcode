package problem179

import (
	"sort"
	"strconv"
	"strings"
)

func largestNumber(nums []int) string {
	numStrings := make([]string, len(nums))
	for i, num := range nums {
		numStrings[i] = strconv.Itoa(num)
	}
	sort.Slice(numStrings, func(i, j int) bool {
		return numStrings[i]+numStrings[j] > numStrings[j]+numStrings[i]
	})
	if numStrings[0] == "0" {
		return numStrings[0]
	}
	return strings.Join(numStrings, "")
}
