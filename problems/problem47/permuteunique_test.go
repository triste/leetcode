package problem47

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestPermuteunique(t *testing.T) {
	scenarios := [...]struct {
		nums   []int
		output [][]int
	}{
		0: {
			nums: []int{1, 1, 2},
			output: [][]int{
				{1, 1, 2},
				{1, 2, 1},
				{2, 1, 1},
			},
		},
		1: {
			nums: []int{1, 2, 3},
			output: [][]int{
				{1, 2, 3},
				{1, 3, 2},
				{2, 1, 3},
				{2, 3, 1},
				{3, 1, 2},
				{3, 2, 1},
			},
		},
	}
	for i, scenario := range scenarios {
		output := permuteUnique(scenario.nums)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
