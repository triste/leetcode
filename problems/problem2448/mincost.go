package problem2448

import (
	"sort"
)

type byNums struct {
	nums  []int
	costs []int
}

func (n byNums) Len() int {
	return len(n.nums)
}

func (n byNums) Less(i, j int) bool {
	return n.nums[i] < n.nums[j]
}

func (n byNums) Swap(i, j int) {
	n.nums[i], n.nums[j] = n.nums[j], n.nums[i]
	n.costs[i], n.costs[j] = n.costs[j], n.costs[i]
}

func minCost(nums []int, costs []int) (minCost int64) {
	n := len(nums)
	sort.Sort(byNums{nums, costs})
	sum := costs[0]
	for i := 1; i < n; i++ {
		minCost += int64((nums[i] - nums[0]) * costs[i])
		sum += costs[i]
	}
	for presum, cost, i := costs[0], minCost, 1; i < n; i++ {
		cost -= int64((nums[i] - nums[i-1]) * (sum - 2*presum))
		if cost < minCost {
			minCost = cost
		}
		presum += costs[i]
	}
	return minCost
}

/*
	1, 3, 5, 2
	2, 3, 1, 14

	1,  2, 3, 5
	2, 14, 3, 1

	fix: 1
		dec 2 x1 = 14 * 1 = 14
		dec 3 x2 = 3 * 2 = 6
		dex 5 x4 = 1 * 4 = 4
		sum := 24
	fix: 2
		inc 1 x1 = 2 * 1 = 2
		dec 3 x1 = 3 * 1 = 3
		dec 5 x3 = 1 * 3 = 3
		sum := 8
	fix: 3
		inc 1 x2 = 2 * 2 = 4
		inc 2 x1 = 14 * 1 = 14
		dec 5 x2 = 1 * 2 = 2
		sum := 20
	fix: 5
		inc 1 x4 = 2 * 4 = 8
		inc 2 x3 = 14 * 3 = 42
		inc 3 x2 = 3 * 2 = 6
		sum := 56

	prefix sum := 2, 16, 19, 20
*/
