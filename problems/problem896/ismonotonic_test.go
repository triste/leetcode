package problem896

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestIsmonotonic(t *testing.T) {
	scenarios := [...]struct {
		nums   []int
		output bool
	}{
		0: {
			nums:   []int{1, 2, 2, 3},
			output: true,
		},
		1: {
			nums:   []int{6, 5, 4, 4},
			output: true,
		},
		2: {
			nums:   []int{1, 3, 2},
			output: false,
		},
	}
	for i, scenario := range scenarios {
		output := isMonotonic(scenario.nums)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
