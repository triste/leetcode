package problem106

import (
	. "gitlab.com/triste/leetcode/pkg/tree"
)

func buildTree(inorder []int, postorder []int) *TreeNode {
	if len(inorder) == 0 || len(postorder) == 0 {
		return nil
	}
	root := &TreeNode{Val: postorder[len(postorder)-1]}
	i := 0
	for i < len(inorder) && inorder[i] != root.Val {
		i++
	}
	root.Left = buildTree(inorder[:i], postorder[:i])
	root.Right = buildTree(inorder[i+1:], postorder[i:len(postorder)-1])
	return root
}

/*
	inorder = 9,3,15,20,7
	postorder = 9,15,7,20,3

	inorder(node):
		visit(left)
		print(node)
		visit(right

	postorder(node):
		visit(left)
		visit(right)
		print(node)

	rev_inorder(node):
		visit(right)
		print(node)
		visit(left)

	rev_postorder(node):
		visit(right)
		visit(left)
		print(node)
*/

/*
           3
		 /   \
		9     20
	     \    / \
	      5  15  7
		 / \
		1   2

	inorder: 9,5,3,15,20,7
	postorder: 5,9,15,7,20,3

	inorder: 9,1,5,2,3,15,20,7
	postorder: 1,2,5,9,15,7,20,3
*/
