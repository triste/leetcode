package problem863

import (
	"testing"

	"github.com/google/go-cmp/cmp"
	. "gitlab.com/triste/leetcode/pkg/tree"
)

func TestDistancek(t *testing.T) {
	scenarios := [...]struct {
		root   *TreeNode
		target *TreeNode
		k      int
		output []int
	}{
		0: {
			root:   NewTree(3, 5, 1, 6, 2, 0, 8, nil, nil, 7, 4),
			target: NewTree(5),
			k:      2,
			output: []int{7, 4, 1},
		},
		1: {
			root:   NewTree(1),
			target: NewTree(1),
			k:      3,
			output: nil,
		},
		2: {
			root:   NewTree(0, 1, nil, 3, 2),
			target: NewTree(2),
			k:      1,
			output: []int{1},
		},
	}
	for i, scenario := range scenarios {
		output := distanceK(scenario.root, scenario.target, scenario.k)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
