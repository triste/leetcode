package problem1914

func rotateGrid(grid [][]int, k int) [][]int {
	m := len(grid)
	n := len(grid[0])
	var layerCount int
	if m < n {
		layerCount = m / 2
	} else {
		layerCount = n / 2
	}
	tmp := make([]int, 0, 2*(m+n-2))
	for i := 0; i < layerCount; i++ {
		for j := i; j < n-i; j++ {
			tmp = append(tmp, grid[i][j])
		}
		for j := i + 1; j < m-i; j++ {
			tmp = append(tmp, grid[j][n-i-1])
		}
		for j := n - i - 2; j >= i; j-- {
			tmp = append(tmp, grid[m-i-1][j])
		}
		for j := m - i - 2; j > i; j-- {
			tmp = append(tmp, grid[j][i])
		}
		k := k % len(tmp)
		for j := i; j < n-i; j++ {
			grid[i][j] = tmp[k%len(tmp)]
			k++
		}
		for j := i + 1; j < m-i; j++ {
			grid[j][n-i-1] = tmp[k%len(tmp)]
			k++
		}
		for j := n - i - 2; j >= i; j-- {
			grid[m-i-1][j] = tmp[k%len(tmp)]
			k++
		}
		for j := m - i - 2; j > i; j-- {
			grid[j][i] = tmp[k%len(tmp)]
			k++
		}
		tmp = tmp[:0]
	}
	return grid
}
