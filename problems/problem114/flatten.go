package problem114

import (
	. "gitlab.com/triste/leetcode/pkg/tree"
)

func flatten(root *TreeNode) {
	var prev *TreeNode
	var dfs func(root *TreeNode)
	dfs = func(root *TreeNode) {
		if root == nil {
			return
		}
		prev = root
		dfs(root.Left)
		prev.Right = root.Right
		dfs(root.Right)
		if root.Left != nil {
			root.Right = root.Left
			root.Left = nil
		}
	}
	dfs(root)
}
