package problem956

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func tallestBillboard(rods []int) int {
	return tallestBillboardDP(rods)
	sum := 0
	for _, rod := range rods {
		sum += rod
	}
	half := sum / 2
	memo := make(map[[3]int]int)
	var solve func(i, sup1, sup2 int) int
	solve = func(i, sup1, sup2 int) int {
		if sup1 > half || sup2 > half {
			return 0
		}
		if i >= len(rods) {
			if sup1 == sup2 {
				return sup1
			}
			return 0
		}
		key1 := [3]int{i, sup1, sup2}
		if val, ok := memo[key1]; ok {
			return val
		}
		key2 := [3]int{i, sup2, sup1}
		if val, ok := memo[key2]; ok {
			return val
		}
		a := solve(i+1, sup1, sup2)
		b := solve(i+1, sup1+rods[i], sup2)
		c := solve(i+1, sup1, sup2+rods[i])
		tallest := max(max(a, b), c)
		memo[key1] = tallest
		return tallest
	}
	ans := solve(0, 0, 0)
	return ans
}

func tallestBillboardDP(rods []int) int {
	dp := make(map[int]int)
	dp[0] = 0
	for _, rod := range rods {
		newDp := make(map[int]int)
		for diff, small := range dp {
			newDp[diff] = max(newDp[diff], small)
			newDp[rod+diff] = max(newDp[rod+diff], small+rod)
			newDp[diff-rod] = max(newDp[diff-rod], small)
		}
		dp = newDp
	}
	return dp[0]
}

/*
	1,2,3,6
	left := 1,2
	0,0
	0,0 0,1 1,0
	0,0 0,1 1,0 2,0 0,2 2,1 0,3 3,0 1,2
	0   -1  1   2   -2   1  -3  3   -1
	right := 3, 6
	0,0
	0,0 0,3 3,0
	0,0 0,3 3,0 0,6 6,0 6,3 0,9 9,0 3,6
	0   -3  3   -6  6   3   -9  9   -3
*/
