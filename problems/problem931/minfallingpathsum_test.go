package problem931

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestMinfallingpathsum(t *testing.T) {
	scenarios := [...]struct {
		matrix [][]int
		output int
	}{
		0: {
			matrix: [][]int{
				{2, 1, 3},
				{6, 5, 4},
				{7, 8, 9},
			},
			output: 13,
		},
		1: {
			matrix: [][]int{
				{-19, 57},
				{-40, -5},
			},
			output: -59,
		},
	}
	for i, scenario := range scenarios {
		output := minFallingPathSum(scenario.matrix)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
