package problem986

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestIntervalintersection(t *testing.T) {
	scenarios := [...]struct {
		firstList  [][]int
		secondList [][]int
		output     [][]int
	}{
		0: {
			[][]int{{0, 2}, {5, 10}, {13, 23}, {24, 25}},
			[][]int{{1, 5}, {8, 12}, {15, 24}, {25, 26}},
			[][]int{{1, 2}, {5, 5}, {8, 10}, {15, 23}, {24, 24}, {25, 25}}},
		1: {
			[][]int{{1, 3}, {5, 9}}, nil, nil,
		},
	}
	for i, scenario := range scenarios {
		output := intervalIntersection(scenario.firstList, scenario.secondList)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
