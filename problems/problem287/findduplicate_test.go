package problem287

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestFindduplicate(t *testing.T) {
	scenarios := [...]struct {
		nums   []int
		output int
	}{
		0: {
			nums:   []int{1, 3, 4, 2, 2},
			output: 2,
		},
		1: {
			nums:   []int{3, 1, 3, 4, 2},
			output: 3,
		},
	}
	for i, scenario := range scenarios {
		output := findDuplicate(scenario.nums)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
