package tree

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func NewTree(nums ...interface{}) *TreeNode {
	if len(nums) == 0 {
		return nil
	}
	num, ok := nums[0].(int)
	if !ok {
		return nil
	}
	nums = nums[1:]
	root := &TreeNode{num, nil, nil}
	queue := []*TreeNode{root}
	for len(queue) != 0 {
		newQueue := []*TreeNode{}
		for _, node := range queue {
			nodes := [2]*TreeNode{}
			for i := 0; i < 2 && len(nums) > 0; i++ {
				if num, ok := nums[0].(int); ok {
					nodes[i] = &TreeNode{num, nil, nil}
					newQueue = append(newQueue, nodes[i])
				} else {
					nodes[i] = nil
				}
				nums = nums[1:]
			}
			node.Left = nodes[0]
			node.Right = nodes[1]
		}
		queue = newQueue
	}
	return root
}
