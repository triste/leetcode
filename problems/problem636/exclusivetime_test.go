package problem636

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestExclusivetime(t *testing.T) {
	scenarios := [...]struct {
		n      int
		logs   []string
		output []int
	}{
		0: {
			n:      2,
			logs:   []string{"0:start:0", "1:start:2", "1:end:5", "0:end:6"},
			output: []int{3, 4},
		},
		1: {
			n:      1,
			logs:   []string{"0:start:0", "0:start:2", "0:end:5", "0:start:6", "0:end:6", "0:end:7"},
			output: []int{8},
		},
		2: {
			n:      2,
			logs:   []string{"0:start:0", "0:start:2", "0:end:5", "1:start:6", "1:end:6", "0:end:7"},
			output: []int{7, 1},
		},
		3: {
			n:      3,
			logs:   []string{"0:start:0", "0:end:0", "1:start:1", "1:end:1", "2:start:2", "2:end:2", "2:start:3", "2:end:3"},
			output: []int{1, 1, 2},
		},
	}
	for i, scenario := range scenarios {
		output := exclusiveTime(scenario.n, scenario.logs)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
