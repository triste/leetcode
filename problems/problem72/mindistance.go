package problem72

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func minDistance(word1 string, word2 string) int {
	if len(word1) > len(word2) {
		word1, word2 = word2, word1
	}
	dp := make([]int, len(word1)+1)
	for i := range dp {
		dp[i] = i
	}
	for i, ch2 := range word2 {
		prev := dp[0]
		dp[0] = i + 1
		for j, ch1 := range word1 {
			tmp := dp[j+1]
			if ch1 == ch2 {
				dp[j+1] = prev
			} else {
				dp[j+1] = min(prev, min(dp[j], dp[j+1])) + 1
			}
			prev = tmp
		}
	}
	return dp[len(word1)]
}

/*
   | r | o | s |
 h |   |   |   |
 o |   |   |   |
 r |   |   |   |
 s |   |   |   |
 e |   |   |   |

*/

/*
	DP

   | h | o | r | s | e |
 r |   |   |   |   |   |
 o |   |   |   |   |   |
 s |   |   |   |   | x |

*/
