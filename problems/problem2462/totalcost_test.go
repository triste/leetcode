package problem2462

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestTotalcost(t *testing.T) {
	scenarios := [...]struct {
		costs      []int
		k          int
		candidates int
		output     int64
	}{
		0: {
			costs:      []int{17, 12, 10, 2, 7, 2, 11, 20, 8},
			k:          3,
			candidates: 4,
			output:     11,
		},
		1: {
			costs:      []int{1, 2, 4, 1},
			k:          3,
			candidates: 3,
			output:     4,
		},
		2: {
			costs:      []int{31, 25, 72, 79, 74, 65, 84, 91, 18, 59, 27, 9, 81, 33, 17, 58},
			k:          11,
			candidates: 2,
			output:     423,
		},
		3: {
			costs:      []int{57, 33, 26, 76, 14, 67, 24, 90, 72, 37, 30},
			k:          11,
			candidates: 2,
			output:     526,
		},
	}
	for i, scenario := range scenarios {
		output := totalCost(scenario.costs, scenario.k, scenario.candidates)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
