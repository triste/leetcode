package problem859

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestBuddystrings(t *testing.T) {
	scenarios := [...]struct {
		s      string
		goal   string
		output bool
	}{
		0: {
			s:      "ab",
			goal:   "ba",
			output: true,
		},
		1: {
			s:      "ab",
			goal:   "ab",
			output: false,
		},
		2: {
			s:      "aa",
			goal:   "aa",
			output: true,
		},
	}
	for i, scenario := range scenarios {
		output := buddyStrings(scenario.s, scenario.goal)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
