package problem673

import (
	"sort"
)

/*
1,3,5,4,7

1
3
5,4
7
*/

type Branch struct {
	num    int
	presum int
}
type Pile = []Branch

const (
	LeftBound  = -1_000_001
	RightBound = 1_000_001
)

func findNumberOfLISPatience(nums []int) int {
	nums = append(nums, RightBound)
	piles := []Pile{{{RightBound, 0}, {LeftBound, 1}}}
	for _, num := range nums {
		i := sort.Search(len(piles), func(i int) bool {
			return piles[i][len(piles[i])-1].num >= num
		})
		prepile := piles[i-1]
		j := sort.Search(len(prepile), func(j int) bool {
			return prepile[j].num < num
		})
		count := prepile[len(prepile)-1].presum - prepile[j-1].presum
		if i < len(piles) {
			piles[i] = append(piles[i], Branch{num, piles[i][len(piles[i])-1].presum + count})
		} else {
			piles = append(piles, Pile{{RightBound, 0}, {num, count}})
		}
	}
	return piles[len(piles)-1][1].presum
}

type Cell struct {
	lenght    int
	pathCount int
}

func findNumberOfLIS(nums []int) int {
	return findNumberOfLISPatience(nums)
	dp := make([]Cell, len(nums))
	maxLis := 1
	for i := 0; i < len(nums); i++ {
		dp[i] = Cell{1, 1}
		for j := 0; j < i; j++ {
			if nums[j] < nums[i] && dp[j].lenght+1 > dp[i].lenght {
				dp[i].lenght = dp[j].lenght + 1
				dp[i].pathCount = dp[j].pathCount
			} else if dp[j].lenght+1 == dp[i].lenght {
				dp[i].pathCount += dp[j].pathCount
			}
		}
		if dp[i].lenght > maxLis {
			maxLis = dp[i].lenght
		}
	}
	var lisCount int
	for _, cell := range dp {
		if cell.lenght == maxLis {
			lisCount += cell.pathCount
		}
	}
	return lisCount
}

/*
	 2 | 2 | 2 | 2 | 2
	1,1|1,2
*/

/*
	1 2 4 3 5 4 7 2
	1 2 1 2 1 1 1
*/

/*
   | 1 | 2 | 4 | 3 | 5 | 4 | 7 | 2 |
   |1,1|2,1|3,1|3,1|4,2|4,1|5,3|2,1|

   | 1 | 2 | 3 | 3 | 4 | 4 | 5 | 2 |

 1 |   |   |   |   |   |   |   |   |
 2 |   |   |   |   |   |   |   |   |
 4 |   |   |   |   |   |   |   |   |
 3 |   |   |   |   |   |   |   |   |
 5 |   |   |   |   |   |   |   |   |
 4 |   |   |   |   |   |   |   |   |
 7 |   |   |   |   |   |   |   |   |
 2 |   |   |   |   |   |   |   |   |

 {1}: 1
 {1,3}: 1

	1,3,5,4,5,7
	1,3,5,7
	4,5

	1,3,5,4,7

	1,3,5,7
	4

	1 - 3 - 5 - 7
	     \     /
	        4
*/

/*
	1,2,4,3,5,4,7,2

	{-1,nil}
	0        1
	{-1,nil}
	{-2,0}, {-2,nil}
	{-4,0}, {-3,nil}
	{-5,1}, {-4,nil},
	{-7,1},
*/

/*
	1,2,4,3,5,4,7,2

	1-2-4-5-7
	1-2-3-4-7
	1-2-3-5-7

	1,2,4,5,7
	3,4
	2

	step 3:
		1,1 - 2,1 - 4,1
	step 4:
		1,1 - 2,1 - 4,1
		       \
			    3,2
	step 5:
		1,1 - 2,1 - 4,1 - 5,2
		         \       /
			        3,2
	step 6:
		1,1 - 2,1 - 4,1 - 5,2
		         \       /
			        3,2 -  4,2
		1 - 2 - 4 - 5
		      \   /
			    3 - 4
	step 7:
		1 - 2 - 4 - 5 - 7
		      \   /   /
			    3 - 4
	step 8:
		1 - 20 - 40 - 50 - 70
		 \    \   /   /
		  \    30 - 40
           \
		    2 - 3 - 4 - 5



	                2
	               /
	          3 - 4
	         / \   \
	1 - 2 - 4 - 5 - 7


	              2
	              |
	        3  -  4
	        |  \   \
	1 - 2 - 4 - 5 - 7


	0       1       2       3       4
	{1,nil},{2,nil},{4,nil},{5,nil},{7,nil}
	{3,2},  {4,3}
	{2,1}

	1,2,4,5,7
	3,4
	2
*/

/*
	1,3,5,4,7

	1,3,5,7
	4

	case {1,3,5,4,7}:
		lis = {1,3,4,7} count = 1
	case {1,3,5,4}:
		lis = {1,3,4}: count = 2
	case {1,3}:
		lis = {1,3}: count = 1
  | 1 | 3 | 5 | 4 | 7 |
1 |   |   |   |   |   |
3 |   |   |   |   |   |
5 |   |   |   |   |   |
4 |   |   |   |   |   |
7 |   |   |   |   |   |

  | 2 | 2 | 2 | 2 | 2 |
2 | 1 | 2 | 3 | 4 | 5 |
2 |   | 1 | 2 | 3 | 4 |
2 |   |   | 1 | 2 | 3 |
2 |   |   |   | 1 | 2 |
2 |   |   |   |   | 1 |


	case {3,1,2}:
		lis = {1,2}: count = 1
	case {3,1}:
		lis = {1}: count = 2
	case {3}:
		lis = {3}: count = 1
  | 3 | 1 | 2 |
3 | 1 | 2 | 1 |
1 |   |   |   |
2 |   |   |   |

*/

/*
	1, 3, 5, 4, 7

	1, 3, 5, 7
	1, 3, 4, 7


	1,2,4,3,5,4,7,2

	1,2,4,5,7
	1,2,3,5,7
	1,2,3,4,7


	3: lis = {{3,1}}
	1: lis = {{1,2}}
	2: lis = {{1,2},{2,1}}
*/
