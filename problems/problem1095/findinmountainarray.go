package problem1095

type MountainArray []int

func (m MountainArray) get(i int) int {
	return m[i]
}

func (m MountainArray) length() int {
	return len(m)
}

func findInMountainArray(target int, mountainArr *MountainArray) int {
	l, r := 1, mountainArr.length()-1
	for l < r {
		m := (l + r) / 2
		first := mountainArr.get(m)
		second := mountainArr.get(m + 1)
		if first < second {
			l = m + 1
		} else {
			r = m
		}
	}
	peak := l
	for l, r = 0, peak; l <= r; {
		m := (l + r) / 2
		if num := mountainArr.get(m); num < target {
			l = m + 1
		} else if num > target {
			r = m - 1
		} else {
			return m
		}
	}
	for l, r = peak, mountainArr.length()-1; l <= r; {
		m := (l + r) / 2
		if num := mountainArr.get(m); num < target {
			r = m - 1
		} else if num > target {
			l = m + 1
		} else {
			return m
		}
	}
	return -1
}
