package problem850

import (
	"container/heap"
	"log"
	"sort"

	"github.com/google/go-cmp/cmp"
)

type End struct {
	x  int
	y1 int
	y2 int
}

type MinHeap []End

func (h MinHeap) Len() int {
	return len(h)
}

func (h MinHeap) Less(i, j int) bool {
	return h[i].x < h[j].x
}

func (h MinHeap) Swap(i, j int) {
	h[i], h[j] = h[j], h[i]
}

func (h *MinHeap) Push(x any) {
	*h = append(*h, x.(End))
}

func (h *MinHeap) Pop() any {
	x := (*h)[len(*h)-1]
	*h = (*h)[:len(*h)-1]
	return x
}

type SegmentTree struct {
	segments []Segment
}

type Segment struct {
	Start, End  int
	Left, Right *Segment
	FillCount   int
	Len         int
	Filled      int
}

/*
	0, 1, 2, 3
	0-1,1-2,2-3

	         0-3
	       /     \
	    0-2      2-3
	   /   \    /   \
	0-1   1-2  2-2  3-3
   /  \   /  \
 0-0 1-1 1-1  2-2
*/

func NewSegment(points []int) *Segment {
	var dfs func(l, r int) *Segment
	dfs = func(l, r int) *Segment {
		log.Println(l, r)
		if r-l == 1 {
			return &Segment{
				Start: l,
				End:   r,
				Left:  nil,
				Right: nil,
				Len:   points[r] - points[l],
			}
		}
		mid := l + (r-l+1)/2
		return &Segment{
			Start: l,
			End:   r,
			Left:  dfs(l, mid),
			Right: dfs(mid, r),
			Len:   points[r] - points[l],
		}
	}
	return dfs(0, len(points)-1)
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func (s *Segment) Update(start, end int, val int) int {
	if start >= end {
		return 0
	}
	if start == s.Start && end == s.End {
		s.FillCount += val
	} else {
		mid := s.Start + (s.End-s.Start+1)/2
		s.Left.Update(start, min(mid, end), val)
		s.Right.Update(max(mid, start), end, val)
	}
	if s.FillCount > 0 {
		s.Filled = s.Len
	} else if s.FillCount < 0 {
		s.Filled = s.Left.Filled + s.Right.Filled
	}
	return s.Filled
}

func rectangleArea(rectangles [][]int) int {
	yset := make(map[int]int, len(rectangles)*2)
	uniqueYs := make([]int, 0, len(rectangles)*2)
	for _, rec := range rectangles {
		for _, y := range [...]int{rec[1], rec[3]} {
			if _, ok := yset[y]; !ok {
				uniqueYs = append(uniqueYs, y)
				yset[y] = 0
			}
		}
	}
	sort.Ints(uniqueYs)
	for i, y := range uniqueYs {
		yset[y] = i
	}
	segment := NewSegment(uniqueYs)
	log.Println(cmp.Diff(segment, &Segment{}))
	log.Println(uniqueYs)
	ends := &MinHeap{}
	heap.Init(ends)
	prevX := rectangles[0][0]
	area := 0
	prevHeight := 0
	sort.Slice(rectangles, func(i, j int) bool {
		return rectangles[i][0] < rectangles[j][0]
	})
	for i := 0; i < len(rectangles) || ends.Len() > 0; {
		if i < len(rectangles) && ends.Len() > 0 && (*ends)[0].x < rectangles[i][0] || i == len(rectangles) {
			// Process end of rectangle
			end := heap.Pop(ends).(End)
			area += prevHeight * (end.x - prevX)
			log.Println("Process end", end, "prevHeight =", prevHeight, "=", area)
			prevHeight = segment.Update(yset[end.y1], yset[end.y2], -1)
			log.Println("height =", prevHeight)
		} else {
			// Process start of rectangle
			area += prevHeight * (rectangles[i][0] - prevX)
			log.Println("Process start", rectangles[i], "prevHeight =", prevHeight, "=", area)
			prevHeight = segment.Update(yset[rectangles[i][1]], yset[rectangles[i][3]], 1)
			log.Println("height =", prevHeight)
			heap.Push(ends, End{rectangles[i][2], rectangles[i][1], rectangles[i][3]})
			i++
		}
		area %= 1_000_000_007
	}
	return area
}
