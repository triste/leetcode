package problem546

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestRemoveboxes(t *testing.T) {
	scenarios := [...]struct {
		boxes  []int
		output int
	}{
		0: {
			boxes:  []int{1, 3, 2, 2, 2, 3, 4, 3, 1},
			output: 23,
		},
		1: {
			boxes:  []int{1, 1, 1},
			output: 9,
		},
		2: {
			boxes:  []int{1},
			output: 1,
		},
		3: {
			boxes:  []int{6, 10, 1, 7, 1, 3, 10, 2, 1, 3},
			output: 16,
		},
		4: {
			boxes:  []int{1, 2, 2, 1, 1, 1, 2, 1, 1, 2, 1, 2, 1, 1, 2, 2, 1, 1, 2, 2, 1, 1, 1, 2, 2, 2, 2, 1, 2, 1, 1, 2, 2, 1, 2, 1, 2, 2, 2, 2, 2, 1, 2, 1, 2, 2, 1, 1, 1, 2, 2, 1, 2, 1, 2, 2, 1, 2, 1, 1, 1, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 1, 1, 1, 1, 2, 2, 1, 1, 1, 1, 1, 1, 1, 2, 1, 2, 2, 1},
			output: 2758,
		},
	}
	for i, scenario := range scenarios {
		output := removeBoxes(scenario.boxes)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
