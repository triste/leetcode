package problem133

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func NewGraph(edges [][2]int) *Node {
	return nil
}

func TestClonegraph(t *testing.T) {
	scenarios := [...]struct {
		node   *Node
		output *Node
	}{
		0: {
			node:   &Node{},
			output: &Node{},
		},
		1: {},
		2: {},
	}
	for i, scenario := range scenarios {
		output := cloneGraph(scenario.node)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
