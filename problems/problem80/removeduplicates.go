package problem80

func removeDuplicates(nums []int) int {
	prevNum := -10001
	prevNumCount := 0
	i := 0
	for _, num := range nums {
		if num != prevNum {
			nums[i] = num
			prevNum = num
			prevNumCount = 1
			i++
		} else if prevNumCount < 2 {
			nums[i] = num
			i++
			prevNumCount++
		}
	}
	return i
}
