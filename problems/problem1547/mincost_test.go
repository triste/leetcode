package problem1547

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestMincost(t *testing.T) {
	scenarios := [...]struct {
		n      int
		cuts   []int
		output int
	}{
		0: {
			n:      7,
			cuts:   []int{1, 3, 4, 5},
			output: 16,
		},
		1: {
			n:      9,
			cuts:   []int{5, 6, 1, 4, 2},
			output: 22,
		},
	}
	for i, scenario := range scenarios {
		output := minCost(scenario.n, scenario.cuts)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
