package problem263

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestIsugly(t *testing.T) {
	scenarios := [...]struct {
		n      int
		output bool
	}{
		0: {
			n:      6,
			output: true,
		},
		1: {
			n:      1,
			output: true,
		},
		2: {
			n:      14,
			output: false,
		},
	}
	for i, scenario := range scenarios {
		output := isUgly(scenario.n)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
