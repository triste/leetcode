package problem1584

import (
	"math"
)

func minCostConnectPoints(points [][]int) (cost int) {
	mst := make([]bool, len(points))
	keys := make([]int, len(points))
	for i := 1; i < len(keys); i++ {
		keys[i] = math.MaxInt
	}
	for i := 0; i < len(points); i++ {
		minkey := math.MaxInt
		var u int
		for i, key := range keys {
			if !mst[i] && key < minkey {
				minkey = key
				u = i
			}
		}
		cost += minkey
		mst[u] = true
		for i := range points {
			if mst[i] {
				continue
			}
			dx := points[i][0] - points[u][0]
			if dx < 0 {
				dx = -dx
			}
			dy := points[i][1] - points[u][1]
			if dy < 0 {
				dy = -dy
			}
			w := dx + dy
			if w < keys[i] {
				keys[i] = w
			}
		}
	}
	return
}
