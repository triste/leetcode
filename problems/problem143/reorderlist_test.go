package problem143

import (
	"testing"

	"github.com/google/go-cmp/cmp"
	. "gitlab.com/triste/leetcode/pkg/list"
)

func TestReorderList(t *testing.T) {
	scenarios := [...]struct {
		head   *ListNode
		output *ListNode
	}{
		0: {
			NewListFromSlice(1, 2, 3, 4),
			NewListFromSlice(1, 4, 2, 3),
		},
		1: {
			NewListFromSlice(1, 2, 3, 4, 5),
			NewListFromSlice(1, 5, 2, 4, 3),
		},
		2: {
			NewListFromSlice(1),
			NewListFromSlice(1),
		},
		3: {
			NewListFromSlice(1, 2),
			NewListFromSlice(1, 2),
		},
		4: {
			NewListFromSlice(1, 2, 3),
			NewListFromSlice(1, 3, 2),
		},
	}
	for i, scenario := range scenarios {
		reorderList(scenario.head)
		if diff := cmp.Diff(scenario.output, scenario.head); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
