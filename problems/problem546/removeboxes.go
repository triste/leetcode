package problem546

type BoxPool struct {
	color int
	count int
}

func removeBoxes(boxes []int) int {
	boxPools := make([]BoxPool, 0, len(boxes))
	for prevColor, i := 0, 0; i < len(boxes); i++ {
		if boxes[i] == prevColor {
			boxPools[len(boxPools)-1].count++
		} else {
			boxPools = append(boxPools, BoxPool{boxes[i], 1})
			prevColor = boxes[i]
		}
	}
	var memo [100][100][100]int
	var solve func(l, r, k int) int
	solve = func(l, r, k int) int {
		if l > r {
			return 0
		}
		if memo[l][r][k] > 0 {
			return memo[l][r][k]
		}
		delCount := boxPools[l].count + k
		maxPoints := delCount*delCount + solve(l+1, r, 0)
		for j := l + 1; j <= r; j++ {
			if boxPools[j].color != boxPools[l].color {
				continue
			}
			leftPoints := solve(l+1, j-1, 0)
			rightPoints := solve(j, r, delCount)
			totalPoints := leftPoints + rightPoints
			if totalPoints > maxPoints {
				maxPoints = totalPoints
			}
		}
		memo[l][r][k] = maxPoints
		return maxPoints
	}
	return solve(0, len(boxPools)-1, 0)
}

/*
	3,2,2,2,3,4,3

	3,2,2,2,3,4 | 3
	OR
	3,2,2,2,3,(3) | 4
*/
