package problem24

import (
	"testing"

	"github.com/google/go-cmp/cmp"
	. "gitlab.com/triste/leetcode/pkg/list"
)

func TestSwapPairs(t *testing.T) {
	scenarios := [...]struct {
		head   *ListNode
		output *ListNode
	}{
		0: {
			NewListFromSlice(1, 2, 3, 4),
			NewListFromSlice(2, 1, 4, 3),
		},
		1: {
			NewListFromSlice(),
			NewListFromSlice(),
		},
		2: {
			NewListFromSlice(1),
			NewListFromSlice(1),
		},
		3: {
			NewListFromSlice(1, 2, 3),
			NewListFromSlice(2, 1, 3),
		},
	}
	for i, scenario := range scenarios {
		output := swapPairs(scenario.head)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
