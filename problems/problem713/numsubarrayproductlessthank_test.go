package problem713

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestNumsubarrayproductlessthank(t *testing.T) {
	scenarios := [...]struct {
		nums   []int
		k      int
		output int
	}{
		0: {[]int{10, 5, 2, 6}, 100, 8},
		1: {[]int{1, 2, 3}, 0, 0},
		2: {[]int{10, 9, 10, 4, 3, 8, 3, 3, 6, 2, 10, 10, 9, 3}, 19, 18},
	}
	for i, scenario := range scenarios {
		output := numSubarrayProductLessThanK(scenario.nums, scenario.k)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
