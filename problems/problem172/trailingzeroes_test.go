package problem172

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestTrailingzeroes(t *testing.T) {
	scenarios := [...]struct {
		n      int
		output int
	}{
		0: {
			n:      3,
			output: 0,
		},
		1: {
			n:      5,
			output: 1,
		},
		2: {
			n:      10,
			output: 2,
		},
	}
	for i, scenario := range scenarios {
		output := trailingZeroes(scenario.n)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
