package problem1793

func maximumScore(nums []int, k int) int {
	l, r := k, k
	score := nums[k]
	maxScore := score
	minNum := nums[k]
	for l > 0 || r < len(nums)-1 {
		var nextNum int
		if r == len(nums)-1 || (l > 0 && nums[l-1] > nums[r+1]) {
			l--
			nextNum = nums[l]
		} else {
			r++
			nextNum = nums[r]
		}
		if nextNum < minNum {
			minNum = nextNum
		}
		score = minNum * (r - l + 1)
		if score > maxScore {
			maxScore = score
		}
	}
	return maxScore
}

/*
0 1 2 3 4 5
1,4,3,7,4,5
      |
	  k

left, right = k, k
*/
