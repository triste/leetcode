package problem13

func romanToInt(s string) (output int) {
	m := ['X'-'C'+1]int{
		'X'-'I': 1,
		'X'-'V': 5,
		'X'-'X': 10,
		'X'-'L': 50,
		'X'-'C': 100,
		'X'-'D': 500,
		'X'-'M': 1000,
	}
	for i := len(s) - 1; i >= 0; i-- {
		ch := s[i]
		val := m['X'-ch]
		if val < output && ch != s[i+1] {
			output -= val
		} else {
			output += val
		}
	}
	return
}
