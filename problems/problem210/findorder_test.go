package problem210

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestFindorder(t *testing.T) {
	scenarios := [...]struct {
		numCourses    int
		prerequisites [][]int
		output        []int
	}{
		0: {
			numCourses: 2,
			prerequisites: [][]int{
				{1, 0},
			},
			output: []int{
				0, 1,
			},
		},
		1: {
			numCourses: 4,
			prerequisites: [][]int{
				{1, 0}, {2, 0}, {3, 1}, {3, 2},
			},
			output: []int{
				0, 1, 2, 3,
			},
		},
		2: {
			numCourses:    1,
			prerequisites: nil,
			output: []int{
				0,
			},
		},
		3: {
			numCourses: 4,
			prerequisites: [][]int{
				{2, 0}, {1, 0}, {3, 1}, {3, 2}, {1, 3},
			},
			output: nil,
		},
	}
	for i, scenario := range scenarios {
		output := findOrder(scenario.numCourses, scenario.prerequisites)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
