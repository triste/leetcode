package problem1456

func isVowel[C byte | rune](ch C) bool {
	switch ch {
	case 'a', 'e', 'i', 'o', 'u':
		return true
	default:
		return false
	}
}

func maxVowels(s string, k int) int {
	vowelCount := 0
	for _, ch := range s[:k] {
		if isVowel(ch) {
			vowelCount++
		}
	}
	maxVowelCount := vowelCount
	for i, ch := range s[k:] {
		if isVowel(s[i]) {
			vowelCount--
		}
		if isVowel(ch) {
			if vowelCount++; vowelCount > maxVowelCount {
				maxVowelCount = vowelCount
			}
		}
	}
	return maxVowelCount
}
