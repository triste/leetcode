package problem2352

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestEqualpairs(t *testing.T) {
	scenarios := [...]struct {
		grid   [][]int
		output int
	}{
		0: {
			grid: [][]int{
				{3, 2, 1},
				{1, 7, 6},
				{2, 7, 7},
			},
			output: 1,
		},
		1: {
			grid: [][]int{
				{3, 1, 2, 2},
				{1, 4, 4, 5},
				{2, 4, 2, 2},
				{2, 4, 2, 2},
			},
			output: 3,
		},
	}
	for i, scenario := range scenarios {
		output := equalPairs(scenario.grid)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
