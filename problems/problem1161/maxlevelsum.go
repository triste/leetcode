package problem1161

import (
	"math"

	. "gitlab.com/triste/leetcode/pkg/tree"
)

func maxLevelSum(root *TreeNode) int {
	queue := []*TreeNode{root}
	var tmpQueue []*TreeNode
	maxSum := math.MinInt
	var maxSumDepth int
	for depth := 1; len(queue) > 0; depth++ {
		sum := 0
		for _, node := range queue {
			sum += node.Val
			if node.Left != nil {
				tmpQueue = append(tmpQueue, node.Left)
			}
			if node.Right != nil {
				tmpQueue = append(tmpQueue, node.Right)
			}
		}
		if sum > maxSum {
			maxSum = sum
			maxSumDepth = depth
		}
		queue, tmpQueue = tmpQueue, queue[:0]
	}
	return maxSumDepth
}
