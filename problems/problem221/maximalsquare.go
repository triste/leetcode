package problem221

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func maximalSquare(matrix [][]byte) int {
	m := len(matrix)
	n := len(matrix[0])
	dp := make([][]int, m)
	for i := 0; i < m; i++ {
		dp[i] = make([]int, n)
	}
	largest := 0
	for i := 0; i < m; i++ {
		dp[i][0] = int(matrix[i][0] - '0')
		largest |= dp[i][0]
	}
	for i := 1; i < n; i++ {
		dp[0][i] = int(matrix[0][i] - '0')
		largest |= dp[0][i]
	}
	for i := 1; i < m; i++ {
		for j := 1; j < n; j++ {
			if matrix[i][j] == '0' {
				continue
			}
			dp[i][j] = min(min(dp[i-1][j-1], dp[i-1][j]), dp[i][j-1]) + 1
			largest = max(largest, dp[i][j])
		}
	}
	return largest * largest
}
