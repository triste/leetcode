package problem309

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestMaxprofit(t *testing.T) {
	scenarios := [...]struct {
		prices []int
		output int
	}{
		0: {
			prices: []int{1, 2, 3, 0, 2},
			output: 3,
		},
		1: {
			prices: []int{1},
			output: 0,
		},
		2: {
			prices: []int{6, 1, 3, 2, 4, 7},
			output: 6,
		},
	}
	for i, scenario := range scenarios {
		output := maxProfit(scenario.prices)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
