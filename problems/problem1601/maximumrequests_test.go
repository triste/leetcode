package problem1601

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestMaximumrequests(t *testing.T) {
	scenarios := [...]struct {
		n        int
		requests [][]int
		output   int
	}{
		0: {
			n:        5,
			requests: [][]int{{0, 1}, {1, 0}, {0, 1}, {1, 2}, {2, 0}, {3, 4}},
			output:   5,
		},
		1: {
			n:        3,
			requests: [][]int{{0, 0}, {1, 2}, {2, 1}},
			output:   3,
		},
		2: {
			n:        4,
			requests: [][]int{{0, 3}, {3, 1}, {1, 2}, {2, 0}},
			output:   4,
		},
	}
	for i, scenario := range scenarios[:1] {
		output := maximumRequests(scenario.n, scenario.requests)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
