package problem87

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestIsscramble(t *testing.T) {
	scenarios := [...]struct {
		s1     string
		s2     string
		output bool
	}{
		0: {
			s1:     "great",
			s2:     "rgeat",
			output: true,
		},
		1: {
			s1:     "abcde",
			s2:     "caebd",
			output: false,
		},
		2: {
			s1:     "a",
			s2:     "a",
			output: true,
		},
		3: {
			s1:     "abcdbdacbdac",
			s2:     "bdacabcdbdac",
			output: true,
		},
		4: {
			s1:     "ccabcbabcbabbbbcbb",
			s2:     "bbbbabccccbbbabcba",
			output: false,
		},
	}
	for i, scenario := range scenarios {
		output := isScramble(scenario.s1, scenario.s2)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
