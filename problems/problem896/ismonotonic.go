package problem896

func isMonotonic(nums []int) bool {
	i := 1
	for i < len(nums) && nums[i] == nums[i-1] {
		i++
	}
	if i >= len(nums) {
		return true
	}
	if nums[i] < nums[i-1] {
		for i++; i < len(nums); i++ {
			if nums[i] > nums[i-1] {
				return false
			}
		}
	} else {
		for i++; i < len(nums); i++ {
			if nums[i] < nums[i-1] {
				return false
			}
		}
	}
	return true
}
