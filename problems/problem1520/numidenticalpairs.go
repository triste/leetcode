package problem1520

func numIdenticalPairs(nums []int) (pairCount int) {
	var freqs [101]int
	for _, num := range nums {
		freqs[num]++
	}
	for _, freq := range freqs {
		pairCount += (freq * (freq - 1)) >> 1
	}
	return
}
