package problem1514

import (
	"container/heap"
	"math"
)

type GraphEdge struct {
	i int
	w float64
}

type PriorityQueue []*GraphEdge

func (pq PriorityQueue) Len() int {
	return len(pq)
}

func (pq PriorityQueue) Less(i, j int) bool {
	return pq[i].w > pq[j].w
}

func (pq PriorityQueue) Swap(i, j int) {
	pq[i], pq[j] = pq[j], pq[i]
}

func (pq *PriorityQueue) Push(x interface{}) {
	*pq = append(*pq, x.(*GraphEdge))
}

func (pq *PriorityQueue) Pop() interface{} {
	old := *pq
	n := len(old)
	x := old[n-1]
	*pq = old[0 : n-1]
	return x
}

func maxProbability(n int, edges [][]int, succProb []float64, start int, end int) float64 {
	graph := make([][]GraphEdge, n)
	for i, edge := range edges {
		graph[edge[0]] = append(graph[edge[0]], GraphEdge{edge[1], math.Log(succProb[i])})
		graph[edge[1]] = append(graph[edge[1]], GraphEdge{edge[0], math.Log(succProb[i])})
	}
	probs := make([]float64, n)
	for i := range probs {
		probs[i] = math.Inf(-1)
	}
	probs[start] = 0.0
	var pq PriorityQueue
	heap.Push(&pq, &GraphEdge{start, 0})
	for pq.Len() > 0 {
		curedge := heap.Pop(&pq).(*GraphEdge)
		if curedge.i == end {
			break
		}
		if curedge.w < probs[curedge.i] {
			continue
		}
		for _, edge := range graph[curedge.i] {
			if probs[edge.i] < probs[curedge.i]+edge.w {
				probs[edge.i] = probs[curedge.i] + edge.w
				heap.Push(&pq, &GraphEdge{edge.i, probs[edge.i]})
			}
		}
	}
	return math.Exp(probs[end])
}
