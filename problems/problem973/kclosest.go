package problem973

import (
	"math/rand"
)

func kClosest(points [][]int, k int) (output [][]int) {
	if len(points) == k {
		return points
	}
	squareDistances := make([][2]int, len(points))
	for i, point := range points {
		squareDistances[i] = [2]int{point[0]*point[0] + point[1]*point[1], i}
	}
	kthLeastIndex := findKthLeast(squareDistances, k)
	for _, d := range squareDistances[:kthLeastIndex] {
		output = append(output, points[d[1]])
	}
	return
}

func findKthLeast(nums [][2]int, k int) int {
	lenght := len(nums)
	if lenght == 1 {
		return 0
	}
	pivotIndex := partition(nums)
	if pivotIndex < k {
		return pivotIndex + 1 + findKthLeast(nums[pivotIndex+1:], k-(pivotIndex+1))
	} else if pivotIndex > k {
		return findKthLeast(nums[:pivotIndex], k)
	} else {
		return pivotIndex
	}
}

func partition(nums [][2]int) int {
	lenght := len(nums)
	randomIndex := rand.Intn(lenght)
	pivot := nums[randomIndex][0]
	nums[randomIndex], nums[lenght-1] = nums[lenght-1], nums[randomIndex]
	tempIndex := 0
	for i, num := range nums[:lenght-1] {
		if num[0] < pivot {
			nums[tempIndex], nums[i] = num, nums[tempIndex]
			tempIndex++
		}
	}
	nums[tempIndex], nums[lenght-1] = nums[lenght-1], nums[tempIndex]
	return tempIndex
}
