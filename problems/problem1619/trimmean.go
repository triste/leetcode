package problem1619

import (
	"sort"
)

func trimMean(arr []int) (output float64) {
	sort.Ints(arr)
	arr = arr[len(arr)/20 : len(arr)-len(arr)/20]
	sum := 0
	for _, n := range arr {
		sum += n
	}
	return float64(sum) / float64(len(arr))
}
