package problem823

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestNumfactoredbinarytrees(t *testing.T) {
	scenarios := [...]struct {
		arr    []int
		output int
	}{
		0: {
			arr:    []int{2, 4},
			output: 3,
		},
		1: {
			arr:    []int{2, 4, 5, 10},
			output: 7,
		},
	}
	for i, scenario := range scenarios {
		output := numFactoredBinaryTrees(scenario.arr)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
