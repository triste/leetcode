package problem980

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestUniquepathsiii(t *testing.T) {
	scenarios := [...]struct {
		grid   [][]int
		output int
	}{
		0: {
			grid: [][]int{
				{1, 0, 0, 0},
				{0, 0, 0, 0},
				{0, 0, 2, -1},
			},
			output: 2,
		},
		1: {
			grid: [][]int{
				{1, 0, 0, 0},
				{0, 0, 0, 0},
				{0, 0, 0, 2},
			},
			output: 4,
		},
		2: {
			grid: [][]int{
				{0, 1},
				{2, 0},
			},
			output: 0,
		},
	}
	for i, scenario := range scenarios {
		output := uniquePathsIII(scenario.grid)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
