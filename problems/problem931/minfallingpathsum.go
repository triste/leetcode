package problem931

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func minFallingPathSum(matrix [][]int) (output int) {
	n := len(matrix)
	for i := 1; i < n; i++ {
		for j := 0; j < n; j++ {
			if j == 0 {
				matrix[i][j] += min(matrix[i-1][j], matrix[i-1][j+1])
			} else if j == n-1 {
				matrix[i][j] += min(matrix[i-1][j], matrix[i-1][j-1])
			} else {
				matrix[i][j] += min(min(matrix[i-1][j-1], matrix[i-1][j]), matrix[i-1][j+1])
			}
		}
	}
	minsum := 10000
	for j := 0; j < n; j++ {
		minsum = min(minsum, matrix[n-1][j])
	}
	return minsum
}
