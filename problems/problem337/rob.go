package problem337

import (
	. "gitlab.com/triste/leetcode/pkg/tree"
)

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func rob(root *TreeNode) int {
	var dfs func(*TreeNode) (int, int)
	dfs = func(root *TreeNode) (int, int) {
		if root == nil {
			return 0, 0
		}
		leftRobMoney, leftSkipMoney := dfs(root.Left)
		rightRobMoney, rightSkipMoney := dfs(root.Right)
		robMoney := root.Val + leftSkipMoney + rightSkipMoney
		skipMoney := max(leftRobMoney, leftSkipMoney) + max(rightRobMoney, rightSkipMoney)
		return robMoney, skipMoney
	}
	robMoney, skipMoney := dfs(root)
	return max(robMoney, skipMoney)
}
