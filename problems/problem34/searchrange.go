package problem34

func searchRange(nums []int, target int) (output []int) {
	lenght := len(nums)
	if lenght == 0 {
		return []int{-1, -1}
	}
	first := binsearchFirstGreater(nums, target-1)
	if first == lenght || nums[first] != target {
		return []int{-1, -1}
	}
	second := binsearchFirstGreater(nums[first+1:], target)
	return []int{first, first + second}
}

func binsearchFirstGreater(nums []int, target int) int {
	left := 0
	right := len(nums) - 1
	for left <= right {
		mid := (left + right) / 2
		if nums[mid] <= target {
			left = mid + 1
		} else {
			right = mid - 1
		}
	}
	return left
}
