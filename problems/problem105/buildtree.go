package problem105

import (
	. "gitlab.com/triste/leetcode/pkg/tree"
)

func buildTree(preorder []int, inorder []int) (output *TreeNode) {
	m := make(map[int]int)
	for i, n := range inorder {
		m[n] = i
	}
	var build func(int, int, int) *TreeNode
	build = func(start, end, offset int) *TreeNode {
		if start >= end {
			return nil
		}
		curval := preorder[start]
		leftEnd := m[curval] + offset
		left := build(start+1, leftEnd+1, offset+1)
		right := build(leftEnd+1, end, offset)
		return &TreeNode{Val: curval, Left: left, Right: right}
	}
	return build(0, len(preorder), 0)
}
