package problem2140

func mostPoints(questions [][]int) (output int64) {
	dp := make([]int, len(questions)+1)
	for i := len(questions) - 1; i >= 0; i-- {
		answer := questions[i][0]
		if skip := i + questions[i][1] + 1; skip < len(questions) {
			answer += dp[skip]
		}
		if next := dp[i+1]; next > answer {
			dp[i] = next
		} else {
			dp[i] = answer
		}
	}
	return int64(dp[0])
}
