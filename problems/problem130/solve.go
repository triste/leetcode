package problem130

func solve(board [][]byte) {
	lastrow := len(board) - 1
	lastcol := len(board[0]) - 1
	var queue [][2]int
	for i := range board[0] {
		if board[0][i] == 'O' {
			queue = append(queue, [2]int{0, i})
			board[0][i] = 0
		}
		if board[lastrow][i] == 'O' {
			queue = append(queue, [2]int{lastrow, i})
			board[lastrow][i] = 0
		}
	}
	for i := range board {
		if board[i][lastcol] == 'O' {
			queue = append(queue, [2]int{i, lastcol})
			board[i][lastcol] = 0
		}
		if board[i][0] == 'O' {
			queue = append(queue, [2]int{i, 0})
			board[i][0] = 0
		}
	}
	var tmpqueue [][2]int
	for len(queue) > 0 {
		for _, cell := range queue {
			r, c := cell[0], cell[1]
			if r < lastrow && board[r+1][c] == 'O' {
				tmpqueue = append(tmpqueue, [2]int{r + 1, c})
				board[r+1][c] = 0
			}
			if c < lastcol && board[r][c+1] == 'O' {
				tmpqueue = append(tmpqueue, [2]int{r, c + 1})
				board[r][c+1] = 0
			}
			if r > 0 && board[r-1][c] == 'O' {
				tmpqueue = append(tmpqueue, [2]int{r - 1, c})
				board[r-1][c] = 0
			}
			if c > 0 && board[r][c-1] == 'O' {
				tmpqueue = append(tmpqueue, [2]int{r, c - 1})
				board[r][c-1] = 0
			}
		}
		queue, tmpqueue = tmpqueue, queue[:0]
	}
	for i, row := range board {
		for j := range row {
			if board[i][j] == 0 {
				board[i][j] = 'O'
			} else {
				board[i][j] = 'X'
			}
		}
	}
	return
}
