package problem69

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestMysqrt(t *testing.T) {
	scenarios := [...]struct {
		x      int
		output int
	}{
		0: {
			x:      4,
			output: 2,
		},
		1: {
			x:      8,
			output: 2,
		},
		2: {
			x:      2147395599,
			output: 46339,
		},
		3: {
			x:      1,
			output: 1,
		},
		4: {
			x:      2,
			output: 1,
		},
	}
	for i, scenario := range scenarios {
		output := mySqrt(scenario.x)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
