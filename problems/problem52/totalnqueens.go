package problem52

func totalNQueens(n int) (total int) {
	cols := make([]bool, n)
	diags1 := make([]bool, n*2-1)
	diags2 := make([]bool, n*2-1)
	var solve func(r int)
	solve = func(r int) {
		if r >= n {
			total++
			return
		}
		for i := 0; i < n; i++ {
			d1 := i - r + n - 1
			d2 := i + r
			if cols[i] || diags1[d1] || diags2[d2] {
				continue
			}
			cols[i] = true
			diags1[d1] = true
			diags2[d2] = true
			solve(r + 1)
			cols[i] = false
			diags1[d1] = false
			diags2[d2] = false
		}
	}
	solve(0)
	return total
}
