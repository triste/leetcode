package problem287

func findDuplicate(nums []int) int {
	slow, fast := nums[0], nums[nums[0]]
	for slow != fast {
		slow = nums[slow]
		fast = nums[nums[fast]]
	}
	for slow = 0; slow != fast; slow = nums[slow] {
		fast = nums[fast]
	}
	return slow
}
