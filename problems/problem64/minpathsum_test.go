package problem64

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestMinpathsum(t *testing.T) {
	scenarios := [...]struct {
		grid   [][]int
		output int
	}{
		0: {
			grid: [][]int{
				{1, 3, 1},
				{1, 5, 1},
				{4, 2, 1},
			},
			output: 7,
		},
		1: {
			grid: [][]int{
				{1, 2, 3},
				{4, 5, 6},
			},
			output: 12,
		},
	}
	for i, scenario := range scenarios {
		output := minPathSum(scenario.grid)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
