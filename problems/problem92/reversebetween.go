package problem92

import (
	. "gitlab.com/triste/leetcode/pkg/list"
)

func reverseBetween(head *ListNode, left int, right int) *ListNode {
	root := ListNode{Val: 501, Next: head}
	preleftPtr := &root
	for i := 1; i < left; i++ {
		preleftPtr = preleftPtr.Next
	}
	reversedEnd := preleftPtr.Next
	var prev *ListNode
	cur := reversedEnd
	for i := left; i <= right; i++ {
		cur.Next, prev, cur = prev, cur, cur.Next
	}
	preleftPtr.Next = prev
	reversedEnd.Next = cur
	return root.Next
}
