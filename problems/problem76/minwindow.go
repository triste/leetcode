package problem76

import "container/list"

func minWindow(s string, t string) string {
	charCounts := make(map[byte]int, 52)
	charCount := 0
	for i := range t {
		charCounts[t[i]]++
		charCount++
	}
	indexList := list.New()
	charNodes := make(map[byte][]*list.Element, 52)
	l, r := 0, -1
	minWindow := len(s)
	for i := range s {
		ch := s[i]
		if count, ok := charCounts[ch]; ok {
			node := indexList.PushBack(i)
			charNodes[ch] = append(charNodes[ch], node)
			if count > 0 {
				charCounts[ch]--
				charCount--
				if charCount == 0 {
					l = indexList.Front().Value.(int)
					r = indexList.Back().Value.(int)
					minWindow = r - l
				}
			} else {
				node := charNodes[ch][0]
				charNodes[ch] = charNodes[ch][1:]
				indexList.Remove(node)
				front := indexList.Front().Value.(int)
				back := indexList.Back().Value.(int)
				if charCount == 0 && back-front < minWindow {
					minWindow = back - front
					l = front
					r = back
				}
			}
		}
	}
	return s[l : r+1]
}
