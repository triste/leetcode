package problem1647

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestMindeletions(t *testing.T) {
	scenarios := [...]struct {
		s      string
		output int
	}{
		0: {
			s:      "aab",
			output: 0,
		},
		1: {
			s:      "aaabbbcc",
			output: 2,
		},
		2: {
			s:      "ceabaacb",
			output: 2,
		},
		3: {
			s:      "abcabc",
			output: 3,
		},
		4: {
			s:      "bbcebab",
			output: 2,
		},
		5: {
			s:      "adec",
			output: 3,
		},
		6: {
			s:      "beaddedbacdcd",
			output: 5,
		},
	}
	for i, scenario := range scenarios {
		output := minDeletions(scenario.s)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
