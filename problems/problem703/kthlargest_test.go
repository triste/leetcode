package problem703

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestKthLargest(t *testing.T) {
	scenarios := [...]struct {
		add    int
		output int
	}{
		0: {
			add:    3,
			output: 4,
		},
		1: {
			add:    5,
			output: 5,
		},
		2: {
			add:    10,
			output: 5,
		},
		3: {
			add:    9,
			output: 8,
		},
		4: {
			add:    4,
			output: 8,
		},
	}
	kthLargest := Constructor(1, []int{4, 5, 8, 2})
	for i, scenario := range scenarios {
		output := kthLargest.Add(scenario.add)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
