package problem138

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestCopyrandomlist(t *testing.T) {
	var headIn0, headOut0 *Node
	{
		nodeIn4 := &Node{
			Val:    1,
			Next:   nil,
			Random: nil,
		}
		nodeIn3 := &Node{
			Val:    10,
			Next:   nodeIn4,
			Random: nil,
		}
		nodeIn2 := &Node{
			Val:    11,
			Next:   nodeIn3,
			Random: nil,
		}
		nodeIn1 := &Node{
			Val:    13,
			Next:   nodeIn2,
			Random: nil,
		}
		nodeIn0 := &Node{
			Val:    7,
			Next:   nodeIn1,
			Random: nil,
		}
		nodeIn0.Random = nil
		nodeIn1.Random = nodeIn0
		nodeIn2.Random = nodeIn4
		nodeIn3.Random = nodeIn2
		nodeIn4.Random = nodeIn0
		headIn0 = nodeIn0
		nodeOut4 := &Node{
			Val:    1,
			Next:   nil,
			Random: nil,
		}
		nodeOut3 := &Node{
			Val:    10,
			Next:   nodeOut4,
			Random: nil,
		}
		nodeOut2 := &Node{
			Val:    11,
			Next:   nodeOut3,
			Random: nil,
		}
		nodeOut1 := &Node{
			Val:    13,
			Next:   nodeOut2,
			Random: nil,
		}
		nodeOut0 := &Node{
			Val:    7,
			Next:   nodeOut1,
			Random: nil,
		}
		nodeOut0.Random = nil
		nodeOut1.Random = nodeOut0
		nodeOut2.Random = nodeOut4
		nodeOut3.Random = nodeOut2
		nodeOut4.Random = nodeOut0
		headOut0 = nodeOut0
	}
	var headIn1, headOut1 *Node
	{
		nodeIn1 := &Node{
			Val:    2,
			Next:   nil,
			Random: nil,
		}
		nodeIn0 := &Node{
			Val:    1,
			Next:   nodeIn1,
			Random: nil,
		}
		nodeIn0.Random = nodeIn1
		nodeIn1.Random = nodeIn1
		headIn1 = nodeIn0
		nodeOut1 := &Node{
			Val:    2,
			Next:   nil,
			Random: nil,
		}
		nodeOut0 := &Node{
			Val:    1,
			Next:   nodeOut1,
			Random: nil,
		}
		nodeOut0.Random = nodeOut1
		nodeOut1.Random = nodeOut1
		headOut1 = nodeOut0
	}
	var headIn2, headOut2 *Node
	{
		nodeIn2 := &Node{
			Val:    3,
			Next:   nil,
			Random: nil,
		}
		nodeIn1 := &Node{
			Val:    3,
			Next:   nodeIn2,
			Random: nil,
		}
		nodeIn0 := &Node{
			Val:    3,
			Next:   nodeIn1,
			Random: nil,
		}
		nodeIn0.Random = nil
		nodeIn1.Random = nodeIn0
		nodeIn2.Random = nil
		headIn2 = nodeIn0
		nodeOut2 := &Node{
			Val:    3,
			Next:   nil,
			Random: nil,
		}
		nodeOut1 := &Node{
			Val:    3,
			Next:   nodeOut2,
			Random: nil,
		}
		nodeOut0 := &Node{
			Val:    3,
			Next:   nodeOut1,
			Random: nil,
		}
		nodeOut0.Random = nil
		nodeOut1.Random = nodeOut0
		nodeOut2.Random = nil
		headOut2 = nodeOut0
	}
	scenarios := [...]struct {
		head   *Node
		output *Node
	}{
		0: {
			head:   headIn0,
			output: headOut0,
		},
		1: {
			head:   headIn1,
			output: headOut1,
		},
		2: {
			head:   headIn2,
			output: headOut2,
		},
	}
	for i, scenario := range scenarios {
		output := copyRandomList(scenario.head)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
