package problem143

import (
	. "gitlab.com/triste/leetcode/pkg/list"
)

func reorderList(head *ListNode) {
	if head.Next == nil {
		return
	}
	slow := head
	for fast := head.Next.Next; fast != nil && fast.Next != nil; fast = fast.Next.Next {
		slow = slow.Next
	}
	var prev *ListNode
	slow, slow.Next = slow.Next, nil
	for current := slow; current != nil; {
		next := current.Next
		current.Next = prev
		prev = current
		current = next
	}
	for left, right := head, prev;; {
		rightNext := right.Next
		leftNext := left.Next
		left.Next = right
		if leftNext == nil {
			break
		}
		right.Next = leftNext
		left = leftNext
		right = rightNext
	}
	return
}
