package problem215

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestFindkthlargest(t *testing.T) {
	scenarios := [...]struct {
		nums   []int
		k      int
		output int
	}{
		0: {[]int{3, 2, 1, 5, 6, 4}, 2, 5},
		1: {[]int{3, 2, 3, 1, 2, 4, 5, 5, 6}, 4, 4},
	}
	for i, scenario := range scenarios {
		output := findKthLargest(scenario.nums, scenario.k)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
