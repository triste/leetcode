package problem9

import (
	"testing"

	"github.com/google/go-cmp/cmp"
	. "gitlab.com/triste/leetcode/pkg/list"
)

func TestLettercombinations(t *testing.T) {
	scenarios := [...]struct {
		nums   *ListNode
		target int
		s      string
		output *ListNode
	}{
		0: {},
		1: {},
	}
	for i, scenario := range scenarios {
		letterCombinations(scenario.nums, scenario.target, scenario.s)
		if diff := cmp.Diff(scenario.output, scenario.nums); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
