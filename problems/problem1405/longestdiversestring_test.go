package problem1405

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestLongestdiversestring(t *testing.T) {
	scenarios := [...]struct {
		a      int
		b      int
		c      int
		output string
	}{
		0: {
			a:      1,
			b:      1,
			c:      7,
			output: "ccaccbcc",
		},
		1: {
			a:      7,
			b:      1,
			c:      0,
			output: "aabaa",
		},
	}
	for i, scenario := range scenarios {
		output := longestDiverseString(scenario.a, scenario.b, scenario.c)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
