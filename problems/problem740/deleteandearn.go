package problem740

import (
	"sort"
)

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func deleteAndEarn(nums []int) (output int) {
	n := len(nums)
	counts := make(map[int]int, n)
	uniques := make([]int, 0, n)
	for _, num := range nums {
		if counts[num] == 0 {
			uniques = append(uniques, num)
		}
		counts[num]++
	}
	sort.Ints(uniques)
	for i := 0; i < len(uniques); i++ {
		first, second := 0, uniques[i]*counts[uniques[i]]
		for i+1 < len(uniques) && uniques[i+1] == uniques[i]+1 {
			first, second = second, max(first+uniques[i+1]*counts[uniques[i+1]], second)
			i++
		}
		output += second
	}
	return output
}

/*
	sum = 9
	  | 2 | 3 | 4 |
	0 |   |   |   |
	1 |   |   |   |

	if 2, then {4}, score = 6
	if 3, then {}, score = 3
	if 4, then {2}, score =
*/
