package problem51

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestSolvenqueens(t *testing.T) {
	scenarios := [...]struct {
		n      int
		output [][]string
	}{
		0: {
			n: 4,
			output: [][]string{
				{
					".Q..",
					"...Q",
					"Q...",
					"..Q.",
				},
				{
					"..Q.",
					"Q...",
					"...Q",
					".Q..",
				},
			},
		},
		1: {
			n: 1,
			output: [][]string{
				{"Q"},
			},
		},
	}
	for i, scenario := range scenarios {
		output := solveNQueens(scenario.n)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
