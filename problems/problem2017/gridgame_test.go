package problem2017

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestGridgame(t *testing.T) {
	scenarios := [...]struct {
		grid   [][]int
		output int64
	}{
		0: {
			grid: [][]int{
				{2, 5, 4},
				{1, 5, 1},
			},
			output: 4,
		},
		1: {
			grid: [][]int{
				{3, 3, 1},
				{8, 5, 2},
			},
			output: 4,
		},
		2: {
			grid: [][]int{
				{1, 3, 1, 15},
				{1, 3, 3, 1},
			},
			output: 7,
		},
		3: {
			grid: [][]int{
				{20, 3, 20, 17, 2, 12, 15, 17, 4, 15},
				{20, 10, 13, 14, 15, 5, 2, 3, 14, 3},
			},
			output: 63,
		},
	}
	for i, scenario := range scenarios {
		output := gridGame(scenario.grid)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
