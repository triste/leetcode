package problem18

import "sort"

func fourSum(nums []int, target int) (output [][]int) {
	sort.Ints(nums)
	last := len(nums) - 1
	for i, first := range nums {
		if i > 0 && first == nums[i-1] {
			continue
		}
		for j := i + 1; j < len(nums); j++ {
			second := nums[j]
			if j > i+1 && second == nums[j-1] {
				continue
			}
			left := j + 1
			right := last
			for left < right {
				third := nums[left]
				fourth := nums[right]
				sum := first + second + third + fourth
				if sum < target {
					left++
				} else if sum > target {
					right--
				} else {
					output = append(output, []int{first, second, third, fourth})
					for {
						left++
						if left >= right || nums[left] != third {
							break
						}
					}
				}
			}
		}
	}
	return
}
