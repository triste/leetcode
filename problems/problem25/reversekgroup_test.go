package problem25

import (
	"testing"

	"github.com/google/go-cmp/cmp"
	. "gitlab.com/triste/leetcode/pkg/list"
)

func TestReverseKGroup(t *testing.T) {
	scenarios := [...]struct {
		head   *ListNode
		k      int
		output *ListNode
	}{
		0: {
			NewListFromSlice(1, 2, 3, 4, 5),
			2,
			NewListFromSlice(2, 1, 4, 3, 5),
		},
		1: {
			NewListFromSlice(1, 2, 3, 4, 5),
			3,
			NewListFromSlice(3, 2, 1, 4, 5),
		},
		2: {
			NewListFromSlice(1),
			1,
			NewListFromSlice(1),
		},
		3: {
			NewListFromSlice(1, 2),
			2,
			NewListFromSlice(2, 1),
		},
	}
	for i, scenario := range scenarios {
		output := reverseKGroup(scenario.head, scenario.k)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
