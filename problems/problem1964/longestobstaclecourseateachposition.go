package problem1964

import "sort"

/*
5, 3
                    5,1,1
                   /
               3,1,1

5, 3, 7
                    5,1,1
                   /     \
               3,1,1      7,2,2

5, 3, 7, 6
                    5,1,1
                   /     \
               3,1,1      7,2,2
                          /
                       6,2

5, 3, 7, 8
                    5,1,1
                   /     \
               3,1,1      7,2,2
                            \
                            8,3,3

5, 3, 7, 4
                    5,1,1
                   /     \
               3,1,1      7,2,2
                  \
                   4,2,2

5, 3, 7, 1
                    5,1,1
                   /     \
               3,1,1      7,2,2
               /
             1,1,1

*/

/*
            5,3,4
		   /
	   1,2,0
	      \
		   3,3,0
		    \
			 4,4,0
*/

/*
             5,4,3
			/     \
	    2,1,3
	   /    \
    1,3,0	4,2,0
	        /
		  3,
*/

/*
             3,1,1
			/    \
		1,1,0     5,2,0
		         /    \
			   4,2,0   6,3,0
*/

type Obstacle struct {
	left   *Obstacle
	right  *Obstacle
	height int
	oclen  int
}

func (o *Obstacle) Insert(height int) int {
	oclen := o.insert(height, 0)
	return oclen
}

func (o *Obstacle) insert(height, oclen int) int {
	if height > o.height {
		if o.oclen > oclen {
			oclen = o.oclen
		}
		if o.right == nil {
			o.right = &Obstacle{nil, nil, height, oclen + 1}
			return o.right.oclen
		} else {
			oclen := o.right.insert(height, oclen)
			return oclen
		}
	} else if height < o.height {
		if o.left == nil {
			o.left = &Obstacle{nil, nil, height, oclen + 1}
			if o.left.oclen > o.oclen {
				o.oclen = o.left.oclen
			}
			return o.left.oclen
		} else {
			oclen := o.left.insert(height, oclen)
			if oclen > o.oclen {
				o.oclen = oclen
			}
			return oclen
		}
	} else {
		if oclen > o.oclen {
			o.oclen = oclen
		}
		o.oclen++
		return o.oclen
	}
}

func longestObstacleCourseAtEachPositionTree(obstacles []int) []int {
	output := make([]int, len(obstacles))
	output[0] = 1
	tree := Obstacle{nil, nil, obstacles[0], 1}
	for i := 1; i < len(obstacles); i++ {
		longest := tree.Insert(obstacles[i])
		output[i] = longest
	}
	return output
}

func longestObstacleCourseAtEachPosition(obstacles []int) []int {
	output := make([]int, len(obstacles))
	lis := make([]int, 0)
	for i, height := range obstacles {
		j := sort.SearchInts(lis, height+1)
		if j == len(lis) {
			lis = append(lis, height)
		} else {
			lis[j] = height
		}
		output[i] = j + 1
	}
	return output
}

/*
                   2,4,1
				  /     \
			 1,1,0      5,2,5
						/
					 3,2,0
					      \
						   4,5,0
*/

/*
                  9,1,6
				 /     \
			1,3,0       10,3,0
			     \
				 6,4,6
				/    \
			5,5,6    7,4,0
			/             \
		2,4,0              8,5,0
		    \
			 4,6,3
			/
		   3,3,0
*/
