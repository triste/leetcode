package problem219

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestContainsnearbyduplicate(t *testing.T) {
	scenarios := [...]struct {
		nums   []int
		k      int
		output bool
	}{
		0: {
			nums:   []int{1, 2, 3, 1},
			k:      3,
			output: true,
		},
		1: {
			nums:   []int{1, 0, 1, 1},
			k:      1,
			output: true,
		},
		2: {
			nums:   []int{1, 2, 3, 1, 2, 3},
			k:      2,
			output: false,
		},
		3: {
			nums:   []int{1, 2, 1},
			k:      0,
			output: false,
		},
	}
	for i, scenario := range scenarios {
		output := containsNearbyDuplicate(scenario.nums, scenario.k)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
