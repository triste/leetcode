#!/bin/python

import argparse
import re
import os

class Field:
    name: str
    type: str
    def __init__(self, field: str) -> None:
        name, typef = field.strip().split(' ', 1)
        self.name = name.strip()
        self.type = typef.strip()

class Config:
    problem_number: int = 0
    function_name: str = ""
    function_params: list[Field] = []
    longest_param_name_lenght: int = 0
    function_return_type: str | None = None
    def __init__(self, argv: list[str] | None = None) -> None:
        parser = argparse.ArgumentParser(description='Problem solution template generator')
        parser.add_argument('number', type=int, help='Problem number')
        parser.add_argument('signature', type=str, help='Function signature')
        args = parser.parse_args(argv)
        self.problem_number = args.number
        fname, fparams, fret = re.split(' *[()] *', args.signature)
        self.function_name = fname
        self.longest_param_name_lenght = 0
        for param in fparams.split(','):
            field = Field(param)
            if (lenght := len(field.name)) > self.longest_param_name_lenght:
                self.longest_param_name_lenght = lenght
            self.function_params.append(Field(param))
        self.function_return_type = fret if fret else None

    def get_types(self) -> list[str]:
        types = []
        for field in self.function_params:
            types.append(field.type)
        if self.function_return_type:
            types.append(self.function_return_type)
        return types

def gen_solution(config: Config, additional_imports: list[str]) -> str:
    import_stmt = ''
    if additional_imports:
        import_stmt = '\nimport (\n'
        for imp in additional_imports:
            import_stmt += f'\t. {imp}\n'
        import_stmt += ')\n'

    function_params_stmt = ''
    for i, param in enumerate(config.function_params, 1):
        function_params_stmt += f'{param.name} {param.type}'
        if i != len(config.function_params):
            function_params_stmt += ', '
    function_return_stmt = ' '
    if config.function_return_type:
        function_return_stmt = f' (output {config.function_return_type}) '
    solution = f'''\
package problem{config.problem_number}
{import_stmt}
func {config.function_name}({function_params_stmt}){function_return_stmt}{{
	return
}}
'''
    return solution

def gen_test(config: Config, additional_imports: list[str]) -> str:
    import_stmt = f'''\
import (
	"testing"

	"github.com/google/go-cmp/cmp"
'''
    for imp in additional_imports:
        import_stmt += f'\t. {imp}\n'
    import_stmt += ')'
    max_field_name_lenght = config.longest_param_name_lenght
    if (l := len('output')) > max_field_name_lenght:
        max_field_name_lenght = l
    scenario_layout = ''
    for f in config.function_params:
        scenario_layout += f'\t\t{f.name.ljust(max_field_name_lenght, " ")} {f.type}\n'
    output_field_name = 'output'
    scenario_layout += f'\t\t{output_field_name.ljust(max_field_name_lenght)} '
    if config.function_return_type:
        scenario_layout += f'{config.function_return_type}'
    else:
        scenario_layout += f'{config.function_params[0].type}'
    call_function_stmt = ''
    if config.function_return_type:
        call_function_stmt = f'output := '
    call_function_stmt += f'{config.function_name}('
    for i, param in enumerate(config.function_params, 1):
        call_function_stmt += f'scenario.{param.name}'
        if i != len(config.function_params):
            call_function_stmt += ', '
    call_function_stmt += ')'
    compare_with_stmt = ''
    if config.function_return_type:
        compare_with_stmt = 'output'
    else:
        compare_with_stmt = f'scenario.{config.function_params[0].name}'
    test = f'''\
package problem{config.problem_number}

{import_stmt}

func Test{config.function_name.capitalize()}(t *testing.T) {{
	scenarios := [...]struct {{
{scenario_layout}
	}}{{
		0: {{
            
        }},
		1: {{
            
        }},
        2: {{
            
        }},
	}}
	for i, scenario := range scenarios {{
		{call_function_stmt}
		if diff := cmp.Diff(scenario.output, {compare_with_stmt}); diff != "" {{
			t.Errorf("scenario [%v] (-want +got):\\n%s", i, diff)
		}}
	}}
}}
'''
    return test


def main() -> None:
    config = Config()
    workdir = f'problems/problem{config.problem_number}'
    os.makedirs(workdir)
    solution_file = open(f'{workdir}/{config.function_name.lower()}.go', 'x')
    solution_test_file = open(f'{workdir}/{config.function_name.lower()}_test.go', 'x')
    additional_imports = []
    all_types = config.get_types()
    for t in all_types:
        if 'ListNode' in t:
            additional_imports.append('"gitlab.com/triste/leetcode/pkg/list"')
            break
    for t in all_types:
        if 'TreeNode' in t:
            additional_imports.append('"gitlab.com/triste/leetcode/pkg/tree"')
            break
    solution = gen_solution(config, additional_imports)
    solution_test = gen_test(config, additional_imports)
    solution_file.write(solution)
    solution_test_file.write(solution_test)

if __name__ == '__main__':
    main()
