package problem1349

import (
	"math/bits"
)

func maxStudents(seats [][]byte) (output int) {
	m := len(seats)
	n := len(seats[0])
	brokens := make([]uint8, m)
	for i, row := range seats {
		for j, seat := range row {
			if seat == '#' {
				brokens[i] |= 1 << j
			}
		}
	}
	memo := make(map[[2]uint8]int, 0)
	var solve func(r int, prevMask uint8) int
	solve = func(r int, prevMask uint8) int {
		if c, ok := memo[[2]uint8{uint8(r), prevMask}]; ok || r >= len(seats) {
			return c
		}
		maxStudentCount := 0
		inaccessibleMask := brokens[r] | prevMask<<1 | prevMask>>1
		for state := 0; state < 1<<n; state++ {
			state := uint8(state)
			if state&(inaccessibleMask|state>>1) != 0 {
				continue
			}
			studentCount := bits.OnesCount8(state) + solve(r+1, state)
			if studentCount > maxStudentCount {
				maxStudentCount = studentCount
			}
		}
		memo[[2]uint8{uint8(r), prevMask}] = maxStudentCount
		return maxStudentCount
	}
	return solve(0, 0)
}
