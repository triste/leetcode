package problem894

import (
	"testing"

	"github.com/google/go-cmp/cmp"
	. "gitlab.com/triste/leetcode/pkg/tree"
)

func TestAllpossiblefbt(t *testing.T) {
	scenarios := [...]struct {
		n      int
		output []*TreeNode
	}{
		0: {
			n: 7,
			output: []*TreeNode{
				NewTree(0, 0, 0, nil, nil, 0, 0, nil, nil, 0, 0),
				NewTree(0, 0, 0, nil, nil, 0, 0, 0, 0),
				NewTree(0, 0, 0, 0, 0, 0, 0),
				NewTree(0, 0, 0, 0, 0, nil, nil, nil, nil, 0, 0),
				NewTree(0, 0, 0, 0, 0, nil, nil, 0, 0),
			},
		},
		1: {
			n: 3,
			output: []*TreeNode{
				NewTree(0, 0, 0),
			},
		},
	}
	for i, scenario := range scenarios {
		output := allPossibleFBT(scenario.n)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
