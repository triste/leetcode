package problem32

func longestValidParentheses(s string) int {
	openCount := 0
	closeCount := 0
	maxPairCount := 0
	for _, ch := range s {
		if ch == '(' {
			openCount++
		} else {
			closeCount++
			if openCount == closeCount {
				if openCount > maxPairCount {
					maxPairCount = openCount
				}
			} else if closeCount > openCount {
				openCount = 0
				closeCount = 0
			}
		}
	}
	openCount = 0
	closeCount = 0
	for i := len(s) - 1; i >= 0; i-- {
		if s[i] == '(' {
			openCount++
			if openCount == closeCount {
				if openCount > maxPairCount {
					maxPairCount = openCount
				}
			} else if openCount > closeCount {
				openCount = 0
				closeCount = 0
			}
		} else {
			closeCount++
		}
	}
	return maxPairCount * 2
}
