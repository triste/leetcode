package problem239

func maxSlidingWindow(nums []int, k int) []int {
	maxs := make([]int, len(nums)-k+1)
	queue := make([]int, 1, k)
	for i, num := range nums {
		if queue[0] == i-k {
			queue = queue[1:]
		}
		l, r := 0, len(queue)-1
		for l <= r {
			mid := l + (r-l)/2
			if nums[queue[mid]] > num {
				l = mid + 1
			} else {
				r = mid - 1
			}
		}
		if l == len(queue) {
			queue = append(queue, i)
		} else {
			queue[l] = i
			queue = queue[:l+1]
		}
		if i >= k-1 {
			maxs[i-k+1] = nums[queue[0]]
		}
	}
	return maxs
}
