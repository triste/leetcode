package problem38

func countAndSay(n int) (output string) {
	cursay := []byte{'1'}
	var nextsay []byte
	for i := 1; i < n; i++ {
		curdigit := cursay[0]
		var curdigitCount byte = '1'
		for _, d := range cursay[1:] {
			if d == curdigit {
				curdigitCount++
			} else {
				nextsay = append(nextsay, curdigitCount, curdigit)
				curdigit = d
				curdigitCount = '1'
			}
		}
		nextsay = append(nextsay, curdigitCount, curdigit)
		cursay = cursay[:0]
		cursay, nextsay = nextsay, cursay
	}
	return string(cursay)
}
