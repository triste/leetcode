package problem1615

func maximalNetworkRank(n int, roads [][]int) int {
	var roadCounts [101]uint8
	var citiesLinked [101][101]bool
	for _, road := range roads {
		roadCounts[road[0]]++
		roadCounts[road[1]]++
		citiesLinked[road[0]][road[1]] = true
		citiesLinked[road[1]][road[0]] = true
	}
	var (
		cities1, cities2 []int
		max1, max2       uint8
	)
	for city, roadCount := range roadCounts {
		if roadCount > max1 {
			cities1, cities2 = cities2, cities1
			cities1 = cities1[:0]
			max2 = max1
			max1 = roadCount
			cities1 = append(cities1, city)
		} else if roadCount == max1 {
			cities1 = append(cities1, city)
		} else if roadCount > max2 {
			max2 = roadCount
			cities2 = cities2[:0]
			cities2 = append(cities2, city)
		} else if roadCount == max2 {
			cities2 = append(cities2, city)
		}
	}
	if len(cities1) > 1 {
		for i, city1 := range cities1 {
			for _, city2 := range cities1[i+1:] {
				if !citiesLinked[city1][city2] {
					return int(max1) * 2
				}
			}
		}
		return int(max1)*2 - 1
	}
	for _, city := range cities2 {
		if !citiesLinked[city][cities1[0]] {
			return int(max1) + int(max2)
		}
	}
	return int(max1) + int(max2) - 1
}
