package problem11

func maxArea(height []int) (output int) {
	left := 0
	right := len(height) - 1
	for left < right {
		var water int
		if l, r := height[left], height[right]; l < r {
			water = l * (right - left)
			left++

		} else {
			water = r * (right - left)
			right--
		}
		if water > output {
			output = water
		}
	}
	return
}
