package problem2009

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestMinoperations(t *testing.T) {
	scenarios := [...]struct {
		nums   []int
		output int
	}{
		0: {
			nums:   []int{4, 2, 5, 3},
			output: 0,
		},
		1: {
			nums:   []int{1, 2, 3, 5, 6},
			output: 1,
		},
		2: {
			nums:   []int{1, 10, 100, 1000},
			output: 3,
		},
	}
	for i, scenario := range scenarios {
		output := minOperations(scenario.nums)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
