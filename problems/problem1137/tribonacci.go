package problem1137

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func tribonacci(n int) int {
	dp := make([]int, max(3, n+1))
	dp[0] = 0
	dp[1] = 1
	dp[2] = 1
	for i := 3; i < len(dp); i++ {
		dp[i] = dp[i-1] + dp[i-2] + dp[i-3]
	}
	return dp[n]
}
