package problem228

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestSummaryranges(t *testing.T) {
	scenarios := [...]struct {
		nums   []int
		output []string
	}{
		0: {
			nums:   []int{0, 1, 2, 4, 5, 7},
			output: []string{"0->2", "4->5", "7"},
		},
		1: {
			nums:   []int{0, 2, 3, 4, 6, 8, 9},
			output: []string{"0", "2->4", "6", "8->9"},
		},
	}
	for i, scenario := range scenarios {
		output := summaryRanges(scenario.nums)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
