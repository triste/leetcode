package problem2090

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func getAverages(nums []int, k int) []int {
	n := len(nums)
	averages := make([]int, n)
	sum := 0
	for i := 0; i < min(k*2, n); i++ {
		sum += nums[i]
		averages[i] = -1
	}
	for i := k; i < max(n-k, 0); i++ {
		sum += nums[i+k]
		averages[i] = sum / (k*2 + 1)
		sum -= nums[i-k]
	}
	for i := max(n-k, k); i < n; i++ {
		averages[i] = -1
	}
	return averages
}
