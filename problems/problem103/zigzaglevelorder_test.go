package problem103

import (
	"testing"

	"github.com/google/go-cmp/cmp"
	. "gitlab.com/triste/leetcode/pkg/tree"
)

func TestZigzaglevelorder(t *testing.T) {
	scenarios := [...]struct {
		root   *TreeNode
		output [][]int
	}{
		0: {
			root:   NewTree(3, 9, 20, nil, nil, 15, 7),
			output: [][]int{{3}, {20, 9}, {15, 7}},
		},
		1: {
			root:   NewTree(1),
			output: [][]int{{1}},
		},
		2: {
			root: NewTree(1,2,3,4,nil,nil,5),
			output: [][]int{{1},{3,2},{4,5}},
		},
	}
	for i, scenario := range scenarios {
		output := zigzagLevelOrder(scenario.root)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
