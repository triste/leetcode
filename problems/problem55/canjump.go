package problem55

func canJump(nums []int) bool {
	curlen := 0
	max := 0
	for i, n := range nums {
		if i > curlen {
			if max == curlen {
				return false
			}
			curlen = max
		}
		if i+n > max {
			max = i + n
		}
	}
	return true
}
