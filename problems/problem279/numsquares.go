package problem279

import "math"

func numSquares(n int) int {
	dp := make([]int, n+1)
	for i := 1; i <= n; i++ {
		dp[i] = math.MaxInt
		for j := 1; j*j <= i; j++ {
			if n := dp[i-j*j] + 1; n < dp[i] {
				dp[i] = n
			}
		}
	}
	return dp[n]
}
