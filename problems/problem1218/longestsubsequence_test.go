package problem1218

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestLongestsubsequence(t *testing.T) {
	scenarios := [...]struct {
		arr        []int
		difference int
		output     int
	}{
		0: {
			arr:        []int{1, 2, 3, 4},
			difference: 1,
			output:     4,
		},
		1: {
			arr:        []int{1, 3, 5, 7},
			difference: 1,
			output:     1,
		},
		2: {
			arr:        []int{1, 5, 7, 8, 5, 3, 4, 2, 1},
			difference: -2,
			output:     4,
		},
	}
	for i, scenario := range scenarios {
		output := longestSubsequence(scenario.arr, scenario.difference)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
