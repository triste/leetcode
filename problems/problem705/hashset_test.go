package problem705

import (
	"testing"
)

func TestHashset(t *testing.T) {
	hs := Constructor()
	hs.Add(1)
	hs.Add(2)
	if got := hs.Contains(1); got == false {
		t.Errorf("Contain(1) = %v; want true", got)
	}
	if got := hs.Contains(3); got == true {
		t.Errorf("Contain(3) = %v; want false", got)
	}
	hs.Add(2)
	if got := hs.Contains(2); got == false {
		t.Errorf("Contain(2) = %v; want true", got)
	}
	hs.Remove(2)
	if got := hs.Contains(2); got == true {
		t.Errorf("Contain(2) = %v; want false", got)
	}
}
