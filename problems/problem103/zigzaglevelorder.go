package problem103

import (
	. "gitlab.com/triste/leetcode/pkg/tree"
)

func zigzagLevelOrder(root *TreeNode) (output [][]int) {
	if root == nil {
		return nil
	}
	reverse := false
	stack := []*TreeNode{root}
	for len(stack) > 0 {
		newStack := []*TreeNode{}
		level := []int{}
		for _, node := range stack {
			if node.Left != nil {
				newStack = append(newStack, node.Left)
			}
			if node.Right != nil {
				newStack = append(newStack, node.Right)
			}
		}
		if reverse == false {
			for _, node := range stack {
				level = append(level, node.Val)
			}
			reverse = true
		} else {
			for i := len(stack) - 1; i >= 0; i-- {
				level = append(level, stack[i].Val)
			}
			reverse = false
		}
		stack = newStack
		output = append(output, level)
	}
	return
}
