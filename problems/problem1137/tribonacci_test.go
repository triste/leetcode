package problem1137

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestTribonacci(t *testing.T) {
	scenarios := [...]struct {
		n      int
		output int
	}{
		0: {
			n:      4,
			output: 4,
		},
		1: {
			n:      25,
			output: 1389537,
		},
		2: {
			n:      0,
			output: 0,
		},
		3: {
			n:      1,
			output: 1,
		},
		4: {
			n:      2,
			output: 1,
		},
	}
	for i, scenario := range scenarios {
		output := tribonacci(scenario.n)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
