package problem84

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestLargestrectanglearea(t *testing.T) {
	scenarios := [...]struct {
		heights []int
		output  int
	}{
		0: {
			heights: []int{2, 1, 5, 6, 2, 3},
			output:  10,
		},
		1: {
			heights: []int{2, 4},
			output:  4,
		},
		2: {
			heights: []int{0, 0},
			output:  0,
		},
		3: {
			heights: []int{1, 1},
			output:  2,
		},
		4: {
			heights: []int{4, 2, 0, 3, 2, 4, 3, 4},
			output:  10,
		},
		5: {
			heights: []int{3, 6, 5, 7, 4, 8, 1, 0},
			output:  20,
		},
	}
	for i, scenario := range scenarios {
		output := largestRectangleArea(scenario.heights)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
