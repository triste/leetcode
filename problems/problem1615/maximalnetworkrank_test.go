package problem1615

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestMaximalnetworkrank(t *testing.T) {
	scenarios := [...]struct {
		n      int
		roads  [][]int
		output int
	}{
		0: {
			n: 4,
			roads: [][]int{
				{0, 1}, {0, 3}, {1, 2}, {1, 3},
			},
			output: 4,
		},
		1: {
			n: 5,
			roads: [][]int{
				{0, 1}, {0, 3}, {1, 2}, {1, 3}, {2, 3}, {2, 4},
			},
			output: 5,
		},
		2: {
			n: 8,
			roads: [][]int{
				{0, 1}, {1, 2}, {2, 3}, {2, 4}, {5, 6}, {5, 7},
			},
			output: 5,
		},
	}
	for i, scenario := range scenarios {
		output := maximalNetworkRank(scenario.n, scenario.roads)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
