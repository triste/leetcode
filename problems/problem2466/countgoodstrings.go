package problem2466

func countGoodStrings(low int, high int, zero int, one int) (count int) {
	const mod = 1_000_000_000 + 7
	dp := make([]int, high+1)
	dp[0] = 1
	for i := 1; i < len(dp); i++ {
		if i >= zero {
			dp[i] += dp[i-zero]
		}
		if i >= one {
			dp[i] += dp[i-one]
		}
		dp[i] = dp[i] % mod
		if i >= low {
			count += dp[i]
			count %= mod
		}
	}
	return
}

/*
	low: 3, high: 3, zero: 1, one: 1

	3 3

	2 2
	2 2

	1 1
	1 1
	1 1
	1 1


*/
