package problem122

func maxProfit(prices []int) (maxProfit int) {
	for i, price := range prices[1:] {
		if price > prices[i] {
			maxProfit += price - prices[i]
		}
	}
	return
}

/*
	7,1,5,3,6,4
	(1,5),(3,6)

	1,2,3,4,5
	(1,2),(2,3),(3,4),(4,5)


	7,1,6,5,4
*/
