package problem686

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestRepeatedstringmatch(t *testing.T) {
	scenarios := [...]struct {
		a      string
		b      string
		output int
	}{
		0: {
			a:      "abcd",
			b:      "cdabcdab",
			output: 3,
		},
		1: {
			a:      "a",
			b:      "aa",
			output: 2,
		},
		2: {
			a:      "abc",
			b:      "cabcabca",
			output: 4,
		},
	}
	for i, scenario := range scenarios {
		output := repeatedStringMatch(scenario.a, scenario.b)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
