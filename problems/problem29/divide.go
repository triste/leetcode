package problem29

import "log"

func divide(dividend int, divisor int) int {
	var sign bool
	if dividend < 0 && divisor < 0 {
		sign = false
		dividend = -dividend
		divisor = -divisor
	} else if dividend < 0 {
		sign = true
		dividend = -dividend
	} else if divisor < 0 {
		sign = true
		divisor = -divisor
	}
	count := 0
	shifts := 0
	for dividend != 0 || divisor != 0 {
		if dividend&1 != 0 {
			count++
		}
		dividend >>= 1
		if divisor&1 != 0 {
			count--
		}
		divisor >>= 1
		shifts++
	}
	log.Println(shifts, sign, count)
	return 0
}
