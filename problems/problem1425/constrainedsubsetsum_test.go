package problem1425

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestConstrainedsubsetsum(t *testing.T) {
	scenarios := [...]struct {
		nums   []int
		k      int
		output int
	}{
		0: {
			nums:   []int{10, 2, -10, 5, 20},
			k:      2,
			output: 37,
		},
		1: {
			nums:   []int{-1, -2, -3},
			k:      1,
			output: -1,
		},
		2: {
			nums:   []int{10, -2, -10, -5, 20},
			k:      2,
			output: 23,
		},
		3: {
			nums:   []int{-5266, 4019, 7336, -3681, -5767},
			k:      2,
			output: 11355,
		},
		4: {
			nums:   []int{10, 10, 10, 10, -10, -1, -10, -1, -10, 10, 10, 10, 10},
			k:      3,
			output: 78,
		},
	}
	for i, scenario := range scenarios {
		output := constrainedSubsetSum(scenario.nums, scenario.k)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
