package problem1000

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestMergestones(t *testing.T) {
	scenarios := [...]struct {
		stones []int
		k      int
		output int
	}{
		0: {
			stones: []int{3, 2, 4, 1},
			k:      2,
			output: 20,
		},
		1: {
			stones: []int{3, 2, 4, 1},
			k:      3,
			output: -1,
		},
		2: {
			stones: []int{3, 5, 1, 2, 6},
			k:      3,
			output: 25,
		},
		3: {
			stones: []int{16, 43, 87, 30, 4, 98, 12, 30, 47, 45, 32, 4, 64, 14, 24, 84, 86, 51, 11, 22, 4},
			k:      2,
			output: 3334,
		},
		4: {
			stones: []int{1, 2},
			k:      2,
			output: 3,
		},
	}
	for i, scenario := range scenarios {
		output := mergeStones(scenario.stones, scenario.k)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
