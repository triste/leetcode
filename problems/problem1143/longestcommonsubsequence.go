package problem1143

func max(a, b int16) int16 {
	if a > b {
		return a
	}
	return b
}

func longestCommonSubsequence(text1 string, text2 string) int {
	if len(text1) > len(text2) {
		text1, text2 = text2, text1
	}
	dp := make([]int16, len(text1)+1)
	dpPrev := make([]int16, len(text1)+1)
	for _, ch1 := range text2 {
		for j, ch2 := range text1 {
			if ch1 == ch2 {
				dp[j+1] = dpPrev[j] + 1
			} else {
				dp[j+1] = max(dpPrev[j+1], dp[j])
			}
		}
		dp, dpPrev = dpPrev, dp
	}
	return int(dpPrev[len(text1)])
}

/*
   | a | b | c | d | e |
 a | 1 | 1 | 1 | 1 | 1 |
 c | 1 | 1 | 2 | 2 | 2 |
 e | 1 | 1 | 2 | 2 | 3 |

*/
