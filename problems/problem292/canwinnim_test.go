package problem292

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestCanwinnim(t *testing.T) {
	scenarios := [...]struct {
		n      int
		output bool
	}{
		0: {
			n:      4,
			output: false,
		},
		1: {
			n:      1,
			output: true,
		},
		2: {
			n:      2,
			output: true,
		},
	}
	for i, scenario := range scenarios {
		output := canWinNim(scenario.n)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
