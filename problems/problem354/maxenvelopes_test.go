package problem354

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestMaxenvelopes(t *testing.T) {
	scenarios := [...]struct {
		envelopes [][]int
		output    int
	}{
		0: {
			envelopes: [][]int{{5, 4}, {6, 4}, {6, 7}, {2, 3}},
			output:    3,
		},
		1: {
			envelopes: [][]int{{1, 1}, {1, 1}, {1, 1}},
			output:    1,
		},
		2: {
			envelopes: [][]int{
				{4, 5}, {4, 6}, {6, 7}, {2, 3}, {1, 1},
			},
			output: 4,
		},
		3: {
			envelopes: [][]int{
				{30, 50}, {12, 2}, {3, 4}, {12, 15},
			},
			output: 3,
		},
		4: {
			envelopes: [][]int{
				{2, 100},
				{3, 200},
				{4, 300},
				{5, 500},
				{5, 400},
				{5, 250},
				{6, 370},
				{6, 360},
				{7, 380},
			},
			output: 5,
		},
		5: {
			envelopes: [][]int{
				{13, 13},
				{13, 13},
				{17, 2},
				{13, 11},
				{4, 6},
				{7, 7},
				{10, 3},
				{1, 3},
				{20, 18},
				{11, 2},
				{8, 7},
				{1, 13},
				{4, 16},
				{14, 16},
				{2, 10},
				{1, 14},
				{5, 7},
				{3, 12},
				{2, 16},
			},
			output: 6,
		},
	}
	for i, scenario := range scenarios {
		output := maxEnvelopes(scenario.envelopes)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
