package problem80

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestRemoveduplicates(t *testing.T) {
	scenarios := [...]struct {
		nums   []int
		output int
	}{
		0: {
			nums:   []int{1, 1, 1, 2, 2, 3},
			output: 5,
		},
		1: {
			nums:   []int{0, 0, 1, 1, 1, 1, 2, 3, 3},
			output: 7,
		},
	}
	for i, scenario := range scenarios {
		output := removeDuplicates(scenario.nums)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
