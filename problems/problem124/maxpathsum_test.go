package problem124

import (
	"testing"

	"github.com/google/go-cmp/cmp"
	. "gitlab.com/triste/leetcode/pkg/tree"
)

func TestMaxpathsum(t *testing.T) {
	scenarios := [...]struct {
		root   *TreeNode
		output int
	}{
		0: {
			root:   NewTree(1, 2, 3),
			output: 6,
		},
		1: {
			root:   NewTree(-10, 9, 20, nil, nil, 15, 7),
			output: 42,
		},
		2: {
			root:   NewTree(2, -1),
			output: 2,
		},
		3: {
			root:   NewTree(1, -2, 3),
			output: 4,
		},
		4: {
			root:   NewTree(9, 6, -3, nil, nil, -6, 2, nil, nil, 2, nil, -6, -6, -6),
			output: 16,
		},
	}
	for i, scenario := range scenarios {
		output := maxPathSum(scenario.root)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
