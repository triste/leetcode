package problem132

func minCut(s string) int {
	isPalindrome := make([][]bool, len(s))
	for i := range isPalindrome {
		isPalindrome[i] = make([]bool, len(s))
	}
	dp := make([]int, len(s)+1)
	for l := len(s) - 1; l >= 0; l-- {
		dp[l] = len(s) - l
		for r := l; r < len(s); r++ {
			if isPalindrome[l][r] || ((r-l < 2 || isPalindrome[l+1][r-1]) && s[l] == s[r]) {
				isPalindrome[l][r] = true
				if dp[r+1]+1 < dp[l] {
					dp[l] = dp[r+1] + 1
				}
			}
		}
	}
	return dp[0] - 1
}

/*
	bbabbb
*/

/*
  | a | a | b | $ |
a | 1 |   |   |   |
a |   | 1 |   |   |
b |   |   | 1 |   |
$ |   |   |   | 0 |
*/

/*
	abb|b
	abb: min = 1, {a}, {bb}
	abbb:
*/

/*
  | a | b | b | b | c | $ |
a | 1 | 2 |   |   |   |   |
b |   | 1 | 1 |   |   |   |
b |   |   | 1 | 1 |   |   |
b |   |   |   | 1 | 2 |   |
c |   |   |   |   | 1 |   |
$ |   |   |   |   |   | 0 |
*/
