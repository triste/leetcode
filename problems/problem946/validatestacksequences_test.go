package problem946

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestValidatestacksequences(t *testing.T) {
	scenarios := [...]struct {
		pushed []int
		popped []int
		output bool
	}{
		0: {
			pushed: []int{1, 2, 3, 4, 5},
			popped: []int{4, 5, 3, 2, 1},
			output: true,
		},
		1: {
			pushed: []int{1, 2, 3, 4, 5},
			popped: []int{4, 3, 5, 1, 2},
			output: false,
		},
	}
	for i, scenario := range scenarios {
		output := validateStackSequences(scenario.pushed, scenario.popped)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
