package problem703

import (
	"sort"
)

type KthLargest struct {
	minheap []int
	k       int
}

func Constructor(k int, nums []int) KthLargest {
	sort.Ints(nums)
	if len(nums) < k {
		nums = append(nums, 0)
		copy(nums[1:], nums[:k])
		nums[0] = -10001
	}
	return KthLargest{
		minheap: nums[len(nums)-k:],
		k:       k,
	}
}

func (this *KthLargest) Add(val int) int {
	if this.minheap[0] > val {
		return this.minheap[0]
	}
	this.minheap[0] = val
	for i := 0; ; {
		l, r := i*2+1, i*2+2
		j := i
		if l < len(this.minheap) && this.minheap[l] < this.minheap[j] {
			j = l
		}
		if r < len(this.minheap) && this.minheap[r] < this.minheap[j] {
			j = r
		}
		if j == i {
			break
		}
		this.minheap[i], this.minheap[j] = this.minheap[j], this.minheap[i]
		i = j
	}
	return this.minheap[0]
}
