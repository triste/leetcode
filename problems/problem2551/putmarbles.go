package problem2551

import (
	"sort"
)

func putMarbles(weights []int, k int) int64 {
	pairSums := make([]int, len(weights)-1)
	for i := range pairSums {
		pairSums[i] = weights[i] + weights[i+1]
	}
	sort.Ints(pairSums)
	output := 0
	for i := 0; i < k-1; i++ {
		output += pairSums[len(weights)-2-i] - pairSums[i]
	}
	return int64(output)
}
