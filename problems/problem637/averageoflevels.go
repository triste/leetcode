package problem637

import (
	. "gitlab.com/triste/leetcode/pkg/tree"
)

func averageOfLevels(root *TreeNode) (averages []float64) {
	queue := []*TreeNode{root}
	var tmpQueue []*TreeNode
	for len(queue) > 0 {
		sum := 0
		for _, node := range queue {
			sum += node.Val
			if node.Left != nil {
				tmpQueue = append(tmpQueue, node.Left)
			}
			if node.Right != nil {
				tmpQueue = append(tmpQueue, node.Right)
			}
		}
		averages = append(averages, float64(sum)/float64(len(queue)))
		queue, tmpQueue = tmpQueue, queue[:0]
	}
	return
}
