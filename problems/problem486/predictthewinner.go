package problem486

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func PredictTheWinner(nums []int) bool {
	return PredictTheWinnerIterativeSpaceOptimized(nums)
}

func PredictTheWinnerRecursive(nums []int) bool {
	var play func(l, r int) int
	play = func(l, r int) int {
		if l > r {
			return 0
		}
		left := nums[l] - play(l+1, r)
		right := nums[r] - play(l, r-1)
		return max(left, right)
	}
	score := play(0, len(nums)-1)
	if score >= 0 {
		return true
	}
	return false
}

func PredictTheWinnerIterative(nums []int) bool {
	/*
		 l\r| 1 | 5 | 233 | 7 |
		 1  | 1 |   |     |   |
		 5  |   | 5 |     |   |
		233 |   |   | 233 |   |
		 7  |   |   |     | 7 |
	*/
	dp := make([][]int, len(nums))
	for i := range dp {
		dp[i] = make([]int, len(nums))
		dp[i][i] = nums[i]
	}
	for diff := 1; diff < len(dp); diff++ {
		for l := 0; l < len(dp)-diff; l++ {
			r := l + diff
			dp[l][r] = max(nums[l]-dp[l+1][r], nums[r]-dp[l][r-1])
		}
	}
	return dp[0][len(dp[0])-1] >= 0
}

func PredictTheWinnerIterativeSpaceOptimized(nums []int) bool {
	dp := make([]int, len(nums))
	copy(dp, nums)
	for diff := 1; diff < len(dp); diff++ {
		for r := len(dp) - 1; r >= diff; r-- {
			l := r - diff
			dp[r] = max(nums[l]-dp[r], nums[r]-dp[r-1])
		}
	}
	return dp[len(dp)-1] >= 0
}
