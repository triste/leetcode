package problem391

import (
	"math"
)

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func isRectangleCover(rectangles [][]int) bool {
	corners := make(map[uint64]struct{})
	minX, minY := math.MaxInt, math.MaxInt
	maxX, maxY := math.MinInt, math.MinInt
	area := 0
	for _, rec := range rectangles {
		minX = min(minX, rec[0])
		minY = min(minY, rec[1])
		maxX = max(maxX, rec[2])
		maxY = max(maxY, rec[3])
		area += (rec[2] - rec[0]) * (rec[3] - rec[1])
		for _, x := range []int{rec[0], rec[2]} {
			for _, y := range []int{rec[1], rec[3]} {
				key := (uint64(x)+100000)<<32 | (uint64(y) + 100000)
				if _, ok := corners[key]; ok {
					delete(corners, key)
				} else {
					corners[key] = struct{}{}
				}
			}
		}
	}
	if len(corners) != 4 {
		return false
	}
	boundary := [...][2]int{
		{minX, minY},
		{minX, maxY},
		{maxX, minY},
		{maxX, maxY},
	}
	for _, corner := range boundary {
		key := (uint64(corner[0])+100000)<<32 | (uint64(corner[1]) + 100000)
		if _, ok := corners[key]; !ok {
			return false
		}
	}
	return (maxX-minX)*(maxY-minY) == area
}

/*
	case 1:
		|
		|--
		|
	case 2:
		|
	  --|--
		|
*/
