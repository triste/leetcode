package problem12

func intToRoman(num int) (output string) {
	digits := make([]int, 0, 4)
	for ; num != 0; num /= 10 {
		digits = append(digits, num % 10)
	}
	romans := [4][2]byte{
		{'I', 'V'},
		{'X', 'L'},
		{'C', 'D'},
		{'M', '.'},
	}
	bytes := make([]byte, 0, 16)
	for i := len(digits) - 1; i >= 0; i-- {
		switch digit := digits[i]; digit {
		case 4:
			bytes = append(bytes, romans[i][0], romans[i][1])
		case 9:
			bytes = append(bytes, romans[i][0], romans[i+1][0])
		case 5, 6, 7, 8:
			bytes = append(bytes, romans[i][1])
			digit -= 5
			fallthrough
		case 1, 2, 3:
			for j := 0; j < digit; j++ {
				bytes = append(bytes, romans[i][0])
			}
		}
	}
	return string(bytes)
}
