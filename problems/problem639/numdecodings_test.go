package problem639

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestNumdecodings(t *testing.T) {
	scenarios := [...]struct {
		s      string
		output int
	}{
		0: {
			s:      "*",
			output: 9,
		},
		1: {
			s:      "1*",
			output: 18,
		},
		2: {
			s:      "2*",
			output: 15,
		},
		3: {
			s:      "**",
			output: 96,
		},
		4: {
			s:      "*********",
			output: 291868912,
		},
		5: {
			s:      "*0**0",
			output: 36,
		},
	}
	for i, scenario := range scenarios {
		output := numDecodings(scenario.s)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
