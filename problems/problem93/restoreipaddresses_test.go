package problem93

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestRestoreipaddresses(t *testing.T) {
	scenarios := [...]struct {
		s      string
		output []string
	}{
		0: {
			s: "25525511135",
			output: []string{
				"255.255.11.135",
				"255.255.111.35",
			},
		},
		1: {
			s:      "0000",
			output: []string{"0.0.0.0"},
		},
		2: {
			s: "101023",
			output: []string{
				"1.0.10.23",
				"1.0.102.3",
				"10.1.0.23",
				"10.10.2.3",
				"101.0.2.3",
			},
		},
	}
	for i, scenario := range scenarios {
		output := restoreIpAddresses(scenario.s)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
