package problem1802

/*
case 1:
	0 0 1 2 3 2 1 0 0
case 2:
	2 3 2 1 0 0 0 0 0
case 3:
	2 3 4 5 6 5 4 3 2
*/

func maxValue(n int, index int, maxSum int) int {
	left, right := 1, maxSum
	leftCount, rightCount := index+1, n-index
	for left < right {
		mid := (left + right + 1) / 2
		// Sn := (2*n - (l-1))*l/2
		var lsum, rsum int
		if mid > index {
			lsum = (2*mid - leftCount + 1) * leftCount / 2
		} else {
			lsum = (1+mid)*mid/2 + leftCount - mid
		}
		if mid < n-index {
			rsum = (1+mid)*mid/2 + rightCount - mid
		} else {
			rsum = (2*mid - rightCount + 1) * rightCount / 2
		}
		if lsum+rsum-mid <= maxSum {
			left = mid
		} else {
			right = mid - 1
		}
	}
	return left
}

/*
 */

/*
	n: 3
	index: 2
	maxSum: 18
	x1,x2,x3

	maxSum - n = 15
	4,5,6 = 15

*/

/*
	n: 8
	index: 7
	maxSum: 14

	x1,x2,x3,x4,x5,x6,x7,x8
	min := max(1, x8 - 7)
	min + min + 1 + min + 2 + min + 3 + min + 4 + min + 5 + min + 6 + x8 <= 14
	minCound := 7

	7min + n + x8 <= 14
	x8 <= 7
	7 + n + x8 <= 14
	x8 + n <= 7
	1,2,3

	(2+(x-1)x)/2 <= 7
	1+(x-1)x/2 <= 7
	(x-1)x/2 <= 6
	(x-1)x <= 12
	x^-x-12 <= 0
	D = 1+48 = 49
	x1 = 1+7/2 = 4
	x2 = 1-7/2 =

	sl := 21
	7min + 21 + x8 <= 14
	7min + x8 <= -7
	7(x8-7) + x8 <= -7
	8x8 - 49 <= -7
	8x8 <= 42
	7+x8 <= -7
	or
	7+x8 <= -7
	x8 <= -14
*/

/*
	n: 6
	index: 1
	maxSum: 10

	x1, x2, x3, x4, x5
	        |
			index

	x2 = x3 - 1
	x4 = x3 - 1
	x1 = x2 - 1
	x5 = x4 - 1

	x1 = x2 - 1 = x3 - 2
	x5 = x3 - 2
*/

/*
	n: 4
	index: 2
	maxSum: 6

	x1, x2, x3, x4
	x1 = max(1, x3 - 2)
	x2 = max(1, x3 - 1)
	x4 = max(1, x3 - 1)

	x1 + x2 + x3 + x4 <= 6
	min := max(1, x3 - 2)
	min + min + 1 + x3 + min + 1 <= 6
	3min + 2 + x3 <= 6
	3min + x3 <= 4

	x3 <= 2 =>
	3 + x3 <= 4
	x3 <= 1

	x3 > 2 =>
	3(x3-2) + x3 <= 4
	3x3-6 + x3 <= 4
	4x3 <= 10
	x3 <= 10/4


	n: 6
	index: 1
	maxSum: 10

	x1,x2,x3,x4,x5,x6
	min := max(1, x2 - 4)
	min + min + 1 + min + 2 + min + 3 + x2 + min + 3 <= 10
	5min + 9 + x2 <= 10
	5min + x2 <= 1

	if x2 <= 4; then
		5 + x2 <= 1
		x2 <= -4
	else
		5(x2-4) + x2 <= 1
		5x2-20 + x2 <= 1
		6x2 <= 21
		x2 <= 3.6
*/
