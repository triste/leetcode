package problem1799

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestMaxscore(t *testing.T) {
	scenarios := [...]struct {
		nums   []int
		output int
	}{
		0: {
			nums:   []int{1, 2},
			output: 1,
		},
		1: {
			nums:   []int{3, 4, 6, 8},
			output: 11,
		},
		2: {
			nums:   []int{1, 2, 3, 4, 5, 6},
			output: 14,
		},
		3: {
			nums:   []int{39759, 619273, 859218, 228161, 944571, 597983, 483239, 179849, 868130, 909935, 912143, 817908, 738222, 653224},
			output: 782,
		},
		4: {
			nums:   []int{415, 230, 471, 705, 902, 87},
			output: 23,
		},
		5: {
			nums:   []int{9, 17, 16, 15, 18, 13, 18, 20},
			output: 91,
		},
	}
	for i, scenario := range scenarios {
		output := maxScore(scenario.nums)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
