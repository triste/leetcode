package problem236

import . "gitlab.com/triste/leetcode/pkg/tree"

func lowestCommonAncestor(root, p, q *TreeNode) *TreeNode {
	if root == nil || root == p || root == q {
		return root
	}
	left := lowestCommonAncestor(root.Left, p, q)
	right := lowestCommonAncestor(root.Right, p, q)
	if left != nil {
		if right != nil {
			return root
		}
		return left
	} else if right != nil {
		return right
	} else {
		return nil
	}
}
