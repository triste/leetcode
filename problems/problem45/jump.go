package problem45

/*
	2, 3, 1, 1, 4
	[-----]
	   [--------]
	      [--]
		     [--]
*/
/*
	5, 9, 3, 2, 1, 0, 2, 3, 3, 1, 0, 0
	[--------------]
	   [--------------------------]
	      [--------]
		     [-----]
			    [--]
				      [-----]
					     [--------]
						    [--------]
							   [--]
*/

/*
	1, 1, 2, 1, 1
	[--]
	   [--]
	      [-----]
		     [--]
*/

/*
	1, 2, 3
	[--]
	   [-----]
*/

func jump(nums []int) int {
	jumpCount := 0
	end := 0
	longest := 0
	for i, n := range nums {
		if i > end {
			end = longest
			jumpCount++
		}
		if i+n > longest {
			longest = i + n
		}
	}
	return jumpCount
}
