package problem65

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestIsnumber(t *testing.T) {
	scenarios := [...]struct {
		s      string
		output bool
	}{
		0: {
			s:      "0",
			output: true,
		},
		1: {
			s:      "e",
			output: false,
		},
		2: {
			s:      ".",
			output: false,
		},
		3: {
			s:      "0089",
			output: true,
		},
		4: {
			s:      "-0.1",
			output: true,
		},
		5: {
			s:      "+3.14",
			output: true,
		},
		6: {
			s:      "4.",
			output: true,
		},
		7: {
			s:      "-.9",
			output: true,
		},
		8: {
			s:      "2e10",
			output: true,
		},
		9: {
			s:      "-90E3",
			output: true,
		},
		10: {
			s:      "3e+7",
			output: true,
		},

		11: {
			s:      "+6e-1",
			output: true,
		},
		12: {
			s:      "53.5e93",
			output: true,
		},
		13: {
			s:      "-123.456e789",
			output: true,
		},
		14: {
			s:      "abc",
			output: false,
		},
		15: {
			s:      "1a",
			output: false,
		},
		16: {
			s:      "1e",
			output: false,
		},
		17: {
			s:      "e3",
			output: false,
		},
		18: {
			s:      "99e2.5",
			output: false,
		},
		19: {
			s:      "--6",
			output: false,
		},
		20: {
			s:      "-+3",
			output: false,
		},
		21: {
			s:      "95a54e53",
			output: false,
		},
		22: {
			s:      "+.",
			output: false,
		},
		23: {
			s:      "-1.",
			output: true,
		},
		24: {
			s:      "+E3",
			output: false,
		},
		25: {
			s:      ".0e7",
			output: true,
		},
		26: {
			s:      ".e1",
			output: false,
		},
		27: {
			s:      "32.e-80123",
			output: true,
		},
		28: {
			s:      ".703e+4144",
			output: true,
		},
		29: {
			s:      "-.7e+0435",
			output: true,
		},
		30: {
			s:      "+.E3",
			output: false,
		},
	}
	for i, scenario := range scenarios {
		output := isNumber(scenario.s)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
