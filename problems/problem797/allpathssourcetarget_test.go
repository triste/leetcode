package problem797

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestAllpathssourcetarget(t *testing.T) {
	scenarios := [...]struct {
		graph  [][]int
		output [][]int
	}{
		0: {[][]int{
			{1, 2}, {3}, {3}, {},
		}, [][]int{
			{0, 1, 3}, {0, 2, 3},
		}},
		1: {[][]int{
			{4, 3, 1}, {3, 2, 4}, {3}, {4}, {},
		}, [][]int{
			{0, 4}, {0, 3, 4}, {0, 1, 4}, {0, 1, 3, 4}, {0, 1, 2, 3, 4},
		}},
	}
	for i, scenario := range scenarios {
		output := allPathsSourceTarget(scenario.graph)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
