package problem115

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestNumdistinct(t *testing.T) {
	scenarios := [...]struct {
		s      string
		t      string
		output int
	}{
		0: {
			s:      "rabbbit",
			t:      "rabbit",
			output: 3,
		},
		1: {
			s:      "babgbag",
			t:      "bag",
			output: 5,
		},
		2: {
			s:      "adbdadeecadeadeccaeaabdabdbcdabddddabcaaadbabaaedeeddeaeebcdeabcaaaeeaeeabcddcebddebeebedaecccbdcbcedbdaeaedcdebeecdaaedaacadbdccabddaddacdddc",
			t:      "bcddceeeebecbc",
			output: 700531452,
		},
	}
	for i, scenario := range scenarios {
		output := numDistinct(scenario.s, scenario.t)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
