package problem1752

func check(nums []int) bool {
	rotated := false
	for i, num := range nums[1:] {
		if num < nums[i] {
			if rotated {
				return false
			}
			rotated = true
		}
	}
	if rotated {
		return nums[len(nums)-1] <= nums[0]
	}
	return true
}
