package problem223

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestComputearea(t *testing.T) {
	scenarios := [...]struct {
		ax1    int
		ay1    int
		ax2    int
		ay2    int
		bx1    int
		by1    int
		bx2    int
		by2    int
		output int
	}{
		0: {
			ax1:    -3,
			ay1:    0,
			ax2:    3,
			ay2:    4,
			bx1:    0,
			by1:    -1,
			bx2:    9,
			by2:    2,
			output: 45,
		},
		1: {
			ax1:    -2,
			ay1:    -2,
			ax2:    2,
			ay2:    2,
			bx1:    -2,
			by1:    -2,
			bx2:    2,
			by2:    2,
			output: 16,
		},
	}
	for i, scenario := range scenarios {
		output := computeArea(scenario.ax1, scenario.ay1, scenario.ax2, scenario.ay2, scenario.bx1, scenario.by1, scenario.bx2, scenario.by2)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
