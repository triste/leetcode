package problem207

func canFinish(numCourses int, prerequisites [][]int) bool {
	graph := make([][]int, numCourses)
	for _, preq := range prerequisites {
		graph[preq[0]] = append(graph[preq[0]], preq[1])
	}
	finished := make([]bool, numCourses)
	visited := make([]bool, numCourses)
	processed := make([]bool, numCourses)
	var dfs func(i int)
	dfs = func(i int) {
		if processed[i] {
			return
		}
		if visited[i] {
			finished[i] = false
		} else {
			visited[i] = true
			finished[i] = true
			for _, e := range graph[i] {
				dfs(e)
				if !finished[e] {
					finished[i] = false
					break
				}
			}
		}
		visited[i] = false
		processed[i] = true
	}
	for i := range graph {
		dfs(i)
		if !finished[i] {
			return false
		}
	}
	return true
}
