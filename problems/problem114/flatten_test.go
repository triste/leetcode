package problem114

import (
	"testing"

	"github.com/google/go-cmp/cmp"
	. "gitlab.com/triste/leetcode/pkg/tree"
)

func TestFlatten(t *testing.T) {
	scenarios := [...]struct {
		root   *TreeNode
		output *TreeNode
	}{
		0: {
			root:   NewTree(1, 2, 5, 3, 4, nil, 6),
			output: NewTree(1, nil, 2, nil, 3, nil, 4, nil, 5, nil, 6),
		},
		1: {
			root:   nil,
			output: nil,
		},
		2: {
			root:   NewTree(0),
			output: NewTree(0),
		},
	}
	for i, scenario := range scenarios {
		flatten(scenario.root)
		if diff := cmp.Diff(scenario.output, scenario.root); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
