package problem2369

func validPartition(nums []int) bool {
	return validPartitionIterative(nums)
}

func validPartitionRecursive(nums []int) bool {
	memo := make(map[int]bool, len(nums)+1)
	memo[len(nums)] = true
	var solve func(i int) bool
	solve = func(i int) bool {
		if v, ok := memo[i]; ok {
			return v
		}
		if i+1 < len(nums) {
			if nums[i] == nums[i+1] {
				if solve(i + 2) {
					memo[i] = true
					return true
				}
			}
		}
		if i+2 < len(nums) {
			if (nums[i] == nums[i+1] && nums[i] == nums[i+2]) || (nums[i]+1 == nums[i+1] && nums[i+1]+1 == nums[i+2]) {
				if solve(i + 3) {
					memo[i] = true
					return true
				}
			}
		}
		memo[i] = false
		return false
	}
	return solve(0)
}

func validPartitionIterative(nums []int) bool {
	/*
		| 4 | 4 | 4 | 5 | 6 |   |
		|   |   |   | f | f | t |
	*/
	dp1 := nums[len(nums)-2] == nums[len(nums)-1]
	for dp2, dp3, i := false, true, len(nums)-3; i >= 0; i-- {
		dp1, dp2, dp3 = nums[i] == nums[i+1] && (dp2 || nums[i+1] == nums[i+2] && dp3) ||
			nums[i]+1 == nums[i+1] && nums[i+1]+1 == nums[i+2] && dp3, dp1, dp2
	}
	return dp1
}
