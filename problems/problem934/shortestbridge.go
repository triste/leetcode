package problem934

type Cell struct {
	r int
	c int
}

func shortestBridge(grid [][]int) (output int) {
	n := len(grid)
	offsets := [...]Cell{{-1, 0}, {0, 1}, {1, 0}, {-1, -1}}
	queue := surroundingWaters(grid)
	var tmpQueue []Cell
	for bridgeLenght := 1; ; bridgeLenght++ {
		for _, cell := range queue {
			for _, offset := range offsets {
				r, c := cell.r+offset.r, cell.c+offset.c
				if r < 0 || r >= n || c < 0 || c >= n {
					continue
				}
				if grid[r][c] == 1 {
					return bridgeLenght
				}
				if grid[r][c] != 0 {
					continue
				}
				grid[r][c] = 2
				tmpQueue = append(tmpQueue, Cell{r, c})
			}
		}
		queue, tmpQueue = tmpQueue, queue[:0]
	}
}

func surroundingWaters(grid [][]int) (waters []Cell) {
	n := len(grid)
	var dfs func(r, c int)
	dfs = func(r, c int) {
		if r < 0 || r >= n || c < 0 || c >= n {
			return
		}
		switch grid[r][c] {
		case 0:
			grid[r][c] = 2
			waters = append(waters, Cell{r, c})
		case 1:
			grid[r][c] = 2
			dfs(r-1, c)
			dfs(r+1, c)
			dfs(r, c-1)
			dfs(r, c+1)
		}
	}
	for i, row := range grid {
		for j, cell := range row {
			if cell == 1 {
				dfs(i, j)
				return
			}
		}
	}
	return
}
