package problem312

func maxCoins(nums []int) int {
	return maxCoinsIterative(nums)
}

func maxCoinsRecursive(nums []int) int {
	nums = append([]int{1}, nums...)
	nums = append(nums, 1)
	memo := make([][]int, len(nums))
	for i := range memo {
		memo[i] = make([]int, len(nums))
	}
	var solve func(l, r int) int
	solve = func(l, r int) int {
		if memo[l][r] > 0 {
			return memo[l][r]
		}
		maxCoins := 0
		for i := l; i <= r; i++ {
			coins := nums[l-1]*nums[i]*nums[r+1] + solve(l, i-1) + solve(i+1, r)
			if coins > maxCoins {
				maxCoins = coins
			}
		}
		memo[l][r] = maxCoins
		return maxCoins
	}
	return solve(1, len(nums)-2)
}

func maxCoinsIterative(nums []int) int {
	/*
		  | 1 | 3 | 1 | 5 | 8 | 1 |
		1 |   |   |   |   |   |   |
		3 |y1 |y2 |y3 |y4 |x1 |   |
		1 |   |   |   |   |y1'|   |
		5 |   |   |   |   |y2'|   |
		8 |   |   |   |   |y3'|   |
		1 |   |   |   |   |y4'|   |
	*/
	nums = append([]int{1}, nums...)
	nums = append(nums, 1)
	dp := make([][]int, len(nums))
	for i := range dp {
		dp[i] = make([]int, len(nums))
	}
	for diff := 0; diff < len(dp)-2; diff++ {
		for l := 1; l < len(dp)-1-diff; l++ {
			r := l + diff
			maxCoins := 0
			for i := l; i <= r; i++ {
				coins := nums[l-1]*nums[i]*nums[r+1] + dp[l][i-1] + dp[i+1][r]
				if coins > maxCoins {
					maxCoins = coins
				}
			}
			dp[l][r] = maxCoins
		}
	}
	return dp[1][len(dp)-2]
}

/*
	case (x0, x1, x2):
		1) 1*x0*1' => case(x1,x2)
		2) 1*x1*1' => case(x0,x0) + case(x2,x2)
		3) 1*x2*1' => case(x0,x1)
*/
