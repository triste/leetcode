package problem139

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestWordbreak(t *testing.T) {
	scenarios := [...]struct {
		s        string
		wordDict []string
		output   bool
	}{
		0: {
			s:        "leetcode",
			wordDict: []string{"leet", "code"},
			output:   true,
		},
		1: {
			s:        "applepenapple",
			wordDict: []string{"apple", "pen"},
			output:   true,
		},
		2: {
			s:        "catsandog",
			wordDict: []string{"cats", "dog", "sand", "and", "cat"},
			output:   false,
		},
	}
	for i, scenario := range scenarios {
		output := wordBreak(scenario.s, scenario.wordDict)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
