package problem68

func buildLine(buffer []byte, words []string, extraSpaceCount int) {
	b := 0
	for ; b < len(words[0]); b++ {
		buffer[b] = words[0][b]
	}
	if len(words) > 1 {
		spacePerWordCount := extraSpaceCount/(len(words)-1) + 1
		extraSpaceCount %= (len(words) - 1)
		for w, word := range words[1:] {
			spaceCount := spacePerWordCount
			if w < extraSpaceCount {
				spaceCount++
			}
			for j := 0; j < spaceCount; j++ {
				buffer[b] = ' '
				b++
			}
			for j := 0; j < len(word); j++ {
				buffer[b] = word[j]
				b++
			}
		}
	}
	for ; b < len(buffer); b++ {
		buffer[b] = ' '
	}
}

func fullJustify(words []string, maxWidth int) (lines []string) {
	buffer := make([]byte, maxWidth)
	for i := 0; i < len(words); {
		start := i
		width := len(words[start])
		for i++; i < len(words) && width+1+len(words[i]) <= maxWidth; i++ {
			width += 1 + len(words[i])
		}
		extraSpaceCount := 0
		if i < len(words) {
			extraSpaceCount = maxWidth - width
		}
		buildLine(buffer, words[start:i], extraSpaceCount)
		lines = append(lines, string(buffer))
	}
	return
}
