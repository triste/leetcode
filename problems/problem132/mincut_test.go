package problem132

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestMincut(t *testing.T) {
	scenarios := [...]struct {
		s      string
		output int
	}{
		0: {
			s:      "aab",
			output: 1,
		},
		1: {
			s:      "a",
			output: 0,
		},
		2: {
			s:      "ab",
			output: 1,
		},
		3: {
			s:      "abbb",
			output: 1,
		},
		4: {
			s:      "abbbc",
			output: 2,
		},
	}
	for i, scenario := range scenarios {
		output := minCut(scenario.s)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
