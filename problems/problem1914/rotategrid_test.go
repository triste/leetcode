package problem1914

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestRotategrid(t *testing.T) {
	scenarios := [...]struct {
		grid   [][]int
		k      int
		output [][]int
	}{
		0: {
			grid: [][]int{
				{40, 10},
				{30, 20},
			},
			k: 1,
			output: [][]int{
				{10, 20},
				{40, 30},
			},
		},
		1: {
			grid: [][]int{
				{1, 2, 3, 4},
				{5, 6, 7, 8},
				{9, 10, 11, 12},
				{13, 14, 15, 16},
			},
			k: 2,
			output: [][]int{
				{3, 4, 8, 12},
				{2, 11, 10, 16},
				{1, 7, 6, 15},
				{5, 9, 13, 14},
			},
		},
	}
	for i, scenario := range scenarios {
		output := rotateGrid(scenario.grid, scenario.k)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
