package problem1420

func numOfArrays(n int, m int, k int) int {
	memo := make(map[[3]int]int)
	var solve func(i, cost, max int) int
	solve = func(i, cost, max int) int {
		key := [3]int{i, cost, max}
		if val, ok := memo[key]; ok {
			return val
		}
		if i == n {
			if cost == k {
				return 1
			}
			return 0
		}
		count := 0
		if cost < k {
			for j := max + 1; j <= m; j++ {
				count += solve(i+1, cost+1, j)
				count %= 1_000_000_007
			}
		}
		count += max * solve(i+1, cost, max) % 1_000_000_007
		count %= 1_000_000_007
		memo[key] = count
		return count
	}
	count := solve(0, 0, 0)
	return count
}

func numOfArraysDP(n int, m int, k int) int {
	return 0
}

/*
k = 0:
	num = 0 for any n >= 1 && m >= 1
k = 1:
	5, 4, 3, 2, 1
	num =
k = 2:

*/
