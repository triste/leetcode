package problem2466

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestCountgoodstrings(t *testing.T) {
	scenarios := [...]struct {
		low    int
		high   int
		zero   int
		one    int
		output int
	}{
		0: {
			low:    3,
			high:   3,
			zero:   1,
			one:    1,
			output: 8,
		},
		1: {
			low:    2,
			high:   3,
			zero:   1,
			one:    2,
			output: 5,
		},
		2: {
			low:    5,
			high:   5,
			zero:   2,
			one:    4,
			output: 0,
		},
		3: {
			low:    200,
			high:   200,
			zero:   10,
			one:    1,
			output: 764262396,
		},
	}
	for i, scenario := range scenarios {
		output := countGoodStrings(scenario.low, scenario.high, scenario.zero, scenario.one)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
