package problem1337

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestKweakestrows(t *testing.T) {
	scenarios := [...]struct {
		mat    [][]int
		k      int
		output []int
	}{
		0: {
			mat: [][]int{
				{1, 1, 0, 0, 0},
				{1, 1, 1, 1, 0},
				{1, 0, 0, 0, 0},
				{1, 1, 0, 0, 0},
				{1, 1, 1, 1, 1},
			},
			k:      3,
			output: []int{2, 0, 3},
		},
		1: {
			mat: [][]int{
				{1, 0, 0, 0},
				{1, 1, 1, 1},
				{1, 0, 0, 0},
				{1, 0, 0, 0},
			},
			k:      2,
			output: []int{0, 2},
		},
		2: {
			mat: [][]int{
				{1, 1, 1, 0, 0, 0, 0},
				{1, 1, 1, 1, 1, 1, 0},
				{0, 0, 0, 0, 0, 0, 0},
				{1, 1, 1, 0, 0, 0, 0},
				{1, 1, 1, 1, 1, 1, 1},
			},
			k:      4,
			output: []int{2, 0, 3, 1},
		},
	}
	for i, scenario := range scenarios {
		output := kWeakestRows(scenario.mat, scenario.k)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
