package problem1799

import (
	"math/bits"
)

func greatestCommonDivisor(a, b int) int {
	for b != 0 {
		a, b = b, a%b
	}
	return a
}

func maxScore(nums []int) int {
	dp := make([]int, 1<<(len(nums)-1))
	for bitmask := 0; bitmask < 1<<len(nums); bitmask++ {
		onesCount := bits.OnesCount16(uint16(bitmask))
		if onesCount&1 > 0 {
			continue
		}
		start := bits.TrailingZeros16(uint16(bitmask))
		end := 16 - bits.LeadingZeros16(uint16(bitmask))
		if onesCount == 2 {
			dp[bitmask>>1] = greatestCommonDivisor(nums[start], nums[end-1])
			continue
		}
		maxScore := 0
		opIdx := onesCount >> 1
		for i := start; i < end; i++ {
			imask := 1 << i
			if bitmask&imask == 0 {
				continue
			}
			for j := i + 1; j < end; j++ {
				jmask := 1 << j
				if bitmask&jmask == 0 {
					continue
				}
				pairMask := imask | jmask
				score := opIdx*dp[pairMask>>1] + dp[(bitmask^pairMask)>>1]
				if score > maxScore {
					maxScore = score
				}
			}
		}
		dp[bitmask>>1] = maxScore
	}
	return dp[1<<(len(nums)-1)-1]
}

/*
	0000
	0001
	0010
	0011 x
	0100
	0101 x
	0110 x
	0111
	1000
	1001 x
	1010 x
	1011
	1100 x
	1101
	1110
	1111 x
*/
