package problem986

func intervalIntersection(firstList [][]int, secondList [][]int) (intersections [][]int) {
	for i, j := 0, 0; i < len(firstList) && j < len(secondList); {
		firstBegin, firstEnd := firstList[i][0], firstList[i][1]
		secondBegin, secondEnd := secondList[j][0], secondList[j][1]
		if secondEnd < firstBegin {
			j++
			continue
		}
		if secondBegin > firstEnd {
			i++
			continue
		}
		var end int
		if firstEnd > secondEnd {
			end = secondEnd
			j++
		} else {
			end = firstEnd
			i++
		}
		begin := max(firstBegin, secondBegin)
		intersections = append(intersections, []int{begin, end})

	}
	return
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}
