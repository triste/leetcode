package problem516

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func longestPalindromeSubseq(s string) (longest int) {
	n := len(s)
	dp := make([][]int, n+1)
	for i := range dp {
		dp[i] = make([]int, n+1)
	}
	for i := range dp {
		dp[i][0] = 0
	}
	for i := range dp[0] {
		dp[0][i] = 0
	}
	for i := 1; i < len(dp); i++ {
		for j := 1; j < len(dp[i]); j++ {
			if s[i-1] == s[len(s)-j] {
				dp[i][j] = dp[i-1][j-1] + 1
			} else {
				dp[i][j] = max(dp[i-1][j], dp[i][j-1])
			}
		}
	}
	return dp[n][n]
}

/*
	DP

  | b | b | b | a | b |
b | 1 |   |   |   |   |
a |   | x | x | 1 |   |
b |   | 1 | 1 |   | 1 |
b |   |   | 1 | x | 1 |
b |   |   |   | x | 1 |

  | b | a | b | b | b |
b | 1 |   |   |   |   |
b |   | x | 1 |   |   |
b |   | 1 |   | 1 |   |
a |   |   | x | x | x |
b |   |   | 1 | 1 | 1 |

*/

/*
	bbbab
	babbb


	cbbd
	dbbc
*/
