package problem140

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestWordbreak(t *testing.T) {
	scenarios := [...]struct {
		s        string
		wordDict []string
		output   []string
	}{
		0: {
			s:        "catsanddog",
			wordDict: []string{"cat", "cats", "and", "sand", "dog"},
			output: []string{
				"cat sand dog",
				"cats and dog",
			},
		},
		1: {
			s:        "pineapplepenapple",
			wordDict: []string{"apple", "pen", "applepen", "pine", "pineapple"},
			output: []string{
				"pine apple pen apple",
				"pine applepen apple",
				"pineapple pen apple",
			},
		},
		2: {
			s:        "catsandog",
			wordDict: []string{"cats", "dog", "sand", "and", "cat"},
			output:   nil,
		},
	}
	for i, scenario := range scenarios {
		output := wordBreak(scenario.s, scenario.wordDict)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
