package problem25

import (

	. "gitlab.com/triste/leetcode/pkg/list"
)

func reverseKGroup(head *ListNode, k int) *ListNode {
	lenght := 1
	for node := head.Next; node != nil; node = node.Next {
		lenght++
	}
	root := ListNode{}
	prev := &root
	for i := 0; i < lenght / k; i++ {
		reversed, tail := reverse(head, k)
		prev.Next = reversed
		prev = head
		head = tail
	}
	prev.Next = head
	return root.Next
}

func reverse(head *ListNode, k int) (reversed, tail *ListNode) {
	prev := head
	current := head.Next
	prev.Next = nil
	for i := 1; i < k; i++ {
		next := current.Next
		current.Next = prev
		prev = current
		current = next
	}
	return prev, current
}
