package problem646

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestFindlongestchain(t *testing.T) {
	scenarios := [...]struct {
		pairs  [][]int
		output int
	}{
		0: {
			pairs:  [][]int{{1, 2}, {2, 3}, {3, 4}},
			output: 2,
		},
		1: {
			pairs:  [][]int{{1, 2}, {7, 8}, {4, 5}},
			output: 3,
		},
	}
	for i, scenario := range scenarios {
		output := findLongestChain(scenario.pairs)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
