package problem236

import (
	"reflect"
	"testing"

	. "gitlab.com/triste/leetcode/pkg/tree"
)

func Test_lowestCommonAncestor(t *testing.T) {
	type args struct {
		root *TreeNode
		p    *TreeNode
		q    *TreeNode
	}
	tree1 := NewTree(3, 5, 1, 6, 2, 0, 8, nil, nil, 7, 4)
	tests := []struct {
		name string
		args args
		want *TreeNode
	}{
		{
			"same level, different branches",
			args{tree1, tree1.Left, tree1.Right},
			tree1,
		},
		{
			"different levels, same branch",
			args{tree1, tree1.Left, tree1.Left.Right.Right},
			tree1.Left,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := lowestCommonAncestor(tt.args.root, tt.args.p, tt.args.q); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("lowestCommonAncestor() = %v, want %v", got, tt.want)
			}
		})
	}
}
