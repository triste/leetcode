package problem636

type FunctionState = uint8

const (
	FunctionStarted FunctionState = iota
	FunctionEnded
)

type Log struct {
	id    uint8
	state FunctionState
	time  uint32
}

func NewLogFromString(str string) Log {
	var i int
	var id uint8
	for i = 0; str[i] != ':'; i++ {
		id = id*10 + uint8(str[i]-'0')
	}
	var state FunctionState
	if i++; str[i] == 's' {
		state = FunctionStarted
		i += 6
	} else {
		state = FunctionEnded
		i += 4
	}
	var time uint32
	for ; i < len(str); i++ {
		time = time*10 + uint32(str[i]-'0')
	}
	return Log{
		id:    id,
		state: state,
		time:  time,
	}
}

func exclusiveTime(n int, logs []string) []int {
	times := make([]int, n)
	stack := make([]uint8, 0, len(logs)/2)
	var prevTime uint32
	for _, logStr := range logs {
		log := NewLogFromString(logStr)
		if log.state == FunctionStarted {
			if len(stack) > 0 {
				times[stack[len(stack)-1]] += int(log.time - prevTime)
			}
			prevTime = log.time
			stack = append(stack, log.id)
		} else {
			times[stack[len(stack)-1]] += int(log.time-prevTime) + 1
			prevTime = log.time + 1
			stack = stack[:len(stack)-1]
		}
	}
	return times
}
