package problem225

type Queue []int

func (q *Queue) PushBack(x int) {
	*q = append(*q, x)
}

func (q *Queue) PopFront() int {
	x := (*q)[0]
	*q = (*q)[1:]
	return x
}

func (q *Queue) PeekFront() int {
	return (*q)[0]
}

func (q Queue) Size() int {
	return len(q)
}

func (q Queue) Empty() bool {
	return len(q) == 0
}

type MyStack struct {
	queue Queue
}

func Constructor() MyStack {
	return MyStack{
		queue: nil,
	}
}

func (s *MyStack) Push(x int) {
	s.queue.PushBack(x)
	for i := s.queue.Size(); i > 1; i-- {
		s.queue.PushBack(s.queue.PopFront())
	}
}

func (s *MyStack) Pop() int {
	return s.queue.PopFront()
}

func (s *MyStack) Top() int {
	return s.queue.PeekFront()
}

func (s *MyStack) Empty() bool {
	return s.queue.Empty()
}
