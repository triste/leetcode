package problem1187

import (
	"sort"
)

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func makeArrayIncreasing(nums1, nums2 []int) int {
	return makeArrayIncreasingIterative(nums1, nums2)
	nums2 = sortUnique(nums2)
	memo := make([]map[int]int, len(nums1)+1)
	for i := range memo {
		memo[i] = make(map[int]int)
	}
	var solve func(i, prev int) int
	solve = func(i, prev int) int {
		if val, ok := memo[i][prev]; ok || i >= len(nums1) {
			return val
		}
		var opCount int
		if nums1[i] > prev {
			k := sort.SearchInts(nums2, prev+1)
			if k < len(nums2) {
				opCount = min(solve(i+1, nums1[i]), 1+solve(i+1, nums2[k]))
			} else {
				opCount = solve(i+1, nums1[i])
			}
		} else {
			k := sort.SearchInts(nums2, prev+1)
			if k < len(nums2) {
				opCount = 1 + solve(i+1, nums2[k])
			} else {
				opCount = 2001
			}
		}
		memo[i][prev] = opCount
		return opCount
	}
	if opCount := solve(0, -1); opCount < 2001 {
		return opCount
	}
	return -1
}

/*
	1,5,3,6,7
	1,3,6

	inf-> 1 -> 3
	3  -> 5 -> 6
	1  -> 3 -> 6
	3  -> 6 -> inf
	6  -> 7 -> inf
   | 1 | 5 | 3 | 6 | 7 |
1  | 0 | 0 | 1 |inf|inf|
5  |   | 0 | 1 | 1 | 1 |
3  |   |   | 0 | 0 | 0 |
6  |   |   |   | 0 | 0 |
7  |   |   |   |   | 0 |

	{-1: 0}
	i = 0, [i] = 1:
		{1: 0}
	i = 1, [i] = 5
		{5: 0, 3: 1}
	i = 2, [i] = 3
		{6: 1}
	i = 3, [i] = 6
		{}
*/

func sortUnique(nums []int) []int {
	sort.Ints(nums)
	j := 0
	for i := 1; i < len(nums); i++ {
		if nums[i] != nums[j] {
			j++
			nums[j] = nums[i]
		}
	}
	return nums[:j+1]
}

func makeArrayIncreasingIterative(nums1, nums2 []int) int {
	nexts := make(map[int]int, len(nums1)+len(nums2))
	sort.Ints(nums2)
	j := 0
	for i := 1; i < len(nums2); i++ {
		if nums2[i] != nums2[j] {
			nexts[nums2[j]] = nums2[i]
			j++
			nums2[j] = nums2[i]
		}
	}
	nums2 = nums2[:j+1]
	for _, num := range nums1 {
		if _, ok := nexts[num]; !ok {
			if i := sort.SearchInts(nums2, num+1); i < len(nums2) {
				nexts[num] = nums2[i]
			}
		}
	}
	nexts[-1] = nums2[0]
	ways := make(map[int]int)
	ways[-1] = 0
	for _, num := range nums1 {
		nextWays := make(map[int]int)
		for prevNum, opCount := range ways {
			if num > prevNum {
				if prevOpCount, ok := nextWays[num]; ok {
					nextWays[num] = min(prevOpCount, opCount)
				} else {
					nextWays[num] = opCount
				}
			}
			if next, ok := nexts[prevNum]; ok {
				if prevOpCount, ok := nextWays[next]; ok {
					nextWays[next] = min(prevOpCount, opCount+1)
				} else {
					nextWays[next] = opCount + 1
				}
			}
		}
		ways = nextWays
	}
	minOpCount := 2001
	for _, opCount := range ways {
		if opCount < minOpCount {
			minOpCount = opCount
		}
	}
	if minOpCount < 2001 {
		return minOpCount
	}
	return -1
}

/*
	-1,1,5,4,6,7
	1,2,3,4

case (1,0): 1,-1
	1 > -1, so keep or replace
		case keep:

	1==1 => keep
case (1,1): [1][1]=5,2
	5>2 => replace or keep







  | 1 | 5 | 4 | 6 | 7 |
-1|   |   |   |   |   |
1 |   |   |   |   |   |
5 |   |   |   |   |   |
4 |   |   |   |   |   |
6 |   |   |   |   |   |
7 |   |   |   |   |   |

*/
