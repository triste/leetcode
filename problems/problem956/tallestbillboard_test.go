package problem956

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestTallestbillboard(t *testing.T) {
	scenarios := [...]struct {
		rods   []int
		output int
	}{
		0: {
			rods:   []int{1, 2, 3, 6},
			output: 6,
		},
		1: {
			rods:   []int{1, 2, 3, 4, 5, 6},
			output: 10,
		},
		2: {
			rods:   []int{1, 2},
			output: 0,
		},
		3: {
			rods:   []int{1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 50, 50, 50, 150, 150, 150, 100, 100, 100, 123},
			output: 1023,
		},
	}
	for i, scenario := range scenarios {
		output := tallestBillboard(scenario.rods)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
