package problem173

import . "gitlab.com/triste/leetcode/pkg/tree"

type BSTIterator struct {
	stack []*TreeNode
}

func Constructor(root *TreeNode) BSTIterator {
	it := BSTIterator{}
	for lnode := root; lnode != nil; lnode = lnode.Left {
		it.stack = append(it.stack, lnode)
	}
	return it
}

func (it *BSTIterator) Next() int {
	last := len(it.stack) - 1
	next := it.stack[last]
	it.stack = it.stack[:last]
	for lnode := next.Right; lnode != nil; lnode = lnode.Left {
		it.stack = append(it.stack, lnode)
	}
	return next.Val
}

func (it *BSTIterator) HasNext() bool {
	return len(it.stack) != 0
}
