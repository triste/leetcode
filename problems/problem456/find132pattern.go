package problem456

import "math"

func find132pattern(nums []int) bool {
	var stack []int
	kth := math.MinInt
	for i := len(nums) - 1; i >= 0; i-- {
		if nums[i] < kth {
			return true
		}
		j := len(stack) - 1
		for j >= 0 && stack[j] < nums[i] {
			kth = stack[j]
			j--
		}
		stack = append(stack[:j+1], nums[i])
	}
	return false
}

/*
132 pattern:
	nums[i] < nums[k] < nums[j], where i < j < k
*/

/*
3,5,0,3,4

3,3,0,0,0
*/
