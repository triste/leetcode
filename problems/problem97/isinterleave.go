package problem97

func isInterleave(s1, s2, s3 string) bool {
	if len(s1)+len(s2) != len(s3) {
		return false
	}
	dp := make([]bool, len(s2)+1)
	dp[len(s2)] = true
	for i := len(s2) - 1; i >= 0 && s2[i] == s3[len(s1)+i]; i-- {
		dp[i] = true
	}
	for i := len(s1) - 1; i >= 0; i-- {
		dp[len(s2)] = dp[len(s2)] && s1[i] == s3[len(s2)+i]
		for j := len(s2) - 1; j >= 0; j-- {
			dp[j] = (s1[i] == s3[i+j] && dp[j]) || (s2[j] == s3[i+j] && dp[j+1])
		}
	}
	return dp[0]
}

/*
	s1 := "aabcc"
	s2 := "dbbca"
	s3 := "aadbbcbcac"

    i\j   0   1   2   3   4   5
        | d | b | b | c | a | $ |
	0 a |   |   |   |   |   | - |
	1 a |   |   |   |   |   | - |
	2 b |   |   | Y | X |   | - |
	3 c |   |   | X |   |   | - |
	4 c |   |   |   |   |   | + |
	5 $ | - | - | - | - | - | + |

	0 1 2 3 4 5 6 7 8 9
	a a d b b c b c a c
*/
