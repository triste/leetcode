package problem2038

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestWinnerofgame(t *testing.T) {
	scenarios := [...]struct {
		colors string
		output bool
	}{
		0: {
			colors: "AAABABB",
			output: true,
		},
		1: {
			colors: "AA",
			output: false,
		},
		2: {
			colors: "ABBBBBBBAAA",
			output: false,
		},
	}
	for i, scenario := range scenarios {
		output := winnerOfGame(scenario.colors)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
