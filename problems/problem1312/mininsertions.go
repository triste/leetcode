package problem1312

func max(a, b int16) int16 {
	if a > b {
		return a
	}
	return b
}

func minInsertions(s string) int {
	dp := make([]int16, len(s)+1)
	for i := len(s) - 1; i >= 0; i-- {
		var prev int16
		for j := 0; j < len(s); j++ {
			tmp := dp[j+1]
			if s[j] == s[i] {
				dp[j+1] = prev + 1
			} else {
				dp[j+1] = max(dp[j+1], dp[j])
			}
			prev = tmp
		}
	}
	return len(s) - int(dp[len(s)])
}

/*
  | m | b | a | d | m |
m | 1 | 1 | 1 | 1 | 1 |
d | 1 | 1 | 1 | 2 | 2 |
a | 1 | 1 | 2 | 2 | 2 |
b | 1 | 2 | 2 | 2 | 2 |
m | 1 | 2 | 2 | 2 | 3 |
*/
