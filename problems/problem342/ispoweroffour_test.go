package problem342

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestIspoweroffour(t *testing.T) {
	scenarios := [...]struct {
		n      int
		output bool
	}{
		0: {
			n:      16,
			output: true,
		},
		1: {
			n:      5,
			output: false,
		},
		2: {
			n:      1,
			output: true,
		},
		3: {
			n:      67108864,
			output: true,
		},
		4: {
			n:      -67108864,
			output: false,
		},
	}
	for i, scenario := range scenarios {
		output := isPowerOfFour(scenario.n)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
