package problem107

import (
	. "gitlab.com/triste/leetcode/pkg/tree"
)

func levelOrderBottom(root *TreeNode) (levels [][]int) {
	if root == nil {
		return nil
	}
	queue := []*TreeNode{root}
	var tmpQueue []*TreeNode
	for len(queue) > 0 {
		level := make([]int, len(queue))
		for i, node := range queue {
			level[i] = node.Val
			if node.Left != nil {
				tmpQueue = append(tmpQueue, node.Left)
			}
			if node.Right != nil {
				tmpQueue = append(tmpQueue, node.Right)
			}
		}
		levels = append(levels, level)
		queue, tmpQueue = tmpQueue, queue[:0]
	}
	for l, r := 0, len(levels)-1; l < r; l, r = l+1, r-1 {
		levels[l], levels[r] = levels[r], levels[l]
	}
	return
}
