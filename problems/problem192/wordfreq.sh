#!/bin/bash

declare -A word_counts
while read -ra line;
do
	for word in "${line[@]}";
	do
		echo "$word";
	done;
done < words.txt
