package problem1203

func sortItems(n int, m int, group []int, beforeItems [][]int) (output []int) {
	for i := range group {
		if group[i] == -1 {
			group[i] = m
			m++
		}
	}
	groupGraph := make(map[int]map[int]struct{}, m)
	itemGraph := make(map[int][]int, n)
	groupGraphIndegrees := make([]int, m)
	for i, deps := range beforeItems {
		for _, dep := range deps {
			itemGraph[dep] = append(itemGraph[dep], i)
			if group[dep] != group[i] {
				if groupGraph[group[dep]] == nil {
					groupGraph[group[dep]] = make(map[int]struct{})
				}
				if _, ok := groupGraph[group[dep]][group[i]]; !ok {
					groupGraph[group[dep]][group[i]] = struct{}{}
					groupGraphIndegrees[group[i]]++
				}
			}
		}
	}
	itemGraphIndegrees := make([]int, n)
	var queue []int
	for i, deps := range beforeItems {
		if len(deps) == 0 {
			queue = append(queue, i)
		} else {
			itemGraphIndegrees[i] = len(deps)
		}
	}
	itemVisitCount := 0
	groupItems := make([][]int, m)
	var tmpQueue []int
	for len(queue) > 0 {
		for _, item := range queue {
			groupItems[group[item]] = append(groupItems[group[item]], item)
			for _, dep := range itemGraph[item] {
				itemGraphIndegrees[dep]--
				if itemGraphIndegrees[dep] == 0 {
					tmpQueue = append(tmpQueue, dep)
				}
			}
		}
		itemVisitCount += len(queue)
		queue, tmpQueue = tmpQueue, queue[:0]
	}
	if itemVisitCount != n {
		return nil
	}
	for i, indegree := range groupGraphIndegrees {
		if indegree == 0 {
			queue = append(queue, i)
		}
	}
	groupVisitCount := 0
	for len(queue) > 0 {
		for _, group := range queue {
			output = append(output, groupItems[group]...)
			for dep := range groupGraph[group] {
				groupGraphIndegrees[dep]--
				if groupGraphIndegrees[dep] == 0 {
					tmpQueue = append(tmpQueue, dep)
				}
			}
		}
		groupVisitCount += len(queue)
		queue, tmpQueue = tmpQueue, queue[:0]
	}
	if groupVisitCount != m {
		return nil
	}
	return
}
