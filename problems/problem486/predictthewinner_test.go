package problem486

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestPredictthewinner(t *testing.T) {
	scenarios := [...]struct {
		nums   []int
		output bool
	}{
		0: {
			nums:   []int{1, 5, 2},
			output: false,
		},
		1: {
			nums:   []int{1, 5, 233, 7},
			output: true,
		},
	}
	for i, scenario := range scenarios {
		output := PredictTheWinner(scenario.nums)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
