package problem79

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestExist(t *testing.T) {
	scenarios := [...]struct {
		board  [][]byte
		word   string
		output bool
	}{
		0: {
			board: [][]byte{
				{'A', 'B', 'C', 'E'},
				{'S', 'F', 'C', 'S'},
				{'A', 'D', 'E', 'E'},
			},
			word:   "ABCCED",
			output: true,
		},
		1: {
			board: [][]byte{
				{'A', 'B', 'C', 'E'},
				{'S', 'F', 'C', 'S'},
				{'A', 'D', 'E', 'E'},
			},
			word:   "SEE",
			output: true,
		},
		2: {
			board: [][]byte{
				{'A', 'B', 'C', 'E'},
				{'S', 'F', 'C', 'S'},
				{'A', 'D', 'E', 'E'},
			},
			word:   "ABCB",
			output: false,
		},
	}
	for i, scenario := range scenarios {
		output := exist(scenario.board, scenario.word)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
