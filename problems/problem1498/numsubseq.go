package problem1498

import (
	"sort"
)

/*
	14, 4, 6, 6, 20, 8 | 22

	{14}
	{14,4}, {14,6}, {14,6}
	{14,4,6}, {14,4,6}, {14,6,6}
	{14,4,6,6}

	{4}
	{4,6},{4,6}
	{4,6,6}

	{6}
	{6,6}
*/

func numSubseq(nums []int, target int) (output int) {
	sort.Ints(nums)
	right := sort.SearchInts(nums, target-nums[0]+1) - 1
	if right < 0 {
		return
	}
	pows := make([]int, right+1)
	pows[0] = 1
	for i, p := range pows[:right] {
		pows[i+1] = (p << 1) % (1e9 + 7)
	}
	for left, n := range nums {
		for n+nums[right] > target {
			if right--; right < left {
				return
			}
		}
		output = (output + pows[right-left]) % (1e9 + 7)
	}
	return
}
