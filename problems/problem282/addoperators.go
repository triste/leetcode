package problem282

func addOperators(num string, target int) (expressions []string) {
	var solve func(i, eval, prevnum int, expr []byte)
	solve = func(i, eval, prevnum int, expr []byte) {
		if i == len(num) {
			if eval == target {
				expressions = append(expressions, string(expr))
			}
			return
		}
		opIdx := len(expr)
		expr = append(expr, 0)
		for acc, j := 0, i; j < len(num); j++ {
			acc = acc*10 + int(num[j]-'0')
			expr = append(expr, num[j])
			expr[opIdx] = '*'
			solve(j+1, eval-prevnum+(acc*prevnum), acc*prevnum, expr)
			expr[opIdx] = '+'
			solve(j+1, eval+acc, acc, expr)
			expr[opIdx] = '-'
			solve(j+1, eval-acc, -acc, expr)
			if acc == 0 {
				break
			}
		}
	}
	expr := make([]byte, 0, len(num)*2-1)
	for acc, i := 0, 0; i < len(num); i++ {
		acc = acc*10 + int(num[i]-'0')
		expr = append(expr, num[i])
		solve(i+1, acc, acc, expr)
		if acc == 0 {
			break
		}
	}
	return
}

/*
case x1 + x2
	x1 + x2 + x3
	x1 + x2 - x3
	x1 + (x2 * x3)
*/

/*
	S -> E + S
	S -> E - S
	E -> N * E
	N -> num
*/
