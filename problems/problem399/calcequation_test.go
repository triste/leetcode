package problem399

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestCalcequation(t *testing.T) {
	scenarios := [...]struct {
		equations [][]string
		values    []float64
		queries   [][]string
		output    []float64
	}{
		0: {
			equations: [][]string{{"a", "b"}, {"b", "c"}},
			values:    []float64{2.0, 3.0},
			queries: [][]string{
				{"a", "c"},
				{"b", "a"},
				{"a", "e"},
				{"a", "a"},
				{"x", "x"},
			},
			output: []float64{6.0, 0.5, -1.0, 1.0, -1.0},
		},
		1: {
			equations: [][]string{{"a", "b"}, {"b", "c"}, {"bc", "cd"}},
			values:    []float64{1.5, 2.5, 5.0},
			queries: [][]string{
				{"a", "c"},
				{"c", "b"},
				{"bc", "cd"},
				{"cd", "bc"},
			},
			output: []float64{3.75, 0.4, 5.0, 0.2},
		},
		2: {
			equations: [][]string{{"a", "b"}},
			values:    []float64{0.5},
			queries: [][]string{
				{"a", "b"},
				{"b", "a"},
				{"a", "c"},
				{"x", "y"},
			},
			output: []float64{0.5, 2.0, -1.0, -1.0},
		},
		3: {
			equations: [][]string{
				{"a", "e"}, {"b", "e"},
			},
			values: []float64{4.0, 3.0},
			queries: [][]string{
				{"a", "b"}, {"e", "e"}, {"x", "x"},
			},
			output: []float64{1.3333333333333333, 1.0, -1.0},
		},
		4: {
			equations: [][]string{
				{"a", "b"}, {"e", "f"}, {"b", "e"},
			},
			values: []float64{3.4, 1.4, 2.3},
			queries: [][]string{
				{"b", "a"},
				{"a", "f"},
				{"f", "f"},
				{"e", "e"},
				{"c", "c"},
				{"a", "c"},
				{"f", "e"},
			},
			output: []float64{
				0.29411764705882354,
				10.947999999999999,
				1.00000,
				1.00000,
				-1.00000,
				-1.00000,
				0.7142857142857143,
			},
		},
	}
	for i, scenario := range scenarios {
		output := calcEquation(scenario.equations, scenario.values, scenario.queries)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
