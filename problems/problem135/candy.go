package problem135

func candy(ratings []int) int {
	candyCount := len(ratings)
	for i := 1; i < len(ratings); {
		for i < len(ratings) && ratings[i] == ratings[i-1] {
			i++
		}

		incStartIdx := i
		for i < len(ratings) && ratings[i] > ratings[i-1] {
			i++
		}
		incCount := i - incStartIdx
		candyCount += (1 + incCount) * incCount / 2

		decStartIdx := i
		for i < len(ratings) && ratings[i] < ratings[i-1] {
			i++
		}
		decCount := i - decStartIdx
		candyCount += (1 + decCount) * decCount / 2

		if incCount < decCount {
			candyCount -= incCount
		} else {
			candyCount -= decCount
		}
	}
	return candyCount
}

/*
	1,2,3
	1,2,3

	1,2,1,2
	1,2,1,2


	1,2,3,2,1
	1,2,3,
*/
