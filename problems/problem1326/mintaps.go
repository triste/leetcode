package problem1326

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func minTaps(n int, ranges []int) int {
	maxJumps := make([]int, n+1)
	for i, r := range ranges {
		start := max(0, i-r)
		maxJumps[start] = max(maxJumps[start], i+r)
	}
	end, localEnd := 0, 0
	tapCount := 0
	for i := 0; i <= n; i++ {
		if i > localEnd {
			return -1
		}
		if i > end {
			tapCount++
			end = localEnd
		}
		localEnd = max(localEnd, maxJumps[i])
	}
	return tapCount
}
