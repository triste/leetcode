package problem2478

func isPrime(digit byte) bool {
	switch digit {
	case '2', '3', '5', '7':
		return true
	default:
		return false
	}
}

func beautifulPartitions(s string, k int, minLength int) int {
	return beautifulPartitionsIterative(s, k, minLength)
}

func beautifulPartitionsRecursive(s string, k int, minLength int) int {
	var solve func(l, i int) int
	solve = func(l, i int) int {
		if l == len(s) {
			if i == k {
				return 1
			}
			return 0
		}
		if !isPrime(s[l]) {
			return 0
		}
		count := 0
		for r := l + minLength - 1; r < len(s); r++ {
			if !isPrime(s[r]) {
				count += solve(r+1, i+1)
				count %= 1_000_000_007
			}
		}
		return count
	}
	return solve(0, 0)
}

func beautifulPartitionsIterative(s string, k int, minLength int) int {
	n := len(s)
	dp := make([][]int, n+1)
	for i := range dp {
		dp[i] = make([]int, k+1)
	}
	dp[n][k] = 1
	for l := n - 1; l >= 0; l-- {
		if !isPrime(s[l]) {
			continue
		}
		for i := 0; i < k; i++ {
			count := 0
			for r := l + minLength - 1; r < len(s); r++ {
				if !isPrime(s[r]) {
					count += dp[r+1][i+1]
					count %= 1_000_000_007
				}
			}
			dp[l][i] = count
		}
	}
	return dp[0][0]
}
