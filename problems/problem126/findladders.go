package problem126

/*
          	           cog
                	 /  |  \
	             _og   c_g  co_
	           /     \
            dog       log
	     /  | \      /  |  \
	(_og) d_g do_ (_og) l_g lo_
	         /               \
			dot               lot
		  /  |  \           /  |  \
		_ot d_t (do_)     _ot l_t (lo_)
		   \             /
		    x---x   x---x
			     \ /
	             hot
	           /  |  \
           (_ot) h_t ho_
                  |
	             hit
*/

/*
                       dog
				   /    |    \
				 _og   d_g   do_
				 /             \
			   hog             dot
			/  |  \          /  |  \
		 (_og) h_g ho_     _ot d_t (do_)
		              \   /
					   hot
*/

/*
                  hit
                /  |  \
              _it h_t hi_
                   |
                  hot
                /  |  \
              _ot *h_t ho_
            /    \
         dot       lot
       /  |  \   /   |  \
   *_ot d_t do_ *_ot l_t lo_
           /              \
         dog              log
       /  |  \          /  |  \
     _og d_g *do_     _og l_g *lo_
       \             /
        *--*     *--*
            \   /
             cog

*/
/*
	                    red
	                 /   |  \
	               _ed  r_d  re_
	              /           \
	            ted           rex
	          /  |  \       /  |  \
	       *_ed t_d te_   _ex r_x  re_*
	           /      \  /
	        tad         tex
	      / |  \      /   |  \
	   _ad *t_d ta_  *_ex t_x te_
	             \        /
				    tax
*/

func wordHash(word string) (hash uint64) {
	for i, ch := range word {
		hash |= uint64(ch) << (i * 8)
	}
	return
}

func hashMask(hash uint64, i int) uint64 {
	return hash & (^(uint64(0xFF) << (i * 8)))
}

func findLadders(beginWord string, endWord string, wordList []string) [][]string {
	words := make(map[uint64]string, len(wordList))
	masksGraph := make(map[uint64][]uint64, len(wordList)*len(beginWord))
	for _, word := range wordList {
		hash := wordHash(word)
		words[hash] = word
		for i := range word {
			mask := hashMask(hash, i)
			masksGraph[mask] = append(masksGraph[mask], hash)
		}
	}
	n := len(beginWord)
	begin := wordHash(beginWord)
	words[begin] = beginWord
	end := wordHash(endWord)
	queue := [][2]uint64{{begin}}
	var tmpQueue [][2]uint64
	depthMap := make(map[uint64]int, len(wordList))
	wordsGraph := make(map[uint64][]uint64, len(wordList))
	for depth := 1; len(queue) > 0; depth++ {
		found := false
		for _, word := range queue {
			if depthMap[word[0]] > 0 && depthMap[word[0]] != depth {
				continue
			}
			depthMap[word[0]] = depth
			linkFound := false
			for _, l := range wordsGraph[word[1]] {
				if l == word[0] {
					linkFound = true
					break
				}
			}
			if linkFound {
				continue
			}
			wordsGraph[word[1]] = append(wordsGraph[word[1]], word[0])
			if word[0] == end {
				found = true
				continue
			}
			for i := 0; i < n; i++ {
				mask := hashMask(word[0], i)
				for _, w := range masksGraph[mask] {
					tmpQueue = append(tmpQueue, [2]uint64{w, word[0]})
				}
			}
		}
		if found {
			break
		}
		queue, tmpQueue = tmpQueue, queue[:0]
	}
	memo := make(map[uint64][][]string, len(wordList))
	memo[end] = [][]string{{}}
	var dfs func(word uint64) [][]string
	dfs = func(word uint64) [][]string {
		if val, ok := memo[word]; ok {
			return val
		}
		var output [][]string
		for _, w := range wordsGraph[word] {
			for _, tail := range dfs(w) {
				ans := []string{words[w]}
				ans = append(ans, tail...)
				output = append(output, ans)
			}
		}
		memo[word] = output
		return output
	}
	return dfs(0)
}
