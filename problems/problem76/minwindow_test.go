package problem76

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestMinwindow(t *testing.T) {
	scenarios := [...]struct {
		s      string
		t      string
		output string
	}{
		0: {
			s:      "ADOBECODEBANC",
			t:      "ABC",
			output: "BANC",
		},
		1: {
			s:      "a",
			t:      "a",
			output: "a",
		},
		2: {
			s:      "a",
			t:      "aa",
			output: "",
		},
		3: {
			s:      "aaaaaaaaaaaabbbbbcdd",
			t:      "abcdd",
			output: "abbbbbcdd",
		},
		4: {
			s:      "abcabdebac",
			t:      "cda",
			output: "cabd",
		},
	}
	for i, scenario := range scenarios {
		output := minWindow(scenario.s, scenario.t)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
