package problem714

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

type State = int

const (
	Empty State = iota
	Full
)

func maxProfitRecursive(prices []int, fee int) int {
	var helper func(state State, i int) int
	helper = func(state State, i int) int {
		if i >= len(prices) {
			return 0
		}
		var max int
		if state == Empty {
			max = helper(Full, i+1) - prices[i] - fee
			if v := helper(Empty, i+1); v > max {
				max = v
			}
		} else if state == Full {
			max = helper(Empty, i+1) + prices[i]
			if v := helper(Full, i+1); v > max {
				max = v
			}
		}
		return max
	}
	return helper(Empty, 0)
}

/*
	|  1  |  3  |  2  |  8  |  4  |  9  |     |     |

empty  |     |     |     |     |     |     |  0  |  0  |
full   |     |     |     |     |     |     |  0  |  0  |
*/
func maxProfitDP(prices []int, fee int) int {
	dp := make([][2]int, len(prices)+2)
	for i := len(prices) - 1; i >= 0; i-- {
		dp[i] = [2]int{
			max(dp[i+1][1]-prices[i]-fee, dp[i+1][0]),
			max(dp[i+1][0]+prices[i], dp[i+1][1]),
		}
	}
	return dp[0][0]
}

func maxProfitDP2(prices []int, fee int) int {
	full, empty := 0, 0
	for i := len(prices) - 1; i >= 0; i-- {
		full, empty = max(empty+prices[i], full), max(full-prices[i]-fee, empty)
	}
	return empty
}

func maxProfit(prices []int, fee int) int {
	return maxProfitDP2(prices, fee)
}
