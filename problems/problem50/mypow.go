package problem50

func myPow(x float64, n int) float64 {
	var solve func(x float64, n int) float64
	solve = func(x float64, n int) float64 {
		if n == 1 {
			return x
		}
		if n%2 == 0 {
			return solve(x*x, n/2)
		} else {
			return x * solve(x*x, (n-1)/2)
		}
	}
	if n == 0 {
		return 1
	}
	if n < 0 {
		return 1.0 / solve(x, -n)
	}
	return solve(x, n)
}
