package problem44

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestIsmatch(t *testing.T) {
	scenarios := [...]struct {
		s      string
		p      string
		output bool
	}{
		0: {
			s:      "aa",
			p:      "a",
			output: false,
		},
		1: {
			s:      "aa",
			p:      "*",
			output: true,
		},
		2: {
			s:      "cb",
			p:      "?a",
			output: false,
		},
		3: {
			s:      "adceb",
			p:      "*a*b",
			output: true,
		},
		4: {
			s:      "acdcb",
			p:      "a*c?b",
			output: false,
		},
		5: {
			s:      "mississippi",
			p:      "m??*ss*?i*pi",
			output: false,
		},
		6: {
			s:      "",
			p:      "******",
			output: true,
		},
		7: {
			s:      "bbbababbbbabbbbababbaaabbaababbbaabbbaaaabbbaaaabb",
			p:      "*b********bb*b*bbbbb*ba",
			output: false,
		},
		8: {
			s:      "",
			p:      "",
			output: true,
		},
		9: {
			s:      "a",
			p:      "",
			output: false,
		},
	}
	for i, scenario := range scenarios {
		output := isMatch(scenario.s, scenario.p)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
