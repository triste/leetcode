package problem1520

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestNumidenticalpairs(t *testing.T) {
	scenarios := [...]struct {
		nums   []int
		output int
	}{
		0: {
			nums:   []int{1, 2, 3, 1, 1, 3},
			output: 4,
		},
		1: {
			nums:   []int{1, 1, 1, 1},
			output: 6,
		},
		2: {
			nums:   []int{1, 2, 3},
			output: 0,
		},
		3: {
			nums:   []int{2, 2, 1, 5, 1, 5, 5, 2, 3, 1, 1, 5, 3, 2, 3, 3, 3, 1, 3, 3, 4, 3, 1, 3, 1, 4, 5, 5, 2, 2, 1, 3, 5, 2, 2, 4, 3, 2, 5, 3, 1, 1, 3, 3, 2, 5, 2, 1, 2, 4, 3, 4, 4, 3, 2, 4, 4, 1, 3, 3, 3, 5, 5, 5, 4, 1, 1, 2, 3, 3, 2, 5, 3, 4, 5, 3, 1, 2, 5, 4, 5, 2, 3, 3, 1, 5, 2, 4, 2, 4, 4, 3, 1, 3},
			output: 885,
		},
	}
	for i, scenario := range scenarios {
		output := numIdenticalPairs(scenario.nums)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
