package problem649

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestPredictpartyvictory(t *testing.T) {
	scenarios := [...]struct {
		senate string
		output string
	}{
		0: {
			senate: "RD",
			output: "Radiant",
		},
		1: {
			senate: "RDD",
			output: "Dire",
		},
		2: {
			senate: "DDRRR",
			output: "Dire",
		},
		3: {
			senate: "DRRDRDRDRDDRDRDR",
			output: "Radiant",
		},
		4: {
			senate: "D",
			output: "Dire",
		},
		5: {
			senate: "RRDRDDRDRRDDDDDRDRDR",
			output: "Radiant",
		},
		6: {
			senate: "DRRDRDRDRDDRDRDRD",
			output: "Dire",
		},
	}
	for i, scenario := range scenarios {
		output := predictPartyVictory(scenario.senate)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
