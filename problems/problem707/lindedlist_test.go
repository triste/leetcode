package problem707

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestSwapPairs(t *testing.T) {
	list := Constructor()
	if diff := cmp.Diff(-1, list.Get(0)); diff != "" {
		t.Errorf("invalid index (-want +got):\n%s", diff)
	}
	list.AddAtHead(1)
	if diff := cmp.Diff(1, list.Get(0)); diff != "" {
		t.Errorf("invalid val (-want +got):\n%s", diff)
	}
	if diff := cmp.Diff(-1, list.Get(1)); diff != "" {
		t.Errorf("invalid index (-want +got):\n%s", diff)
	}

	list.AddAtTail(3)
	if diff := cmp.Diff(1, list.Get(0)); diff != "" {
		t.Errorf("invalid index (-want +got):\n%s", diff)
	}
	if diff := cmp.Diff(3, list.Get(1)); diff != "" {
		t.Errorf("invalid index (-want +got):\n%s", diff)
	}
	if diff := cmp.Diff(-1, list.Get(2)); diff != "" {
		t.Errorf("invalid index (-want +got):\n%s", diff)
	}

	list.AddAtIndex(1, 2)
	if diff := cmp.Diff(1, list.Get(0)); diff != "" {
		t.Errorf("invalid index (-want +got):\n%s", diff)
	}
	if diff := cmp.Diff(2, list.Get(1)); diff != "" {
		t.Errorf("invalid index (-want +got):\n%s", diff)
	}
	if diff := cmp.Diff(3, list.Get(2)); diff != "" {
		t.Errorf("invalid index (-want +got):\n%s", diff)
	}
	if diff := cmp.Diff(-1, list.Get(3)); diff != "" {
		t.Errorf("invalid index (-want +got):\n%s", diff)
	}
	
	list.DeleteAtIndex(2)
	if diff := cmp.Diff(1, list.Get(0)); diff != "" {
		t.Errorf("invalid index (-want +got):\n%s", diff)
	}
	if diff := cmp.Diff(2, list.Get(1)); diff != "" {
		t.Errorf("invalid index (-want +got):\n%s", diff)
	}
	if diff := cmp.Diff(-1, list.Get(2)); diff != "" {
		t.Errorf("invalid index (-want +got):\n%s", diff)
	}
	list.DeleteAtIndex(1)
	if diff := cmp.Diff(1, list.Get(0)); diff != "" {
		t.Errorf("invalid index (-want +got):\n%s", diff)
	}
	if diff := cmp.Diff(-1, list.Get(1)); diff != "" {
		t.Errorf("invalid index (-want +got):\n%s", diff)
	}
	list.DeleteAtIndex(0)
	if diff := cmp.Diff(-1, list.Get(0)); diff != "" {
		t.Errorf("invalid index (-want +got):\n%s", diff)
	}
}
