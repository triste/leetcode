package problem1143

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestLongestcommonsubsequence(t *testing.T) {
	scenarios := [...]struct {
		text1  string
		text2  string
		output int
	}{
		0: {
			text1:  "abcde",
			text2:  "ace",
			output: 3,
		},
		1: {
			text1:  "abc",
			text2:  "abc",
			output: 3,
		},
		2: {
			text1:  "abc",
			text2:  "def",
			output: 0,
		},
	}
	for i, scenario := range scenarios {
		output := longestCommonSubsequence(scenario.text1, scenario.text2)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
