package problem111

import (
	"math"

	. "gitlab.com/triste/leetcode/pkg/tree"
)

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func minDepth(root *TreeNode) int {
	if root == nil {
		return 0
	}
	minDepth := math.MaxInt
	var dfs func(node *TreeNode, depth int)
	dfs = func(node *TreeNode, depth int) {
		if node.Left == nil && node.Right == nil {
			minDepth = min(minDepth, depth)
			return
		}
		if node.Left != nil {
			dfs(node.Left, depth+1)
		}
		if node.Right != nil {
			dfs(node.Right, depth+1)
		}
	}
	dfs(root, 1)
	return minDepth
}
