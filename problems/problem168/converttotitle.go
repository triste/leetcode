package problem168

func convertToTitle(column int) string {
	var bytes [7]byte // ceil(log26(2^31 - 1))
	b := len(bytes)
	for i := column; i != 0; i /= 26 {
		i--
		b--
		bytes[b] = 'A' + byte(i%26)
	}
	return string(bytes[b:])
}
