package problem1569

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestNumofways(t *testing.T) {
	scenarios := [...]struct {
		nums   []int
		output int
	}{
		0: {
			nums:   []int{2, 1, 3},
			output: 1,
		},
		1: {
			nums:   []int{3, 4, 5, 1, 2},
			output: 5,
		},
		2: {
			nums:   []int{1, 2, 3},
			output: 0,
		},
		3: {
			nums:   []int{3, 5, 4, 6, 1, 2},
			output: 19,
		},
		4: {
			nums:   []int{9, 4, 2, 1, 3, 6, 5, 7, 8, 14, 11, 10, 12, 13, 16, 15, 17, 18},
			output: 216212978,
		},
		5: {
			nums:   []int{31, 23, 14, 24, 15, 12, 25, 28, 5, 35, 17, 6, 9, 11, 1, 27, 18, 20, 2, 3, 33, 10, 13, 4, 7, 36, 32, 29, 8, 30, 26, 19, 34, 22, 21, 16},
			output: 936157466,
		},
		6: {
			nums:   []int{19, 3, 57, 34, 15, 89, 58, 35, 2, 33, 46, 13, 40, 79, 60, 30, 61, 26, 54, 22, 84, 51, 75, 6, 87, 44, 55, 48, 27, 8, 72, 47, 16, 69, 36, 76, 41, 1, 80, 62, 73, 24, 93, 50, 92, 65, 39, 5, 32, 67, 12, 29, 90, 45, 9, 38, 88, 52, 10, 85, 74, 66, 83, 18, 20, 77, 49, 28, 23, 53, 86, 64, 78, 82, 37, 42, 56, 17, 81, 4, 14, 70, 59, 31, 7, 25, 43, 68, 91, 71, 21, 63, 94, 11},
			output: 188718086,
		},
	}
	for i, scenario := range scenarios {
		output := numOfWays(scenario.nums)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
