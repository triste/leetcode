package problem1361

func validateBinaryTreeNodes(n int, leftChild []int, rightChild []int) bool {
	parentCounts := make([]int, n+1)
	for i := 0; i < n; i++ {
		parentCounts[leftChild[i]+1]++
		parentCounts[rightChild[i]+1]++
	}
	root := -1
	for i := 1; i <= n; i++ {
		if parentCounts[i] == 0 {
			root = i - 1
			break
		}
	}
	if root < 0 {
		return false
	}
	visited := make([]bool, n)
	queue := []int{root}
	visitedCount := 0
	var nextQueue []int
	for len(queue) > 0 {
		for _, i := range queue {
			if i < 0 {
				continue
			}
			if visited[i] {
				return false
			}
			visitedCount++
			visited[i] = true
			nextQueue = append(nextQueue, leftChild[i], rightChild[i])
		}
		queue, nextQueue = nextQueue, queue[:0]
	}
	if visitedCount != n {
		return false
	}
	return true
}
