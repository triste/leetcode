package problem29

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestDivide(t *testing.T) {
	scenarios := [...]struct {
		dividend int
		divisor  int
		output   int
	}{
		0: {
			dividend: 10,
			divisor:  3,
			output:   3,
		},
		1: {
			dividend: 7,
			divisor:  -3,
			output:   -2,
		},
	}
	for i, scenario := range scenarios {
		output := divide(scenario.dividend, scenario.divisor)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
