package problem149

func gcd(a, b int) int {
	for b != 0 {
		a, b = b, a%b
	}
	return a
}

func maxPoints(points [][]int) int {
	maxCount := 1
	slopes := make(map[[3]int]map[int]struct{})
	for i, p1 := range points {
		for j := i + 1; j < len(points); j++ {
			p2 := points[j]
			dx := p2[0] - p1[0]
			dy := p2[1] - p1[1]
			gcd := gcd(dx, dy)
			dx /= gcd
			dy /= gcd
			if dy < 0 {
				dx = -dx
				dy = -dy
			}
			key := [3]int{dx, dy, p1[1]*dx - p1[0]*dy}
			if slopes[key] == nil {
				slopes[key] = make(map[int]struct{})
			}
			m := slopes[key]
			m[i] = struct{}{}
			m[j] = struct{}{}
			if count := len(m); count > maxCount {
				maxCount = count
			}
		}
	}
	return maxCount
}
