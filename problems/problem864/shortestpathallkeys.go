package problem864

type State struct {
	row, col int
	keys     uint8
}

func shortestPathAllKeys(grid []string) int {
	var start [2]int
	var end uint8
	for i, row := range grid {
		for j, ch := range row {
			if ch == '@' {
				start = [2]int{i, j}
			} else if ch >= 'a' {
				end |= (1 << (ch - 'a'))
			}
		}
	}
	visited := make([][][64]bool, len(grid))
	for i := range visited {
		visited[i] = make([][64]bool, len(grid[i]))
	}
	dirs := [4][2]int{{-1, 0}, {0, 1}, {1, 0}, {0, -1}}
	queue := []State{{start[0], start[1], 0}}
	var tmpQueue []State
	for stepCount := 0; len(queue) > 0; stepCount++ {
		for _, state := range queue {
			if visited[state.row][state.col][state.keys] {
				continue
			}
			visited[state.row][state.col][state.keys] = true
			for _, dir := range dirs {
				r, c := state.row+dir[0], state.col+dir[1]
				if r >= 0 && r < len(grid) && c >= 0 && c < len(grid[r]) {
					ch := grid[r][c]
					if ch >= 'a' {
						keys := state.keys | (1 << (ch - 'a'))
						if keys == end {
							return stepCount + 1
						}
						tmpQueue = append(tmpQueue, State{r, c, keys})
					} else if ch >= 'A' && state.keys&(1<<(ch-'A')) == 0 {
						continue
					} else if ch != '#' {
						tmpQueue = append(tmpQueue, State{r, c, state.keys})
					}
				}
			}
		}
		queue, tmpQueue = tmpQueue, queue[:0]
	}
	return -1
}
