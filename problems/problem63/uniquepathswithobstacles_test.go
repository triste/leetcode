package problem63

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestUniquepathswithobstacles(t *testing.T) {
	scenarios := [...]struct {
		obstacleGrid [][]int
		output       int
	}{
		0: {
			obstacleGrid: [][]int{
				{0, 0, 0},
				{0, 1, 0},
				{0, 0, 0},
			},
			output: 2,
		},
		1: {
			obstacleGrid: [][]int{
				{0, 1},
				{0, 0},
			},
			output: 1,
		},
		2: {
			obstacleGrid: [][]int{
				{0, 0},
				{0, 1},
			},
			output: 0,
		},
		3: {
			obstacleGrid: [][]int{
				{0},
			},
			output: 1,
		},
	}
	for i, scenario := range scenarios {
		output := uniquePathsWithObstacles(scenario.obstacleGrid)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
