package problem174

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestCalculateminimumhp(t *testing.T) {
	scenarios := [...]struct {
		dungeon [][]int
		output  int
	}{
		0: {
			dungeon: [][]int{
				{-2, -3, 3},
				{-5, -10, 1},
				{10, 30, -5},
			},
			output: 7,
		},
		1: {
			dungeon: [][]int{
				{0},
			},
			output: 1,
		},
		2: {
			dungeon: [][]int{
				{100},
			},
			output: 1,
		},
	}
	for i, scenario := range scenarios {
		output := calculateMinimumHP(scenario.dungeon)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
