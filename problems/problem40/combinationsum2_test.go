package problem40

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestCombinationsum2(t *testing.T) {
	scenarios := [...]struct {
		candidates []int
		target     int
		output     [][]int
	}{
		0: {
			candidates: []int{10, 1, 2, 7, 6, 1, 5},
			target:     8,
			output: [][]int{
				{1, 1, 6},
				{1, 2, 5},
				{1, 7},
				{2, 6},
			},
		},
		1: {
			candidates: []int{2, 5, 2, 1, 2},
			target:     5,
			output: [][]int{
				{1, 2, 2},
				{5},
			},
		},
	}
	for i, scenario := range scenarios {
		output := combinationSum2(scenario.candidates, scenario.target)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
