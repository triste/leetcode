package problem1035

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func maxUncrossedLines(nums1 []int, nums2 []int) (output int) {
	if len(nums1) > len(nums2) {
		nums1, nums2 = nums2, nums1
	}
	dp := make([]int, len(nums1)+1)
	for _, n2 := range nums2 {
		var prev int
		for j, n1 := range nums1 {
			var cur int
			if n1 == n2 {
				cur = 1 + prev
			} else {
				cur = max(dp[j+1], dp[j])
			}
			prev = dp[j+1]
			dp[j+1] = cur
		}
	}
	return dp[len(dp)-1]
}

/*
	   | 3 | 3 | 3
	 1 | 0 | 0 | 0
	 3 | 1 | 1 | 1
	 1 |   |   |
	 2 |   |   |
	 1 |   |   |

	   | 3 | 3 |
	 1 | 0 | 0 |
	 3 | 1 | 1 |
	 1 |   |   |
	 2 |   |   |
	 1 |   |   |

	   | 1 | 4 | 2 |
	 1 | 1 |   |   |
	 2 |   |   |   |
	 4 |   |   |   |

	   | 1 | 3 | 7 | 1 | 7 | 5
	 1 |   |   |   |   |   |
	 9 |   |   |   |   |   |
	 2 |   |   |   |   |   |
	 5 |   |   |   |   |   |
	 1 |   |   |   |   |   |

	   | 2 | 3 | 4 | 6 | 5 |
	 5 | 2 | 2 | 1 | 1 | 1 |
	 9 | 2 | 2 | 1 | 0 | 0 |
	 3 | 2 | 2 | 1 | 0 | 0 |
	 4 | 1 | 1 | 1 | 0 | 0 |
	 2 | 1 | 0 | 0 | 0 | 0 |

	  | 1 | 4 | 2
	1 | 2 | 1 | 1
	2 | 1 | 1 | 1
	4 | 1 | 1 | 0

	2  5 1 2 5         2 5 1 2 5
	   |  \  |         5 2 1 5 2
	10 5 2 1 5 2

	1 3 7 1 7 5
	|      \
	1 9 2 5 1

	   | 2 | 5 | 1 | 2 | 5 |
	10 | 3 |   |   |   |   |
	 5 | 3 | 3 |   |   |   |
	 2 | 3 | 2 | 2 |   |   |
	 1 |   | 2 | 2 | 1 |   |
	 5 |   |   | 1 | 1 | 1 |
	 2 |   |   |   | 1 | 0 |

	   | 1 | 3 | 7 | 1 | 7 | 5
	 1 | 2 | 1 | 1 |   |   |
	 9 |   | 1 | 1 | 1 |   |
	 2 |   |   | 1 | 1 | 1 |
	 5 |   |   |   | 1 | 1 | 1
	 1 |   |   |   |   | 0 | 0


shift 0:
	 2 5 1 2 5
	     |
	 5 2 1 5 2
	 output = 1

shift 1:
	 2 5 1 2 5
	   |     |
	   5 2 1 5 2

shift 2:
	 2 5 1 2 5
	       |
	     5 2 1 5 2

shift 3:
	 2 5 1 2 5

	       5 2 1 5 2

shift 4:
	 2 5 1 2 5
	         |
	         5 2 1 5 2

shift -1:
	   2 5 1 2 5
	   |     |
	 5 2 1 5 2

shift -2:
	     2 5 1 2 5
	       |
	 5 2 1 5 2

shift -3:
	       2 5 1 2 5

	 5 2 1 5 2

shift -4:
	         2 5 1 2 5
	         |
	 5 2 1 5 2
*/
