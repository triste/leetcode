package problem137

func singleNumber(nums []int) int {
	single := 0
	double := 0
	for _, num := range nums {
		single ^= num & ^double
		double ^= num & ^single
	}
	return single
}

/*

	2,2,2,3
	10
	10
	10
	11

	2,1,3,1,2,2,1

	10
	01
	11
	01
	10
	10
	01
*/
