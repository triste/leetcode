package problem146

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestList(t *testing.T) {
	var list List
	node1 := &Node{1, 1, nil, nil}
	node2 := &Node{2, 2, nil, nil}
	node3 := &Node{3, 3, nil, nil}
	node4 := &Node{4, 4, nil, nil}
	list.PushFront(node1)
	list.PushFront(node2)
	list.PushFront(node3)
	want := Node{1, 1, nil, nil}
	if got := list.PopBack(); *got != want {
		t.Errorf("PopBack() = %v; got %v", want, got)
	}
	for p := list.head; p != nil; p = p.next {
		if p.next != nil && p.next.prev != p {
			t.Error("Broken links")
		}
	}
	for p := list.tail; p != nil; p = p.prev {
		if p.prev != nil && p.prev.next != p {
			t.Error("Broken links")
		}
	}
	if !cmp.Equal(list.Slice(), []int{3, 2}) {
		t.Error("Mismtach")
	}
	list.PushFront(node4)
	for p := list.head; p != nil; p = p.next {
		if p.next != nil && p.next.prev != p {
			t.Error("Broken links")
		}
	}
	for p := list.tail; p != nil; p = p.prev {
		if p.prev != nil && p.prev.next != p {
			t.Error("Broken links")
		}
	}
	if !cmp.Equal(list.Slice(), []int{4, 3, 2}) {
		t.Error("Mismtach")
	}
	list.Pop(node3)
	for p := list.head; p != nil; p = p.next {
		if p.next != nil && p.next.prev != p {
			t.Error("Broken links")
		}
	}
	for p := list.tail; p != nil; p = p.prev {
		if p.prev != nil && p.prev.next != p {
			t.Error("Broken links")
		}
	}
	if !cmp.Equal(list.Slice(), []int{4, 2}) {
		t.Error("Mismtach")
	}
}

func TestLRUCache(t *testing.T) {
	lruCache := Constructor(2)
	lruCache.Put(1, 1)
	lruCache.Put(2, 2)
	if got := lruCache.Get(1); got != 1 {
		t.Errorf("Get(1) = 1; got %v", got)
	}
	lruCache.Put(3, 3)
	if got := lruCache.Get(2); got != -1 {
		t.Errorf("Get(2) = -1; got %v", got)
	}
	lruCache.Put(4, 4)
	if got := lruCache.Get(1); got != -1 {
		t.Errorf("Get(1) = -1; got %v", got)
	}
	if got := lruCache.Get(3); got != 3 {
		t.Errorf("Get(3) = 3; got %v", got)
	}
	if got := lruCache.Get(4); got != 4 {
		t.Errorf("Get(4) = 4; got %v", got)
	}

	lruCache = Constructor(1)
	lruCache.Put(2, 1)
	if got := lruCache.Get(2); got != 1 {
		t.Errorf("Get(2) = 1; got %v", got)
	}
	lruCache.Put(3, 2)
	if got := lruCache.Get(2); got != -1 {
		t.Errorf("Get(2) = -1; got %v", got)
	}
	if got := lruCache.Get(3); got != 2 {
		t.Errorf("Get(3) = 2; got %v", got)
	}

	lruCache = Constructor(10)
	lruCache.Put(10, 14)
	lruCache.Put(3, 17)
	lruCache.Put(6, 11)
	lruCache.Put(10, 5)
	lruCache.Put(9, 10)
	if got := lruCache.Get(13); got != -1 {
		t.Errorf("Get(13) = -1; got %v", got)
	}
	lruCache.Put(2, 19)
	if got := lruCache.Get(2); got != 19 {
		t.Errorf("Get(2) = 19; got %v", got)
	}
	if got := lruCache.Get(3); got != 17 {
		t.Errorf("Get(2) = 17; got %v", got)
	}
	lruCache.Put(5, 25)
	if got := lruCache.Get(8); got != -1 {
		t.Errorf("Get(8) = -1; got %v", got)
	}
	lruCache.Put(9, 22)
}

/*
[null,null,null,null,null,null,-1,null,19,17,null,-1,null,null,null,-1,null,-1,5,-1,12,null,null,3,5,5,null,null,1,null,-1,null,30,5,30,null,null,null,-1,null,-1,24,null,null,18,null,null,null,null,14,null,null,18,null,null,11,null,null,null,null,null,18,null,null,24,null,4,29,30,null,12,11,null,null,null,null,29,null,null,null,null,17,22,18,null,null,null,24,null,null,null,20,null,null,null,29,18,18,null,null,null,null,20,null,null,null,null,null,null,null]
*/

/*
[null,null,null,null,null,null,-1,null,19,17,null,-1,null,null,null,-1,null,-1,5,-1,12,null,null,3,5,5,null,null,1,null,-1,null,30,5,30,null,null,null,-1,null,-1,24,null,null,18,null,null,null,null,-1,null,null,18,null,null,-1,null,null,null,null,null,18,null,null,-1,null,4,29,30,null,12,-1,null,null,null,null,29,null,null,null,null,17,22,18,null,null,null,-1,null,null,null,20,null,null,null,-1,18,18,null,null,null,null,20,null,null,null,null,null,null,null]
*/
