package problem91

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestNumdecodings(t *testing.T) {
	scenarios := [...]struct {
		s      string
		output int
	}{
		0: {
			s:      "12",
			output: 2,
		},
		1: {
			s:      "226",
			output: 3,
		},
		2: {
			s:      "06",
			output: 0,
		},
	}
	for i, scenario := range scenarios {
		output := numDecodings(scenario.s)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
