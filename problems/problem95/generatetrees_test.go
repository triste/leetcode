package problem95

import (
	"testing"

	"github.com/google/go-cmp/cmp"
	. "gitlab.com/triste/leetcode/pkg/tree"
)

func TestGeneratetrees(t *testing.T) {
	scenarios := [...]struct {
		n      int
		output []*TreeNode
	}{
		0: {
			n: 3,
			output: []*TreeNode{
				NewTree(1, nil, 2, nil, 3),
				NewTree(1, nil, 3, 2),
				NewTree(2, 1, 3),
				NewTree(3, 1, nil, nil, 2),
				NewTree(3, 2, nil, 1),
			},
		},
		1: {
			n: 1,
			output: []*TreeNode{
				NewTree(1),
			},
		},
	}
	for i, scenario := range scenarios {
		output := generateTrees(scenario.n)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
