package problem2218

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func maxValueOfCoins(piles [][]int, k int) int {
	return maxValueOfCoinsIterative(piles, k)
}

func maxValueOfCoinsRecursive(piles [][]int, k int) int {
	memo := make(map[[2]int]int)
	var solve func(p int, k int) int
	solve = func(p int, k int) int {
		key := [2]int{p, k}
		if val, ok := memo[key]; ok {
			return val
		}
		if p == len(piles) {
			return 0
		}
		maxSum := solve(p+1, k)
		sum := 0
		for i := 0; i < min(len(piles[p]), k); i++ {
			sum += piles[p][i]
			maxSum = max(maxSum, sum+solve(p+1, k-i-1))
		}
		memo[key] = maxSum
		return maxSum
	}
	return solve(0, k)
}

func maxValueOfCoinsIterative(piles [][]int, k int) int {
	/*
		  | 0 | 1 | 2 |
		0 | 0 | 7 |101|
		1 | 0 | 7 | 15|
		2 | 0 | 0 | 0 |
	*/
	for _, pile := range piles {
		for i := 1; i < len(pile); i++ {
			pile[i] += pile[i-1]
		}
	}
	dp := make([]int, k+1)
	for _, pile := range piles {
		for i := len(dp) - 1; i > 0; i-- {
			for j, sum := range pile[:min(len(pile), i)] {
				dp[i] = max(dp[i], sum+dp[i-j-1])
			}
		}
	}
	return dp[k]
}
