package problem572

import (
	"testing"

	"github.com/google/go-cmp/cmp"
	. "gitlab.com/triste/leetcode/pkg/tree"
)

func TestIssubtree(t *testing.T) {
	scenarios := [...]struct {
		root    *TreeNode
		subRoot *TreeNode
		output  bool
	}{
		0: {NewTree(3, 4, 5, 1, 2), NewTree(4, 1, 2), true},
		1: {NewTree(3, 4, 5, 1, 2, nil, nil, nil, nil, 0), NewTree(4, 1, 2), false},
		2: {NewTree(1, 2, 3), NewTree(1, 2), false},
		3: {NewTree(3, 4, 5, 1, 2, nil, nil, nil, nil, 0), NewTree(4, 1, 2), false},
	}
	for i, scenario := range scenarios {
		output := isSubtree(scenario.root, scenario.subRoot)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}

/*
           3
		 /   \
		4     5
	   / \
	  1   2
	     /
	    0
*/
