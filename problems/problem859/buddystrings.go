package problem859

func buddyStrings(s string, goal string) bool {
	if len(s) != len(goal) {
		return false
	}
	chCounts := [26]int{}
	var diffs []int
	for i := range s {
		chCounts[s[i]-'a']++
		if s[i] == goal[i] {
			continue
		}
		if len(diffs) == 2 {
			return false
		}
		diffs = append(diffs, i)
	}
	if len(diffs) == 0 {
		for _, c := range chCounts {
			if c > 1 {
				return true
			}
		}
		return false
	}
	if len(diffs) == 1 {
		return false
	}
	if s[diffs[1]] == goal[diffs[0]] && s[diffs[0]] == goal[diffs[1]] {
		return true
	}
	return false
}
