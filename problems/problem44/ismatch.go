package problem44

func isMatch(str string, pattern string) bool {
	return isMatchIterative(str, pattern)
}

func isMatchRecursive(str string, pattern string) bool {
	if len(str) == 0 && len(pattern) == 0 {
		return true
	}
	if len(pattern) == 0 {
		return false
	}
	if pattern[0] == '?' {
		if len(str) == 0 {
			return false
		}
		return isMatch(str[1:], pattern[1:])
	}
	if pattern[0] == '*' {
		for i := 0; i <= len(str); i++ {
			if isMatch(str[i:], pattern[1:]) {
				return true
			}
		}
		return false
	}
	if len(str) == 0 {
		return false
	}
	if pattern[0] == str[0] {
		return isMatch(str[1:], pattern[1:])
	}
	return false
}

func isMatchIterative(str string, pattern string) bool {
	dp := make([][]bool, len(str)+1)
	for i := range dp {
		dp[i] = make([]bool, len(pattern)+1)
	}
	dp[len(str)][len(pattern)] = true
	for i := len(pattern) - 1; i >= 0 && pattern[i] == '*'; i-- {
		dp[len(str)][i] = true
	}
	for i := len(str) - 1; i >= 0; i-- {
		for j := len(pattern) - 1; j >= 0; j-- {
			if pattern[j] == '?' {
				dp[i][j] = dp[i+1][j+1]
			} else if pattern[j] == '*' {
				for k := i; k <= len(str); k++ {
					dp[i][j] = dp[i][j] || dp[k][j+1]
				}
			} else if pattern[j] == str[i] {
				dp[i][j] = dp[i+1][j+1]
			} else {
				dp[i][j] = false
			}
		}
	}
	return dp[0][0]
}

/*
   | a | * | c | ? | b | $ |
 a |   |   |   |   |   | - |
 c |   |   |   |   |   | - |
 d |   |   | - |   |   | - |
 c |   |   |   | + |   | - |
 b |   |   |   |   | + | - |
 $ |   |   |   |   |   | + |

*/
