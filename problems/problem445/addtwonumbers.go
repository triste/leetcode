package problem445

import (
	. "gitlab.com/triste/leetcode/pkg/list"
)

func addTwoNumbers(l1 *ListNode, l2 *ListNode) *ListNode {
	list1, lenght1 := reverse(l1)
	list2, lenght2 := reverse(l2)
	if lenght1 < lenght2 {
		list1, list2 = list2, list1
		lenght1, lenght2 = lenght2, lenght1
	}
	l1 = list1
	overflow := 0
	for l2 = list2; l2 != nil; l1, l2 = l1.Next, l2.Next {
		sum := l1.Val + l2.Val + overflow
		l1.Val = sum % 10
		overflow = sum / 10
	}
	for ; overflow != 0 && l1 != nil; l1 = l1.Next {
		sum := l1.Val + 1
		l1.Val = sum % 10
		overflow = sum / 10
	}
	l1, _ = reverse(list1)
	if overflow != 0 {
		l1 = &ListNode{Val: 1, Next: l1}
	}
	return l1
}

func reverse(list *ListNode) (*ListNode, int) {
	prev, cur := list, list.Next
	list.Next = nil
	var nodeCount int
	for nodeCount = 1; cur != nil; nodeCount++ {
		tmp := cur.Next
		cur.Next = prev
		prev = cur
		cur = tmp
	}
	return prev, nodeCount
}
