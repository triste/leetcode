package problem707

type MyLinkedList struct {
	first  *Node
	last   *Node
	lenght int
}

type Node struct {
	val  int
	next *Node
	prev *Node
}

func Constructor() MyLinkedList {
	return MyLinkedList{}
}

func (l *MyLinkedList) findNodeAtIndex(index int) *Node {
	if index >= l.lenght {
		return nil
	}
	var node *Node
	if index < l.lenght/2 {
		node = l.first
		for i := 0; i < index; i++ {
			node = node.next
		}
	} else {
		node = l.last
		for i := l.lenght - 1; i > index; i-- {
			node = node.prev
		}
	}
	return node
}

func (l *MyLinkedList) Get(index int) int {
	node := l.findNodeAtIndex(index)
	if node == nil {
		return -1
	}
	return node.val
}

func (l *MyLinkedList) AddAtHead(val int) {
	node := &Node{val, l.first, nil}
	if l.lenght == 0 {
		l.first = node
		l.last = node
	} else {
		l.first.prev = node
		l.first = node
	}
	l.lenght++
}

func (l *MyLinkedList) AddAtTail(val int) {
	node := &Node{val, nil, l.last}
	if l.lenght == 0 {
		l.first = node
		l.last = node
	} else {
		l.last.next = node
		l.last = node
	}
	l.lenght++
}

func (l *MyLinkedList) AddAtIndex(index int, val int) {
	if index == l.lenght {
		l.AddAtTail(val)
		return
	}
	node := l.findNodeAtIndex(index)
	if node == nil {
		return
	}
	if node.prev == nil {
		l.AddAtHead(val)
		return
	}
	newNode := &Node{val, node, node.prev}
	node.prev.next = newNode
	node.prev = newNode
	l.lenght++
}

func (l *MyLinkedList) DeleteAtIndex(index int) {
	node := l.findNodeAtIndex(index)
	if node == nil {
		return
	}
	if node.prev == nil {
		if l.first = l.first.next; l.first != nil {
			l.first.prev = nil
		}
	} else if node.next == nil {
		l.last = l.last.prev
		l.last.next = nil
	} else {
		node.prev.next = node.next
		node.next.prev = node.prev
	}
	l.lenght--
}

/**
 * Your MyLinkedList object will be instantiated and called as such:
 * obj := Constructor();
 * param_1 := obj.Get(index);
 * obj.AddAtHead(val);
 * obj.AddAtTail(val);
 * obj.AddAtIndex(index,val);
 * obj.DeleteAtIndex(index);
 */
