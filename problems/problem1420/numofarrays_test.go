package problem1420

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestNumofarrays(t *testing.T) {
	scenarios := [...]struct {
		n      int
		m      int
		k      int
		output int
	}{
		0: {
			n:      2,
			m:      3,
			k:      1,
			output: 6,
		},
		1: {
			n:      5,
			m:      2,
			k:      3,
			output: 0,
		},
		2: {
			n:      9,
			m:      1,
			k:      1,
			output: 1,
		},
		3: {
			n:      50,
			m:      100,
			k:      25,
			output: 34549172,
		},
	}
	for i, scenario := range scenarios {
		output := numOfArrays(scenario.n, scenario.m, scenario.k)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
