package problem2478

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestBeautifulpartitions(t *testing.T) {
	scenarios := [...]struct {
		s         string
		k         int
		minLength int
		output    int
	}{
		0: {
			s:         "23542185131",
			k:         3,
			minLength: 2,
			output:    3,
		},
		1: {
			s:         "23542185131",
			k:         3,
			minLength: 3,
			output:    1,
		},
		2: {
			s:         "3312958",
			k:         3,
			minLength: 1,
			output:    1,
		},
		3: {
			s:         "7753639519183359148598823162755335682921461647796985255166979917649578972791819356618496239687361868933775339936875893219782348459522657159781118588765368954599285197845124455559747963186111993765269",
			k:         24,
			minLength: 2,
			output:    616385996,
		},
	}
	for i, scenario := range scenarios {
		output := beautifulPartitions(scenario.s, scenario.k, scenario.minLength)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
