package problem222

import (
	. "gitlab.com/triste/leetcode/pkg/tree"
)

func countNodes(root *TreeNode) int {
	var dfs func(root *TreeNode) int
	dfs = func(root *TreeNode) int {
		l := root
		level := 0
		for r := root; r != nil; l, r = l.Left, r.Right {
			level++
		}
		if l == nil {
			return 1<<level - 1
		} else {
			return 1 + dfs(root.Left) + dfs(root.Right)
		}
	}
	return dfs(root)
}
