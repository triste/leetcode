package problem377

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestCombinationsum4(t *testing.T) {
	scenarios := [...]struct {
		nums   []int
		target int
		output int
	}{
		0: {
			nums:   []int{1, 2, 3},
			target: 4,
			output: 7,
		},
		1: {
			nums:   []int{9},
			target: 3,
			output: 0,
		},
		2: {
			nums:   []int{3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25},
			target: 10,
			output: 9,
		},
	}
	for i, scenario := range scenarios {
		output := combinationSum4(scenario.nums, scenario.target)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
