package problem81

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestSearch(t *testing.T) {
	scenarios := [...]struct {
		nums   []int
		target int
		output bool
	}{
		0: {
			nums:   []int{2, 5, 6, 0, 0, 1, 2},
			target: 0,
			output: true,
		},
		1: {
			nums:   []int{2, 5, 6, 0, 0, 1},
			target: 3,
			output: false,
		},
		2: {
			nums:   []int{2, 2, 2, 3, 2, 2, 2},
			target: 3,
			output: true,
		},
		3: {
			nums:   []int{1, 0, 1, 1, 1},
			target: 0,
			output: true,
		},
		4: {
			nums:   []int{2, 2, 2, 2, 2, 2},
			target: 0,
			output: false,
		},
	}
	for i, scenario := range scenarios {
		output := search(scenario.nums, scenario.target)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
