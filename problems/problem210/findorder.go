package problem210

func findOrder(numCourses int, prerequisites [][]int) []int {
	graph := make([][]int, numCourses)
	indegrees := make([]int, numCourses)
	for _, prereq := range prerequisites {
		graph[prereq[1]] = append(graph[prereq[1]], prereq[0])
		indegrees[prereq[0]]++
	}
	orderedCourses := make([]int, 0, numCourses)
	for i := range graph {
		if indegrees[i] == 0 {
			orderedCourses = append(orderedCourses, i)
		}
	}
	for i := 0; i < len(orderedCourses); i++ {
		for _, j := range graph[orderedCourses[i]] {
			if indegrees[j]--; indegrees[j] == 0 {
				orderedCourses = append(orderedCourses, j)
			}
		}
	}
	if len(orderedCourses) != numCourses {
		return nil
	}
	return orderedCourses
}
