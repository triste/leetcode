package problem1406

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func stoneGameIII(stones []int) string {
	n := len(stones)
	memo := make([]int, n)
	for i := range memo {
		memo[i] = -1
	}
	var play func(i int) int
	play = func(i int) int {
		if i >= n {
			return 0
		}
		if memo[i] != -1 {
			return memo[i]
		}
		maxScore := -1000000
		turnScore := 0
		for j, stone := range stones[i:min(n, i+3)] {
			turnScore += stone
			score := turnScore - play(i+j+1)
			maxScore = max(maxScore, score)
		}
		memo[i] = maxScore
		return maxScore
	}
	if diff := play(0); diff < 0 {
		return "Bob"
	} else if diff == 0 {
		return "Tie"
	} else {
		return "Alice"
	}
}
