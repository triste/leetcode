package problem171

func titleToNumber(columnTitle string) (num int) {
	for _, letter := range columnTitle {
		num = num*26 + int(letter) - 'A' + 1
	}
	return
}

/*
	1: A
	2: B
	...
	26: Z
	27: AA
	28: AB
	29: AC
		...
	AZ: 52
	BA: 53
	CA:


	AZ - AA = 25
	Z - A = 25
*/
