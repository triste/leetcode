package problem1992

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestFindfarmland(t *testing.T) {
	scenarios := [...]struct {
		land   [][]int
		output [][]int
	}{
		0: {
			land: [][]int{
				{1, 0, 0}, {0, 1, 1}, {0, 1, 1},
			},
			output: [][]int{
				{0, 0, 0, 0}, {1, 1, 2, 2},
			},
		},
		1: {
			land: [][]int{
				{1, 1}, {1, 1},
			},
			output: [][]int{
				{0, 0, 1, 1},
			},
		},
		2: {
			land:   [][]int{{0}},
			output: nil,
		},
		3: {
			land: [][]int{
				{1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 1, 1},
			},
			output: [][]int{
				{0, 0, 0, 0},
				{0, 3, 0, 3},
				{0, 5, 0, 6},
				{0, 9, 0, 9},
				{0, 11, 0, 12},
			},
		},
	}
	for i, scenario := range scenarios {
		output := findFarmland(scenario.land)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
