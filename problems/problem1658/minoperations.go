package problem1658

import (
	"sort"
)

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func minOperations(nums []int, x int) int {
	for i := 1; i < len(nums); i++ {
		nums[i] += nums[i-1]
	}
	x = nums[len(nums)-1] - x
	bestLenght := -1
	r := sort.SearchInts(nums, x)
	if r >= len(nums) {
		return -1
	}
	if nums[r] == x {
		r++
		bestLenght = r
	}
	for l := 0; r < len(nums); r++ {
		l += sort.SearchInts(nums[l:r], nums[r]-x)
		if sum := nums[r] - nums[l]; sum < x {
			continue
		} else if sum == x {
			bestLenght = max(bestLenght, r-l)
		}
		l++
	}
	if bestLenght == -1 {
		return -1
	}
	return len(nums) - bestLenght
}
