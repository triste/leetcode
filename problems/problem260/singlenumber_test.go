package problem260

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestSinglenumber(t *testing.T) {
	scenarios := [...]struct {
		nums   []int
		output []int
	}{
		0: {
			nums:   []int{1, 2, 1, 3, 2, 5},
			output: []int{3, 5},
		},
		1: {
			nums:   []int{-1, 0},
			output: []int{-1, 0},
		},
		2: {
			nums:   []int{0, 1},
			output: []int{1, 0},
		},
	}
	for i, scenario := range scenarios {
		output := singleNumber(scenario.nums)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
