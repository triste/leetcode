package problem980

import "log"

func uniquePathsIII(grid [][]int) int {
	nonObstacleCount := 0
	var start, end [2]int
	for r, row := range grid {
		for c, num := range row {
			if num < 0 {
				continue
			}
			nonObstacleCount++
			if num == 1 {
				start = [2]int{r, c}
			} else if num == 2 {
				end = [2]int{r, c}
			}
		}
	}
	log.Println(nonObstacleCount, start, end)
	uniquePathCount := 0
	return uniquePathCount
}
