package problem664

func strangePrinter(s string) int {
	return strangePrinterIterative(s)
}

func strangePrinterRecursive(s string) int {
	memo := make([][]int, len(s)+1)
	for i := range memo {
		memo[i] = make([]int, len(s)+1)
		for j := range memo[i] {
			memo[i][j] = -1
		}
	}
	var solve func(l, r int) int
	solve = func(l, r int) int {
		if l == r {
			return 0
		}
		if memo[l][r] >= 0 {
			return memo[l][r]
		}
		j := r
		for i := l; i < r; i++ {
			if s[i] != s[r] {
				j = i
				break
			}
		}
		if j == r {
			memo[l][r] = 0
			return 0
		}
		minOpCount := len(s)
		for i := j; i < r; i++ {
			opCount := 1 + solve(j, i) + solve(i+1, r)
			if opCount < minOpCount {
				minOpCount = opCount
			}
		}
		memo[l][r] = minOpCount
		return minOpCount
	}
	return 1 + solve(0, len(s)-1)
}

func strangePrinterIterative(s string) int {
	/*
		  | a | b | c | a | b | c |
		a | 0 | 2 | 3 | 3 | 4 | 5 |
		b |   | 0 | 2 | 3 | 3 | 4 |
		c |   |   | 0 | 2 | 3 | 3 |
		a |   |   |   | 0 | 2 | 3 |
		b |   |   |   |   | 0 | 2 |
		c |   |   |   |   |   | 0 |
	*/
	dp := make([][]int, len(s))
	for i := range dp {
		dp[i] = make([]int, len(s))
	}
	for diff := 1; diff < len(s); diff++ {
		for r := diff; r < len(s); r++ {
			l := r - diff
			j := r
			for i := l; i < r; i++ {
				if s[i] != s[r] {
					j = i
					break
				}
			}
			if j == r {
				dp[l][r] = 0
				continue
			}
			minOpCount := len(s)
			for i := j; i < r; i++ {
				opCount := 1 + dp[j][i] + dp[i+1][r]
				if opCount < minOpCount {
					minOpCount = opCount
				}
			}
			dp[l][r] = minOpCount
		}
	}
	return 1 + dp[0][len(s)-1]
}

/*
	abcabc
	cccccc
	j=0
	1 + solve(0,0) + solve(1,5)
	OR
	1 + solve(0,1) + solve(2,5)

	abcabccb
	bbbbbbbb ->
*/
