package problem712

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func minimumDeleteSum(s1 string, s2 string) int {
	dp := make([][]int, len(s1)+1)
	for i := range dp {
		dp[i] = make([]int, len(s2)+1)
	}
	sum := 0
	for i := len(s1) - 1; i >= 0; i-- {
		sum += int(s1[i])
		dp[i][len(s2)] = sum
	}
	sum = 0
	for i := len(s2) - 1; i >= 0; i-- {
		sum += int(s2[i])
		dp[len(s1)][i] = sum
	}
	for i := len(s1) - 1; i >= 0; i-- {
		for j := len(s2) - 1; j >= 0; j-- {
			if s1[i] == s2[j] {
				dp[i][j] = dp[i+1][j+1]
			} else {
				dp[i][j] = min(int(s1[i])+dp[i+1][j], int(s2[j])+dp[i][j+1])
			}
		}
	}
	return dp[0][0]
}

/*
   | d | e | l | e | t | e |   |
 l |   |   |   |   |   |   |   |
 e |   |   |   |   |   |   |   |
 e |   |   |   |   |   |   |   |
 t |   |   |   |   |   |   |   |

   |1c |2c |3a |4c |5c |6j |7p |
1f |   |   |   |   |   |   |   |
2w |   |   |   |   |   |   |   |
3o |   |   |   |   |   |   |   |
4s |   |   |   |   |   |   |   |
5a |   |   |   |   |   |   |   |
6r |   |   |   |   |   |   |   |
7c |   |   |   |   |   |   |   |
8w |   |   |   |   |   |   |   |
9g |   |   |   |   |   |   |   |
10e|   |   |   |   |   |   |   |

   |115|101|97 |
101|   |   |   |
97 |   |   |   |
116|   |   |213|
*/
