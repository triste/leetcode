package problem1

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestTwoSum(t *testing.T) {
	scenarios := [...]struct {
		nums   []int
		target int
		output []int
	}{
		0: {[]int{2, 7, 11, 15}, 9, []int{0, 1}},
		1: {[]int{3, 2, 4}, 6, []int{1, 2}},
		2: {[]int{3, 3}, 6, []int{0, 1}},
	}
	for i, scenario := range scenarios {
		output := twoSum(scenario.nums, scenario.target)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
