package problem2130

import (
	. "gitlab.com/triste/leetcode/pkg/list"
)

func pairSum(head *ListNode) (maxSum int) {
	slow := head
	for fast := slow.Next.Next; fast != nil; fast = fast.Next.Next {
		slow = slow.Next
	}
	var prev *ListNode
	for slow, slow.Next = slow.Next, nil; slow != nil; {
		slow.Next, slow, prev = prev, slow.Next, slow
	}
	for p1, p2 := head, prev; p1 != nil; p1, p2 = p1.Next, p2.Next {
		if sum := p1.Val + p2.Val; sum > maxSum {
			maxSum = sum
		}
	}
	return maxSum
}
