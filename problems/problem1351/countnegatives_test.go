package problem1351

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestCountnegatives(t *testing.T) {
	scenarios := [...]struct {
		grid   [][]int
		output int
	}{
		0: {
			grid: [][]int{
				{4, 3, 2, -1},
				{3, 2, 1, -1},
				{1, 1, -1, -2},
				{-1, -1, -2, -3},
			},
			output: 8,
		},
		1: {
			grid: [][]int{
				{3, 2},
				{1, 0},
			},
			output: 0,
		},
	}
	for i, scenario := range scenarios[:1] {
		output := countNegatives(scenario.grid)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
