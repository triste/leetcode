package problem78

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestSubsets(t *testing.T) {
	scenarios := [...]struct {
		nums   []int
		output [][]int
	}{
		0: {
			nums: []int{1, 2, 3},
			output: [][]int{
				nil, {1}, {2}, {1, 2}, {3}, {1, 3}, {2, 3}, {1, 2, 3},
			},
		},
		1: {
			nums: []int{0},
			output: [][]int{
				nil, {0},
			},
		},
	}
	for i, scenario := range scenarios {
		output := subsets(scenario.nums)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
