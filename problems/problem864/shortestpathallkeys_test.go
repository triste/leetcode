package problem864

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestShortestpathallkeys(t *testing.T) {
	scenarios := [...]struct {
		grid   []string
		output int
	}{
		0: {
			grid: []string{
				"@.a..",
				"###.#",
				"b.A.B",
			},
			output: 8,
		},
		1: {
			grid: []string{
				"@..aA",
				"..B#.",
				"....b",
			},
			output: 6,
		},
		2: {
			grid: []string{
				"@Aa",
			},
			output: -1,
		},
	}
	for i, scenario := range scenarios {
		output := shortestPathAllKeys(scenario.grid)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
