package problem1359

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestCountorders(t *testing.T) {
	scenarios := [...]struct {
		n      int
		output int
	}{
		0: {
			n:      1,
			output: 1,
		},
		1: {
			n:      2,
			output: 6,
		},
		2: {
			n:      3,
			output: 90,
		},
		3: {
			n:      8,
			output: 729647433,
		},
	}
	for i, scenario := range scenarios {
		output := countOrders(scenario.n)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
