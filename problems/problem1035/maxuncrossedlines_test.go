package problem1035

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestMaxuncrossedlines(t *testing.T) {
	scenarios := [...]struct {
		nums1  []int
		nums2  []int
		output int
	}{
		0: {
			nums1:  []int{1, 4, 2},
			nums2:  []int{1, 2, 4},
			output: 2,
		},
		1: {
			nums1:  []int{2, 5, 1, 2, 5},
			nums2:  []int{10, 5, 2, 1, 5, 2},
			output: 3,
		},
		2: {
			nums1:  []int{1, 3, 7, 1, 7, 5},
			nums2:  []int{1, 9, 2, 5, 1},
			output: 2,
		},
		3: {
			nums1:  []int{3, 3},
			nums2:  []int{1, 3, 1, 2, 1},
			output: 1,
		},
		4: {
			nums1:  []int{3, 1, 2, 1, 4, 1, 2, 2, 5, 3, 2, 1, 1, 4, 5, 2, 3, 2, 5, 5},
			nums2:  []int{2, 4, 1, 2, 3, 4, 2, 4, 5, 5, 1, 1, 2, 1, 1, 1, 5, 4, 1, 4, 2, 1, 5, 4, 2, 3, 1, 5, 2, 1},
			output: 14,
		},
		5: {
			nums1:  []int{4, 4, 4, 2, 4, 1, 4, 3, 2, 4, 4, 2, 2, 2, 5, 2, 5, 4, 5, 1},
			nums2:  []int{2, 1, 5, 4, 2, 3, 5, 1, 2, 2},
			output: 6,
		},
	}
	for i, scenario := range scenarios {
		output := maxUncrossedLines(scenario.nums1, scenario.nums2)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
