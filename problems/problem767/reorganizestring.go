package problem767

func reorganizeString(s string) string {
	var freqs [26]int
	for _, ch := range s {
		freqs[ch-'a']++
	}
	maxFreq, maxFreqIdx := freqs[0], 0
	for i := 1; i < len(freqs); i++ {
		if freqs[i] > maxFreq {
			maxFreq = freqs[i]
			maxFreqIdx = i
		}
	}
	if maxFreq > (len(s)+1)/2 {
		return ""
	}
	bytes := make([]byte, len(s))
	i := 0
	for ; freqs[maxFreqIdx] > 0; freqs[maxFreqIdx]-- {
		bytes[i] = 'a' + byte(maxFreqIdx)
		i += 2
	}
	for j, freq := range freqs {
		for ; freq > 0; freq-- {
			if i >= len(s) {
				i = 1
			}
			bytes[i] = 'a' + byte(j)
			i += 2
		}
	}
	return string(bytes)
}
