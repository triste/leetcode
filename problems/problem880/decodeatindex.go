package problem880

func decodeAtIndex(s string, k int) string {
	decodedLenght := 0
	i := 0
	for ; decodedLenght < k; i++ {
		if s[i] >= 'a' {
			decodedLenght++
		} else {
			decodedLenght *= int(s[i] - '0')
		}
	}
	for i--; ; i-- {
		if s[i] >= 'a' {
			if k == 0 || k == decodedLenght {
				return s[i : i+1]
			}
			decodedLenght--
		} else {
			decodedLenght /= int(s[i] - '0')
			k %= decodedLenght
		}
	}
}
