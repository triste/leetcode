package problem130

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestSolve(t *testing.T) {
	scenarios := [...]struct {
		board  [][]byte
		output [][]byte
	}{
		0: {[][]byte{
			{'X', 'X', 'X', 'X'},
			{'X', 'O', 'O', 'X'},
			{'X', 'X', 'O', 'X'},
			{'X', 'O', 'X', 'X'},
		}, [][]byte{
			{'X', 'X', 'X', 'X'},
			{'X', 'X', 'X', 'X'},
			{'X', 'X', 'X', 'X'},
			{'X', 'O', 'X', 'X'},
		}},
		1: {[][]byte{
			{'X'},
		}, [][]byte{
			{'X'},
		}},
		2: {[][]byte{
			{'X', 'O', 'X', 'O', 'O', 'O', 'O'},
			{'X', 'O', 'O', 'O', 'O', 'O', 'O'},
			{'X', 'O', 'O', 'O', 'O', 'X', 'O'},
			{'O', 'O', 'O', 'O', 'X', 'O', 'X'},
			{'O', 'X', 'O', 'O', 'O', 'O', 'O'},
			{'O', 'O', 'O', 'O', 'O', 'O', 'O'},
			{'O', 'X', 'O', 'O', 'O', 'O', 'O'},
		}, [][]byte{
			{'X', 'O', 'X', 'O', 'O', 'O', 'O'},
			{'X', 'O', 'O', 'O', 'O', 'O', 'O'},
			{'X', 'O', 'O', 'O', 'O', 'X', 'O'},
			{'O', 'O', 'O', 'O', 'X', 'O', 'X'},
			{'O', 'X', 'O', 'O', 'O', 'O', 'O'},
			{'O', 'O', 'O', 'O', 'O', 'O', 'O'},
			{'O', 'X', 'O', 'O', 'O', 'O', 'O'},
		}},
		3: {[][]byte{
			{'X', 'O', 'X', 'O', 'X', 'O', 'O', 'O', 'X', 'O'},
			{'X', 'O', 'O', 'X', 'X', 'X', 'O', 'O', 'O', 'X'},
			{'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'X', 'X'},
			{'O', 'O', 'O', 'O', 'O', 'O', 'X', 'O', 'O', 'X'},
			{'O', 'O', 'X', 'X', 'O', 'X', 'X', 'O', 'O', 'O'},
			{'X', 'O', 'O', 'X', 'X', 'X', 'O', 'X', 'X', 'O'},
			{'X', 'O', 'X', 'O', 'O', 'X', 'X', 'O', 'X', 'O'},
			{'X', 'X', 'O', 'X', 'X', 'O', 'X', 'O', 'O', 'X'},
			{'O', 'O', 'O', 'O', 'X', 'O', 'X', 'O', 'X', 'O'},
			{'X', 'X', 'O', 'X', 'X', 'X', 'X', 'O', 'O', 'O'},
		}, [][]byte{
			{'X', 'O', 'X', 'O', 'X', 'O', 'O', 'O', 'X', 'O'},
			{'X', 'O', 'O', 'X', 'X', 'X', 'O', 'O', 'O', 'X'},
			{'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'X', 'X'},
			{'O', 'O', 'O', 'O', 'O', 'O', 'X', 'O', 'O', 'X'},
			{'O', 'O', 'X', 'X', 'O', 'X', 'X', 'O', 'O', 'O'},
			{'X', 'O', 'O', 'X', 'X', 'X', 'X', 'X', 'X', 'O'},
			{'X', 'O', 'X', 'X', 'X', 'X', 'X', 'O', 'X', 'O'},
			{'X', 'X', 'O', 'X', 'X', 'X', 'X', 'O', 'O', 'X'},
			{'O', 'O', 'O', 'O', 'X', 'X', 'X', 'O', 'X', 'O'},
			{'X', 'X', 'O', 'X', 'X', 'X', 'X', 'O', 'O', 'O'},
		}},
	}
	for i, scenario := range scenarios {
		solve(scenario.board)
		if diff := cmp.Diff(scenario.output, scenario.board); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
