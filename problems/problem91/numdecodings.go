package problem91

func numDecodings(s string) int {
	return numDecodingsIterative(s)
}

func numDecodingsRecursive(s string) int {
	var solve func(i int) int
	solve = func(i int) int {
		if i >= len(s) {
			return 1
		}
		if s[i] == '0' {
			return 0
		}
		decodeingCount := solve(i + 1)
		if i+1 < len(s) {
			if (s[i]-'0')*10+s[i+1]-'0' <= 26 {
				decodeingCount += solve(i + 2)
			}
		}
		return decodeingCount
	}
	return solve(0)
}

func numDecodingsIterative(s string) int {
	first, second := 0, 1
	if s[len(s)-1] != '0' {
		first = 1
	}
	for i := len(s) - 2; i >= 0; i-- {
		tmp := first
		if s[i] == '0' {
			first = 0
		} else if (s[i]-'0')*10+s[i+1]-'0' <= 26 {
			first += second
		}
		second = tmp
	}
	return first
}

/*
  | 2 | 2 | 6 |
2 |   |   |   |
2 |   |   |   |
6 |   |   |   |
*/
