package problem139

func wordBreak(s string, words []string) bool {
	dict := make(map[string]bool, len(words))
	for _, word := range words {
		dict[word] = true
	}
	n := len(s)
	memo := make(map[int]bool, n)
	var segment func(i int) bool
	segment = func(i int) bool {
		if i >= n {
			return true
		}
		if val, ok := memo[i]; ok {
			return val
		}
		segmented := false
		for j := i; j < n; j++ {
			if dict[s[i:j+1]] {
				if segment(j + 1) {
					segmented = true
					break
				}
			}
		}
		memo[i] = segmented
		return segmented
	}
	return segment(0)
}
