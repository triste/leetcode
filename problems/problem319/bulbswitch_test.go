package problem319

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestBulbswitch(t *testing.T) {
	scenarios := [...]struct {
		n      int
		output int
	}{
		0: {
			n:      3,
			output: 1,
		},
		1: {
			n:      0,
			output: 0,
		},
		2: {
			n:      1,
			output: 1,
		},
		3: {
			n:      4,
			output: 2,
		},
	}
	for i, scenario := range scenarios {
		output := bulbSwitch(scenario.n)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
