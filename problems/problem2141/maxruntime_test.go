package problem2141

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestMaxruntime(t *testing.T) {
	scenarios := [...]struct {
		n         int
		batteries []int
		output    int64
	}{
		0: {
			n:         2,
			batteries: []int{3, 3, 3},
			output:    4,
		},
		1: {
			n:         2,
			batteries: []int{1, 1, 1, 1},
			output:    2,
		},
		2: {
			n:         2,
			batteries: []int{2, 1, 3, 5, 4},
			output:    7,
		},
		3: {
			n:         3,
			batteries: []int{10, 10, 3, 5},
			output:    8,
		},
		4: {
			n:         1,
			batteries: []int{53, 96},
			output:    149,
		},
	}
	for i, scenario := range scenarios[4:] {
		output := maxRunTime(scenario.n, scenario.batteries)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
