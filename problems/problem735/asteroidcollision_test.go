package problem735

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestAsteroidcollision(t *testing.T) {
	scenarios := [...]struct {
		asteroids []int
		output    []int
	}{
		0: {
			asteroids: []int{5, 10, -5},
			output:    []int{5, 10},
		},
		1: {
			asteroids: []int{8, -8},
			output:    nil,
		},
		2: {
			asteroids: []int{10, 2, -5},
			output:    []int{10},
		},
	}
	for i, scenario := range scenarios {
		output := asteroidCollision(scenario.asteroids)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
