package problem973

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestKclosest(t *testing.T) {
	scenarios := [...]struct {
		points [][]int
		k      int
		output [][]int
	}{
		0: {[][]int{{1, 3}, {-2, 2}}, 1, [][]int{{-2, 2}}},
		1: {[][]int{{3, 3}, {5, -1}, {-2, 4}}, 2, [][]int{{3, 3}, {-2, 4}}},
		2: {[][]int{{0, 1}, {1, 0}}, 2, [][]int{{0, 1}, {1, 0}}},
	}
	for i, scenario := range scenarios {
		output := kClosest(scenario.points, scenario.k)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
