package problem1146

import "testing"

func TestSnapshotArray(t *testing.T) {
	sa := Constructor(3)
	if got := sa.Get(0, 0); got != 0 {
		t.Errorf("Get(0,0) = 0; got %v", got)
	}
	sa.Set(0, 5)
	if got := sa.Get(0, 0); got != 5 {
		t.Errorf("Get(0,0) = 5; got %v", got)
	}
	if got := sa.Snap(); got != 0 {
		t.Errorf("Snap = 0; got %v", got)
	}
	sa.Set(0, 6)
	if got := sa.Get(0, 0); got != 5 {
		t.Errorf("Get(0,0) = 5; got %v", got)
	}
	if got := sa.Get(0, 1); got != 6 {
		t.Errorf("Get(0,1) = 6; got %v", got)
	}
}
