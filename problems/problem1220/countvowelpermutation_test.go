package problem1220

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestCountvowelpermutation(t *testing.T) {
	scenarios := [...]struct {
		n      int
		output int
	}{
		0: {
			n:      1,
			output: 5,
		},
		1: {
			n:      2,
			output: 10,
		},
		2: {
			n:      5,
			output: 68,
		},
	}
	for i, scenario := range scenarios {
		output := countVowelPermutation(scenario.n)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
