package problem1823

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestFindthewinner(t *testing.T) {
	scenarios := [...]struct {
		n      int
		k      int
		output int
	}{
		0: {5,2,3},
		1: {6,5,1},
	}
	for i, scenario := range scenarios {
		output := findTheWinner(scenario.n, scenario.k)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
