package problem391

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestIsrectanglecover(t *testing.T) {
	scenarios := [...]struct {
		rectangles [][]int
		output     bool
	}{
		0: {
			rectangles: [][]int{
				{1, 1, 3, 3},
				{3, 1, 4, 2},
				{3, 2, 4, 4},
				{1, 3, 2, 4},
				{2, 3, 3, 4},
			},
			output: true,
		},
		1: {
			rectangles: [][]int{
				{1, 1, 2, 3},
				{1, 3, 2, 4},
				{3, 1, 4, 2},
				{3, 2, 4, 4},
			},
			output: false,
		},
		2: {
			rectangles: [][]int{
				{1, 1, 3, 3},
				{3, 1, 4, 2},
				{1, 3, 2, 4},
				{2, 2, 4, 4},
			},
			output: false,
		},
		3: {
			rectangles: [][]int{
				{0, 0, 4, 1},
				{0, 0, 4, 1},
			},
			output: false,
		},
		4: {
			rectangles: [][]int{
				{0, 0, 3, 3},
				{1, 1, 2, 2},
				{1, 1, 2, 2},
			},
			output: false,
		},
		5: {
			rectangles: [][]int{
				{0, 0, 1, 1},
				{0, 2, 1, 3},
				{1, 1, 2, 2},
				{2, 0, 3, 1},
				{2, 2, 3, 3},
				{1, 0, 2, 3},
				{0, 1, 3, 2},
			},
			output: false,
		},
	}
	for i, scenario := range scenarios {
		output := isRectangleCover(scenario.rectangles)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
