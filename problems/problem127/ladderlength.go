package problem127

func wordHash(word string) (hash uint64) {
	for i, ch := range word {
		hash |= uint64(ch) << (i * 8)
	}
	return
}

func hashMask(hash uint64, i int) uint64 {
	return hash & (^(uint64(0xFF) << (i * 8)))
}

func ladderLength(beginWord string, endWord string, wordList []string) int {
	masksGraph := make(map[uint64][]uint64, len(wordList)*len(beginWord))
	for _, word := range wordList {
		hash := wordHash(word)
		for i := range word {
			mask := hashMask(hash, i)
			masksGraph[mask] = append(masksGraph[mask], hash)
		}
	}
	n := len(beginWord)
	begin := wordHash(beginWord)
	end := wordHash(endWord)
	queue := []uint64{begin}
	var tmpQueue []uint64
	depthMap := make(map[uint64]int, len(wordList))
	for depth := 1; len(queue) > 0; depth++ {
		for _, word := range queue {
			if depthMap[word] > 0 && depthMap[word] != depth {
				continue
			}
			if word == end {
				return depth
			}
			depthMap[word] = depth
			for i := 0; i < n; i++ {
				mask := hashMask(word, i)
				for _, w := range masksGraph[mask] {
					tmpQueue = append(tmpQueue, w)
				}
			}
		}
		queue, tmpQueue = tmpQueue, queue[:0]
	}
	return 0
}
