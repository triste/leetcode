package problem99

import (
	"testing"

	"github.com/google/go-cmp/cmp"
	. "gitlab.com/triste/leetcode/pkg/tree"
)

func TestRecovertree(t *testing.T) {
	scenarios := [...]struct {
		root   *TreeNode
		output *TreeNode
	}{
		0: {
			root:   NewTree(1, 3, nil, nil, 2),
			output: NewTree(3, 1, nil, nil, 2),
		},
		1: {
			root:   NewTree(3, 1, 4, nil, nil, 2),
			output: NewTree(2, 1, 4, nil, nil, 3),
		},
		2: {
			root:   NewTree(2, 3, 1),
			output: NewTree(2, 1, 3),
		},
		3: {
			root:   NewTree(3, nil, 2, nil, 1),
			output: NewTree(1, nil, 2, nil, 3),
		},
	}
	for i, scenario := range scenarios {
		recoverTree(scenario.root)
		if diff := cmp.Diff(scenario.output, scenario.root); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
