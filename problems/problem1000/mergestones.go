package problem1000

import (
	"math"
)

func mergeStones(stonePiles []int, k int) int {
	return mergeStonesRecursive(stonePiles, k)
}

func mergeStonesRecursiveV2(stonePiles []int, k int) int {
	n := len(stonePiles)
	if !isMember(k, k-1, n) {
		return -1
	}
	presums := make([]int, n+1)
	for i := 0; i < n; i++ {
		presums[i+1] = presums[i] + stonePiles[i]
	}
	var memo [31][31][31]int
	var solve func(l, r, m int) int
	solve = func(l, r, m int) int {
		if memo[l][r][m] != 0 {
			return memo[l][r][m]
		}
		if m == 1 {
			return solve(l, r, k)
		}
		if l == r {
			return 0
		}
		if l > r {
			return -1
		}
		minCost := math.MaxInt
		for i := l; i < r; i = i + k - 1 {
			leftCost := solve(l, i, k)
			if leftCost < 0 {
				continue
			}
			leftCost += presums[i+1] - presums[l]
			rightCost := solve(i+1, r, m-1)
			if rightCost < 0 {
				continue
			}
			cost := leftCost + rightCost
			if cost < minCost {
				minCost = cost
			}
		}
		if minCost == math.MaxInt {
			memo[l][r][m] = -1
		} else {
			memo[l][r][m] = minCost
		}
		return memo[l][r][m]
	}
	return presums[len(presums)-1] + solve(0, len(stonePiles)-1, k)
}

/*
	k = 1
	1,2,3,4,5...
	k = 2
	2,3,4,5...
	k = 3
	3,5,7,9,11
	k = 4
	4,7,10,13

	(x0, x1, x2, x3, x4, x5), k = 2
	last moves:
		x0,x1+x2+x3+x4+x5
		x0+x1,x2+x3+x4+x5
		x0+x1+x2,x3+x4+x5
		x0+x1+x2+x3,x4+x5
		x0+x1+x2+x3+x4,x5

	(x0, x1, x2, x3, x4), k = 3
	last moves:
		x0,x1,x2+x3+x4
		x0,x1+x2+x3,x4
		x0+x1+x2,x3,x4

	(x0, x1, x2, x3, x4, x5, x6), k = 3
	last moves:
		x0,x1,x2+x3+x4+x5+x6 => case(x0)+case(x1)+case(x2,x3,x4,x5,x6)
		x0,x1+x2+x3,x4+x5+x6 => case(x0)+case(x1,x2,x3)+case(x4,x5,x6)
		x0,x1+x2+x3+x4+x5,x6 => case(x0)+case(x1,x2,x3,x4,x5)+case(x6)   case(x0, 1) + case(x1,x2,x3,x4,x5,x6, 2)

		x0+x1+x2,x3,x4+x5+x6 => case(x0,x1,x2)+case(x3)+case(x4,x5,x6)   case(x0,x1,x2, 1) + case(x3,x4,x5,x6, 2)
		x0+x1+x2,x3+x4+x5,x6 => case(x0,x1,x2)+case(x3,x4,x5)+case(x6)

		x0+x1+x2+x3+x4,x5,x6 => case(x0,x1,x2,x3,x4)+case(x5)+case(x6)   case(x0,x1,x2,x3,x4, 1) + case(x5,x6, 2)
*/

func isMember(start, diff, num int) bool {
	if diff == 0 {
		return start == num
	}
	return ((num-start)%diff == 0) && (num-start/diff >= 0)
}

func mergeStonesRecursive(stonePiles []int, k int) int {
	n := len(stonePiles)
	presums := make([]int, n+1)
	for i := 0; i < n; i++ {
		presums[i+1] = presums[i] + stonePiles[i]
	}
	var memo [31][31][31]int
	var solve func(l, r, m int) int
	solve = func(l, r, m int) int {
		if memo[l][r][m] != 0 {
			return memo[l][r][m]
		}
		minCost := -1
		if l == r {
			if m == 1 {
				minCost = 0
			}
		} else if m == 1 {
			minCost = solve(l, r, k)
			if minCost != -1 {
				minCost += presums[r+1] - presums[l]
			}
		} else {
			min := math.MaxInt
			for i := l; i < r; i = i + k - 1 {
				leftCost := solve(l, i, 1)
				if leftCost < 0 {
					continue
				}
				rightCost := solve(i+1, r, m-1)
				if rightCost < 0 {
					continue
				}
				cost := leftCost + rightCost
				if cost < min {
					min = cost
				}
			}
			if min != math.MaxInt {
				minCost = min
			}
		}
		memo[l][r][m] = minCost
		return minCost
	}
	return solve(0, len(stonePiles)-1, 1)
}

func mergeStonesIterative(stonePiles []int, k int) int {
	var dp [31][31][31]int
	return dp[0][0][0]
}

/*
stones := 3,2,4,1
k := 2
*/

/*
	(x0, x1, x2, x3, x4, x5), k = 2
	last moves:
		x0,x1+x2+x3+x4+x5
		x0+x1,x2+x3+x4+x5
		x0+x1+x2,x3+x4+x5
		x0+x1+x2+x3,x4+x5
		x0+x1+x2+x3+x4,x5

	(x0, x1, x2, x3, x4), k = 3
	last moves:
		x0,x1,x2+x3+x4
		x0,x1+x2+x3,x4
		x0+x1+x2,x3,x4

	(x0, x1, x2, x3, x4, x5, x6), k = 3
	last moves:
		x0,x1,x2+x3+x4+x5+x6 => case(x0)+case(x1)+case(x2,x3,x4,x5,x6)
		x0,x1+x2+x3,x4+x5+x6 => case(x0)+case(x1,x2,x3)+case(x4,x5,x6)
		x0,x1+x2+x3+x4+x5,x6 => case(x0)+case(x1,x2,x3,x4,x5)+case(x6)   case(x0, 1) + case(x1,x2,x3,x4,x5,x6, 2)

		x0+x1+x2,x3,x4+x5+x6 => case(x0,x1,x2)+case(x3)+case(x4,x5,x6)   case(x0,x1,x2, 1) + case(x3,x4,x5,x6, 2)
		x0+x1+x2,x3+x4+x5,x6 => case(x0,x1,x2)+case(x3,x4,x5)+case(x6)
		x0+x1+x2+x3+x4,x5,x6

	k = 1
	1,2,3,4,5...
	k = 2
	2,3,4,5...
	k = 3
	3,5,7,9,11
	k = 4
	4,7,10,13
*/
