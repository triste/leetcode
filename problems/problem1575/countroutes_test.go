package problem1575

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestCountroutes(t *testing.T) {
	scenarios := [...]struct {
		locations []int
		start     int
		finish    int
		fuel      int
		output    int
	}{
		0: {
			locations: []int{2, 3, 6, 8, 4},
			start:     1,
			finish:    3,
			fuel:      5,
			output:    4,
		},
		1: {
			locations: []int{4, 3, 1},
			start:     1,
			finish:    0,
			fuel:      6,
			output:    5,
		},
		2: {
			locations: []int{5, 2, 1},
			start:     0,
			finish:    2,
			fuel:      3,
			output:    0,
		},
		3: {
			locations: []int{1, 2, 3},
			start:     0,
			finish:    2,
			fuel:      40,
			output:    615088286,
		},
	}
	for i, scenario := range scenarios {
		output := countRoutes(scenario.locations, scenario.start, scenario.finish, scenario.fuel)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
