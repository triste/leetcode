package problem451

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestFrequencysort(t *testing.T) {
	scenarios := [...]struct {
		s      string
		output string
	}{
		0: {"tree", "eert"},
		1: {"cccaaa", "aaaccc"},
		2: {"Aabb", "bbAa"},
	}
	for i, scenario := range scenarios {
		output := frequencySort(scenario.s)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
