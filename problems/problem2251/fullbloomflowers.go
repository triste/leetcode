package problem2251

import (
	"container/heap"
	"sort"
)

type IntHeap []int

func (h IntHeap) Len() int           { return len(h) }
func (h IntHeap) Less(i, j int) bool { return h[i] < h[j] }
func (h IntHeap) Swap(i, j int)      { h[i], h[j] = h[j], h[i] }

func (h *IntHeap) Push(x any) {
	*h = append(*h, x.(int))
}

func (h *IntHeap) Pop() any {
	old := *h
	n := len(old)
	x := old[n-1]
	*h = old[0 : n-1]
	return x
}

type MultiSlice struct {
	people  []int
	indices []int
}

func (ms MultiSlice) Len() int           { return len(ms.people) }
func (ms MultiSlice) Less(i, j int) bool { return ms.people[i] < ms.people[j] }
func (ms MultiSlice) Swap(i, j int) {
	ms.people[i], ms.people[j] = ms.people[j], ms.people[i]
	ms.indices[i], ms.indices[j] = ms.indices[j], ms.indices[i]
}

func fullBloomFlowers(flowers [][]int, people []int) []int {
	sort.Slice(flowers, func(i, j int) bool {
		return flowers[i][0] < flowers[j][0]
	})
	indices := make([]int, len(people))
	for i := range indices {
		indices[i] = i
	}
	ms := MultiSlice{people, indices}
	sort.Sort(ms)
	ends := &IntHeap{}
	heap.Init(ends)
	f := 0
	bloomCount := 0
	output := make([]int, len(people))
	for i, day := range people {
		for ; f < len(flowers) && flowers[f][0] <= day; f++ {
			heap.Push(ends, flowers[f][1])
			bloomCount++
		}
		for ends.Len() > 0 && (*ends)[0] < day {
			heap.Pop(ends)
			bloomCount--
		}
		output[indices[i]] = bloomCount
	}
	return output
}
