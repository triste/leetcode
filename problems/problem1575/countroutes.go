package problem1575

func minmax(a, b int) (int, int) {
	if a < b {
		return a, b
	}
	return b, a
}

func countRoutes(locations []int, start int, finish int, fuel int) int {
	memo := make([][]int, len(locations))
	for i := range memo {
		memo[i] = make([]int, fuel+1)
		for j := range memo[i] {
			memo[i][j] = -1
		}
	}
	var solve func(i, fuel int) int
	solve = func(i, fuel int) int {
		if memo[i][fuel] >= 0 {
			return memo[i][fuel]
		}
		routeCount := 0
		if i == finish {
			routeCount++
		}
		for j := range locations {
			if i != j {
				lowest, greatest := minmax(locations[i], locations[j])
				if cost := greatest - lowest; cost <= fuel {
					routeCount = (routeCount + solve(j, fuel-cost)) % 1_000_000_007
				}
			}
		}
		memo[i][fuel] = routeCount
		return routeCount
	}
	return solve(start, fuel)
}
