package problem165

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestCompareversion(t *testing.T) {
	scenarios := [...]struct {
		version1 string
		version2 string
		output   int
	}{
		0: {
			version1: "1.01",
			version2: "1.001",
			output:   0,
		},
		1: {
			version1: "1.0",
			version2: "1.0.0",
			output:   0,
		},
		2: {
			version1: "0.1",
			version2: "1.1",
			output:   -1,
		},
		3: {
			version1: "1.1",
			version2: "0.1",
			output:   1,
		},
	}
	for i, scenario := range scenarios {
		output := compareVersion(scenario.version1, scenario.version2)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
