package problem292

func canWinNim(n int) bool {
	return n%4 != 0
}

/*
	case 1:
		true
	case 2:
		true
	case 3:
		true
	case 4:
		false
	case 5:
		true
	case 6:
		true
	case 7:
		true
	case 8:
		false
	case 9:
		true
	case 10:
		true
	case 11:
		true
	case 12:
		false
*/
