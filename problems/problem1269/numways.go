package problem1269

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func numWays(steps int, arrLen int) int {
	dp := make([]int, min(steps/2+1, arrLen)+1)
	dp[0] = 1
	for i := 0; i < steps; i++ {
		for prev, j := 0, 0; j < len(dp)-1; j++ {
			dp[j], prev = (prev+dp[j]+dp[j+1])%1_000_000_007, dp[j]
		}
	}
	return dp[0]
}

/*
s\i| 0 | 1 | 2 | 3 |
 0 | 1 | 0 | 0 | 0 |
 1 |   | x | x | x |
 2 |   |   | y |   |
*/
