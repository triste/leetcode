package problem32

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestLongestvalidparentheses(t *testing.T) {
	scenarios := [...]struct {
		s      string
		output int
	}{
		0: {
			s:      "(()",
			output: 2,
		},
		1: {
			s:      ")()())",
			output: 4,
		},
		2: {
			s:      "",
			output: 0,
		},
		3: {
			s:      ")(",
			output: 0,
		},
		4: {
			s:      "()(()",
			output: 2,
		},
	}
	for i, scenario := range scenarios {
		output := longestValidParentheses(scenario.s)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
