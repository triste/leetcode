package problem403

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestCancross(t *testing.T) {
	scenarios := [...]struct {
		stones []int
		output bool
	}{
		0: {
			stones: []int{0, 1, 3, 5, 6, 8, 12, 17},
			output: true,
		},
		1: {
			stones: []int{0, 1, 2, 3, 4, 8, 9, 11},
			output: false,
		},
	}
	for i, scenario := range scenarios {
		output := canCross(scenario.stones)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
