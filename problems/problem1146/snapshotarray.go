package problem1146

import (
	"sort"
)

type SnapshotArray struct {
	histories [][][2]int
	snapCount int
}

func Constructor(length int) SnapshotArray {
	return SnapshotArray{
		histories: make([][][2]int, length),
	}
}

func (s *SnapshotArray) Set(index int, val int) {
	lastIndexRecord := len(s.histories[index]) - 1
	if lastIndexRecord >= 0 && s.histories[index][lastIndexRecord][0] == s.snapCount {
		s.histories[index][lastIndexRecord][1] = val
	} else {
		s.histories[index] = append(s.histories[index], [2]int{s.snapCount, val})
	}
}

func (s *SnapshotArray) Snap() int {
	s.snapCount++
	return s.snapCount - 1
}

func (s *SnapshotArray) Get(index int, snap_id int) int {
	i := sort.Search(len(s.histories[index]), func(i int) bool {
		return s.histories[index][i][0] > snap_id
	})
	if i--; i >= 0 && i < len(s.histories[index]) {
		return s.histories[index][i][1]
	}
	return 0
}

/*
	[]
Set(0,5)
	[(0,5)]
Snap()
	[(0,5)]
	[]
Set(0,6)
	[(0,5)]
	[(0,6)]
*/
