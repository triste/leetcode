package problem1721

import (
	. "gitlab.com/triste/leetcode/pkg/list"
)

func swapNodes(head *ListNode, k int) *ListNode {
	faster := head
	for i := 1; i < k; i++ {
		faster = faster.Next
	}
	first := faster
	second := head
	for faster = faster.Next; faster != nil; faster = faster.Next {
		second = second.Next
	}
	first.Val, second.Val = second.Val, first.Val
	return head
}
