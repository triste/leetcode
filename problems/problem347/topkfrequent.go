package problem347

import (
	_ "log"
	"math/rand"
)

var m map[int]int

func partition(nums []int) int {
	lenght := len(nums)
	randomIndex := rand.Intn(lenght)
	pivot := m[nums[randomIndex]]
	nums[randomIndex], nums[lenght-1] = nums[lenght-1], nums[randomIndex]
	tempIndex := 0
	for i, num := range nums[:lenght-1] {
		if m[num] < pivot {
			nums[tempIndex], nums[i] = num, nums[tempIndex]
			tempIndex++
		}
	}
	nums[tempIndex], nums[lenght-1] = nums[lenght-1], nums[tempIndex]
	return tempIndex
}

func findKthLargest(nums []int, k int) int {
	lenght := len(nums)
	if lenght == 1 {
		return 0
	}
	pivotIndex := partition(nums)
	if lenght-pivotIndex < k {
		return findKthLargest(nums[:pivotIndex], k-(lenght-pivotIndex))
	} else if lenght-pivotIndex > k {
		return pivotIndex + 1 + findKthLargest(nums[pivotIndex+1:], k)
	} else {
		return pivotIndex
	}
}

func topKFrequent(nums []int, k int) []int {
	m = make(map[int]int)
	var uniques []int
	for _, num := range nums {
		if _, ok := m[num]; ok == false {
			uniques = append(uniques, num)
		}
		m[num]++
	}
	kthLargestIndex := findKthLargest(uniques, k)
	return uniques[kthLargestIndex:]
}
