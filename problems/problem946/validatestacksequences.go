package problem946

func validateStackSequences(pushed []int, popped []int) (output bool) {
	stack := make([]int, 0, len(pushed))
	for _, p := range popped {
		if len(stack) > 0 && stack[len(stack)-1] == p {
			stack = stack[:len(stack)-1]
			continue
		}
		for i := 0; ; i++ {
			if i == len(pushed) {
				return false
			}
			if pushed[i] == p {
				pushed = pushed[i+1:]
				break
			}
			stack = append(stack, pushed[i])
		}
	}
	return true
}
