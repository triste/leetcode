package problem212

type Node struct {
	children [26]*Node
	word     string
}

type Trie struct {
	root Node
}

func (t *Trie) insert(word string) {
	curnode := &t.root
	for _, ch := range word {
		i := ch - 'a'
		if curnode.children[i] == nil {
			curnode.children[i] = &Node{}
		}
		curnode = curnode.children[i]
	}
	curnode.word = word
}

func (node *Node) empty() bool {
	for _, child := range node.children {
		if child != nil {
			return false
		}
	}
	return true
}

func (t *Trie) remove(word string) {
	path := make([]*Node, len(word)+1)
	path[0] = &t.root
	for i, ch := range word {
		path[i+1] = path[i].children[ch-'a']
	}
	path[len(path)-1].word = ""
	for i := len(path) - 2; i >= 0 && path[i+1].empty(); i-- {
		path[i].children[word[i]-'a'] = nil
	}
}

func findWords(board [][]byte, words []string) (output []string) {
	var trie Trie
	for _, word := range words {
		trie.insert(word)
	}
	dirs := [...][2]int{{-1, 0}, {0, 1}, {1, 0}, {0, -1}}
	var dfs func(r, c int, node *Node)
	dfs = func(r, c int, node *Node) {
		if r < 0 || r >= len(board) || c < 0 || c >= len(board[r]) || board[r][c] == 0 {
			return
		}
		ch := board[r][c]
		child := node.children[ch-'a']
		if child == nil {
			return
		}
		if child.word != "" {
			output = append(output, child.word)
			trie.remove(child.word)
		}
		board[r][c] = 0
		for _, dir := range dirs {
			dfs(r+dir[0], c+dir[1], child)
		}
		board[r][c] = ch
	}
	for r, row := range board {
		for c := range row {
			dfs(r, c, &trie.root)
		}
	}
	return
}
