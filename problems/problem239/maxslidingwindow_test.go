package problem239

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestMaxslidingwindow(t *testing.T) {
	scenarios := [...]struct {
		nums   []int
		k      int
		output []int
	}{
		0: {
			nums: []int{
				1, 3, -1, -3, 5, 3, 6, 7,
			},
			k: 3,
			output: []int{
				3, 3, 5, 5, 6, 7,
			},
		},
		1: {
			nums: []int{
				1,
			},
			k:      1,
			output: []int{1},
		},
	}
	for i, scenario := range scenarios {
		output := maxSlidingWindow(scenario.nums, scenario.k)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
