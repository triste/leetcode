package problem51

func rowConfigurations(n int) []string {
	rows := make([]string, n)
	rowBytes := make([]byte, n)
	for i := range rowBytes {
		rowBytes[i] = '.'
	}
	for i := range rowBytes {
		rowBytes[i] = 'Q'
		rows[i] = string(rowBytes)
		rowBytes[i] = '.'
	}
	return rows
}

func solveNQueens(n int) (boards [][]string) {
	rowConfigs := rowConfigurations(n)
	cols := make([]bool, n)
	diags1 := make([]bool, n*2-1)
	diags2 := make([]bool, n*2-1)
	board := make([]string, n)
	var solve func(r int)
	solve = func(r int) {
		if r >= n {
			b := make([]string, n)
			copy(b, board)
			boards = append(boards, b)
			return
		}
		for i := 0; i < n; i++ {
			d1 := i - r + n - 1
			d2 := i + r
			if cols[i] || diags1[d1] || diags2[d2] {
				continue
			}
			cols[i] = true
			diags1[d1] = true
			diags2[d2] = true
			board[r] = rowConfigs[i]
			solve(r + 1)
			cols[i] = false
			diags1[d1] = false
			diags2[d2] = false
		}
	}
	solve(0)
	return
}

/*
	n = 5

	| x |   |   |   |   |
	|   |   | x |   |   |
	|   |   |   |   | x |
	|   | x |   |   |   |
	|   |   |   | x |   |

	|   | x |   |   |   |
	|   |   |   | x |   |
	| x |   |   |   |   |
	|   |   | x |   |   |
	|   |   |   |   | x |

	|   |   | x |   |   |
	|   |   |   |   | x |
	|   | x |   |   |   |
	|   |   |   | x |   |
	| x |   |   |   |   |

	|   |   | x |   |   |
	| x |   |   |   |   |
	|   |   |   | x |   |
	|   | x |   |   |   |
	|   |   |   |   | x |
*/
