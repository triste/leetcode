package problem572

import (
	. "gitlab.com/triste/leetcode/pkg/tree"
)

/*
func isSubtree(root *TreeNode, subRoot *TreeNode) (output bool) {
	if root == nil {
		return false
	}
	if isEqual(root, subRoot) {
		return true
	}
	return isSubtree(root.Left, subRoot) || isSubtree(root.Right, subRoot)
}
*/

func isSubtree(root *TreeNode, subRoot *TreeNode) (output bool) {
	subRootHash := hash(subRoot)
	var dfs func(root *TreeNode) (int64, bool)
	dfs = func(root *TreeNode) (int64, bool) {
		if root == nil {
			return 0, false
		}
		left, found := dfs(root.Left)
		if found {
			return left, found
		}
		right, found := dfs(root.Right)
		if found {
			return right, found
		}
		h := hashVec3(left, int64(root.Val), right)
		found = h == subRootHash && isEqual(root, subRoot)
		return h, found
	}
	_, found := dfs(root)
	return found
}

func hashVec3(a, b, c int64) int64 {
	return a*48538375 ^ b*23570375 ^ c*92757184
}

func hash(root *TreeNode) int64 {
	if root == nil {
		return 0
	}
	left := hash(root.Left)
	right := hash(root.Right)
	return hashVec3(left, int64(root.Val), right)
}

func isEqual(a, b *TreeNode) bool {
	if a == nil {
		if b == nil {
			return true
		}
		return false
	}
	if b == nil || b.Val != a.Val {
		return false
	}
	return isEqual(a.Left, b.Left) && isEqual(a.Right, b.Right)
}
