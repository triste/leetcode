package problem81

func search(nums []int, target int) bool {
	for l, r := 0, len(nums)-1; l <= r; {
		m := (l + r) / 2
		if nums[m] == target {
			return true
		}
		if nums[m] == nums[l] && nums[m] == nums[r] {
			l++
			r--
		} else if nums[l] <= nums[m] {
			if nums[l] <= target && target < nums[m] {
				r = m - 1
			} else {
				l = m + 1
			}
		} else if nums[m] < target && target <= nums[r] {
			l = m + 1
		} else {
			r = m - 1
		}
	}
	return false
}
