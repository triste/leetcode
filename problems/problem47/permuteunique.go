package problem47

import "sort"

func permuteUnique(nums []int) (permutations [][]int) {
	sort.Ints(nums)
	var dfs func(nums []int, path []int)
	dfs = func(nums []int, path []int) {
		if len(nums) == 0 {
			perm := make([]int, len(path))
			copy(perm, path)
			permutations = append(permutations, perm)
			return
		}
		prev := -11
		for i, num := range nums {
			if num == prev {
				continue
			}
			prev = num
			tail := make([]int, len(nums)-1)
			copy(tail, nums[:i])
			copy(tail[i:], nums[i+1:])
			dfs(tail, append(path, num))
		}
	}
	dfs(nums, nil)
	return
}
