package problem125

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestIspalindrome(t *testing.T) {
	scenarios := [...]struct {
		s      string
		output bool
	}{
		0: {
			s:      "A man, a plan, a canal: Panama",
			output: true,
		},
		1: {
			s:      "race a car",
			output: false,
		},
		2: {
			s:      " ",
			output: true,
		},
		3: {
			s:      "a.",
			output: true,
		},
		4: {
			s:      "0P",
			output: false,
		},
		5: {
			s:      "aa",
			output: true,
		},
	}
	for i, scenario := range scenarios {
		output := isPalindrome(scenario.s)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
