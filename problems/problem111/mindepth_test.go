package problem111

import (
	"testing"

	"github.com/google/go-cmp/cmp"
	. "gitlab.com/triste/leetcode/pkg/tree"
)

func TestMindepth(t *testing.T) {
	scenarios := [...]struct {
		root   *TreeNode
		output int
	}{
		0: {
			root:   NewTree(3, 9, 20, nil, nil, 15, 7),
			output: 2,
		},
		1: {
			root:   NewTree(2, nil, 3, nil, 4, nil, 5, nil, 6),
			output: 5,
		},
	}
	for i, scenario := range scenarios {
		output := minDepth(scenario.root)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
