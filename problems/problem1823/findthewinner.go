package problem1823

func findTheWinner(n int, k int) (output int) {
	if n == 1 {
		return 1
	}
	return (k - 1 + findTheWinner(n-1, k)) % n + 1
}

/*
	n == 1
		return 1
	n == 2
		return (k - 1 + case(1)) % n + 1
*/
