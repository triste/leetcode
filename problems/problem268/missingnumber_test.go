package problem268

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestMissingnumber(t *testing.T) {
	scenarios := [...]struct {
		nums   []int
		output int
	}{
		0: {
			nums:   []int{3, 0, 1},
			output: 2,
		},
		1: {
			nums:   []int{0, 1},
			output: 2,
		},
		2: {
			nums:   []int{9, 6, 4, 2, 3, 5, 7, 0, 1},
			output: 8,
		},
	}
	for i, scenario := range scenarios {
		output := missingNumber(scenario.nums)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
