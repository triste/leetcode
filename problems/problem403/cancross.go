package problem403

func canCross(stones []int) bool {
	return canCrossRecursive(stones)
}

func canCrossRecursive(stones []int) bool {
	stoneIndexes := make(map[int]int, len(stones)-1)
	for i := 1; i < len(stones); i++ {
		stoneIndexes[stones[i]] = i
	}
	memo := make(map[[2]int]bool)
	var solve func(i, k int) bool
	solve = func(i, k int) bool {
		key := [2]int{i, k}
		if val, ok := memo[key]; ok {
			return val
		}
		j := stoneIndexes[stones[i]+k]
		if k == 0 || j == 0 {
			return false
		}
		if j == len(stones)-1 {
			return true
		}
		memo[key] = solve(j, k-1) || solve(j, k) || solve(j, k+1)
		return memo[key]
	}
	return solve(0, 1)
}

func canCrossIterative(stones []int) bool {
	stoneIndexes := make(map[int]int, len(stones))
	for i := 0; i < len(stones); i++ {
		stoneIndexes[stones[i]] = i
	}
	var dp [2001][2001]bool
	for i := range dp[len(stones)-1] {
		dp[len(stones)-1][i] = true
	}
	for i := len(stones) - 2; i >= 0; i-- {
		for k := 1; k <= i+1; k++ {
			stone := stones[i] + k
			j := stoneIndexes[stone]
			dp[i][k] = (j < len(stones) && stones[j] == stone) && (dp[j][k-1] || dp[j][k] || dp[j][k+1])
		}
	}
	return dp[0][1]
}

/*
  0, 1, 3, 6,

  Xn = Xn-1 + Xn-1 - (Xn-2) + 1
*/
