package problem863

import (
	. "gitlab.com/triste/leetcode/pkg/tree"
)

func kDown(root *TreeNode, k int) (output []int) {
	var dfs func(node *TreeNode, dist int)
	dfs = func(node *TreeNode, dist int) {
		if node == nil {
			return
		}
		if dist == k {
			output = append(output, node.Val)
		} else {
			dfs(node.Left, dist+1)
			dfs(node.Right, dist+1)
		}
	}
	dfs(root, 0)
	return
}

func kUp(path []*TreeNode, k int) (output []int) {
	k--
	for i := len(path) - 2; i >= 0; i-- {
		if k == 0 {
			output = append(output, path[i].Val)
			break
		}
		if path[i].Left != path[i+1] {
			output = append(output, kDown(path[i].Left, k-1)...)
		} else {
			output = append(output, kDown(path[i].Right, k-1)...)
		}
		k--
	}
	return
}

func distanceK(root *TreeNode, target *TreeNode, k int) (output []int) {
	var path []*TreeNode
	var buildPath func(node *TreeNode) bool
	buildPath = func(node *TreeNode) bool {
		if node == nil {
			return false
		}
		path = append(path, node)
		if node.Val == target.Val {
			output = kDown(node, k)
			return true
		}
		if buildPath(node.Left) || buildPath(node.Right) {
			return true
		}
		path = path[:len(path)-1]
		return false
	}
	buildPath(root)
	output = append(output, kUp(path, k)...)
	return
}

/*
	     0
		/ \
	   1  nil
	  / \
	 3   2
*/
