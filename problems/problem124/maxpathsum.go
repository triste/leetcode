package problem124

import (
	. "gitlab.com/triste/leetcode/pkg/tree"
)

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func maxPathSum(root *TreeNode) int {
	maxSum := -1000
	var dfs func(node *TreeNode) int
	dfs = func(node *TreeNode) int {
		if node == nil {
			return 0
		}
		left := dfs(node.Left)
		right := dfs(node.Right)
		maxSum = max(right+node.Val, max(left+node.Val, max(node.Val, max(maxSum, left+right+node.Val))))
		return max(node.Val, max(left, right)+node.Val)
	}
	dfs(root)
	return maxSum
}
