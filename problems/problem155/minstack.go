package problem155

type MinStack struct {
	arr []Node
}

type Node struct {
	val int
	min int
}


func Constructor() MinStack {
	return MinStack{}
}


func (s *MinStack) Push(val int)  {
	node := Node{val, val}
	if len(s.arr) > 0 {
		if min := s.arr[len(s.arr)-1].min; min < node.min {
			node.min = min
		}
	}
	s.arr = append(s.arr, node)
}


func (s *MinStack) Pop()  {
	s.arr = s.arr[:len(s.arr)-1]
}


func (s *MinStack) Top() int {
	return s.arr[len(s.arr)-1].val
}


func (s *MinStack) GetMin() int {
	return s.arr[len(s.arr)-1].min
}


/**
 * Your MinStack object will be instantiated and called as such:
 * obj := Constructor();
 * obj.Push(val);
 * obj.Pop();
 * param_3 := obj.Top();
 * param_4 := obj.GetMin();
 */
