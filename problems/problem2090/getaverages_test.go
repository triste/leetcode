package problem2090

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestGetaverages(t *testing.T) {
	scenarios := [...]struct {
		nums   []int
		k      int
		output []int
	}{
		0: {
			nums:   []int{7, 4, 3, 9, 1, 8, 5, 2, 6},
			k:      3,
			output: []int{-1, -1, -1, 5, 4, 4, -1, -1, -1},
		},
		1: {
			nums:   []int{100000},
			k:      0,
			output: []int{100000},
		},
		2: {
			nums:   []int{8},
			k:      100000,
			output: []int{-1},
		},
	}
	for i, scenario := range scenarios {
		output := getAverages(scenario.nums, scenario.k)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
