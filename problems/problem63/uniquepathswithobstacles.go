package problem63

func uniquePathsWithObstacles(obstacleGrid [][]int) int {
	m := len(obstacleGrid)
	n := len(obstacleGrid[0])
	dp := make([][]int, m)
	for i := range dp {
		dp[i] = make([]int, n)
	}
	for i := n - 1; i >= 0 && obstacleGrid[m-1][i] == 0; i-- {
		dp[m-1][i] = 1
	}
	for i := m - 1; i >= 0 && obstacleGrid[i][n-1] == 0; i-- {
		dp[i][n-1] = 1
	}
	for i := m - 2; i >= 0; i-- {
		for j := n - 2; j >= 0; j-- {
			if obstacleGrid[i][j] == 1 {
				dp[i][j] = 0
			} else {
				dp[i][j] = dp[i+1][j] + dp[i][j+1]
			}
		}
	}
	return dp[0][0]
}
