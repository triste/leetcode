package problem60

func getPermutation(n int, k int) string {
	factorials := make([]int, n+1)
	factorials[0] = 1
	for i := 1; i < len(factorials); i++ {
		factorials[i] = factorials[i-1] * i
	}
	digits := make([]byte, n)
	for i := range digits {
		digits[i] = byte('1' + i)
	}
	bytes := make([]byte, 0, n)
	for len(digits) > 0 {
		i := (k - 1) / factorials[n-1]
		bytes = append(bytes, digits[i])
		copy(digits[i:], digits[i+1:])
		digits = digits[:len(digits)-1]
		k -= i * factorials[n-1]
		n--
	}
	return string(bytes)
}

/*
n = 1:
	1
n = 2:
	1,2
	2,1
n = 3:
	1,2,3
	1,3,2
	2,1,3
	2,3,1
	3,1,2
	3,2,1
n= 4:
	1,2,3,4
	1,2,4,3
	1,3,2,4
	1,3,4,2
	1,4,2,3
	1,4,3,2

	2,1,3,4
	2,1,4,3
	2,3,1,4
	2,3,4,1
	2,4,1,3
	2,4,3,1

	3,1,2,4
	3,1,4,2
	3,2,1,4
	3,2,4,1
	3,4,1,2
	3,4,2,1

	4,1,2,3
	4,1,3,2
	4,2,1,3
	4,2,3,1
	4,3,1,2
	4,3,2,1
*/
