package problem1249

func minRemoveToMakeValid(s string) string {
	stack := []int{}
	validations := make([]bool, len(s))
	for i, ch := range s {
		switch ch {
		case '(':
			stack = append(stack, i)
		case ')':
			if l := len(stack); l > 0 {
				validations[stack[l-1]] = true
				validations[i] = true
				stack = stack[:l-1]
			}
		default:
			validations[i] = true
		}
	}
	output := make([]byte, 0, len(s))
	for i, ch := range s {
		if validations[i] == true {
			output = append(output, byte(ch))
		}
	}
	return string(output)
}
