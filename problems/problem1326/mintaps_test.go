package problem1326

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestMintaps(t *testing.T) {
	scenarios := [...]struct {
		n      int
		ranges []int
		output int
	}{
		0: {
			n:      5,
			ranges: []int{3, 4, 1, 1, 0, 0},
			output: 1,
		},
		1: {
			n:      3,
			ranges: []int{0, 0, 0, 0},
			output: -1,
		},
		2: {
			n:      7,
			ranges: []int{1, 2, 1, 0, 2, 1, 0, 1},
			output: 3,
		},
		3: {
			n:      8,
			ranges: []int{4, 0, 0, 0, 4, 0, 0, 0, 4},
			output: 1,
		},
		4: {
			n:      7,
			ranges: []int{3, 0, 5, 4, 5, 4, 4, 3},
			output: 1,
		},
	}
	for i, scenario := range scenarios {
		output := minTaps(scenario.n, scenario.ranges)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
