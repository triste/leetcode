package problem740

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestDeleteandearn(t *testing.T) {
	scenarios := [...]struct {
		nums   []int
		output int
	}{
		0: {
			nums:   []int{3, 4, 2},
			output: 6,
		},
		1: {
			nums:   []int{2, 2, 3, 3, 3, 4},
			output: 9,
		},
	}
	for i, scenario := range scenarios {
		output := deleteAndEarn(scenario.nums)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
