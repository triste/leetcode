package problem2551

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestPutmarbles(t *testing.T) {
	scenarios := [...]struct {
		weights []int
		k       int
		output  int64
	}{
		0: {
			weights: []int{1, 3, 5, 1},
			k:       2,
			output:  4,
		},
		1: {
			weights: []int{1, 3},
			k:       2,
			output:  0,
		},
		2: {
			weights: []int{1, 4, 2, 5, 2},
			k:       3,
			output:  3,
		},
	}
	for i, scenario := range scenarios[2:] {
		output := putMarbles(scenario.weights, scenario.k)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
