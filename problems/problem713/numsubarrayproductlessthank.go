package problem713

func numSubarrayProductLessThanK(nums []int, k int) (output int) {
	product := 1
	left := 0
	for i, num := range nums {
		for product *= num; left <= i && product >= k; left++ {
			product /= nums[left]
			output += i - left
		}
	}
	suflen := len(nums) - left
	output += ((suflen + 1) * suflen) / 2
	return
}
