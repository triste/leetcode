package problem332

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestFinditinerary(t *testing.T) {
	scenarios := [...]struct {
		tickets [][]string
		output  []string
	}{
		0: {
			tickets: [][]string{
				{"MUC", "LHR"},
				{"JFK", "MUC"},
				{"SFO", "SJC"},
				{"LHR", "SFO"},
			},
			output: []string{
				"JFK", "MUC", "LHR", "SFO", "SJC",
			},
		},
		1: {
			tickets: [][]string{
				{"JFK", "SFO"},
				{"JFK", "ATL"},
				{"SFO", "ATL"},
				{"ATL", "JFK"},
				{"ATL", "SFO"},
			},
			output: []string{
				"JFK", "ATL", "JFK", "SFO", "ATL", "SFO",
			},
		},
		2: {
			tickets: [][]string{
				{"JFK", "KUL"},
				{"JFK", "NRT"},
				{"NRT", "JFK"},
			},
			output: []string{
				"JFK",
				"NRT",
				"JFK",
				"KUL",
			},
		},
	}
	for i, scenario := range scenarios {
		output := findItinerary(scenario.tickets)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
