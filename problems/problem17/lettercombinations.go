package problem17

func letterCombinations(digits string) []string {
	if len(digits) == 0 {
		return nil
	}
	output := []string{}
	for _, bytes := range combinations([]byte(digits)) {
		output = append(output, string(bytes))
	}
	return output
}

func combinations(digits []byte) [][]byte {
	m := [10][]byte{
		2: {'a', 'b', 'c'},
		3: {'d', 'e', 'f'},
		4: {'g', 'h', 'i'},
		5: {'j', 'k', 'l'},
		6: {'m', 'n', 'o'},
		7: {'p', 'q', 'r', 's'},
		8: {'t', 'u', 'v'},
		9: {'w', 'x', 'y', 'z'},
	}
	output := [][]byte{}
	if len(digits) == 1 {
		for _, letter := range m[digits[0]-'0'] {
			output = append(output, []byte{letter})
		}
		return output
	}
	digit := digits[0] - '0'
	tails := combinations(digits[1:])
	for _, letter := range m[digit] {
		for _, tail := range tails {
			result := append([]byte{letter}, tail...)
			output = append(output, result)
		}
	}
	return output
}
