package problem1425

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func constrainedSubsetSum(nums []int, k int) int {
	queue := make([][2]int, 1, k)
	queue[0] = [2]int{0, nums[0]}
	maxSum := nums[0]
	prevSum := nums[0]
	for i := 1; i < len(nums); i++ {
		if i-queue[0][0] > k {
			queue = queue[1:]
		}
		var sum int
		if len(queue) == 0 {
			sum = max(prevSum+nums[i], nums[i])
			queue = append(queue, [2]int{i, sum})
		} else {
			sum = max(queue[0][1]+nums[i], nums[i])
			j := len(queue) - 1
			for j >= 0 && queue[j][1] < sum {
				j--
			}
			queue = append(queue[:j+1], [2]int{i, sum})
		}
		if sum > maxSum {
			maxSum = sum
		}
		prevSum = sum
	}
	return maxSum
}
