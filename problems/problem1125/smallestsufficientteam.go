package problem1125

import (
	"log"
	"math"
)

func smallestSufficientTeam(req_skills []string, people [][]string) []int {
	skillIds := make(map[string]uint16, len(req_skills))
	for i, skill := range req_skills {
		skillIds[skill] = 1 << i
	}
	personSkills := make([]uint16, len(people))
	for i, skills := range people {
		var skillsBitmask uint16
		for _, skill := range skills {
			skillsBitmask |= skillIds[skill]
		}
		personSkills[i] = skillsBitmask
	}
	dp := make([][]int, 1<<len(req_skills))
	for i := 1; i < 1<<len(req_skills); i++ {
		min := math.MaxInt
		newPersonId := 0
		for j, skillsBitmask := range personSkills {
			if len(dp[i&(^int(skillsBitmask))]) < min {
				newPersonId = j
				min = len(dp[i&(^int(skillsBitmask))])
			}
		}
		log.Println(newPersonId)
		dp[i] = make([]int, len(dp[i&(^int(personSkills[newPersonId]))]))
		copy(dp[i], dp[i&(^int(personSkills[newPersonId]))])
		dp[i] = append(dp[i], newPersonId)
	}
	log.Println("XXX")
	return dp[(1<<len(skillIds))-1]
}

/*
	1 algo = 0,1
	2 math = 0,1,4
	3 algo & math = 0,1
	4 java = 0,2,5
	5 java & algo = 0
	6 java & math = 0
	7 java & math & algo = 0
*/
