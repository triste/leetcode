package problem2562

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestFindthearrayconcval(t *testing.T) {
	scenarios := [...]struct {
		nums   []int
		output int64
	}{
		0: {
			nums:   []int{7, 52, 2, 4},
			output: 596,
		},
		1: {
			nums:   []int{5, 14, 13, 8, 12},
			output: 673,
		},
	}
	for i, scenario := range scenarios {
		output := findTheArrayConcVal(scenario.nums)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
