package problem1751

import (
	"sort"
)

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func maxValueRecursive(events [][]int, k int) int {
	sort.Slice(events, func(i, j int) bool {
		return events[i][0] < events[j][0]
	})
	var solve func(i int, k int) int
	solve = func(i int, k int) int {
		if i == len(events) || k == 0 {
			return 0
		}
		valueIfSkipped := solve(i+1, k)
		j := sort.Search(len(events), func(j int) bool {
			return events[j][0] > events[i][1]
		})
		valueIfAttended := events[i][2] + solve(j, k-1)
		return max(valueIfSkipped, valueIfAttended)
	}
	return solve(0, k)
}

func maxValueIterative(events [][]int, k int) int {
	sort.Slice(events, func(i, j int) bool {
		return events[i][0] < events[j][0]
	})
	dp := make([]int, len(events)+1)
	dpPrev := make([]int, len(events)+1)
	nextEvents := make([]int, len(events))
	for i := range nextEvents {
		nextEvents[i] = sort.Search(len(events), func(j int) bool {
			return events[j][0] > events[i][1]
		})
	}
	for i := 0; i < k; i++ {
		for j := len(events) - 1; j >= 0; j-- {
			dp[j] = max(dp[j+1], events[j][2]+dpPrev[nextEvents[j]])
		}
		dp, dpPrev = dpPrev, dp
	}
	return dpPrev[0]
}

func maxValue(events [][]int, k int) int {
	return maxValueIterative(events, k)
}

/*
	k\i| 0 | 1 | 2 | 3 |
	 2 |   |   |   | 0 |
	 1 |   |   |   | 0 |
	 0 | 0 | 0 | 0 | 0 |
*/
