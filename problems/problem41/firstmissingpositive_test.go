package problem41

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestFirstmissingpositive(t *testing.T) {
	scenarios := [...]struct {
		nums   []int
		output int
	}{
		0: {
			nums:   []int{1, 2, 0},
			output: 3,
		},
		1: {
			nums:   []int{3, 4, -1, 1},
			output: 2,
		},
		2: {
			nums:   []int{7, 8, 9, 11, 12},
			output: 1,
		},
		3: {
			nums:   []int{1, 1000},
			output: 2,
		},
		4: {
			nums:   []int{5, 1, 2, 4},
			output: 3,
		},
		5: {
			nums:   []int{2, 1, 4, 3, 5},
			output: 6,
		},
		6: {
			nums:   []int{-1, 4, 2, 1, 9, 10},
			output: 3,
		},
		7: {
			nums:   []int{-4, 24, 32, 25, 16, -8, 3, -5, -6, 30, 3, 3, 29, -5, 6, -3, 1, 29, -2, 4, 4, 7, 14, 20, 5, 0, 25, 2, 13, 26, -9, 7, 6, 33},
			output: 8,
		},
		8: {
			nums:   []int{0, 1, 2},
			output: 3,
		},
	}
	for i, scenario := range scenarios {
		output := firstMissingPositive(scenario.nums)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
