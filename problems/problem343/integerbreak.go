package problem343

import (
	"math"
)

func integerBreak(n int) int {
	if n <= 3 {
		return n - 1
	}
	res := int(math.Pow(3, float64(n/3)))
	switch n % 3 {
	case 1:
		res = res / 3 * 4
	case 2:
		res *= 2
	}
	return res
}

func integerBreakDP(n int) int {
	if n <= 3 {
		return n - 1
	}
	dp1, dp2, dp3 := 2, 3, 4
	for i := 5; i <= n; i++ {
		dp1, dp2, dp3 = dp2, dp3, dp1*3
	}
	return dp3
}
