package problem2369

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestValidpartition(t *testing.T) {
	scenarios := [...]struct {
		nums   []int
		output bool
	}{
		0: {
			nums: []int{
				4, 4, 4, 5, 6,
			},
			output: true,
		},
		1: {
			nums: []int{
				1, 1, 1, 2,
			},
			output: false,
		},
	}
	for i, scenario := range scenarios {
		output := validPartition(scenario.nums)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
