package problem802

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestEventualsafenodes(t *testing.T) {
	scenarios := [...]struct {
		graph  [][]int
		output []int
	}{
		0: {
			graph:  [][]int{{1, 2}, {2, 3}, {5}, {0}, {5}, {}, {}},
			output: []int{2, 4, 5, 6},
		},
		1: {
			graph:  [][]int{{1, 2, 3, 4}, {1, 2}, {3, 4}, {0, 4}, nil},
			output: []int{4},
		},
	}
	for i, scenario := range scenarios {
		output := eventualSafeNodes(scenario.graph)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
