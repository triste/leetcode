package problem1376

func numOfMinutes(n int, headID int, manager []int, informTime []int) int {
	graph := make([][]int, n)
	for i, m := range manager {
		if m >= 0 {
			graph[m] = append(graph[m], i)
		}
	}
	var dfs func(i int) int
	dfs = func(i int) int {
		maxTime := 0
		for _, empl := range graph[i] {
			time := dfs(empl)
			if time > maxTime {
				maxTime = time
			}
		}
		return maxTime + informTime[i]
	}
	return dfs(headID)
}
