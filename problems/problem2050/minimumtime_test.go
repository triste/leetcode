package problem2050

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestMinimumtime(t *testing.T) {
	scenarios := [...]struct {
		n         int
		relations [][]int
		time      []int
		output    int
	}{
		0: {
			n: 3,
			relations: [][]int{
				{1, 3}, {2, 3},
			},
			time:   []int{3, 2, 5},
			output: 8,
		},
		1: {
			n: 5,
			relations: [][]int{
				{1, 5}, {2, 5}, {3, 5}, {3, 4}, {4, 5},
			},
			time:   []int{1, 2, 3, 4, 5},
			output: 12,
		},
	}
	for i, scenario := range scenarios {
		output := minimumTime(scenario.n, scenario.relations, scenario.time)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
