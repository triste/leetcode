package problem2472

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func isPalindrome(s string) bool {
	for l, r := 0, len(s)-1; l < r; l, r = l+1, r-1 {
		if s[l] != s[r] {
			return false
		}
	}
	return true
}

func maxPalindromes(s string, k int) int {
	memo := make(map[int]int, len(s)+1)
	memo[len(s)] = 0
	var solve func(i int) int
	solve = func(i int) int {
		if val, ok := memo[i]; ok {
			return val
		}
		var maxPalindromes int
		for r := i + k; r <= len(s); r++ {
			if isPalindrome(s[i:r]) {
				maxPalindromes = solve(r) + 1
				break
			}
		}
		maxPalindromes = max(maxPalindromes, solve(i+1))
		memo[i] = maxPalindromes
		return maxPalindromes
	}
	return solve(0)
}
