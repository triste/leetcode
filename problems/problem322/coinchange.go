package problem322

import "math"

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func coinChange(coins []int, amount int) int {
	dp := make([]int, amount+1)
	for i := 1; i < len(dp); i++ {
		dp[i] = math.MaxInt
		for _, coin := range coins {
			if coin <= i && dp[i-coin] >= 0 {
				dp[i] = min(dp[i], 1+dp[i-coin])
			}
		}
		if dp[i] == math.MaxInt {
			dp[i] = -1
		}
	}
	return dp[amount]
}

/*
	   | 1 | 2 | 5 |
	0  | 0 | 0 | 0 |
	1  | 1 |-1 |-1 | min = 1
	2  | 2 | 1 |-1 | min = 1
	3  | 2 | 2 |-1 | min = 2
	4  | 3 | 2 |-1 | min = 2
	5  | 3 |   |   |
	6  |   |   |   |
	7  |   |   |   |
	8  |   |   |   |
	9  |   |   |   |
	10 |   |   |   |
	11 |   |   |   |
*/
