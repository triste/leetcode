package problem40

import "sort"

func combinationSum2(candidates []int, target int) (combinations [][]int) {
	sort.Ints(candidates)
	var path []int
	var solve func(i, target int)
	solve = func(i, target int) {
		if target == 0 {
			combination := make([]int, len(path))
			copy(combination, path)
			combinations = append(combinations, combination)
		} else if target > 0 {
			prev := 0
			for j := i; j < len(candidates); j++ {
				if candidates[j] == prev {
					continue
				}
				prev = candidates[j]
				path = append(path, candidates[j])
				solve(j+1, target-candidates[j])
				path = path[:len(path)-1]
			}
		}
	}
	solve(0, target)
	return
}
