package problem87

func isScrambleRecursive(s1 string, s2 string) bool {
	n := len(s1)
	memo := make([][][]bool, n)
	processed := make([][][]bool, n)
	for i := 0; i < n; i++ {
		memo[i] = make([][]bool, n)
		processed[i] = make([][]bool, n)
		for j := 0; j < n; j++ {
			memo[i][j] = make([]bool, n+1)
			processed[i][j] = make([]bool, n+1)
			memo[i][j][1] = s1[i] == s2[j]
			processed[i][j][1] = true
		}
	}
	var solve func(i, j, l int) bool
	solve = func(i, j, l int) bool {
		if processed[i][j][l] {
			return memo[i][j][l]
		}
		scrambled := false
		for k := 1; k < l; k++ {
			if solve(i, j, k) && solve(i+k, j+k, l-k) || solve(i+k, j, l-k) && solve(i, j+l-k, k) {
				scrambled = true
				break
			}
		}
		memo[i][j][l] = scrambled
		processed[i][j][l] = true
		return scrambled
	}
	return solve(0, 0, len(s1))
}

func isScrambleIterative(s1 string, s2 string) bool {
	n := len(s1)
	dp := make([][][]bool, n)
	for i := range dp {
		dp[i] = make([][]bool, n)
		for j := range dp[i] {
			dp[i][j] = make([]bool, n+1)
			dp[i][j][1] = s1[i] == s2[j]
		}
	}
	for l := 2; l <= n; l++ {
		for i := 0; i < n+1-l; i++ {
			for j := 0; j < n+1-l; j++ {
				for nl := 1; nl < l; nl++ {
					dp[i][j][l] = dp[i][j][l] || (dp[i][j][nl] && dp[i+nl][j+nl][l-nl]) || (dp[i][j+l-nl][nl] && dp[i+nl][j][l-nl])
				}
			}
		}
	}
	return dp[0][0][n]
}

func isScramble(s1 string, s2 string) bool {
	return isScrambleIterative(s1, s2)
}

/*
	abcde | fghij
	 |

	ab -> fg AND cde -> hij
	OR
	cdeab | fghij
	cde -> fgi AND ab -> ij
*/
