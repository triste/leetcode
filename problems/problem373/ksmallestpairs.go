package problem373

import "sort"

type Node struct {
	sum   int
	first int
}

type PriorityQueue struct {
	nodes []Node
}

func (pq *PriorityQueue) min() Node {
	if len(pq.nodes) == 0 {
		return Node{2_000_000_002, 1_000_000_001}
	}
	return pq.nodes[0]
}

func (pq *PriorityQueue) push(node Node) {
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func kSmallestPairs(nums1 []int, nums2 []int, pairCount int) [][]int {
	n1 := len(nums1)
	n2 := len(nums2)
	pairs := make([][]int, pairCount)
	pairs[0] = []int{nums1[0], nums2[0]}
	sum := nums1[0] + nums2[0]
	var pq PriorityQueue
	for i, j, k := 0, 0, 1; k < pairCount; k++ {
		var ni, nj int
		if j+1 == len(nums2) {
			ni = i + 1
			nj = sort.SearchInts(nums2, nums1[i]+nums2[j]-nums1[ni])
		} else if i+1 == len(nums1) {
			nj = j + 1
			ni = sort.SearchInts(nums1, nums1[i]+nums2[j]-nums2[nj])
		} else {
			first := nums1[i] + nums2[j+1]
			second := nums1[i+1] + nums2[j]
			if first < second {
				ni = i
				nj = j + 1
			} else {
				ni = i + 1
				nj = j
			}
		}
	}
	return pairs
}

/*
	1, 5, 7
	2, 3, 11

	1+2=3, heap := {}
	(1+3||5+2),
*/
