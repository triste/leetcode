package problem69

func mySqrt(x int) int {
	l, r := 0, x/2+1
	for l < r {
		m := (r + l + 1) / 2
		if m*m > x {
			r = m - 1
		} else {
			l = m
		}
	}
	return l
}
