package problem530

import (
	"math"

	. "gitlab.com/triste/leetcode/pkg/tree"
)

func getMinimumDifference(root *TreeNode) int {
	mindif := math.MaxInt
	var bfs func(root *TreeNode) (int, int)
	bfs = func(root *TreeNode) (min int, max int) {
		if root.Left != nil {
			lmin, lmax := bfs(root.Left)
			if dif := root.Val - lmax; dif < mindif {
				mindif = dif
			}
			min = lmin
		} else {
			min = root.Val
		}
		if root.Right != nil {
			rmin, rmax := bfs(root.Right)
			if dif := rmin - root.Val; dif < mindif {
				mindif = dif
			}
			max = rmax
		} else {
			max = root.Val
		}
		return
	}
	bfs(root)
	return mindif
}
