package problem688

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestKnightprobability(t *testing.T) {
	scenarios := [...]struct {
		n      int
		k      int
		row    int
		column int
		output float64
	}{
		0: {
			n:      3,
			k:      2,
			row:    0,
			column: 0,
			output: 0.06250,
		},
		1: {
			n:      1,
			k:      0,
			row:    0,
			column: 0,
			output: 1.0,
		},
	}
	for i, scenario := range scenarios {
		output := knightProbability(scenario.n, scenario.k, scenario.row, scenario.column)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
