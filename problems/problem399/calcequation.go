package problem399

type Node struct {
	parent        *Node
	equationValue float64
}

func (n *Node) rootEquationValue() (*Node, float64) {
	root := n
	equationValue := 1.0
	for root.parent != nil {
		equationValue *= root.equationValue
		root = root.parent
	}
	return root, equationValue
}

func calcEquation(equations [][]string, values []float64, queries [][]string) []float64 {
	nodes := make(map[string]*Node, 0)
	for i, equation := range equations {
		dividend := equation[0]
		divider := equation[1]
		dividendNode, dividentFound := nodes[dividend]
		dividerNode, dividerFound := nodes[divider]
		if dividentFound && dividerFound {
			dividendRoot, dividentEqval := dividendNode.rootEquationValue()
			dividerRoot, dividerEqval := dividerNode.rootEquationValue()
			if dividendRoot != dividerRoot {
				dividerRoot.parent = dividendRoot
				dividerRoot.equationValue = dividentEqval / dividerEqval * values[i]
			}
		} else if dividentFound {
			root, eqval := dividendNode.rootEquationValue()
			nodes[divider] = &Node{root, eqval * values[i]}
		} else if dividerFound {
			root, eqval := dividerNode.rootEquationValue()
			nodes[dividend] = &Node{root, eqval / values[i]}
		} else {
			root := &Node{nil, 1.0}
			nodes[dividend] = root
			nodes[divider] = &Node{root, values[i]}
		}
	}
	answers := make([]float64, len(queries))
	for i, query := range queries {
		dividend := query[0]
		divider := query[1]
		dividendNode, dividentFound := nodes[dividend]
		dividerNode, dividerFound := nodes[divider]
		if dividentFound && dividerFound {
			dividendRoot, dividentEqval := dividendNode.rootEquationValue()
			dividerRoot, dividerEqval := dividerNode.rootEquationValue()
			if dividendRoot != dividerRoot {
				answers[i] = -1.0
			} else {
				answers[i] = dividerEqval / dividentEqval
			}
		} else {
			answers[i] = -1.0
		}
	}
	return answers
}
