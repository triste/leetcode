package problem34

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestSearchrange(t *testing.T) {
	scenarios := [...]struct {
		nums   []int
		target int
		output []int
	}{
		0: {[]int{5, 7, 7, 8, 8, 10}, 8, []int{3, 4}},
		1: {[]int{5, 7, 7, 8, 8, 10}, 6, []int{-1, -1}},
		2: {[]int{}, 0, []int{-1, -1}},
		3: {[]int{1}, 1, []int{0, 0}},
		4: {[]int{2, 2}, 2, []int{0, 1}},
		5: {[]int{2, 2}, 3, []int{-1, -1}},
	}
	for i, scenario := range scenarios {
		output := searchRange(scenario.nums, scenario.target)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
