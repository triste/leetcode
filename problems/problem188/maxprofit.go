package problem188

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

type State = int

const (
	Empty State = iota
	Full
)

func maxProfitRecursive(k int, prices []int) int {
	var helper func(state State, i int, depth int) int
	helper = func(state State, i int, depth int) int {
		if i >= len(prices) || depth > k {
			return 0
		}
		var max int
		if state == Empty {
			max = helper(Full, i+1, depth+1) - prices[i]
			if v := helper(Empty, i+1, depth); v > max {
				max = v
			}
		} else if state == Full {
			max = helper(Empty, i+1, depth) + prices[i]
			if v := helper(Full, i+1, depth); v > max {
				max = v
			}
		}
		return max
	}
	return helper(Empty, 0, 0)
}

/*
  |  3  |  3  |  5  |  0  |  0  |  3  |  1  |  4  |     |     |
0 |     |     |     |     |     |     |     |     | 0,0 | 0,0 |
1 |     |     |     |     |     |     |     |     | 0,0 | 0,0 |
2 |     |     |     |     |     |     |     |     | 0,0 | 0,0 |
..|     |     |     |     |     |     |     |     | 0,0 | 0,0 |
k | 0,0 | 0,0 | 0,0 | 0,0 | 0,0 | 0,0 | 0,0 | 0,0 | 0,0 | 0,0 |
*/

func maxProfitDP(k int, prices []int) int {
	dp := make([][2]int, len(prices)+2)
	for i := 0; i <= k; i++ {
		var prev [2]int
		for j := len(prices) - 1; j >= 0; j-- {
			tmp := dp[j]
			dp[j] = [2]int{
				max(prev[1]-prices[j], dp[j+1][0]),
				max(dp[j+1][0]+prices[j], dp[j+1][1]),
			}
			prev = tmp
		}
	}
	return dp[0][0]
}

func maxProfit(k int, prices []int) int {
	return maxProfitDP(k, prices)
}
