package problem714

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestMaxprofit(t *testing.T) {
	scenarios := [...]struct {
		prices []int
		fee    int
		output int
	}{
		0: {
			prices: []int{1, 3, 2, 8, 4, 9},
			fee:    2,
			output: 8,
		},
		1: {
			prices: []int{1, 3, 7, 5, 10, 3},
			fee:    3,
			output: 6,
		},
	}
	for i, scenario := range scenarios {
		output := maxProfit(scenario.prices, scenario.fee)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
