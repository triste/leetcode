package problem223

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func computeArea(ax1 int, ay1 int, ax2 int, ay2 int, bx1 int, by1 int, bx2 int, by2 int) int {
	area1 := (ax2 - ax1) * (ay2 - ay1)
	area2 := (bx2 - bx1) * (by2 - by1)
	area := area1 + area2
	y1 := max(ay1, by1)
	y2 := min(ay2, by2)
	x1 := max(ax1, bx1)
	x2 := min(ax2, bx2)
	if y2 > y1 && x2 > x1 {
		area -= (y2 - y1) * (x2 - x1)
	}
	return area
}
