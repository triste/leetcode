package problem1252

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestOddcells(t *testing.T) {
	scenarios := [...]struct {
		m       int
		n       int
		indices [][]int
		output  int
	}{
		0: {
			m:       2,
			n:       3,
			indices: [][]int{{0, 1}, {1, 1}},
			output:  6,
		},
		1: {
			m:       2,
			n:       2,
			indices: [][]int{{1, 1}, {0, 0}},
			output:  0,
		},
	}
	for i, scenario := range scenarios {
		output := oddCells(scenario.m, scenario.n, scenario.indices)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
