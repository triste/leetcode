package problem1282

func groupThePeople(groupSizes []int) (output [][]int) {
	groups := make(map[int][]int)
	for i, gs := range groupSizes {
		groups[gs] = append(groups[gs], i)
		if len(groups[gs]) == gs {
			output = append(output, groups[gs])
			delete(groups, gs)
		}
	}
	return
}
