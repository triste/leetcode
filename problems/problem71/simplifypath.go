package problem71

import "strings"

func simplifyPath(path string) string {
	var dirs []string
	for _, dir := range strings.Split(path, "/") {
		switch dir {
		case "", ".":
			// Do nothing.
		case "..":
			if len(dirs) > 0 {
				dirs = dirs[:len(dirs)-1]
			}
		default:
			dirs = append(dirs, dir)
		}
	}
	if len(dirs) == 0 {
		return "/"
	}
	var builder strings.Builder
	for _, dir := range dirs {
		builder.WriteByte('/')
		builder.WriteString(dir)
	}
	return builder.String()
}
