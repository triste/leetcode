package problem207

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestCanfinish(t *testing.T) {
	scenarios := [...]struct {
		numCourses    int
		prerequisites [][]int
		output        bool
	}{
		0: {
			numCourses:    2,
			prerequisites: [][]int{{1, 0}},
			output:        true,
		},
		1: {
			numCourses:    2,
			prerequisites: [][]int{{1, 0}, {0, 1}},
			output:        false,
		},
	}
	for i, scenario := range scenarios {
		output := canFinish(scenario.numCourses, scenario.prerequisites)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
