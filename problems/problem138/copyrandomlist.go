package problem138

type Node struct {
	Val    int
	Next   *Node
	Random *Node
}

func copyRandomList(head *Node) *Node {
	for p := head; p != nil; p = p.Next.Next {
		p.Next = &Node{p.Val, p.Next, nil}
	}
	for p := head; p != nil; p = p.Next.Next {
		if p.Random != nil {
			p.Next.Random = p.Random.Next
		}
	}
	headOrigin, headCopy := Node{}, Node{}
	prevOrigin, prevCopy := &headOrigin, &headCopy
	for p := head; p != nil; p = p.Next.Next {
		prevOrigin, prevOrigin.Next = p, p
		prevCopy, prevCopy.Next = p.Next, p.Next
	}
	prevOrigin.Next = nil
	return headCopy.Next
}
