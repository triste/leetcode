package problem459

import "strings"

func repeatedSubstringPattern(s string) bool {
	t := s + s
	return strings.Contains(t[1:len(t)-1], s)
}

/*
	S = (x1,x2,x3,...,xn)

	Let T = (x2,x3,...,xn) + (x1,x2,x3,...,xn-1)
	If S contain repeated substing, then
		S = p*k
		then T = head + p(k-1) + p(k-1) + tail
		T = head + 2p(k-1) + tail
*/
