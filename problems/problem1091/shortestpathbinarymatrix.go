package problem1091

func shortestPathBinaryMatrix(grid [][]int) (output int) {
	last := len(grid) - 1
	if grid[0][0] != 0 || grid[last][last] != 0 {
		return -1
	}
	queue := [][2]int{{0, 0}}
	for depth := 1; len(queue) > 0; depth++ {
		var tmpqueue [][2]int
		for _, cell := range queue {
			if cell[0] == last && cell[1] == last {
				return depth
			}
			r, c := cell[0], cell[1]
			if r > 0 {
				if c > 0 && grid[r-1][c-1] == 0 {
					tmpqueue = append(tmpqueue, [2]int{r - 1, c - 1})
					grid[r][c] = 1
				}
				if grid[r-1][c] == 0 {
					tmpqueue = append(tmpqueue, [2]int{r - 1, c})
					grid[r-1][c] = 1
				}
				if c < last && grid[r-1][c+1] == 0 {
					tmpqueue = append(tmpqueue, [2]int{r - 1, c + 1})
					grid[r-1][c+1] = 1
				}
			}
			if r < last {
				if c > 0 && grid[r+1][c-1] == 0 {
					tmpqueue = append(tmpqueue, [2]int{r + 1, c - 1})
					grid[r+1][c-1] = 1
				}
				if grid[r+1][c] == 0 {
					tmpqueue = append(tmpqueue, [2]int{r + 1, c})
					grid[r+1][c] = 1
				}
				if c < last && grid[r+1][c+1] == 0 {
					tmpqueue = append(tmpqueue, [2]int{r + 1, c + 1})
					grid[r+1][c+1] = 1
				}
			}
			if c > 0 && grid[r][c-1] == 0 {
				tmpqueue = append(tmpqueue, [2]int{r, c - 1})
				grid[r][c-1] = 1
			}
			if c < last && grid[r][c+1] == 0 {
				tmpqueue = append(tmpqueue, [2]int{r, c + 1})
				grid[r][c+1] = 1
			}
		}
		queue = tmpqueue
	}
	return -1
}
