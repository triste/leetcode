package problem140

import "strings"

type Node struct {
	nodes [26]*Node
	word  string
}

type Trie struct {
	root Node
}

func (t *Trie) Insert(word string) {
	n := &t.root
	for _, ch := range word {
		if n.nodes[ch-'a'] == nil {
			n.nodes[ch-'a'] = &Node{}
		}
		n = n.nodes[ch-'a']
	}
	n.word = word
}

func wordBreak(s string, wordDict []string) (output []string) {
	var trie Trie
	for _, word := range wordDict {
		trie.Insert(word)
	}
	var dfs func(i int, path []string)
	dfs = func(i int, path []string) {
		if i >= len(s) {
			output = append(output, strings.Join(path, " "))
			return
		}
		node := &trie.root
		for j := i; j < len(s); j++ {
			if node.nodes[s[j]-'a'] == nil {
				break
			}
			node = node.nodes[s[j]-'a']
			if node.word != "" {
				dfs(j+1, append(path, node.word))
			}
		}
	}
	dfs(0, nil)
	return
}
