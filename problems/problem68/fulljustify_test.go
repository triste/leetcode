package problem68

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestFulljustify(t *testing.T) {
	scenarios := [...]struct {
		words    []string
		maxWidth int
		output   []string
	}{
		0: {
			words: []string{
				"This", "is", "an", "example", "of", "text", "justification.",
			},
			maxWidth: 16,
			output: []string{
				"This    is    an",
				"example  of text",
				"justification.  ",
			},
		},
		1: {
			words: []string{
				"What", "must", "be", "acknowledgment", "shall", "be",
			},
			maxWidth: 16,
			output: []string{
				"What   must   be",
				"acknowledgment  ",
				"shall be        ",
			},
		},
		2: {
			words: []string{
				"Science", "is", "what", "we", "understand", "well", "enough", "to",
				"explain", "to", "a", "computer.", "Art", "is", "everything",
				"else", "we", "do",
			},
			maxWidth: 20,
			output: []string{
				"Science  is  what we",
				"understand      well",
				"enough to explain to",
				"a  computer.  Art is",
				"everything  else  we",
				"do                  ",
			},
		},
		3: {
			words: []string{
				"ask", "not", "what", "your", "country", "can", "do", "for", "you", "ask", "what", "you", "can", "do", "for", "your", "country",
			},
			maxWidth: 16,
			output: []string{
				"ask   not   what",
				"your country can",
				"do  for  you ask",
				"what  you can do",
				"for your country",
			},
		},
	}
	for i, scenario := range scenarios {
		output := fullJustify(scenario.words, scenario.maxWidth)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
