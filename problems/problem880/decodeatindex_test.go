package problem880

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestDecodeatindex(t *testing.T) {
	scenarios := [...]struct {
		s      string
		k      int
		output string
	}{
		0: {
			s:      "leet2code3",
			k:      10,
			output: "o",
		},
		1: {
			s:      "ha22",
			k:      5,
			output: "h",
		},
		2: {
			s:      "a2345678999999999999999",
			k:      1,
			output: "a",
		},
		3: {
			s:      "yyuele72uthzyoeut7oyku2yqmghy5luy9qguc28ukav7an6a2bvizhph35t86qicv4gyeo6av7gerovv5lnw47954bsv2xruaej",
			k:      123365626,
			output: "l",
		},
		4: {
			s:      "abc",
			k:      3,
			output: "c",
		},
		5: {
			s:      "a2b3c4d5e6f7g8h9",
			k:      3,
			output: "b",
		},
		6: {
			s:      "a23",
			k:      6,
			output: "a",
		},
		7: {
			s:      "a2b3c4d5e6f7g8h9",
			k:      9,
			output: "b",
		},
	}
	for i, scenario := range scenarios {
		output := decodeAtIndex(scenario.s, scenario.k)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
