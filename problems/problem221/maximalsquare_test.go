package problem221

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestMaximalsquare(t *testing.T) {
	scenarios := [...]struct {
		matrix [][]byte
		output int
	}{
		0: {
			matrix: [][]byte{
				{'1', '0', '1', '0', '0'},
				{'1', '0', '1', '1', '1'},
				{'1', '1', '1', '1', '1'},
				{'1', '0', '0', '1', '0'},
			},
			output: 4,
		},
		1: {
			matrix: [][]byte{
				{'0', '1'},
				{'1', '0'},
			},
			output: 1,
		},
		2: {
			matrix: [][]byte{
				{'0'},
			},
			output: 0,
		},
		3: {
			matrix: [][]byte{
				{'0', '1'},
				{'1', '0'},
			},
			output: 1,
		},
	}
	for i, scenario := range scenarios[:1] {
		output := maximalSquare(scenario.matrix)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
