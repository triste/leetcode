package problem54

const (
	Right = iota
	Down  = iota
	Left  = iota
	Up    = iota
)

func spiralOrder(matrix [][]int) []int {
	m := len(matrix)
	n := len(matrix[0])
	output := make([]int, m*n)
	top, bottom := 0, m-1
	left, right := 0, n-1
	direction := Right
	i, j := 0, 0
	for k := range output {
		output[k] = matrix[i][j]
		switch direction {
		case Right:
			if j != right {
				j++
			} else {
				top++
				i++
				direction = Down
			}
		case Down:
			if i != bottom {
				i++
			} else {
				right--
				j--
				direction = Left
			}
		case Left:
			if j != left {
				j--
			} else {
				bottom--
				i--
				direction = Up
			}
		case Up:
			if i != top {
				i--
			} else {
				left++
				j++
				direction = Right
			}
		}
	}
	return output
}
