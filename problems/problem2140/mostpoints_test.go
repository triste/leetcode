package problem2140

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestMostpoints(t *testing.T) {
	scenarios := [...]struct {
		questions [][]int
		output    int64
	}{
		0: {
			questions: [][]int{{3, 2}, {4, 3}, {4, 4}, {2, 5}},
			output:    5,
		},
		1: {
			questions: [][]int{{1, 1}, {2, 2}, {3, 3}, {4, 4}, {5, 5}},
			output:    7,
		},
	}
	for i, scenario := range scenarios {
		output := mostPoints(scenario.questions)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
