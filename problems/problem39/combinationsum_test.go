package problem39

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestCombinationsum(t *testing.T) {
	scenarios := [...]struct {
		candidates []int
		target     int
		output     [][]int
	}{
		0: {
			candidates: []int{2, 3, 6, 7},
			target:     7,
			output: [][]int{
				{2, 2, 3},
				{7},
			},
		},
		1: {
			candidates: []int{2, 3, 5},
			target:     8,
			output: [][]int{
				{2, 2, 2, 2},
				{2, 3, 3},
				{3, 5},
			},
		},
		2: {
			candidates: []int{2},
			target:     1,
			output:     nil,
		},
	}
	for i, scenario := range scenarios {
		output := combinationSum(scenario.candidates, scenario.target)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
