package problem2542

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestMaxscore(t *testing.T) {
	scenarios := [...]struct {
		nums1  []int
		nums2  []int
		k      int
		output int64
	}{
		0: {
			nums1:  []int{1, 3, 3, 2},
			nums2:  []int{2, 1, 3, 4},
			k:      3,
			output: 12,
		},
		1: {
			nums1:  []int{4, 2, 3, 1, 1},
			nums2:  []int{7, 5, 10, 9, 6},
			k:      1,
			output: 30,
		},
		2: {
			nums1:  []int{2, 1, 14, 12},
			nums2:  []int{11, 7, 13, 6},
			k:      3,
			output: 168,
		},
		3: {
			nums1:  []int{1, 4},
			nums2:  []int{3, 1},
			k:      2,
			output: 5,
		},
	}
	for i, scenario := range scenarios {
		output := maxScore(scenario.nums1, scenario.nums2, scenario.k)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
