package problem16

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestThreesumclosest(t *testing.T) {
	scenarios := [...]struct {
		nums   []int
		target int
		output int
	}{
		0: {[]int{-1, 2, 1, -4}, 1, 2},
		1: {[]int{0, 0, 0}, 1, 0},
	}
	for i, scenario := range scenarios {
		output := threeSumClosest(scenario.nums, scenario.target)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
