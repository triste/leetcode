package problem2328

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestCountpaths(t *testing.T) {
	scenarios := [...]struct {
		grid   [][]int
		output int
	}{
		0: {
			grid: [][]int{
				{1, 1},
				{3, 4},
			},
			output: 8,
		},
		1: {
			grid: [][]int{
				{1},
				{2},
			},
			output: 3,
		},
	}
	for i, scenario := range scenarios {
		output := countPaths(scenario.grid)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
