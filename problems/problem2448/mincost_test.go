package problem2448

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestMincost(t *testing.T) {
	scenarios := [...]struct {
		nums   []int
		cost   []int
		output int64
	}{
		0: {
			nums:   []int{1, 3, 5, 2},
			cost:   []int{2, 3, 1, 14},
			output: 8,
		},
		1: {
			nums:   []int{2, 2, 2, 2, 2},
			cost:   []int{4, 2, 8, 1, 3},
			output: 0,
		},
	}
	for i, scenario := range scenarios {
		output := minCost(scenario.nums, scenario.cost)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
