package problem1489

import (
	"sort"
)

type Set struct {
	parent *Set
}

func (n *Set) Find() *Set {
	p := n
	for p.parent != nil {
		p = p.parent
	}
	return p
}

func (n *Set) Union(node *Set) bool {
	x := n.Find()
	y := node.Find()
	if x == y {
		return false
	}
	x.parent = y
	return true
}

func findCriticalAndPseudoCriticalEdges(n int, edges [][]int) [][]int {
	for i := range edges {
		edges[i] = append(edges[i], i)
	}
	sort.Slice(edges, func(i, j int) bool {
		return edges[i][2] < edges[j][2]
	})
	mstWeight := 0
	sets := make([]Set, n)
	for _, edge := range edges {
		if sets[edge[0]].Union(&sets[edge[1]]) {
			mstWeight += edge[2]
		}
	}

	var criticalEdges, pseudoCriticalEdges []int
	for i := range edges {
		for i := range sets {
			sets[i].parent = nil
		}
		weight := 0
		forestSize := 1
		for j, edge := range edges {
			if i != j && sets[edge[0]].Union(&sets[edge[1]]) {
				weight += edge[2]
				forestSize++
			}
		}
		if forestSize < n || weight > mstWeight {
			criticalEdges = append(criticalEdges, edges[i][3])
		} else {
			for i := range sets {
				sets[i].parent = nil
			}
			sets[edges[i][0]].Union(&sets[edges[i][1]])
			weight := edges[i][2]
			for j, edge := range edges {
				if i != j && sets[edge[0]].Union(&sets[edge[1]]) {
					weight += edge[2]
				}
			}
			if weight == mstWeight {
				pseudoCriticalEdges = append(pseudoCriticalEdges, edges[i][3])
			}
		}
	}
	return [][]int{criticalEdges, pseudoCriticalEdges}
}
