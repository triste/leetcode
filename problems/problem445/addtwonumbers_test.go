package problem445

import (
	"testing"

	"github.com/google/go-cmp/cmp"
	. "gitlab.com/triste/leetcode/pkg/list"
)

func TestAddtwonumbers(t *testing.T) {
	scenarios := [...]struct {
		l1     *ListNode
		l2     *ListNode
		output *ListNode
	}{
		0: {
			l1:     NewList(7, 2, 4, 3),
			l2:     NewList(5, 6, 4),
			output: NewList(7, 8, 0, 7),
		},
		1: {
			l1:     NewList(2, 4, 3),
			l2:     NewList(5, 6, 4),
			output: NewList(8, 0, 7),
		},
		2: {
			l1:     NewList(0),
			l2:     NewList(0),
			output: NewList(0),
		},
		3: {
			l1:     NewList(5),
			l2:     NewList(5),
			output: NewList(1, 0),
		},
		4: {
			l1:     NewList(1),
			l2:     NewList(9, 9),
			output: NewList(1, 0, 0),
		},
	}
	for i, scenario := range scenarios {
		output := addTwoNumbers(scenario.l1, scenario.l2)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
