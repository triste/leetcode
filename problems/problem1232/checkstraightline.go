package problem1232

func checkStraightLine(coords [][]int) bool {
	x1, y1 := coords[0][0], coords[0][1]
	dx := x1 - coords[1][0]
	dy := y1 - coords[1][1]
	for _, coord := range coords[2:] {
		if (x1-coord[0])*dy != (y1-coord[1])*dx {
			return false
		}
	}
	return true
}
