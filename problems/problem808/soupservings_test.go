package problem808

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestSoupservings(t *testing.T) {
	scenarios := [...]struct {
		n      int
		output float64
	}{
		0: {
			n:      50,
			output: 0.625,
		},
		1: {
			n:      100,
			output: 0.71875,
		},
		2: {
			n:      1000,
			output: 0.9765650521094358,
		},
	}
	for i, scenario := range scenarios {
		output := soupServings(scenario.n)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
