package problem279

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestNumsquares(t *testing.T) {
	scenarios := [...]struct {
		n      int
		output int
	}{
		0: {
			n:      12,
			output: 3,
		},
		1: {
			n:      13,
			output: 2,
		},
	}
	for i, scenario := range scenarios {
		output := numSquares(scenario.n)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
