package problem673

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestFindnumberoflis(t *testing.T) {
	scenarios := [...]struct {
		nums   []int
		output int
	}{
		0: {
			nums:   []int{1, 3, 5, 4, 7},
			output: 2,
		},
		1: {
			nums:   []int{2, 2, 2, 2, 2},
			output: 5,
		},
		2: {
			nums:   []int{1, 2, 4, 3, 5, 4, 7, 2},
			output: 3,
		},
		3: {
			nums:   []int{3, 1, 2},
			output: 1,
		},
	}
	for i, scenario := range scenarios {
		output := findNumberOfLIS(scenario.nums)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
