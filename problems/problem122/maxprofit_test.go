package problem122

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestMaxprofit(t *testing.T) {
	scenarios := [...]struct {
		prices []int
		output int
	}{
		0: {
			prices: []int{7, 1, 5, 3, 6, 4},
			output: 7,
		},
		1: {
			prices: []int{1, 2, 3, 4, 5},
			output: 4,
		},
		2: {
			prices: []int{7, 6, 4, 3, 1},
			output: 0,
		},
	}
	for i, scenario := range scenarios {
		output := maxProfit(scenario.prices)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
