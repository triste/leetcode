package problem688

func knightProbability(n int, k int, row int, column int) (probability float64) {
	dp := make([][]float64, n)
	dpPrev := make([][]float64, n)
	for i := range dp {
		dp[i] = make([]float64, n)
		dpPrev[i] = make([]float64, n)
	}
	dpPrev[row][column] = 1
	moves := [...][2]int{{-2, 1}, {-1, 2}, {1, 2}, {2, 1}, {2, -1}, {1, -2}, {-1, -2}, {-2, -1}}
	for m := 0; m < k; m++ {
		for i := range dp {
			for j := range dp[i] {
				dp[i][j] = 0
				for _, move := range moves {
					r := i - move[0]
					c := j - move[1]
					if r >= 0 && r < n && c >= 0 && c < n {
						dp[i][j] += dpPrev[r][c] / 8
					}
				}
			}
		}
		dp, dpPrev = dpPrev, dp
	}
	for i := range dpPrev {
		for j := range dpPrev[i] {
			probability += dpPrev[i][j]
		}
	}
	return
}

/*
n = 3, k = 2, row = 0, collumn = 0



  ^   ^ 1 ^   ^ 1 ^   ^   ^   ^
  ^ 1 ^   ^   ^ 3 ^ 1 ^ 3 ^   ^
  ^   ^   | 2 |   | 2 |   ^ 3 ^
  ^ 1 ^ 2 |   |   | . | 2 ^   ^
  ^   ^ 1 | 3 | . |   |   ^ 3 ^
  ^   ^ 2 ^   ^ 3 ^   ^ 2 ^   ^
  ^   ^   ^ 2 ^   ^ 2 ^   ^   ^
*/
