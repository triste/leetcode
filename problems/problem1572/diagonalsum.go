package problem1572

func diagonalSum(mat [][]int) (sum int) {
	n := len(mat)
	for i := 0; i < n; i++ {
		sum += mat[i][i] + mat[n-i-1][i]
	}
	if n&1 != 0 {
		c := n >> 1
		sum -= mat[c][c]
	}
	return
}
