package problem1405

type LetterInfo struct {
	avalibleCount   int
	currentUseCount int
}

func longestDiverseString(a int, b int, c int) string {
	lettersInfos := [...]LetterInfo{{a, 0}, {b, 0}, {c, 0}}
	letters := make([]byte, 0, a+b+c)
	for {
		var maxAvailableCount, maxAvailableIdx int
		for i, linfo := range lettersInfos {
			if linfo.currentUseCount != 2 && linfo.avalibleCount > maxAvailableCount {
				maxAvailableCount = linfo.avalibleCount
				maxAvailableIdx = i
			}
		}
		if maxAvailableCount == 0 {
			return string(letters)
		}
		letters = append(letters, 'a'+byte(maxAvailableIdx))
		lettersInfos[maxAvailableIdx].currentUseCount++
		lettersInfos[maxAvailableIdx].avalibleCount--
		lettersInfos[(maxAvailableIdx+1)%3].currentUseCount = 0
		lettersInfos[(maxAvailableIdx+2)%3].currentUseCount = 0
	}
}
