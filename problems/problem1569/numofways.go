package problem1569

const modulo = 1_000_000_007

var triangle [1000][1000]int

func buildTriangle() {
	for i := range triangle {
		triangle[i][0] = 1
		for j := 1; j < i; j++ {
			triangle[i][j] = (triangle[i-1][j] + triangle[i-1][j-1]) % modulo
		}
		triangle[i][i] = 1
	}
}

type BinaryTreeNode struct {
	Val       int
	NodeCount int
	Left      *BinaryTreeNode
	Right     *BinaryTreeNode
}

func NewBinaryTree(nums []int) *BinaryTreeNode {
	root := &BinaryTreeNode{Val: nums[0], Left: nil, Right: nil, NodeCount: 1}
	for _, num := range nums[1:] {
		root.insert(num)
	}
	return root
}

func (n *BinaryTreeNode) insert(num int) {
	n.NodeCount++
	if num < n.Val {
		if n.Left == nil {
			n.Left = &BinaryTreeNode{Val: num, Left: nil, Right: nil, NodeCount: 1}
		} else {
			n.Left.insert(num)
		}
	} else {
		if n.Right == nil {
			n.Right = &BinaryTreeNode{Val: num, Left: nil, Right: nil, NodeCount: 1}
		} else {
			n.Right.insert(num)
		}
	}
}

func numOfWays(nums []int) int {
	if triangle[0][0] == 0 {
		buildTriangle()
	}
	var dfs func(root *BinaryTreeNode) int
	dfs = func(root *BinaryTreeNode) int {
		if root == nil || root.NodeCount < 3 {
			return 1
		}
		var c int
		if root.Left == nil {
			c = triangle[root.NodeCount-1][root.Right.NodeCount]
		} else {
			c = triangle[root.NodeCount-1][root.Left.NodeCount]
		}
		return c * dfs(root.Left) % modulo * dfs(root.Right) % modulo
	}
	root := NewBinaryTree(nums)
	return dfs(root) - 1
}

/*
          1
		1   1
	  1   2   1

*/

/*
	3,4,5,1,2
       3
	  / \
	 1   4
	  \	  \
	   2   5
	fix 3: n = 4
		fix 1: n = 3
			fix 2: n = 2
				2
			fix 4: = n = 2

	12..  45..
	1.2.  4.5.
	1..2  4..5
	.12.  .45.
	.1.2  .4.5
	..12  ..45

	3
	1,4
	{2,4},{1,5}


	nums := {3,5,4,6,1,2}
	n := len(nums) = 6
         3
	  /     \
	 1       5
	  \	    / \
	   2   4   6

	fix 3: n = 5
		5!/(5-2)! = 20
		fix 1: n = 4
			4!/(4-1)!=4




	fix 3:
		n = 5
		fix 1:
			n = 4
			2...
			.2..
			..2.
			...2
			binkof = 4
		fix 5:
			n = 4
			fix 4:
				n = 3
				6..
				.6.
				..6
				binkof = 3
			fix 6:
				n = 3
				4..
				.4.
				..4
				binkof = 3
			46..
			4.6.
			4..6
			.46.
			.4.6
			..46
			64..
			6.4.
			6..4
			.64.
			.6.4
			..64
		12...
		1.2..
		1..2.
		1...2
		.12..
		.1.2.
		.1..2
		..12.
		..1.2
		...12
		binkof = 10
*/

/*
	2,1,3

	 2
	/ \
   1   3
   fix 2, then solve for 1 and 3



   3,4,5,1,2
               3
			  / \
			 1   4
		      \	  \
			   2   5

	sorted: 1,2,3,4,5
	fix 3, then solve for   1         4
	                         \   and   \
						      2         5

	fix 3, then solve for {1,2} and {4,5}:
		we can fix 1 from {1,2} OR fix 4 from {4,5}:
		case 1: fix 1
			we can fix 2 from {2} OR fix 4 from {4,5}:
			case 1.1: fix 2
				we can fix 4 from {4,5}
				fix 4:
					we can fix 5 from {5}
					fix 5:
						ANSWER: 3,1,2,4,5
			case 1.2: fix 4
				we can fix 2 from {2} OR 5 from {5}
				case 1.2.1: fix 2
					we can fix 5 from {5}
					fix 5:
						ANSWER: 3,1,4,2,5
				case 1.2.2: fix 5
					we can fi 2 from {2}
					fix 2:
						ANSWER: 3,1,4,5,2
		case 2: fix 4
			we can fix 1 from {1,2} OR fix 5 from {5}
			case 2.1: fix 1
				we can fix 2 from {2} OR fix 5 from {5}
				case 2.1.1: fix 2
					we can fix 5 from {5}
					fix 5
					ANSWER: 3,4,1,2,5
				case 2.1.2: fix 5
					we can fix 2 from {2}
					fix 2
					ANSWER: 3,4,1,5,2
			case 2.2: fix 5
				we can fix 1 from {1,2}
				fix 1
				we can fix 2 from {2}
				fix 2
				ANSWER: 3,4,5,1,2
*/
