package problem1203

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestSortitems(t *testing.T) {
	scenarios := [...]struct {
		n           int
		m           int
		group       []int
		beforeItems [][]int
		output      []int
	}{
		0: {
			n: 8,
			m: 2,
			group: []int{
				-1, -1, 1, 0, 0, 1, 0, -1,
			},
			beforeItems: [][]int{
				nil, {6}, {5}, {6}, {3, 6}, nil, nil, nil,
			},
			output: []int{
				6, 3, 4, 1, 5, 2, 0, 7,
			},
		},
		1: {
			n: 8,
			m: 2,
			group: []int{
				-1, -1, 1, 0, 0, 1, 0, -1,
			},
			beforeItems: [][]int{
				nil, {6}, {5}, {6}, {3}, {}, {4}, nil,
			},
			output: nil,
		},
		2: {
			n: 7,
			m: 2,
			group: []int{
				-1, 0, 0, 1, 0, 1, 1,
			},
			beforeItems: [][]int{
				nil, {2}, {3}, nil, nil, nil, nil,
			},
			output: []int{},
		},
	}
	for i, scenario := range scenarios {
		output := sortItems(scenario.n, scenario.m, scenario.group, scenario.beforeItems)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
