package problem1547

import (
	"math"
	"sort"
)

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

/*
			n = 7, cuts = {1,3,4,5}

	  | 0 | 1 | 3 | 4 | 5 | 7 |

0 |   | 0 | 3 | 7 |14 |16 |
1 |   |   | 0 | 3 | 6 |15 |
3 |   |   |   | 0 | 2 | 6 |
4 |   |   |   |   | 0 | 3 |
5 |   |   |   |   |   | 0 |
7 |   |   |   |   |   |   |

	| 7 | 5 | 4 | 3 | 1 | 0 |

0 |   |   |   |   |   |   |
1 |   |   |   |   |   | 0 |
3 |   |   |   |   | 0 | 3 |
4 |   |   |   | 0 | 3 |   |
5 |   |   | 0 | 2 |   |   |
7 |   | 0 | 3 |   |   |   |

		dp[2][5] = min(dp[2][4] + dp[0][0], dp[2][3] + dp[0][1])
	    0 1  3 4
		/-/--/-/

		0 1  3 4 5
		/-/--/-/-/
*/
var dp [102][102]int

func minCost(n int, cuts []int) int {
	cuts = append(cuts, 0, n)
	sort.Ints(cuts)
	for i := 2; i < len(cuts); i++ {
		for j := 0; j < len(cuts)-i; j++ {
			min := math.MaxInt
			for k := j + 1; k < j+i; k++ {
				if sum := dp[j][k] + dp[k][j+i]; sum < min {
					min = sum
				}
			}
			dp[j][j+i] = cuts[j+i] - cuts[j] + min
		}
	}
	return dp[0][len(cuts)-1]
}

func minCostRecursive(n int, cuts []int) int {
	var helper func(int, int) int
	helper = func(left, right int) int {
		if right-left < 2 {
			return 0
		}
		min := 100000
		for i := left + 1; i < right; i++ {
			cost := cuts[right] - cuts[left] + helper(left, i) + helper(i, right)
			if cost < min {
				min = cost
			}
		}
		return min
	}
	sort.Ints(cuts)
	cuts = append([]int{0}, cuts...)
	cuts = append(cuts, n)
	return helper(0, len(cuts)-1)
}
