package problem1318

func minFlips(a int, b int, c int) (flipCount int) {
	for a > 0 || b > 0 || c > 0 {
		if c&1 > 0 {
			flipCount += ^(a | b) & 1
		} else {
			flipCount += a&1 + b&1
		}
		a >>= 1
		b >>= 1
		c >>= 1
	}
	return
}

/*
000: 0
001: 1
010: 1
011: 0
100: 1
101: 0
110: 2
111: 0

000011
001101
010100
*/

/*
a: 1000
b: 0011
c: 0101

mask: 0100
a: 0000
b: 0000
c: 0100


0011
0101
0100
*/

/*
	0011
	0101
	0120
*/

/*
a: 0010
b: 0110
c: 0101

a ^ b: 0100
^(a^b): 1011

a: 0010
b: 0010
c: 0001

b:      0010
c ^= a, 0011


a: 0111
b: 0111
c: 0111

^(a^b): 0111
c |= a, 0111
kkkkkkkkkkkkkkkk

*/
