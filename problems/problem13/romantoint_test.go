package problem13

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestRomantoint(t *testing.T) {
	scenarios := [...]struct {
		s      string
		output int
	}{
		0: {"III", 3},
		1: {"LVIII", 58},
		2: {"MCMXCIV", 1994},
	}
	for i, scenario := range scenarios {
		output := romanToInt(scenario.s)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
