package problem1220

func countVowelPermutation(n int) int {
	const mod = 1_000_000_007
	a, e, i, o, u := 1, 1, 1, 1, 1
	for j := 1; j < n; j++ {
		a, e, i, o, u = e, (a+i)%mod, (a+e+o+u)%mod, (i+u)%mod, a
	}
	return (a + e + i + o + u) % mod
}
