package problem92

import (
	"testing"

	"github.com/google/go-cmp/cmp"
	. "gitlab.com/triste/leetcode/pkg/list"
)

func TestReversebetween(t *testing.T) {
	scenarios := [...]struct {
		head   *ListNode
		left   int
		right  int
		output *ListNode
	}{
		0: {
			head:   NewList(1, 2, 3, 4, 5),
			left:   2,
			right:  4,
			output: NewList(1, 4, 3, 2, 5),
		},
		1: {
			head:   NewList(5),
			left:   1,
			right:  1,
			output: NewList(5),
		},
		2: {
			head:   NewList(1, 2, 3, 4, 5),
			left:   1,
			right:  4,
			output: NewList(4, 3, 2, 1, 5),
		},
	}
	for i, scenario := range scenarios {
		output := reverseBetween(scenario.head, scenario.left, scenario.right)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
