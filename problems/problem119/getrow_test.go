package problem119

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestGetrow(t *testing.T) {
	scenarios := [...]struct {
		rowIndex int
		output   []int
	}{
		0: {
			rowIndex: 3,
			output:   []int{1, 3, 3, 1},
		},
		1: {
			rowIndex: 0,
			output:   []int{1},
		},
		2: {
			rowIndex: 1,
			output:   []int{1, 1},
		},
		3: {
			rowIndex: 4,
			output:   []int{1, 4, 6, 4, 1},
		},
	}
	for i, scenario := range scenarios {
		output := getRow(scenario.rowIndex)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
