package problem1802

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestMaxvalue(t *testing.T) {
	scenarios := [...]struct {
		n      int
		index  int
		maxSum int
		output int
	}{
		0: {
			n:      4,
			index:  2,
			maxSum: 6,
			output: 2,
		},
		1: {
			n:      6,
			index:  1,
			maxSum: 10,
			output: 3,
		},
		2: {
			n:      8,
			index:  7,
			maxSum: 14,
			output: 4,
		},
		3: {
			n:      3,
			index:  2,
			maxSum: 18,
			output: 7,
		},
		4: {
			n:      4,
			index:  0,
			maxSum: 4,
			output: 1,
		},
		5: {
			n:      9,
			index:  3,
			maxSum: 16,
			output: 3,
		},
		6: {
			n:      9,
			index:  3,
			maxSum: 9,
			output: 1,
		},
		7: {
			n:      8257285,
			index:  4828516,
			maxSum: 850015631,
			output: 29014,
		},
	}
	for i, scenario := range scenarios {
		output := maxValue(scenario.n, scenario.index, scenario.maxSum)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
