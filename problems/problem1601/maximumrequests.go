package problem1601

func maximumRequests(n int, requests [][]int) int {
	maxReqs := 0
	buildings := make([]int, n)
	var solve func(i, accepted int)
	solve = func(i, accepted int) {
		if i == len(requests) {
			for _, building := range buildings {
				if building != 0 {
					return
				}
			}
			if accepted > maxReqs {
				maxReqs = accepted
			}
			return
		}
		buildings[requests[i][0]]--
		buildings[requests[i][1]]++
		solve(i+1, accepted+1)
		buildings[requests[i][0]]++
		buildings[requests[i][1]]--
		solve(i+1, accepted)
	}
	solve(0, 0)
	return maxReqs
}
