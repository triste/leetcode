package problem1456

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestMaxvowels(t *testing.T) {
	scenarios := [...]struct {
		s      string
		k      int
		output int
	}{
		0: {
			s:      "abciiidef",
			k:      3,
			output: 3,
		},
		1: {
			s:      "aeiou",
			k:      2,
			output: 2,
		},
		2: {
			s:      "leetcode",
			k:      3,
			output: 2,
		},
	}
	for i, scenario := range scenarios {
		output := maxVowels(scenario.s, scenario.k)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
