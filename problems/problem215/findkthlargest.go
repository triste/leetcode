package problem215

import (
	_ "log"
	"math/rand"
)

func findKthLargest(nums []int, k int) int {
	lenght := len(nums)
	if lenght == 1 {
		return nums[0]
	}
	pivotIndex := partition(nums)
	if lenght-pivotIndex < k {
		return findKthLargest(nums[:pivotIndex], k-(lenght-pivotIndex))
	} else if lenght-pivotIndex > k {
		return findKthLargest(nums[pivotIndex+1:], k)
	} else {
		return nums[pivotIndex]
	}
}

func partition(nums []int) int {
	lenght := len(nums)
	randomIndex := rand.Intn(lenght)
	pivot := nums[randomIndex]
	nums[randomIndex], nums[lenght-1] = nums[lenght-1], nums[randomIndex]
	tempIndex := 0
	for i, num := range nums[:lenght-1] {
		if num < pivot {
			nums[tempIndex], nums[i] = num, nums[tempIndex]
			tempIndex++
		}
	}
	nums[tempIndex], nums[lenght-1] = nums[lenght-1], nums[tempIndex]
	return tempIndex
}
