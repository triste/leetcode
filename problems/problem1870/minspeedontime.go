package problem1870

func travelTime(dist []int, speed int) float64 {
	var time int
	for i := 0; i < len(dist)-1; i++ {
		time += dist[i] / speed
		if dist[i]%speed > 0 {
			time++
		}
	}
	return float64(time) + float64(dist[len(dist)-1])/float64(speed)
}

func minSpeedOnTime(dist []int, hour float64) int {
	l, r := 1, 10_000_001
	for l < r {
		m := (l + r) / 2
		time := travelTime(dist, m)
		if time > hour {
			l = m + 1
		} else if time < hour {
			r = m
		} else {
			return m
		}
	}
	if r == 10_000_001 {
		return -1
	}
	return r
}
