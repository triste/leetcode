package problem31

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestNextpermutation(t *testing.T) {
	scenarios := [...]struct {
		nums   []int
		output []int
	}{
		0: {
			nums:   []int{1, 2, 3},
			output: []int{1, 3, 2},
		},
		1: {
			nums:   []int{3, 2, 1},
			output: []int{1, 2, 3},
		},
		2: {
			nums:   []int{1, 1, 5},
			output: []int{1, 5, 1},
		},
		3: {
			nums:   []int{2, 3, 1},
			output: []int{3, 1, 2},
		},
	}
	for i, scenario := range scenarios {
		nextPermutation(scenario.nums)
		if diff := cmp.Diff(scenario.output, scenario.nums); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
