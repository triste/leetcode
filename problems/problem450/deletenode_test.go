package problem450

import (
	"testing"

	"github.com/google/go-cmp/cmp"
	. "gitlab.com/triste/leetcode/pkg/tree"
)

func TestDeletenode(t *testing.T) {
	scenarios := [...]struct {
		root   *TreeNode
		key    int
		output *TreeNode
	}{
		0: {
			NewTree(5,3,6,2,4,nil,7),
			3,
			NewTree(5,4,6,2,nil,nil,7),
		},
		1: {
			NewTree(5,3,6,2,4,nil,7),
			0,
			NewTree(5,3,6,2,4,nil,7),
		},
		2: {
			nil,
			0,
			nil,
		},
	}
	for i, scenario := range scenarios {
		output := deleteNode(scenario.root, scenario.key)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
