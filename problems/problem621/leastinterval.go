package problem621

import (
	"container/heap"
)

type Task struct {
	id       byte
	priority int
}

type PriorityQueue []Task

func (pq PriorityQueue) Len() int           { return len(pq) }
func (pq PriorityQueue) Less(i, j int) bool { return pq[i].priority > pq[j].priority }
func (pq PriorityQueue) Swap(i, j int)      { pq[i], pq[j] = pq[j], pq[i] }
func (pq *PriorityQueue) Push(x any)        { *pq = append(*pq, x.(Task)) }
func (pq *PriorityQueue) Pop() any {
	x := (*pq)[len(*pq)-1]
	*pq = (*pq)[:len(*pq)-1]
	return x
}

func leastInterval(tasks []byte, n int) int {
	if n == 0 {
		return len(tasks)
	}
	var taskCounts [26]int
	for _, task := range tasks {
		taskCounts[task-'A']++
	}
	var pq PriorityQueue
	heap.Init(&pq)
	for i, tCount := range taskCounts {
		if tCount != 0 {
			task := Task{
				id:       'A' + byte(i),
				priority: tCount,
			}
			heap.Push(&pq, task)
		}
	}
	n++
	taskCooldownEnds := make([]Task, n)
	tick := 0
	for i := 0; i < len(tasks); tick++ {
		if task := taskCooldownEnds[tick%n]; task.priority > 0 {
			heap.Push(&pq, task)
		}
		if len(pq) > 0 {
			task := heap.Pop(&pq).(Task)
			task.priority--
			taskCooldownEnds[tick%n] = task
			i++
		} else {
		}
	}
	return tick
}

/*

 */
