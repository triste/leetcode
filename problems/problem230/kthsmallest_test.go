package problem230

import (
	"testing"

	"github.com/google/go-cmp/cmp"
	. "gitlab.com/triste/leetcode/pkg/tree"
)

func TestKthsmallest(t *testing.T) {
	scenarios := [...]struct {
		root   *TreeNode
		k      int
		output int
	}{
		0: {
			NewTree(3,1,4,nil,2),
			1,
			1,
		},
		1: {
			NewTree(5,3,6,2,4,nil,nil,1),
			3,
			3,
		},
	}
	for i, scenario := range scenarios {
		output := kthSmallest(scenario.root, scenario.k)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
