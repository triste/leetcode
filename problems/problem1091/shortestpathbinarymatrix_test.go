package problem1091

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestShortestpathbinarymatrix(t *testing.T) {
	scenarios := [...]struct {
		grid   [][]int
		output int
	}{
		0: {[][]int{
			{0, 1},
			{1, 0},
		}, 2},
		1: {[][]int{
			{0, 0, 0},
			{1, 1, 0},
			{1, 1, 0},
		}, 4},
		2: {[][]int{
			{1, 0, 0},
			{1, 1, 0},
			{1, 1, 0},
		}, -1},
	}
	for i, scenario := range scenarios {
		output := shortestPathBinaryMatrix(scenario.grid)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
