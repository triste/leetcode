package problem450

import (
	. "gitlab.com/triste/leetcode/pkg/tree"
)

func deleteNode(root *TreeNode, key int) *TreeNode {
	var deleteNodeHelper func(**TreeNode, int)
	deleteNodeHelper = func(curlink **TreeNode, key int) {
		curnode := *curlink
		if curnode == nil {
			return
		}
		if key < curnode.Val {
			deleteNodeHelper(&curnode.Left, key)
		} else if key > curnode.Val {
			deleteNodeHelper(&curnode.Right, key)
		} else if curnode.Right == nil {
			*curlink = curnode.Left
		} else {
			*curlink = curnode.Right
			last := curnode.Right
			for last.Left != nil {
				last = last.Left
			}
			last.Left = curnode.Left
		}
	}
	deleteNodeHelper(&root, key)
	return root
}
