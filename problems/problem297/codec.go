package problem297

import (
	"encoding/binary"
	"math"
	"strings"

	. "gitlab.com/triste/leetcode/pkg/tree"
)

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */

type Codec struct {
}

func Constructor() Codec {
	return Codec{}
}

// Serializes a tree to a single string.
func (c *Codec) serialize(root *TreeNode) string {
	if root == nil {
		return ""
	}
	var writer strings.Builder
	queue := []*TreeNode{root}
	for len(queue) > 0 {
		newQueue := []*TreeNode{}
		for _, node := range queue {
			var num int16
			if node == nil {
				num = math.MaxInt16
			} else {
				num = int16(node.Val)
				newQueue = append(newQueue, node.Left)
				newQueue = append(newQueue, node.Right)
			}
			binary.Write(&writer, binary.LittleEndian, &num)
		}
		queue = newQueue
	}
	return writer.String()
}

// Deserializes your encoded data to tree.
func (c *Codec) deserialize(data string) *TreeNode {
	reader := strings.NewReader(data)
	var num int16
	if err := binary.Read(reader, binary.LittleEndian, &num); err != nil {
		return nil
	}
	root := &TreeNode{Val: int(num), Left: nil, Right: nil}
	queue := []*TreeNode{root}
	for len(queue) > 0 {
		newQueue := []*TreeNode{}
		for _, node := range queue {
			nodes := [2]*TreeNode{}
			for i := 0; i < 2; i++ {
				if err := binary.Read(reader, binary.LittleEndian, &num); err != nil {
					break
				}
				if num == math.MaxInt16 {
					nodes[i] = nil
				} else {
					nodes[i] = &TreeNode{Val: int(num), Left: nil, Right: nil}
					newQueue = append(newQueue, nodes[i])
				}
			}
			node.Left = nodes[0]
			node.Right = nodes[1]
		}
		queue = newQueue
	}
	return root
}

/**
 * Your Codec object will be instantiated and called as such:
 * ser := Constructor();
 * deser := Constructor();
 * data := ser.serialize(root);
 * ans := deser.deserialize(data);
 */
