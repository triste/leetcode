package problem515

import (
	"testing"

	"github.com/google/go-cmp/cmp"
	. "gitlab.com/triste/leetcode/pkg/tree"
)

func TestLargestvalues(t *testing.T) {
	scenarios := [...]struct {
		root   *TreeNode
		output []int
	}{
		0: {
			root:   NewTree(1, 3, 2, 5, 3, nil, 9),
			output: []int{1, 3, 9},
		},
		1: {
			root:   NewTree(1, 2, 3),
			output: []int{1, 3},
		},
	}
	for i, scenario := range scenarios {
		output := largestValues(scenario.root)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
