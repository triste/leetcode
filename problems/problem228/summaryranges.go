package problem228

import (
	"fmt"
	"strconv"
)

func rangeToString(start, end int) string {
	if start == end {
		return strconv.Itoa(start)
	}
	return fmt.Sprintf("%v->%v", start, end)
}

func summaryRanges(nums []int) (ranges []string) {
	if len(nums) == 0 {
		return
	}
	start := nums[0]
	for i, num := range nums[1:] {
		if prev := nums[i]; num-prev > 1 {
			ranges = append(ranges, rangeToString(start, prev))
			start = num
		}
	}
	return append(ranges, rangeToString(start, nums[len(nums)-1]))
}
