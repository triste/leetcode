package problem45

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestJump(t *testing.T) {
	scenarios := [...]struct {
		nums   []int
		output int
	}{
		0: {
			nums:   []int{2, 3, 1, 1, 4},
			output: 2,
		},
		1: {
			nums:   []int{2, 3, 0, 1, 4},
			output: 2,
		},
		2: {
			nums:   []int{2, 1},
			output: 1,
		},
		3: {
			nums:   []int{0},
			output: 0,
		},
		4: {
			nums:   []int{1},
			output: 0,
		},
		5: {
			nums:   []int{2, 2, 0, 1},
			output: 2,
		},
		6: {
			nums:   []int{1, 1, 1, 1},
			output: 3,
		},
		7: {
			nums:   []int{5, 9, 3, 2, 1, 0, 2, 3, 3, 1, 0, 0},
			output: 3,
		},
		8: {
			nums:   []int{1, 1, 2, 1, 1},
			output: 3,
		},
		9: {
			nums:   []int{1, 2, 3},
			output: 2,
		},
	}
	for i, scenario := range scenarios {
		output := jump(scenario.nums)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
