package problem106

import (
	"testing"

	"github.com/google/go-cmp/cmp"
	. "gitlab.com/triste/leetcode/pkg/tree"
)

func TestBuildtree(t *testing.T) {
	scenarios := [...]struct {
		inorder   []int
		postorder []int
		output    *TreeNode
	}{
		0: {
			inorder:   []int{9, 3, 15, 20, 7},
			postorder: []int{9, 15, 7, 20, 3},
			output:    NewTree(3, 9, 20, nil, nil, 15, 7),
		},
		1: {
			inorder:   []int{-1},
			postorder: []int{-1},
			output:    NewTree(-1),
		},
	}
	for i, scenario := range scenarios {
		output := buildTree(scenario.inorder, scenario.postorder)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
