package problem2483

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestBestclosingtime(t *testing.T) {
	scenarios := [...]struct {
		customers string
		output    int
	}{
		0: {
			customers: "YYNY",
			output:    2,
		},
		1: {
			customers: "NNNNN",
			output:    0,
		},
		2: {
			customers: "YYYY",
			output:    4,
		},
	}
	for i, scenario := range scenarios {
		output := bestClosingTime(scenario.customers)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
