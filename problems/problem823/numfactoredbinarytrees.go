package problem823

import (
	"math"
	"sort"
)

func numFactoredBinaryTrees(nums []int) (output int) {
	sort.Ints(nums)
	set := make(map[int]int, len(nums))
	dp := make([]int, len(nums))
	for i := 0; i < len(nums); i++ {
		set[nums[i]] = i
		dp[i] = 1
	}
	for i := range dp {
		for j := 0; nums[j] <= int(math.Sqrt(float64(nums[i]))); j++ {
			if nums[i]%nums[j] == 0 {
				if k, found := set[nums[i]/nums[j]]; found {
					if j == k {
						dp[i] += dp[j] * dp[k]
					} else {
						dp[i] += dp[j] * dp[k] * 2
					}
				}
			}
		}
		output = (output + dp[i]) % 1_000_000_007
	}
	return
}

/*
           8
		/     \
	   4       2
	  / \
	 2   2


	   4
	 /   \
	2     2
*/
