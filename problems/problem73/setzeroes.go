package problem73

func setZeroes(matrix [][]int) {
	firstColumnZeroed := false
	for r, row := range matrix {
		if row[0] == 0 {
			firstColumnZeroed = true
		}
		for c := 1; c < len(row); c++ {
			if row[c] == 0 {
				matrix[r][0] = 0
				matrix[0][c] = 0
			}
		}
	}
	for r := len(matrix) - 1; r >= 0; r-- {
		for c := len(matrix[r]) - 1; c > 0; c-- {
			if matrix[r][0] == 0 || matrix[0][c] == 0 {
				matrix[r][c] = 0
			}
		}
		if firstColumnZeroed {
			matrix[r][0] = 0
		}
	}
}
