package problem1970

const (
	MatLeft = iota
	MatTmp
	MatVisited
	MatCount
)

func latestDayToCross(row int, col int, cells [][]int) int {
	var mats [MatCount][][]bool
	for i := range mats {
		mats[i] = make([][]bool, row)
		for j := range mats[i] {
			mats[i][j] = make([]bool, col)
		}
	}
	dirs := [4][2]int{{-1, 0}, {0, 1}, {1, 0}, {0, -1}}
	l, r := 0, len(cells)-1
	var queue, tmpQueue [][2]int
	for l < r {
		mid := r - (r-l)/2
		for i := range mats[MatLeft] {
			copy(mats[MatTmp][i], mats[MatLeft][i])
			copy(mats[MatVisited][i], mats[MatLeft][i])
		}
		for i := l; i < mid; i++ {
			r, c := cells[i][0]-1, cells[i][1]-1
			mats[MatTmp][r][c] = true
			mats[MatVisited][r][c] = true
		}
		for i := 0; i < col; i++ {
			if !mats[MatVisited][0][i] {
				queue = append(queue, [2]int{0, i})
			}
		}
		for len(queue) > 0 {
			for _, coord := range queue {
				if coord[0] == row-1 {
					l = mid
					mats[MatLeft], mats[MatTmp] = mats[MatTmp], mats[MatLeft]
					goto found
				}
				for _, dir := range dirs {
					r, c := coord[0]+dir[0], coord[1]+dir[1]
					if r >= 0 && r < row && c >= 0 && c < col && !mats[MatVisited][r][c] {
						mats[MatVisited][r][c] = true
						tmpQueue = append(tmpQueue, [2]int{r, c})
					}
				}
			}
			queue, tmpQueue = tmpQueue, queue[:0]
		}
		r = mid - 1
	found:
		queue, tmpQueue = queue[:0], tmpQueue[:0]
	}
	return l
}
