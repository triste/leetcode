package problem105

import (
	"testing"

	"github.com/google/go-cmp/cmp"
	. "gitlab.com/triste/leetcode/pkg/tree"
)

func TestBuildtree(t *testing.T) {
	scenarios := [...]struct {
		preorder []int
		inorder  []int
		output   *TreeNode
	}{
		0: {
			preorder: []int{3, 9, 20, 15, 7},
			inorder:  []int{9, 3, 15, 20, 7},
			output:   NewTree(3, 9, 20, nil, nil, 15, 7),
		},
		1: {
			preorder: []int{-1},
			inorder:  []int{-1},
			output:   NewTree(-1),
		},
	}
	for i, scenario := range scenarios {
		output := buildTree(scenario.preorder, scenario.inorder)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
