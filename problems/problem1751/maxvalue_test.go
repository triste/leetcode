package problem1751

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestMaxvalue(t *testing.T) {
	scenarios := [...]struct {
		events [][]int
		k      int
		output int
	}{
		0: {
			events: [][]int{
				{1, 2, 4}, {3, 4, 3}, {2, 3, 1},
			},
			k:      2,
			output: 7,
		},
		1: {
			events: [][]int{
				{1, 2, 4}, {3, 4, 3}, {2, 3, 10},
			},
			k:      2,
			output: 10,
		},
		2: {
			events: [][]int{
				{1, 1, 1}, {2, 2, 2}, {3, 3, 3}, {4, 4, 4},
			},
			k:      3,
			output: 9,
		},
	}
	for i, scenario := range scenarios {
		output := maxValue(scenario.events, scenario.k)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
