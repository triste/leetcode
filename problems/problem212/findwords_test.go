package problem212

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestFindwords(t *testing.T) {
	scenarios := [...]struct {
		board  [][]byte
		words  []string
		output []string
	}{
		0: {
			board: [][]byte{
				{'o', 'a', 'a', 'n'},
				{'e', 't', 'a', 'e'},
				{'i', 'h', 'k', 'r'},
				{'i', 'f', 'l', 'v'},
			},
			words: []string{
				"oath", "pea", "eat", "rain",
			},
			output: []string{
				"oath", "eat",
			},
		},
		1: {
			board: [][]byte{
				{'a', 'b'},
				{'c', 'd'},
			},
			words:  []string{"abcd"},
			output: nil,
		},
		2: {
			board: [][]byte{
				{'o', 'a', 'b', 'n'},
				{'o', 't', 'a', 'e'},
				{'a', 'h', 'k', 'r'},
				{'a', 'f', 'l', 'v'},
			},
			words: []string{
				"oa", "oaa",
			},
			output: []string{
				"oa", "oaa",
			},
		},
	}
	for i, scenario := range scenarios {
		output := findWords(scenario.board, scenario.words)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
