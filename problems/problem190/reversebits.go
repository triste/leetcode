package problem190

func reverseBits(x uint32) uint32 {
	// return bits.Reverse32(num)
	x = (x&0x55555555)<<1 | (x&0xaaaaaaaa)>>1
	x = (x&0x33333333)<<2 | (x&0xcccccccc)>>2
	x = (x&0xf0f0f0f)<<4 | (x&0xf0f0f0f0)>>4
	x = (x&0xFF00FF)<<8 | (x&0xFF00FF00)>>8
	return x<<16 | x>>16
}
