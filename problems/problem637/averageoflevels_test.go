package problem637

import (
	"testing"

	"github.com/google/go-cmp/cmp"
	. "gitlab.com/triste/leetcode/pkg/tree"
)

func TestAverageoflevels(t *testing.T) {
	scenarios := [...]struct {
		root   *TreeNode
		output []float64
	}{
		0: {
			root:   NewTree(3, 9, 20, nil, nil, 15, 7),
			output: []float64{3, 14.5, 11},
		},
		1: {
			root:   NewTree(3, 9, 20, 15, 7),
			output: []float64{3, 14.5, 11},
		},
	}
	for i, scenario := range scenarios {
		output := averageOfLevels(scenario.root)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
