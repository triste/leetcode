package problem1125

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestSmallestsufficientteam(t *testing.T) {
	scenarios := [...]struct {
		req_skills []string
		people     [][]string
		output     []int
	}{
		0: {
			req_skills: []string{"java", "nodejs", "reactjs"},
			people: [][]string{
				{"java"},
				{"nodejs"},
				{"nodejs", "reactjs"},
			},
			output: []int{0, 2},
		},
		1: {
			req_skills: []string{"algorithms", "math", "java", "reactjs", "csharp", "aws"},
			people: [][]string{
				{"algorithms", "math", "java"},
				{"algorithms", "math", "reactjs"},
				{"java", "csharp", "aws"},
				{"reactjs", "csharp"},
				{"csharp", "math"},
				{"aws", "java"},
			},
			output: []int{1, 2},
		},
	}
	for i, scenario := range scenarios {
		output := smallestSufficientTeam(scenario.req_skills, scenario.people)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
