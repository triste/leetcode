package problem55

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestCanjump(t *testing.T) {
	scenarios := [...]struct {
		nums   []int
		output bool
	}{
		0: {
			nums:   []int{2, 3, 1, 1, 4},
			output: true,
		},
		1: {
			nums:   []int{3, 2, 1, 0, 4},
			output: false,
		},
		2: {
			nums:   []int{3, 2, 1, 0, 4},
			output: false,
		},
		3: {
			nums:   []int{0, 2, 3},
			output: false,
		},
		4: {
			nums:   []int{0},
			output: true,
		},
	}
	for i, scenario := range scenarios {
		output := canJump(scenario.nums)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
