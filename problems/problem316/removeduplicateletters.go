package problem316

func removeDuplicateLetters(s string) string {
	var lastOccurrences [26]int
	for i, ch := range s {
		lastOccurrences[ch-'a'] = i
	}
	var used [26]bool
	stack := make([]byte, 0, 26)
	for i := range s {
		if used[s[i]-'a'] {
			continue
		}
		used[s[i]-'a'] = true
		var j int
		for j = len(stack) - 1; j >= 0 && stack[j] > s[i] && lastOccurrences[stack[j]-'a'] > i; j-- {
			used[stack[j]-'a'] = false
		}
		stack = append(stack[:j+1], s[i])
	}
	return string(stack)
}
