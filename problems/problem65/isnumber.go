package problem65

func isInteger(s string) bool {
	i := 0
	if s[i] == '-' || s[i] == '+' {
		if i++; i == len(s) {
			return false
		}
	}
	for ; i < len(s); i++ {
		if s[i] < '0' || s[i] > '9' {
			return false
		}
	}
	return true
}

func isExponent(b byte) bool {
	return b == 'e' || b == 'E'
}

func isDigit(b byte) bool {
	return b >= '0' && b <= '9'
}

func isSign(b byte) bool {
	return b == '-' || b == '+'
}

func isDot(b byte) bool {
	return b == '.'
}

func isNumber(s string) bool {
	if i := 0; isSign(s[i]) {
		if i++; i == len(s) { // [+-]
			return false
		}
		for ; i < len(s) && !isDot(s[i]) && !isExponent(s[i]); i++ {
			if !isDigit(s[i]) {
				return false
			}
		}
		if i == len(s) { // [+-][0-9]+
			return true
		}
		if isDot(s[i]) {
			for i++; i < len(s) && !isExponent(s[i]); i++ {
				if !isDigit(s[i]) {
					return false
				}
			}
			if i == 2 { // [+-].[eE]*
				return false
			}
			if i == len(s) {
				return true
			}
			if i++; i == len(s) { // [+-].[0-9]+[eE]
				return false
			}
			return isInteger(s[i:])
		} else {
			if i++; i == 2 || i == len(s) { // [+-][eE] || [+-][0-9]+[eE]
				return false
			}
			return isInteger(s[i:])
		}
	} else if isDot(s[i]) {
		for i++; i < len(s) && !isExponent(s[i]); i++ {
			if !isDigit(s[i]) {
				return false
			}
		}
		if i == 1 { // .[eE]
			return false
		}
		if i == len(s) {
			return true
		}
		if i++; i == len(s) { // .[eE]
			return false
		}
		return isInteger(s[i:])
	} else {
		for ; i < len(s) && !isDot(s[i]) && !isExponent(s[i]); i++ {
			if !isDigit(s[i]) {
				return false
			}
		}
		if i == len(s) {
			return true
		}
		if isDot(s[i]) {
			for i++; i < len(s) && !isExponent(s[i]); i++ {
				if !isDigit(s[i]) {
					return false
				}
			}
			if i == len(s) {
				return true
			}
			if i++; i == len(s) { // [0-9]+.[eE]
				return false
			}
			return isInteger(s[i:])
		} else if i > 0 {
			if i++; i == len(s) {
				return false
			}
			return isInteger(s[i:])
		}
	}
	return false // ^[-+.0-9]
}
