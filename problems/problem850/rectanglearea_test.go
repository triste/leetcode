package problem850

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestRectanglearea(t *testing.T) {
	scenarios := [...]struct {
		rectangles [][]int
		output     int
	}{
		0: {
			rectangles: [][]int{
				{0, 0, 2, 2},
				{1, 0, 2, 3},
				{1, 0, 3, 1},
			},
			output: 6,
		},
		1: {
			rectangles: [][]int{
				{0, 0, 1000000000, 1_000_000_000},
			},
			output: 49,
		},
		2: {
			rectangles: [][]int{
				{0, 0, 1, 1},
				{2, 2, 3, 3},
			},
			output: 2,
		},
		3: {
			rectangles: [][]int{
				{25, 20, 70, 27},
				{68, 80, 79, 100},
				{37, 41, 66, 76},
			},
			output: 1550,
		},
	}
	for i, scenario := range scenarios[:1] {
		output := rectangleArea(scenario.rectangles)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
