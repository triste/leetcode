package problem50

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestMypow(t *testing.T) {
	scenarios := [...]struct {
		x      float64
		n      int
		output float64
	}{
		0: {
			x:      2.0,
			n:      10,
			output: 1024.0,
		},
		1: {
			x:      2.1,
			n:      3,
			output: 9.26100,
		},
		2: {
			x:      2.0,
			n:      -2,
			output: 0.25,
		},
	}
	for i, scenario := range scenarios {
		output := myPow(scenario.x, scenario.n)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
