package problem934

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestShortestbridge(t *testing.T) {
	scenarios := [...]struct {
		grid   [][]int
		output int
	}{
		0: {
			grid: [][]int{
				{0, 1},
				{1, 0},
			},
			output: 1,
		},
		1: {
			grid: [][]int{
				{0, 1, 0},
				{0, 0, 0},
				{0, 0, 1},
			},
			output: 2,
		},
		2: {
			grid: [][]int{
				{1, 1, 1, 1, 1},
				{1, 0, 0, 0, 1},
				{1, 0, 1, 0, 1},
				{1, 0, 0, 0, 1},
				{1, 1, 1, 1, 1},
			},
			output: 1,
		},
	}
	for i, scenario := range scenarios {
		output := shortestBridge(scenario.grid)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
