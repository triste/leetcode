package problem219

func containsNearbyDuplicate(nums []int, k int) bool {
	if k == 0 {
		return false
	}
	m := make(map[int]bool, k)
	for i, num := range nums {
		if m[num] {
			return true
		}
		if len(m) >= k {
			delete(m, nums[i-k])
		}
		m[num] = true
	}
	return false
}
