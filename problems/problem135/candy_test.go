package problem135

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestCandy(t *testing.T) {
	scenarios := [...]struct {
		ratings []int
		output  int
	}{
		0: {
			ratings: []int{1, 0, 2},
			output:  5,
		},
		1: {
			ratings: []int{1, 2, 2},
			output:  4,
		},
		2: {
			ratings: []int{1, 2, 3, 2, 1},
			output:  9,
		},
		3: {
			ratings: []int{1, 3, 2, 2, 1},
			output:  7,
		},
	}
	for i, scenario := range scenarios {
		output := candy(scenario.ratings)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
