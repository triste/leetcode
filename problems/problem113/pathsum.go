package problem113

import (

	. "gitlab.com/triste/leetcode/pkg/tree"
)

func pathSum(root *TreeNode, targetSum int) (output [][]int) {
	var path func(*TreeNode, int, []int)
	path = func(root *TreeNode, targetSum int, prevNums []int) {
		if root == nil {
			return
		}
		newTargetSum := targetSum - root.Val
		if newTargetSum == 0 && root.Left == nil && root.Right == nil {
			path := make([]int, len(prevNums))
			copy(path, prevNums)
			path = append(path, root.Val)
			output = append(output, path)
		}
		prevNums = append(prevNums, root.Val)
		path(root.Left, newTargetSum, prevNums)
		path(root.Right, newTargetSum, prevNums)
	}
	path(root, targetSum, []int{})
	return
}
