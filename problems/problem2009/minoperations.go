package problem2009

import (
	"log"
	"sort"
)

func minOperations(nums []int) int {
	sort.Ints(nums)
	prev := nums[0] - 1
	curlen := 0
	maxlen := 0
	log.Println(nums, prev)
	for _, num := range nums {
		if num == prev+1 {
			curlen++
			if curlen > maxlen {
				maxlen = curlen
			}
		} else {
			curlen = 1
		}
		prev = num
	}
	log.Println(maxlen)
	return len(nums) - maxlen
}

/*
n = 1:
	ans = 0
n = 2:
	diff = 1
	ans = 0 | 1
*/

/*
	1,2,3,5,6,8,10
	(1,2,3),(5,6),(8),(10)
*/
