package problem913

type Turn uint8

const (
	MouseTurn Turn = iota
	CatTurn
)

type Position uint8

type State struct {
	mousePos Position
	catPos   Position
	turn     Turn
}

func (s State) Hash() uint16 {
	return uint16(s.mousePos) << 7 & uint16(s.catPos) << 1 & uint16(s.turn)
}

func catMouseGame(graph [][]int) int {
	return 0
}
