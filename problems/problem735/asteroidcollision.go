package problem735

func asteroidCollision(asteroids []int) (output []int) {
	var rightMovingAsteroids []int
	for _, asteroid := range asteroids {
		if asteroid > 0 {
			rightMovingAsteroids = append(rightMovingAsteroids, asteroid)
			continue
		}
		for {
			if len(rightMovingAsteroids) == 0 {
				output = append(output, asteroid)
				break
			}
			diff := rightMovingAsteroids[len(rightMovingAsteroids)-1] + asteroid
			if diff > 0 {
				break
			}
			rightMovingAsteroids = rightMovingAsteroids[:len(rightMovingAsteroids)-1]
			if diff == 0 {
				break
			}
		}
	}
	output = append(output, rightMovingAsteroids...)
	return
}
