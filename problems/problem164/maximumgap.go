package problem164

func maximumGap(nums []int) int {
	var bucketsPrev [10][]int
	maxNum := 0
	for _, num := range nums {
		if num > maxNum {
			maxNum = num
		}
		i := num % 10
		bucketsPrev[i] = append(bucketsPrev[i], num)
	}
	for i := 100; maxNum != 0; i = i * 10 {
		maxNum /= 10
		var buckets [10][]int
		for _, bucket := range bucketsPrev {
			for _, num := range bucket {
				j := (num % i) * 10 / i
				buckets[j] = append(buckets[j], num)
			}
		}
		buckets, bucketsPrev = bucketsPrev, buckets
	}
	prev := bucketsPrev[0][0]
	maxGap := 0
	for _, num := range bucketsPrev[0] {
		if gap := num - prev; gap > maxGap {
			maxGap = gap
		}
		prev = num
	}
	return maxGap
}

/*
	1_000_000_000

	1, 13, 1001, 103

	bucket[0] = {}, {1, 1001}, {}, {13, 103}
	bukcet[1] = {01,1001,103}, {13}
	bucket[2] = {001, 1001,13},{103}
	bucket[3] = {0001,13,103}, {1001}
*/
