package problem345

func isVower(b byte) bool {
	switch b {
	case 'a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U':
		return true
	}
	return false
}

func reverseVowels(s string) string {
	bytes := make([]byte, len(s))
	for l, r := 0, len(s)-1; l <= r; l, r = l+1, r-1 {
		for ; l < r && !isVower(s[l]); l++ {
			bytes[l] = s[l]
		}
		for ; l < r && !isVower(s[r]); r-- {
			bytes[r] = s[r]
		}
		bytes[l] = s[r]
		bytes[r] = s[l]
	}
	return string(bytes)
}
