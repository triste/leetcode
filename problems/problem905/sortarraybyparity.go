package problem905

func sortArrayByParity(nums []int) []int {
	lastEvenIdx := 0
	for i, num := range nums {
		if num&1 == 0 {
			nums[i] = nums[lastEvenIdx]
			nums[lastEvenIdx] = num
			lastEvenIdx++
		}
	}
	return nums
}
