package problem2562

func findTheArrayConcVal(nums []int) int64 {
	sums := make(chan int)
	go func() {
		sums <- concat(nums, 1)
	}()
	sum := concat(nums[1:], 2)
	sum += <-sums
	return int64(sum)
}

func concat(nums []int, step int) int {
	sum := 0
	var l, r int
	for l, r = 0, len(nums)-step; l < r; l, r = l+2, r-2 {
		first := nums[l]
		for last := nums[r]; last != 0; last /= 10 {
			first *= 10
		}
		sum += first + nums[r]
	}
	if l == r {
		sum += nums[l]
	}
	return sum
}
