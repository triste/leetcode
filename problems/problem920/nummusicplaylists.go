package problem920

func numMusicPlaylists(n int, goal int, k int) int {
	dp := make([]int, goal+1)
	dpPrev := make([]int, goal+1)
	dpPrev[0] = 1
	for r := 1; r <= n; r++ {
		for c := 1; c <= goal; c++ {
			dp[c] = dpPrev[c-1] * (n - r + 1) % 1_000_000_007
			if r > k {
				dp[c] += dp[c-1] * (r - k)
				dp[c] %= 1_000_000_007
			}
		}
		dpPrev[0] = 0
		dp, dpPrev = dpPrev, dp
	}
	return dpPrev[goal]
}

/*
	n = 1, goal = 3, k = 0
	1,1,1
*/

/*
	case k = 0, n = 1, goal = 1
		return 1
	case k = 0, n = 1, goal = 2
		return {1,1}
	case k = 0, n = 2, goal =
		return {
*/

/*
	case n = 2, goal = 2, k = 0
		(1,2), (2,1)
	case n = 2, goal = 3, k = 0
		{(1,2),1,2}
		{(2,1),1,2}
	case n = 2, goal = 4, k = 0
		{(1,1,2),1,2}
		{(1,2,1),1,2}
		{(1,2,2),1,2}
		{(2,1,1),1,2}
		{(2,1,2),1,2}
		{(2,2,1),1,2}

		{(1,2),(1,2)}
		{(1,2),(2,1)}
		{(2,1),(1,2)}
		{(2,1),(2,1)}
		1,1,1,2
		1,1,2,1 +
		1,1,2,2 +
		1,2,1,1 +
		1,2,1,2 +
		1,2,2,1 +
		1,2,2,2 +
		2,1,1,1 +
		2,1,1,2 +
		2,1,2,1 +
		2,1,2,2 +
		2,2,1,1 +
		2,2,1,2 +
		2,2,2,1

*/

/*
	case n = 3, goal = 3, k = 0
		1,2,3
		1,3,2
		2,1,3
		2,3,1
		3,1,2
		3,2,1
	case n = 3, goal = 4, k = 0
		case(3,3,0),1
		case(3,3,0),2
		case(3,3,0),3
*/
