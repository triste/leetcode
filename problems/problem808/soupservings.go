package problem808

func soupServings(n int) float64 {
	memo := make(map[uint64]float64)
	var solve func(a, b int) float64
	solve = func(a, b int) float64 {
		key := uint64(a<<32 | b)
		if val, ok := memo[key]; ok {
			return val
		}
		if a <= 0 && b <= 0 {
			return 0.5
		}
		if a <= 0 {
			return 1
		}
		if b <= 0 {
			return 0
		}
		prob := 0.25 * (solve(a-4, b) + solve(a-3, b-1) + solve(a-2, b-2) + solve(a-1, b-3))
		memo[key] = prob
		return prob
	}
	m := n / 25
	if n%25 > 0 {
		m++
	}
	for i := 1; i < m; i++ {
		if solve(i, i) > 1-1e-5 {
			return 1
		}
	}
	return solve(m, m)
}

/*
	Ops := {
		1: f(100, 0),
		2: f(75, 25),
		3: f(50, 50),
		4: f(25, 75)
	}

	n = 100
	A = 100, B = 100
	op1 => A = 0, B = 100
	op2 => A = 25, B = 75
	op3 => A = 50, B = 50
	op4 => A = 75, B = 25
	prob = 0.25

	A = 25, B = 75
	op1 => A = 0, B = 75
	op2 => A = 0, B = 50
	op3 => A = 0, B = 25
	op4 => A = 0, B = 0
	prob = 0.25 * (1+1+1+0.5) = 0.875

	A = 50, B = 50
	op1 => A = 0, B = 50
	op2 => A = 0, B = 25
	op3 => A = 0, B = 0
	op4 => A = 25, B = 0
	prob = 0.25 * (1+1+0.5) = 0.625

	A = 75, B = 25
	op1 => A = 0, B = 25
	op2 => A = 0, B = 0
	op3 => A = 25, B = 0
	op4 => A = 50, B = 0
	prob = 0.25 * (1+0.5) = 0.375
*/
