package problem712

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestMinimumdeletesum(t *testing.T) {
	scenarios := [...]struct {
		s1     string
		s2     string
		output int
	}{
		0: {
			s1:     "sea",
			s2:     "eat",
			output: 231,
		},
		1: {
			s1:     "delete",
			s2:     "leet",
			output: 403,
		},
		2: {
			s1:     "ccaccjp",
			s2:     "fwosarcwge",
			output: 1399,
		},
	}
	for i, scenario := range scenarios {
		output := minimumDeleteSum(scenario.s1, scenario.s2)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
