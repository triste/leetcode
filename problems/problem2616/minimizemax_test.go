package problem2616

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestMinimizemax(t *testing.T) {
	scenarios := [...]struct {
		nums   []int
		p      int
		output int
	}{
		0: {
			nums:   []int{10, 1, 2, 7, 1, 3},
			p:      2,
			output: 1,
		},
		1: {
			nums:   []int{4, 2, 1, 2},
			p:      1,
			output: 0,
		},
		2: {
			nums:   []int{120, 12, 2843},
			p:      0,
			output: 0,
		},
	}
	for i, scenario := range scenarios {
		output := minimizeMax(scenario.nums, scenario.p)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
