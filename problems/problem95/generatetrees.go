package problem95

import (
	. "gitlab.com/triste/leetcode/pkg/tree"
)

func toBST(root *TreeNode) *TreeNode {
	var dfs func(*TreeNode, int) (*TreeNode, int)
	dfs = func(root *TreeNode, offset int) (*TreeNode, int) {
		if root == nil {
			return nil, 0
		}
		left, leftCount := dfs(root.Left, offset)
		val := leftCount + 1 + offset
		right, rightCount := dfs(root.Right, val)
		return &TreeNode{
			Val:   val,
			Left:  left,
			Right: right,
		}, leftCount + rightCount + 1
	}
	bst, _ := dfs(root, 0)
	return bst
}

func generateTrees(n int) []*TreeNode {
	dp := make([][]*TreeNode, n+1)
	dp[0] = append(dp[0], nil)
	for i := 1; i < len(dp); i++ {
		for j := 0; j < i; j++ {
			for _, left := range dp[j] {
				for _, right := range dp[i-1-j] {
					root := &TreeNode{
						Val:   i,
						Left:  left,
						Right: right,
					}
					dp[i] = append(dp[i], root)
				}
			}
		}
	}
	for i, tree := range dp[n] {
		dp[n][i] = toBST(tree)
	}
	return dp[n]
}

/*
	case n = 1:
		1
	case n = 2:
		  2
	   /     \
	case(1)  case(1)

	case n = 3:
		3        3                  3
	   /          \              /     \
	case(2)        case(2)    case(1)  case(1)

	case n = 4:
		4    4               4                   4
	  /        \          /     \             /     \
	case(3)    case(3)  case(2)  case(1)    case(1) case(2)
*/
