package problem1458

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func maxDotProduct(nums1 []int, nums2 []int) int {
	if len(nums2) > len(nums1) {
		nums1, nums2 = nums2, nums1
	}
	n := len(nums2)
	dpPrev := make([]int, n)
	dpPrev[0] = nums1[0] * nums2[0]
	for i := 1; i < n; i++ {
		dpPrev[i] = max(dpPrev[i-1], nums1[0]*nums2[i])
	}
	dp := make([]int, n)
	for _, num := range nums1[1:] {
		dp[0] = max(dpPrev[0], num*nums2[0])
		for j := 1; j < n; j++ {
			dp[j] = max(dp[j-1], dpPrev[j])
			dp[j] = max(dp[j], num*nums2[j]+dpPrev[j-1])
			dp[j] = max(dp[j], num*nums2[j])
		}
		dp, dpPrev = dpPrev, dp
	}
	return dpPrev[n-1]
}

/*

   | 2 | 1 | -2 | 5 |
3  |   |   |    |15 |
0  |   |   |    | 0 |
-6 | 12|12 | 12 |-30|

   | 3 | 3 | 5 | 5 |
-5 |   |   |   |-5 |
-1 |   |   |   |-5 |
-2 |-6 |-6 |-10|-10|

*/
