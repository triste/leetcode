package problem79

func exist(board [][]byte, word string) (output bool) {
	var dfs func(i, j, k int) bool
	dfs = func(i, j, k int) bool {
		if k >= len(word) {
			return true
		}
		if i < 0 || i >= len(board) || j < 0 || j >= len(board[i]) || board[i][j] != word[k] {
			return false
		}
		board[i][j] = 0
		for _, dir := range [...][2]int{{-1, 0}, {0, 1}, {1, 0}, {0, -1}} {
			if dfs(i+dir[0], j+dir[1], k+1) {
				return true
			}
		}
		board[i][j] = word[k]
		return false
	}
	for i := range board {
		for j := range board[i] {
			if dfs(i, j, 0) {
				return true
			}
		}
	}
	return false
}
