package problem37

type Point struct {
	x int
	y int
}

const (
	DigitCount = 9
)

func solveSudoku(board [][]byte) {
	boardLenght := len(board)
	hmap := make([][DigitCount]bool, boardLenght)
	vmap := make([][DigitCount]bool, boardLenght)
	cmap := make([][DigitCount]bool, boardLenght)
	empties := make([]Point, 0)
	for i := range board {
		for j, ch := range board[i] {
			if ch == '.' {
				empties = append(empties, Point{i, j})
				continue
			}
			digit := ch - '1'
			hmap[j][digit] = true
			vmap[i][digit] = true
			cmap[(i/3)*3+j/3][digit] = true
		}
	}
	var solve func(i int) bool
	solve = func(e int) bool {
		if e == len(empties) {
			return true
		}
		empty := empties[e]
		for i := 0; i < DigitCount; i++ {
			if hmap[empty.y][i] || vmap[empty.x][i] || cmap[(empty.x/3)*3+empty.y/3][i] {
				continue
			}
			board[empty.x][empty.y] = byte(i + '1')
			hmap[empty.y][i] = true
			vmap[empty.x][i] = true
			cmap[(empty.x/3)*3+empty.y/3][i] = true
			if ok := solve(e + 1); ok {
				return ok
			}
			hmap[empty.y][i] = false
			vmap[empty.x][i] = false
			cmap[(empty.x/3)*3+empty.y/3][i] = false
		}
		return false
	}
	solve(0)
	return
}
