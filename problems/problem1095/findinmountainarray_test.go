package problem1095

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestFindinmountainarray(t *testing.T) {
	scenarios := [...]struct {
		target      int
		mountainArr *MountainArray
		output      int
	}{
		0: {
			target:      3,
			mountainArr: &MountainArray{1, 2, 3, 4, 5, 3, 1},
			output:      2,
		},
		1: {
			target:      3,
			mountainArr: &MountainArray{0, 1, 2, 4, 2, 1},
			output:      -1,
		},
		2: {
			target:      3,
			mountainArr: &MountainArray{0, 1, 2, 4, 3, 1},
			output:      4,
		},
		3: {
			target:      2,
			mountainArr: &MountainArray{1, 2, 3, 4, 5, 3, 1},
			output:      1,
		},
	}
	for i, scenario := range scenarios {
		output := findInMountainArray(scenario.target, scenario.mountainArr)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
