package problem2328

func countPaths(grid [][]int) int {
	dp := make([][]int, len(grid))
	for i := range dp {
		dp[i] = make([]int, len(grid[i]))
	}
	var dfs func(i, j int) int
	dfs = func(i, j int) int {
		pathCount := dp[i][j]
		if pathCount > 0 {
			return pathCount
		}
		if i > 0 && grid[i-1][j] > grid[i][j] {
			pathCount = dfs(i-1, j)
		}
		if i+1 < len(grid) && grid[i+1][j] > grid[i][j] {
			pathCount += dfs(i+1, j)
		}
		if j > 0 && grid[i][j-1] > grid[i][j] {
			pathCount += dfs(i, j-1)
		}
		if j+1 < len(grid[i]) && grid[i][j+1] > grid[i][j] {
			pathCount += dfs(i, j+1)
		}
		dp[i][j] = (1 + pathCount) % 1_000_000_007
		return dp[i][j]
	}
	pathCount := 0
	for i := range dp {
		for j := range dp[i] {
			pathCount = (pathCount + dfs(i, j)) % 1_000_000_007
		}
	}
	return pathCount
}
