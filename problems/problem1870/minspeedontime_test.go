package problem1870

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestMinspeedontime(t *testing.T) {
	scenarios := [...]struct {
		dist   []int
		hour   float64
		output int
	}{
		0: {
			dist:   []int{1, 3, 2},
			hour:   6,
			output: 1,
		},
		1: {
			dist:   []int{1, 3, 2},
			hour:   2.7,
			output: 3,
		},
		2: {
			dist:   []int{1, 3, 2},
			hour:   1.9,
			output: -1,
		},
		3: {
			dist:   []int{1, 1, 100000},
			hour:   2.01,
			output: 10_000_000,
		},
		4: {
			dist:   []int{5, 3, 4, 6, 2, 2, 7},
			hour:   10.92,
			output: 4,
		},
		5: {
			dist:   []int{2, 1, 5, 4, 4, 3, 2, 9, 2, 10},
			hour:   75.12,
			output: 1,
		},
		6: {
			dist:   []int{1, 1},
			hour:   1.0,
			output: -1,
		},
	}
	for i, scenario := range scenarios {
		output := minSpeedOnTime(scenario.dist, scenario.hour)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
