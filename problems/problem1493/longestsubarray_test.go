package problem1493

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestLongestsubarray(t *testing.T) {
	scenarios := [...]struct {
		nums   []int
		output int
	}{
		0: {
			nums:   []int{1, 1, 0, 1},
			output: 3,
		},
		1: {
			nums:   []int{0, 1, 1, 1, 0, 1, 1, 0, 1},
			output: 5,
		},
		2: {
			nums:   []int{1, 1, 1},
			output: 2,
		},
	}
	for i, scenario := range scenarios {
		output := longestSubarray(scenario.nums)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
