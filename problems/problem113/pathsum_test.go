package problem113

import (
	"testing"

	"github.com/google/go-cmp/cmp"
	. "gitlab.com/triste/leetcode/pkg/tree"
)

func TestPathsum(t *testing.T) {
	scenarios := [...]struct {
		root      *TreeNode
		targetSum int
		output    [][]int
	}{
		0: {
			NewTree(5,4,8,11,nil,13,4,7,2,nil,nil,5,1),
			22,
			[][]int{{5,4,11,2},{5,8,4,5}},
		},
		1: {
			NewTree(1,2,3),
			5,
			nil,
		},
		2: {
			NewTree(1,2),
			0,
			nil,
		},
		3: {
			nil,
			0,
			nil,
		},
		4: {
			NewTree(1),
			1,
			[][]int{{1}},
		},
	}
	for i, scenario := range scenarios {
		output := pathSum(scenario.root, scenario.targetSum)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
