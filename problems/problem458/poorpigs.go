package problem458

func poorPigs(buckets int, minutesToDie int, minutesToTest int) int {
	iterationCount := minutesToDie / minutesToDie
	return 0
}

/*
buckets:
1 2 3 4


case 1:
	x1
	_
	output = 0
case 2:
	x1 x2
	p1 _
	output = 1
case 3:
	x1 x3 x3
	p1 p2 _
	output = 2
case 4:
	x1 x2    x3 x4
	p1 p1,p2 p2 _
	output = 2
case 5:
	x1 x2    x3    x4 x5
	p1 p1,p2 p2,p3 p3 _
	output = 3
case 6:
	x1 x2    x3    x4    x5 x6
	p1 p1,p2 p2,p3 p3,p4 p4 _
	output = 4
*/
