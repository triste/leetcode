package problem1312

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestMininsertions(t *testing.T) {
	scenarios := [...]struct {
		s      string
		output int
	}{
		0: {
			s:      "zzazz",
			output: 0,
		},
		1: {
			s:      "mbadm",
			output: 2,
		},
		2: {
			s:      "leetcode",
			output: 5,
		},
	}
	for i, scenario := range scenarios {
		output := minInsertions(scenario.s)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
