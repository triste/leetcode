package problem168

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestConverttotitle(t *testing.T) {
	scenarios := [...]struct {
		columnNumber int
		output       string
	}{
		0: {
			columnNumber: 1,
			output:       "A",
		},
		1: {
			columnNumber: 28,
			output:       "AB",
		},
		2: {
			columnNumber: 701,
			output:       "ZY",
		},
	}
	for i, scenario := range scenarios {
		output := convertToTitle(scenario.columnNumber)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
