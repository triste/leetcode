package problem547

/*
func findCircleNum(isConnected [][]int) int {
	lenght := len(isConnected)
	visited := make([]bool, lenght)
	var visit func(city int)
	visit = func(city int) {
		visited[city] = true
		for i := city + 1; i < lenght; i++ {
			if isConnected[city][i] == 1 && visited[i] == false {
				visit(i)
			}
		}
		for i := city - 1; i > 0; i-- {
			if isConnected[i][city] == 1 && visited[i] == false {
				visit(i)
			}
		}
	}
	count := 0
	for i, v := range visited {
		if v == true {
			continue
		}
		visit(i)
		count++
	}
	return count
}
*/

func findCircleNum(isConnected [][]int) int {
	lenght := len(isConnected)
	union := make([]int, lenght)
	for i := range union {
		union[i] = i
	}
	for i := 0; i < lenght-1; i++ {
		for j := i + 1; j < lenght; j++ {
			if isConnected[i][j] == 1 {
				left := find(union, i)
				down := find(union, j)
				union[down] = left
			}
		}
	}
	count := 0
	for i, u := range union {
		if u == i {
			count++
		}
	}
	return count
}

func find(union []int, city int) int {
	for {
		parent := union[city]
		if parent == city {
			return parent
		}
		city = parent
	}
}
