package problem30

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestFindsubstring(t *testing.T) {
	scenarios := [...]struct {
		s      string
		words  []string
		output []int
	}{
		0: {
			s:      "barfoothefoobarman",
			words:  []string{"foo", "bar"},
			output: []int{0, 9},
		},
		1: {
			s:      "wordgoodgoodgoodbestword",
			words:  []string{"word", "good", "best", "word"},
			output: []int{},
		},
		2: {
			s:      "barfoofoobarthefoobarman",
			words:  []string{"bar", "foo", "the"},
			output: []int{6, 9, 12},
		},
	}
	for i, scenario := range scenarios {
		output := findSubstring(scenario.s, scenario.words)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
