package problem1498

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestNumsubseq(t *testing.T) {
	scenarios := [...]struct {
		nums   []int
		target int
		output int
	}{
		0: {
			nums:   []int{3, 5, 6, 7},
			target: 9,
			output: 4,
		},
		1: {
			nums:   []int{3, 3, 6, 8},
			target: 10,
			output: 6,
		},
		2: {
			nums:   []int{2, 3, 3, 4, 6, 7},
			target: 12,
			output: 61,
		},
		3: {
			nums:   []int{14, 4, 6, 6, 20, 8, 5, 6, 8, 12, 6, 10, 14, 9, 17, 16, 9, 7, 14, 11, 14, 15, 13, 11, 10, 18, 13, 17, 17, 14, 17, 7, 9, 5, 10, 13, 8, 5, 18, 20, 7, 5, 5, 15, 19, 14},
			target: 22,
			output: 272187084,
		},
		4: {
			nums:   []int{27, 21, 14, 2, 15, 1, 19, 8, 12, 24, 21, 8, 12, 10, 11, 30, 15, 18, 28, 14, 26, 9, 2, 24, 23, 11, 7, 12, 9, 17, 30, 9, 28, 2, 14, 22, 19, 19, 27, 6, 15, 12, 29, 2, 30, 11, 20, 30, 21, 20, 2, 22, 6, 14, 13, 19, 21, 10, 18, 30, 2, 20, 28, 22},
			target: 31,
			output: 688052206,
		},
	}
	for i, scenario := range scenarios {
		output := numSubseq(scenario.nums, scenario.target)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
