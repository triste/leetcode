package problem11

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestMaxarea(t *testing.T) {
	scenarios := [...]struct {
		height []int
		output int
	}{
		0: {
			[]int{1,8,6,2,5,4,8,3,7},
			49,
		},
		1: {
			[]int{1,1},
			1,
		},
	}
	for i, scenario := range scenarios {
		output := maxArea(scenario.height)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
