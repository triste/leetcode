package problem2462

import (
	"sort"
)

type PriorityQueue struct {
	arr []int
}

func (pq *PriorityQueue) Pop() int {
	min := pq.arr[0]
	pq.arr[0] = pq.arr[len(pq.arr)-1]
	pq.arr = pq.arr[:len(pq.arr)-1]
	pq.siftDown(0)
	return min
}

func (pq *PriorityQueue) Min() int {
	return pq.arr[0]
}

func (pq *PriorityQueue) siftDown(i int) {
	for {
		j, l, r := i, i*2+1, i*2+2
		if l < len(pq.arr) && pq.arr[l] < pq.arr[j] {
			j = l
		}
		if r < len(pq.arr) && pq.arr[r] < pq.arr[j] {
			j = r
		}
		if j == i {
			break
		}
		pq.arr[i], pq.arr[j] = pq.arr[j], pq.arr[i]
		i = j
	}
}

func (pq *PriorityQueue) Replace(num int) int {
	min := pq.arr[0]
	pq.arr[0] = num
	pq.siftDown(0)
	return min
}

func (pq *PriorityQueue) Len() int {
	return len(pq.arr)
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func totalCost(costs []int, k int, candidates int) (totalCost int64) {
	head := costs[:candidates]
	tail := costs[max(candidates, len(costs)-candidates):]
	sort.Ints(head)
	sort.Ints(tail)
	leftQueue := PriorityQueue{head}
	righQueue := PriorityQueue{tail}
	l, r := candidates, len(costs)-candidates-1
	for i := 0; i < k; i++ {
		if righQueue.Len() == 0 || leftQueue.Len() > 0 && leftQueue.Min() <= righQueue.Min() {
			if l <= r {
				totalCost += int64(leftQueue.Replace(costs[l]))
				l++
			} else {
				totalCost += int64(leftQueue.Pop())
			}
		} else {
			if l <= r {
				totalCost += int64(righQueue.Replace(costs[r]))
				r--
			} else {
				totalCost += int64(righQueue.Pop())
			}
		}
	}
	return totalCost
}
