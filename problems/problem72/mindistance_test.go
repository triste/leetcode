package problem72

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestMindistance(t *testing.T) {
	scenarios := [...]struct {
		word1  string
		word2  string
		output int
	}{
		0: {
			word1:  "horse",
			word2:  "ros",
			output: 3,
		},
		1: {
			word1:  "intention",
			word2:  "execution",
			output: 5,
		},
	}
	for i, scenario := range scenarios {
		output := minDistance(scenario.word1, scenario.word2)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
