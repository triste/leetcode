package problem107

import (
	"testing"

	"github.com/google/go-cmp/cmp"
	. "gitlab.com/triste/leetcode/pkg/tree"
)

func TestLevelorderbottom(t *testing.T) {
	scenarios := [...]struct {
		root   *TreeNode
		output [][]int
	}{
		0: {
			root: NewTree(3, 9, 20, nil, nil, 15, 7),
			output: [][]int{
				{15, 7}, {9, 20}, {3},
			},
		},
		1: {
			root:   NewTree(1),
			output: [][]int{{1}},
		},
		2: {
			root:   nil,
			output: nil,
		},
	}
	for i, scenario := range scenarios {
		output := levelOrderBottom(scenario.root)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
