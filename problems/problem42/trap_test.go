package problem42

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestTrap(t *testing.T) {
	scenarios := [...]struct {
		height []int
		output int
	}{
		0: {
			height: []int{0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1},
			output: 6,
		},
		1: {
			height: []int{4, 2, 0, 3, 2, 5},
			output: 9,
		},
		2: {
			height: []int{2, 0, 2},
			output: 2,
		},
	}
	for i, scenario := range scenarios {
		output := trap(scenario.height)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
