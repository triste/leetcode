package problem1318

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestMinflips(t *testing.T) {
	scenarios := [...]struct {
		a      int
		b      int
		c      int
		output int
	}{
		0: {
			a:      2,
			b:      6,
			c:      5,
			output: 3,
		},
		1: {
			a:      4,
			b:      2,
			c:      7,
			output: 1,
		},
		2: {
			a:      1,
			b:      2,
			c:      3,
			output: 0,
		},
		3: {
			a:      7,
			b:      7,
			c:      7,
			output: 0,
		},
		4: {
			a:      8,
			b:      3,
			c:      5,
			output: 3,
		},
	}
	for i, scenario := range scenarios {
		output := minFlips(scenario.a, scenario.b, scenario.c)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
