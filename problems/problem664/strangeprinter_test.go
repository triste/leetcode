package problem664

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestStrangeprinter(t *testing.T) {
	scenarios := [...]struct {
		s      string
		output int
	}{
		0: {
			s:      "aaabbb",
			output: 2,
		},
		1: {
			s:      "aba",
			output: 2,
		},
		2: {
			s:      "abcabc",
			output: 5,
		},
		3: {
			s:      "tbgtgb",
			output: 4,
		},
		4: {
			s:      "baacdddaaddaaaaccbddbcabdaabdbbcdcbbbacbddcabcaaa",
			output: 19,
		},
	}
	for i, scenario := range scenarios {
		output := strangePrinter(scenario.s)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
