package problem16

import (
	"math"
	"sort"
)

func threeSumClosest(nums []int, target int) (output int) {
	sort.Slice(nums, func(i, j int) bool {
		return nums[i] < nums[j]
	})
	minDiff := math.MaxInt
	for i, first := range nums {
		left := i + 1
		right := len(nums) - 1
		for left < right {
			sum := first + nums[left] + nums[right]
			if sum < target {
				left++
			} else {
				right--
			}
			minDiffAbs := minDiff
			if minDiffAbs < 0 {
				minDiffAbs = -minDiffAbs
			}
			diff := target - sum
			diffAbs := diff
			if diff < 0 {
				diffAbs = -diffAbs
			}
			if diffAbs < minDiffAbs {
				if minDiff = diff; minDiff == 0 {
					return target
				}
			}
		}
	}
	return target - minDiff
}
