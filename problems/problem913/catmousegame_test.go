package problem913

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestCatmousegame(t *testing.T) {
	scenarios := [...]struct {
		graph  [][]int
		output int
	}{
		0: {
			graph:  [][]int{{2, 5}, {3}, {0, 4, 5}, {1, 4, 5}, {2, 3}, {0, 2, 3}},
			output: 0,
		},
		1: {
			graph:  [][]int{{1, 3}, {0}, {3}, {0, 2}},
			output: 1,
		},
	}
	for i, scenario := range scenarios {
		output := catMouseGame(scenario.graph)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
