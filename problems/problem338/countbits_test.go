package problem338

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestCountbits(t *testing.T) {
	scenarios := [...]struct {
		n      int
		output []int
	}{
		0: {
			n:      2,
			output: []int{0, 1, 1},
		},
		1: {
			n:      5,
			output: []int{0, 1, 1, 2, 1, 2},
		},
		2: {
			n:      16,
			output: []int{0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4, 1},
		},
	}
	for i, scenario := range scenarios {
		output := countBits(scenario.n)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
