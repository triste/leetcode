package problem12

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestInttoroman(t *testing.T) {
	scenarios := [...]struct {
		num    int
		output string
	}{
		0: {3, "III"},
		1: {58, "LVIII"},
		2: {1994, "MCMXCIV"},
		3: {1, "I"},
		4: {1000, "M"},
	}
	for i, scenario := range scenarios {
		output := intToRoman(scenario.num)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
