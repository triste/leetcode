package problem1048

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestLongeststrchain(t *testing.T) {
	scenarios := [...]struct {
		words  []string
		output int
	}{
		0: {
			words:  []string{"a", "b", "ba", "bca", "bda", "bdca"},
			output: 4,
		},
		1: {
			words:  []string{"xbc", "pcxbcf", "xb", "cxbc", "pcxbc"},
			output: 5,
		},
		2: {
			words:  []string{"abcd", "dbqca"},
			output: 1,
		},
	}
	for i, scenario := range scenarios {
		output := longestStrChain(scenario.words)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
