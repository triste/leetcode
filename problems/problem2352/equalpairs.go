package problem2352

func equalPairs(grid [][]int) (pairCount int) {
	var rows [200][]int
	for r, row := range grid {
		var hash uint64 = 7
		for _, num := range row {
			hash = (31*hash + uint64(num)<<1) % 200
		}
		rows[hash] = append(rows[hash], r)
	}
	for c := range grid {
		var hash uint64 = 7
		for r := range grid {
			hash = (31*hash + uint64(grid[r][c])<<1) % 200
		}
		for _, r := range rows[hash] {
			pairCount++
			for r, num := range grid[r] {
				if num != grid[r][c] {
					pairCount--
					break
				}
			}
		}
	}
	return
}
