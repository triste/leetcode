package problem515

import (
	"math"

	. "gitlab.com/triste/leetcode/pkg/tree"
)

func largestValues(root *TreeNode) (output []int) {
	if root == nil {
		return
	}
	queue := []*TreeNode{root}
	var nextQueue []*TreeNode
	for len(queue) > 0 {
		maxNum := math.MinInt
		for _, node := range queue {
			if node.Val > maxNum {
				maxNum = node.Val
			}
			if node.Left != nil {
				nextQueue = append(nextQueue, node.Left)
			}
			if node.Right != nil {
				nextQueue = append(nextQueue, node.Right)
			}
		}
		output = append(output, maxNum)
		queue, nextQueue = nextQueue, queue[:0]
	}
	return
}
