package problem57

import (
	"sort"
)

func insert(intervals [][]int, newInterval []int) [][]int {
	i := sort.Search(len(intervals), func(i int) bool {
		return intervals[i][0] > newInterval[0]
	})
	if i > 0 && intervals[i-1][1] >= newInterval[0] {
		i--
		newInterval[0] = intervals[i][0]
	}
	j := sort.Search(len(intervals), func(i int) bool {
		return intervals[i][0] > newInterval[1]
	})
	if j > 0 && intervals[j-1][1] >= newInterval[1] {
		newInterval[1] = intervals[j-1][1]
	}
	if i == j {
		intervals = append(intervals, nil)
		copy(intervals[i+1:], intervals[i:])
	} else {
		copy(intervals[i+1:], intervals[j:])
		intervals = intervals[:len(intervals)-j+i+1]
	}
	intervals[i] = newInterval
	return intervals
}

/*
  |---| |---|
*/
