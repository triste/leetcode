package problem894

import (
	. "gitlab.com/triste/leetcode/pkg/tree"
)

func allPossibleFBT(n int) []*TreeNode {
	if n%2 == 0 {
		return nil
	}
	dp := make([][]*TreeNode, n+1)
	dp[1] = append(dp[1], &TreeNode{})
	for i := 3; i <= n; i += 2 {
		for j := 1; j < i-1; j += 2 {
			for _, left := range dp[j] {
				for _, right := range dp[i-1-j] {
					dp[i] = append(dp[i], &TreeNode{Left: left, Right: right})
				}
			}
		}
	}
	return dp[n]
}
