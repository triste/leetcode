package problem129

import (
	"testing"

	"github.com/google/go-cmp/cmp"
	. "gitlab.com/triste/leetcode/pkg/tree"
)

func TestSumnumbers(t *testing.T) {
	scenarios := [...]struct {
		root   *TreeNode
		output int
	}{
		0: {
			root:   NewTree(1, 2, 3),
			output: 25,
		},
		1: {
			root:   NewTree(4, 9, 0, 5, 1),
			output: 1026,
		},
	}
	for i, scenario := range scenarios {
		output := sumNumbers(scenario.root)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
