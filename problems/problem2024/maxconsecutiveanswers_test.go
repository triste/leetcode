package problem2024

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestMaxconsecutiveanswers(t *testing.T) {
	scenarios := [...]struct {
		answerKey string
		k         int
		output    int
	}{
		0: {
			answerKey: "TTFF",
			k:         2,
			output:    4,
		},
		1: {
			answerKey: "TFFT",
			k:         1,
			output:    3,
		},
		2: {
			answerKey: "TTFTTFTT",
			k:         1,
			output:    5,
		},
	}
	for i, scenario := range scenarios {
		output := maxConsecutiveAnswers(scenario.answerKey, scenario.k)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
