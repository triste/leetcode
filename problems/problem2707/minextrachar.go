package problem2707

type Node struct {
	children [26]*Node
	word     string
}

type Trie struct {
	root Node
}

func (t *Trie) Insert(word string) {
	p := &t.root
	for _, ch := range word {
		key := ch - 'a'
		if p.children[key] == nil {
			p.children[key] = &Node{}
		}
		p = p.children[key]
	}
	p.word = word
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func minExtraChar(s string, dictionary []string) int {
	var trie Trie
	for _, word := range dictionary {
		trie.Insert(word)
	}
	dp := make([]int, len(s)+1)
	for l := len(s) - 1; l >= 0; l-- {
		p := &trie.root
		dp[l] = dp[l+1] + 1
		for r := l; r < len(s) && p.children[s[r]-'a'] != nil; r++ {
			p = p.children[s[r]-'a']
			if p.word != "" {
				dp[l] = min(dp[l], dp[r+1])
			}
		}
	}
	return dp[0]
}
