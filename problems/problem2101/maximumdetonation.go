package problem2101

func maximumDetonation(bombs [][]int) int {
	var graph [100][]uint8
	for i, bomb1 := range bombs {
		for j := i + 1; j < len(bombs); j++ {
			bomb2 := bombs[j]
			a := bomb1[0] - bomb2[0]
			b := bomb1[1] - bomb2[1]
			distSquared := a*a + b*b
			if distSquared <= bomb1[2]*bomb1[2] {
				graph[i] = append(graph[i], uint8(j))
			}
			if distSquared <= bomb2[2]*bomb2[2] {
				graph[j] = append(graph[j], uint8(i))
			}
		}
	}
	max := 0
	for i := range bombs {
		var detonated [2]uint64
		var dfs func(i uint8) int
		dfs = func(i uint8) int {
			if (detonated[i>>6]>>(i&63))&1 > 0 {
				return 0
			}
			detonated[i>>6] |= 1 << (i & 63)
			detonationCount := 1
			for _, bombIdx := range graph[i] {
				detonationCount += dfs(bombIdx)
			}
			return detonationCount
		}
		detonationCount := dfs(uint8(i))
		if detonationCount > max {
			max = detonationCount
		}
	}
	return max
}
