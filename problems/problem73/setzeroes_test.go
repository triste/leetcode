package problem73

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestSetzeroes(t *testing.T) {
	scenarios := [...]struct {
		matrix [][]int
		output [][]int
	}{
		0: {
			matrix: [][]int{
				{1, 1, 1},
				{1, 0, 1},
				{1, 1, 1},
			},
			output: [][]int{
				{1, 0, 1},
				{0, 0, 0},
				{1, 0, 1},
			},
		},
		1: {
			matrix: [][]int{
				{0, 1, 2, 0},
				{3, 4, 5, 2},
				{1, 3, 1, 5},
			},
			output: [][]int{
				{0, 0, 0, 0},
				{0, 4, 5, 0},
				{0, 3, 1, 0},
			},
		},
	}
	for i, scenario := range scenarios {
		setZeroes(scenario.matrix)
		if diff := cmp.Diff(scenario.output, scenario.matrix); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
