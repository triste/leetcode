package problem2483

func bestClosingTime(customers string) (closingTime int) {
	penalty, minPenalty := 0, 0
	for i, customer := range customers {
		if customer == 'Y' {
			if penalty--; penalty < minPenalty {
				minPenalty = penalty
				closingTime = i + 1
			}
		} else {
			penalty++
		}
	}
	return
}
