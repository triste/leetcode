package problem127

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestLadderlength(t *testing.T) {
	scenarios := [...]struct {
		beginWord string
		endWord   string
		wordList  []string
		output    int
	}{
		0: {
			beginWord: "hit",
			endWord:   "cog",
			wordList:  []string{"hot", "dot", "dog", "lot", "log", "cog"},
			output:    5,
		},
		1: {
			beginWord: "hit",
			endWord:   "cog",
			wordList:  []string{"hot", "dot", "dog", "lot", "log"},
			output:    0,
		},
	}
	for i, scenario := range scenarios {
		output := ladderLength(scenario.beginWord, scenario.endWord, scenario.wordList)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
