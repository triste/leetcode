package problem649

func predictPartyVictory(senate string) (output string) {
	radCount, direCount := 0, 0
	for _, s := range senate {
		if s == 'R' {
			radCount++
		} else {
			direCount++
		}
	}
	banned := make([]bool, len(senate))
	for radBanCount, direBanCount := 0, 0; ; {
		for i, s := range senate {
			if banned[i] {
				continue
			}
			if s == 'R' {
				if radBanCount > 0 {
					banned[i] = true
					radBanCount--
				} else {
					direBanCount++
					if direCount--; direCount <= 0 {
						return "Radiant"
					}
				}
			} else if direBanCount > 0 {
				banned[i] = true
				direBanCount--
			} else {
				radBanCount++
				if radCount--; radCount <= 0 {
					return "Dire"
				}
			}
		}
	}
}
