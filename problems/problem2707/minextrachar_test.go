package problem2707

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestMinextrachar(t *testing.T) {
	scenarios := [...]struct {
		s          string
		dictionary []string
		output     int
	}{
		0: {
			s:          "leetscode",
			dictionary: []string{"leet", "code", "leetcode"},
			output:     1,
		},
		1: {
			s:          "sayhelloworld",
			dictionary: []string{"hello", "world"},
			output:     3,
		},
		2: {
			s: "octncmdbgnxapjoqlofuzypthlytkmchayflwky",
			dictionary: []string{
				"m", "its", "imaby", "pa", "ijmnvj", "k", "mhka", "n", "y", "nc", "wq", "p", "mjqqa", "ht", "dfoa", "yqa", "kk", "pixq", "ixsdln", "rh", "dwl", "dbgnxa", "kmpfz", "nhxjm", "wg", "wky", "oct", "og", "uhin", "zxb", "qz", "tpf", "hlrc", "j", "l", "tew", "xbn", "a", "uzypt", "uvln", "mchay", "onnbi", "hlytk", "pjoqlo", "dxsjr", "u", "uj",
			},
			output: 2,
		},
	}
	for i, scenario := range scenarios {
		output := minExtraChar(scenario.s, scenario.dictionary)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
