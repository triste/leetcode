package problem2272

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestLargestvariance(t *testing.T) {
	scenarios := [...]struct {
		s      string
		output int
	}{
		0: {
			s:      "aababbb",
			output: 3,
		},
		1: {
			s:      "abcde",
			output: 0,
		},
		2: {
			s:      "lripaa",
			output: 1,
		},
		3: {
			s:      "bbc",
			output: 1,
		},
	}
	for i, scenario := range scenarios {
		output := largestVariance(scenario.s)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
