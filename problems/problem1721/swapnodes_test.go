package problem1721

import (
	"testing"

	"github.com/google/go-cmp/cmp"
	. "gitlab.com/triste/leetcode/pkg/list"
)

func TestSwapnodes(t *testing.T) {
	scenarios := [...]struct {
		head   *ListNode
		k      int
		output *ListNode
	}{
		0: {
			head:   NewListFromSlice(1, 2, 3, 4, 5),
			k:      2,
			output: NewListFromSlice(1, 4, 3, 2, 5),
		},
		1: {
			head:   NewListFromSlice(7, 9, 6, 6, 7, 8, 3, 0, 9, 5),
			k:      5,
			output: NewListFromSlice(7, 9, 6, 6, 8, 7, 3, 0, 9, 5),
		},
	}
	for i, scenario := range scenarios {
		output := swapNodes(scenario.head, scenario.k)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
