package problem847

type Path struct {
	startIdx    int
	visitedMask uint16
}

func shortestPathLength(graph [][]int) int {
	n := len(graph)
	visited := make([][]bool, n)
	for i := range visited {
		visited[i] = make([]bool, 1<<n)
	}
	queue := make([]Path, n)
	for i := range queue {
		queue[i] = Path{i, 1 << i}
	}
	tmpQueue := make([]Path, 0, n)
	for depth := 0; ; depth++ {
		for _, path := range queue {
			if path.visitedMask == (1<<n)-1 {
				return depth
			}
			for _, i := range graph[path.startIdx] {
				mask := path.visitedMask | (1 << i)
				if !visited[i][mask] {
					tmpQueue = append(tmpQueue, Path{i, mask})
					visited[i][mask] = true
				}
			}
		}
		queue, tmpQueue = tmpQueue, queue[:0]
	}
}
