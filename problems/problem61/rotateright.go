package problem61

import (
	. "gitlab.com/triste/leetcode/pkg/list"
)

func rotateRight(head *ListNode, k int) *ListNode {
	if head == nil {
		return nil
	}
	if k == 0 {
		return head
	}
	nodeCount := 1
	var end *ListNode
	for end = head; end.Next != nil; end = end.Next {
		nodeCount++
	}
	k %= nodeCount
	if k == 0 {
		return head
	}
	faster := head
	for i := 0; i < k; i++ {
		faster = faster.Next
	}
	slower := head
	for faster.Next != nil {
		faster = faster.Next
		slower = slower.Next
	}
	tmp := slower.Next
	slower.Next = nil
	end.Next = head
	return tmp
}
