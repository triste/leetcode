package problem84

type Bar struct {
	height int
	width  int
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func largestRectangleArea(heights []int) (largestArea int) {
	bars := make([]Bar, 1, len(heights))
	bars[0] = Bar{heights[0], 1}
	for _, height := range heights[1:] {
		lastBarIdx := len(bars) - 1
		if bars[lastBarIdx].height == height {
			bars[lastBarIdx].width++
		} else if bars[lastBarIdx].height < height {
			bars = append(bars, Bar{height, 1})
		} else {
			newWidth := 1
			widthAcc := 0
			for i := lastBarIdx; i >= 0; i-- {
				widthAcc += bars[i].width
				area := bars[i].height * widthAcc
				if area > largestArea {
					largestArea = area
				}
				if bars[i].height > height {
					newWidth += bars[i].width
					bars = bars[:i]
				}
			}
			bars = append(bars, Bar{height, newWidth})
		}
	}
	widthAcc := 0
	for i := len(bars) - 1; i >= 0; i-- {
		widthAcc += bars[i].width
		area := bars[i].height * widthAcc
		if area > largestArea {
			largestArea = area
		}
	}
	return
}

/*
	case (x1,x2) where x1 < x2:
		return max(x2, x1*2)
	case (x1,x2) where x2 < x1:

	2,1,5,6,2,3
	1,1,2,2,2,3
*/
