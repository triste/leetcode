package problem799

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestChampagnetower(t *testing.T) {
	scenarios := [...]struct {
		poured      int
		query_row   int
		query_glass int
		output      float64
	}{
		0: {
			poured:      1,
			query_row:   1,
			query_glass: 1,
			output:      0,
		},
		1: {
			poured:      2,
			query_row:   1,
			query_glass: 1,
			output:      0.5,
		},
		2: {
			poured:      100000009,
			query_row:   33,
			query_glass: 17,
			output:      1.0,
		},
		3: {
			poured:      25,
			query_row:   6,
			query_glass: 1,
			output:      0.18750,
		},
	}
	for i, scenario := range scenarios {
		output := champagneTower(scenario.poured, scenario.query_row, scenario.query_glass)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
