package problem42

func trap(heights []int) (waterCount int) {
	maxHeightLeft, maxHeightRight := 0, 0
	for l, r := 0, len(heights)-1; l < r; {
		if heights[l] > heights[r] {
			if heights[r] > maxHeightRight {
				maxHeightRight = heights[r]
			} else {
				waterCount += maxHeightRight - heights[r]
			}
			r--
		} else {
			if heights[l] > maxHeightLeft {
				maxHeightLeft = heights[l]
			} else {
				waterCount += maxHeightLeft - heights[l]
			}
			l++
		}
	}
	return
}
