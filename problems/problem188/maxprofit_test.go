package problem188

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestMaxprofit(t *testing.T) {
	scenarios := [...]struct {
		k      int
		prices []int
		output int
	}{
		0: {
			k:      2,
			prices: []int{2, 4, 1},
			output: 2,
		},
		1: {
			k:      2,
			prices: []int{3, 2, 6, 5, 0, 3},
			output: 7,
		},
	}
	for i, scenario := range scenarios {
		output := maxProfit(scenario.k, scenario.prices)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
