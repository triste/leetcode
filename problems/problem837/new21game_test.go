package problem837

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestNew21game(t *testing.T) {
	scenarios := [...]struct {
		n      int
		k      int
		maxPts int
		output float64
	}{
		0: {
			n:      10,
			k:      1,
			maxPts: 10,
			output: 0.9999999999999999,
		},
		1: {
			n:      6,
			k:      1,
			maxPts: 10,
			output: 0.6000,
		},
		2: {
			n:      21,
			k:      17,
			maxPts: 10,
			output: 0.7327777870686082,
		},
		3: {
			n:      0,
			k:      0,
			maxPts: 1,
			output: 1.0,
		},
		4: {
			n:      1,
			k:      0,
			maxPts: 1,
			output: 1.0,
		},
	}
	for i, scenario := range scenarios {
		output := new21Game(scenario.n, scenario.k, scenario.maxPts)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
