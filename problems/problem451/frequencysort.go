package problem451

import "sort"

func frequencySort(s string) (output string) {
	var countArr ['z' + 1]int
	for _, ch := range s {
		countArr[ch]++
	}
	var uniqueBytes []byte
	var asciiIndex byte = '0'
	for _, c := range countArr['0':] {
		if c != 0 {
			uniqueBytes = append(uniqueBytes, asciiIndex)
		}
		asciiIndex++
	}
	sort.Slice(uniqueBytes, func(i, j int) bool {
		return countArr[uniqueBytes[i]] > countArr[uniqueBytes[j]]
	})
	var bytes []byte
	for _, b := range uniqueBytes {
		for i := 0; i < countArr[b]; i++ {
			bytes = append(bytes, b)
		}
	}
	return string(bytes)
}
