package problem152

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestMaxproduct(t *testing.T) {
	scenarios := [...]struct {
		nums   []int
		output int
	}{
		0: {
			nums:   []int{2, 3, -2, 4},
			output: 6,
		},
		1: {
			nums:   []int{-2, 0, -1},
			output: 0,
		},
		2: {
			nums:   []int{-2},
			output: -2,
		},
		3: {
			nums:   []int{0, 2},
			output: 2,
		},
		4: {
			nums:   []int{-1, 4, -4, 5, -2, -1, -1, -2, -3},
			output: 960,
		},
	}
	for i, scenario := range scenarios {
		output := maxProduct(scenario.nums)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
