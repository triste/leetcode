package problem229

import (
	"math"
)

func majorityElement(nums []int) (majorities []int) {
	majority1, majority2 := math.MaxInt, math.MaxInt
	counter1, counter2 := 0, 0
	for _, num := range nums {
		if majority1 == num {
			counter1++
		} else if majority2 == num {
			counter2++
		} else if counter1 == 0 {
			majority1 = num
			counter1 = 1
		} else if counter2 == 0 {
			majority2 = num
			counter2 = 1
		} else {
			counter1--
			counter2--
		}
	}
	counter1, counter2 = 0, 0
	for _, num := range nums {
		if num == majority1 {
			counter1++
		}
		if num == majority2 {
			counter2++
		}
	}
	if counter1 > len(nums)/3 {
		majorities = append(majorities, majority1)
	}
	if counter2 > len(nums)/3 {
		majorities = append(majorities, majority2)
	}
	return
}
