package problem1161

import (
	"testing"

	"github.com/google/go-cmp/cmp"
	. "gitlab.com/triste/leetcode/pkg/tree"
)

func TestMaxlevelsum(t *testing.T) {
	scenarios := [...]struct {
		root   *TreeNode
		output int
	}{
		0: {
			root:   NewTree(1, 7, 0, 7, -8, nil, nil),
			output: 2,
		},
		1: {
			root:   NewTree(989, nil, 10250, 98693, -89388, nil, nil, nil, -32127),
			output: 2,
		},
	}
	for i, scenario := range scenarios {
		output := maxLevelSum(scenario.root)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
