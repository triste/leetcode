package problem474

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestFindmaxform(t *testing.T) {
	scenarios := [...]struct {
		strs   []string
		m      int
		n      int
		output int
	}{
		0: {
			strs:   []string{"10", "0001", "111001", "1", "0"},
			m:      5,
			n:      3,
			output: 4,
		},
		1: {
			strs:   []string{"10", "0", "1"},
			m:      1,
			n:      1,
			output: 2,
		},
	}
	for i, scenario := range scenarios {
		output := findMaxForm(scenario.strs, scenario.m, scenario.n)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
