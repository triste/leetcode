package problem1140

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func stoneGameII(piles []int) int {
	n := len(piles)
	memo := make([][]int, n)
	sum := 0
	for i := range memo {
		memo[i] = make([]int, n*2)
		for j := range memo[i] {
			memo[i][j] = -1
		}
		sum += piles[i]
	}
	var play func(i int, m int) int
	play = func(i int, m int) int {
		if i >= n {
			return 0
		}
		if memo[i][m] != -1 {
			return memo[i][m]
		}
		maxScore := -1000000
		turnScore := 0
		for j, p := range piles[i:min(n, i+m*2)] {
			turnScore += p
			totalScore := turnScore - play(i+j+1, max(m, j+1))
			maxScore = max(maxScore, totalScore)
		}
		memo[i][m] = maxScore
		return maxScore
	}
	return (sum + play(0, 1)) / 2
}

/*
	2, 7, 9, 4, 4

	               {2, 7, 9, 4, 4}, M = 1
				1/                        \2
		{7, 9, 4, 4}, M=1                  {9, 4, 4}, M = 2
	 1'/            \2'                   1'/    2'|       \3'
 {9, 4, 4}, M=1     {4,4}, M=2   {4, 4}, M=2   {4}, M=2     {}

	(score1,score2,M)
       |  0  |  1  |  2  |  3  |  4  |  5  |
	1  |     |     |     |     |     |0,5,1|
	2  |     |     |     |     |     |0,5,2|
	3  |     |     |     |     |     |0,5,3|
	4  |     |     |     |     |     |0,5,4|
	5  |     |     |     |     |     |0,5,5|
*/
