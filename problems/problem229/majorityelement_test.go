package problem229

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestMajorityelement(t *testing.T) {
	scenarios := [...]struct {
		nums   []int
		output []int
	}{
		0: {
			nums:   []int{3, 2, 3},
			output: []int{3},
		},
		1: {
			nums:   []int{1},
			output: []int{1},
		},
		2: {
			nums:   []int{1, 2},
			output: []int{1, 2},
		},
		3: {
			nums:   []int{3, 3, 1, 2, 2, 2},
			output: []int{2},
		},
		4: {
			nums:   []int{1, 2, 3, 4},
			output: nil,
		},
		5: {
			nums:   []int{1, 1, 1, 3, 2, 2, 2},
			output: []int{1, 2},
		},
		6: {
			nums:   []int{2, 2, 1, 3},
			output: []int{2},
		},
		7: {
			nums:   []int{2, 1, 1, 3, 1, 4, 5, 6},
			output: []int{1},
		},
	}
	for i, scenario := range scenarios {
		output := majorityElement(scenario.nums)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
