package problem920

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestNummusicplaylists(t *testing.T) {
	scenarios := [...]struct {
		n      int
		goal   int
		k      int
		output int
	}{
		0: {
			n:      3,
			goal:   3,
			k:      1,
			output: 6,
		},
		1: {
			n:      2,
			goal:   3,
			k:      0,
			output: 6,
		},
		2: {
			n:      2,
			goal:   3,
			k:      1,
			output: 2,
		},
		3: {
			n:      16,
			goal:   16,
			k:      4,
			output: 789741546,
		},
		4: {
			n:      2,
			goal:   4,
			k:      0,
			output: 14,
		},
		5: {
			n:      1,
			goal:   3,
			k:      0,
			output: 1,
		},
	}
	for i, scenario := range scenarios {
		output := numMusicPlaylists(scenario.n, scenario.goal, scenario.k)
		if diff := cmp.Diff(scenario.output, output); diff != "" {
			t.Errorf("scenario [%v] (-want +got):\n%s", i, diff)
		}
	}
}
